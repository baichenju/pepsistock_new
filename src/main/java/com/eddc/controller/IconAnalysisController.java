package com.eddc.controller;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eddc.model.Craw_goods_Info;
import com.eddc.model.JobAndTrigger;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.service.IconAnalysisService;
import com.eddc.service.JobAndTriggerService;
import com.eddc.util.Fields;
import com.github.pagehelper.PageInfo;
import com.google.gson.Gson;

import net.sourceforge.htmlunit.corejs.javascript.ast.ArrayLiteral;  
@RestController
@RequestMapping(value="/chart")
public class IconAnalysisController extends BaseController{
	@Autowired	
	private IconAnalysisService iconAnalysisService;
	private static Logger logger = LoggerFactory.getLogger(IconAnalysisController.class);
	private  SimpleDateFormat data=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	//页面展示
		@PostMapping(value="/dataAnalysis",produces = {"application/text;charset=UTF-8"})
		public String dataAnalysis(HttpServletRequest request) {
			List<Craw_goods_Info> list=new ArrayList<Craw_goods_Info>();
			Gson gson=new Gson();
			//获取当前监控数据库用户
			List<QrtzCrawlerTable> crawlerTable = iconAnalysisService.getIconDetails();
			for(int i=0;i<crawlerTable.size();i++){
				List<Craw_goods_Info>info=iconAnalysisService.GoodsFrameNumber(crawlerTable.get(i).getPlatform(),crawlerTable.get(i).getUser_Id(),Fields.STATUS_ON);
				for(int kk=0;kk<info.size();kk++){
					if(crawlerTable.get(i).getUser_Id().equals(info.get(kk).getCust_account_id())){
						Craw_goods_Info good=new Craw_goods_Info();
						//good.set
					//	good.setCust_account_id(cust_account_id);
					}	
				}

				
				
				if(info.size()>0){
					info.get(0).setCust_account_id(crawlerTable.get(i).getUser_Id());
					list.addAll(info); 
				}
			}
			return gson.toJson(list);
		}
}
