/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.method 
 * @author: jack.zhao   
 * @date: 2018年9月9日 下午3:35:06 
 */
package com.eddc.method;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import com.eddc.mapper.CrawlerPublicClassMapper;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.model.Craw_keywords_delivery_place;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.redis.JedisService;
import com.eddc.service.CrawlerPublicClassService;
import com.eddc.task.Tmall_DataCrawlTask;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.SpringContextUtil;
import com.eddc.util.publicClass;

/**   
 *    
 * 项目名称：Price_monitoring_crawler   
 * 类名称：TmallData   
 * 类描述：   
 * 创建人：jack.zhao   
 * 创建时间：2018年9月9日 下午3:35:06   
 * 修改人：jack.zhao   
 * 修改时间：2018年9月9日 下午3:35:06   
 * 修改备注：   
 * @version    
 *    
 */
@Component
public class TmallData {
	@Autowired
	private CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	private CrawlerPublicClassMapper mapper;
	
	private static Logger _log = Logger.getLogger(TmallData.class); 
	//开始爬虫
	public void crawlerTmallPriceMonitoring(List<QrtzCrawlerTable> listjobName, String tableName, String count, int pageTop) {
		_log.info(">>> Invoke Tmall_Job crawlerTmallPriceMonitoring method >>>");
		String time = SimpleDate.SimpleDateFormatData().format(new Date());
		//从数据库获取城市code抓取库存
		List<Craw_keywords_delivery_place> List_Code = crawlerPublicClassService.Keywords_Delivery_Place(listjobName, listjobName.get(0).getDatabases());
		String cookie  = List_Code.get(0).getDelivery_place_code2();
		try {
			if(List_Code.size()>0){
				for(Craw_keywords_delivery_place delivery:List_Code){
					try {
						_log.info("当前商品抓取地区是----------===="+delivery.getDelivery_place_name()+"==========="+SimpleDate.SimpleDateFormatData().format(new Date()));
						//crawlerPublicClassService.SynchronousData(listjobName.get(0).getPlatform(),listjobName.get(0).getDatabases(),listjobName.get(0).getUser_Id(),tableName);//同步数据
						tmall_Craw_Item(listjobName,time,delivery.getDelivery_place_code(),tableName,count,delivery.getDelivery_place_name(),pageTop,cookie); //开始抓取数据
					} catch (Exception e) {
						_log.error("地区抓取出错>>>>>>>>>>>>>>>>"+e);
					}
				}
			}else{ 
				tmall_Craw_Item(listjobName,time,SimpleDate.SimpleDateFormatDataHours().toString(),tableName,count,null,pageTop,cookie); //开始抓取数据
			}
		} catch (Exception e) { 
			_log.info("爬虫失败----------"+e+"==============="+SimpleDate.SimpleDateFormatData().format(new Date()));
		}finally{
			crawlerPublicClassService.updateCrawKeywordHistory(listjobName, listjobName.get(0).getDatabases());//更新已经抓取商品的状态
			crawlerPublicClassService.SynchronousData(listjobName.get(0).getPlatform(),listjobName.get(0).getDatabases(),listjobName.get(0).getUser_Id(),tableName);//同步数据					
		}
	}

	//开始爬数据
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void tmall_Craw_Item(List<QrtzCrawlerTable> listjobName, String time, String code, String tableNname, String count, String delivery, int pageTop,String cookie) throws InterruptedException {
		_log.info(">>> Invoke Tmall_Job tmall_Craw_Item method >>>");
		int jj = 0;
		
		long awaitTime = Integer.valueOf(listjobName.get(0).getDocker()) * 1000 * 60;
		ExecutorService taskExecutor = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());  //初始化线程池 listjobName.get(0).getThread_sum()
		CompletionService completion=new ExecutorCompletionService(taskExecutor);
		List<CrawKeywordsInfo> list = crawlerPublicClassService.crawAndParseInfo(listjobName, listjobName.get(0).getDatabases(), tableNname, count, pageTop);
		//开始解析数据
		//jedisService.setList(listjobName.get(0).getJob_name(), list);
		ArrayList<Future> futureList = new ArrayList<Future>();
		for (CrawKeywordsInfo accountInfo : list) {
			jj++;
			String url="https://detail.m.tmall.com/item.htm?id="+accountInfo.getCust_keyword_name()+"&areaId="+code;
			//String url=Fields.TMALL_APP_2.replace("EGOODSID",accountInfo.getCust_keyword_name()).replace("CODE", code);
			//String url=Fields.TMALL_APP_2.replace("EGOODSID",accountInfo.getCust_keyword_name()).replace("CODE", code);
			//String url=Fields.TMALL_APP_3.replace("EGOODSID",accountInfo.getCust_keyword_name())+"&areaId="+code;
			Tmall_DataCrawlTask dataCrawlTask = (Tmall_DataCrawlTask) SpringContextUtil.getApplicationContext().getBean(Tmall_DataCrawlTask.class);
			Map<String, Object> TmallMap = new HashMap<String, Object>();
			TmallMap.put("delivery", delivery);
			dataCrawlTask.setDataType(listjobName.get(0).getPlatform());
			dataCrawlTask.setStorage(listjobName.get(0).getStorage());
			dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
			dataCrawlTask.setTimeDate(time);
			dataCrawlTask.setPageTop(pageTop);
			dataCrawlTask.setSum("" + jj + "/" + list.size() + "_" + delivery);
			dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id());
			dataCrawlTask.setAccountinfo(accountInfo);
			dataCrawlTask.setSetUrl(url);
			dataCrawlTask.setProxy(listjobName.get(0).getIP());
			dataCrawlTask.setType(Fields.STYPE_1);
			dataCrawlTask.setCode(code);
			dataCrawlTask.setTmallMap(TmallMap);
			dataCrawlTask.setClient(listjobName.get(0).getClient());
			dataCrawlTask.setStatus_type(String.valueOf(listjobName.get(0).getPromotion_status()));
			dataCrawlTask.setCookie(cookie);
			dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource()));
			Future<Map<String, Object>> future = completion.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值
			futureList.add(future);
			_log.info(">>> " + dataCrawlTask.getSetUrl() + " finish run >>>");
		}
		try {
			taskExecutor.shutdown();
			_log.info(">>> Tmall_Job Future List prepare finished >>>");
			if (pageTop == Fields.STATUS_COUNT) {
				_log.info(">>> START COUNT = 0 >>>");
				while (true) {
					if (((ExecutorService) taskExecutor).isTerminated()) {
						_log.info(">>> TaskExecutor has finished >>>");
						taskExecutor.shutdownNow();
						_log.info(">>> TaskExecutor has shutdown >>>");

						_log.info(">>> Start batch insert crawler data >>>");
						if(!listjobName.get(0).getStorage().contains(Fields.COUNT_4)){
							crawlerPublicClassService.bulkInsertData(futureList,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据
						}
						_log.info(">>> Finish batch insert crawler data >>>");
						break;
					}
				}
			} else {
				if (!taskExecutor.awaitTermination(awaitTime, TimeUnit.MILLISECONDS)) {
					_log.info(">>> TaskExecutor timeout,shut down now ! >>>");
					taskExecutor.shutdownNow();
				}else{
					taskExecutor.shutdownNow();  
				}
			}
		} catch (Exception e) {
			// 超时的时候向线程池中所有的线程发出中断(interrupted)。
			taskExecutor.shutdownNow();
			_log.error("tmall_Craw_Item fail,please check!");
			_log.error(e.getMessage());

		} finally {
			if (pageTop == Fields.STATUS_COUNT_1) {
				_log.info(">>> Start batch insert crawler data >>>");
				if(!listjobName.get(0).getStorage().contains(Fields.COUNT_4)){
					crawlerPublicClassService.bulkInsertData(futureList,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据
				}
			}
		}
		_log.info(">>> Finish invoke Tmall_Job tmall_craw_item method >>>");
	}
}
