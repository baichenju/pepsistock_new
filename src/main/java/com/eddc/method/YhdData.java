/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.method 
 * @author: jack.zhao   
 * @date: 2018年9月10日 上午9:39:42 
 */
package com.eddc.method;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import com.eddc.model.CommodityPrices;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.model.Craw_keywords_delivery_place;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.service.CrawlerPublicClassService;
import com.eddc.service.JobAndTriggerService;
import com.eddc.task.Vip_DataCrawlTask;
import com.eddc.task.Yhd_DataCrawlTask;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.SpringContextUtil;
import com.eddc.util.publicClass;

/**   
 *    
 * 项目名称：Price_monitoring_crawler   
 * 类名称：YhdData   
 * 类描述： 一号店 监控数据
 * 创建人：jack.zhao   
 * 创建时间：2018年9月10日 上午9:39:42   
 * 修改人：jack.zhao   
 * 修改时间：2018年9月10日 上午9:39:42   
 * 修改备注：   
 * @version    
 *    
 */
@Component
public class YhdData {
	@Autowired
	private ApplicationContext contexts= SpringContextUtil.getApplicationContext();
	@Autowired
	private CrawlerPublicClassService crawlerPublicClassService;
	private static Logger _log = Logger.getLogger(YhdData.class);
	long date_time=System.currentTimeMillis();

	//启动爬虫
	public void atartCrawlerYhd(List<QrtzCrawlerTable>listjobName,String tableName,String count,int pageTop) throws InterruptedException{
		String time=SimpleDate.SimpleDateFormatData().format(new Date()); 
		//从数据库获取城市code抓取库存
		List<Craw_keywords_delivery_place>List_Code=crawlerPublicClassService.Keywords_Delivery_Place(listjobName,listjobName.get(0).getDatabases());
		try {
			if(listjobName.size()>0){
				try{
					if(List_Code.size()>0){
						for(Craw_keywords_delivery_place place:List_Code){
							try {
								_log.info("当前用户抓取库存是--"+place.getDelivery_place_name()+"----"+new Date());	
								crawAndParseInfoAndPriceYHD(listjobName,time,place.getDelivery_place_code(),tableName,count,place.getDelivery_place_name(),pageTop);//启动爬虫		
							} catch (Exception e) {
								_log.error("抓取多个地区出现错误了。。。。。"+e);
							}
						}
					}else{
						crawAndParseInfoAndPriceYHD(listjobName,time,null,tableName,count,null,pageTop);//启动爬虫		
					}
				}catch(Exception e){
					_log.info("抓取数据失败"+e.getMessage()+new Date());	
				}
			}else {
				_log.info("当前用户不存在！爬虫结束------"+new Date());		
			}
		} catch (Exception e) {
			_log.error(e);
		}finally{
			crawlerPublicClassService.updateCrawKeywordHistory(listjobName, listjobName.get(0).getDatabases());//更新已经抓取商品的状态
			crawlerPublicClassService.SynchronousData(listjobName.get(0).getPlatform(),listjobName.get(0).getDatabases(),listjobName.get(0).getUser_Id(),tableName);//同步数据				
		}
		
	}

	//商品详情
	@SuppressWarnings({ "rawtypes" })
	public void crawAndParseInfoAndPriceYHD (List<QrtzCrawlerTable>listjobName,String time,String code,String tableName,String count,String delivery,int pageTop) throws InterruptedException{
		int jj=0;
		ExecutorService taskExecutor = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());  //初始化线程池 
		List<CrawKeywordsInfo>list=crawlerPublicClassService.crawAndParseInfo(listjobName,listjobName.get(0).getDatabases(),tableName,count,pageTop);//开始解析数据 
		ArrayList<Future> futureList = new ArrayList<Future>();
		for(CrawKeywordsInfo accountInfo:list){ jj++;
		Yhd_DataCrawlTask dataCrawlTask = (Yhd_DataCrawlTask) contexts.getBean(Yhd_DataCrawlTask.class);
		Map<String,Object>yhdMap=new HashMap<String,Object>();
		dataCrawlTask.setDataType(listjobName.get(0).getPlatform());
		dataCrawlTask.setYhdMap(yhdMap);
		dataCrawlTask.setTimeDate(time);
		dataCrawlTask.setSum(""+jj+"/"+list.size()+"");
		dataCrawlTask.setType(Fields.STYPE_1); 
		dataCrawlTask.setIp(listjobName.get(0).getIP());
		dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id()); 
		dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
		dataCrawlTask.setCrawKeywordsInfo(accountInfo);
		dataCrawlTask.setStorage(listjobName.get(0).getStorage());
		dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource())); 
		dataCrawlTask.setSetUrl(Fields.YHD_URL_APP+accountInfo.getCust_keyword_name()+".html"); 
		dataCrawlTask.setCodeCookice(code);
		dataCrawlTask.setPromotion_status(listjobName.get(0).getPromotion_status());
		Future<Map<String, Object>> future=taskExecutor.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值  
		futureList.add(future);
		_log.info(Thread.currentThread().getName()+Runtime.getRuntime().availableProcessors());
		}
		taskExecutor.shutdown(); 
		while(true){
			if(((ExecutorService) taskExecutor).isTerminated()){
				taskExecutor.shutdownNow(); 
				crawlerPublicClassService.bulkInsertData(futureList,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据	
				break;
			}	

		} 

	} 
}
