/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.method 
 * @author: jack.zhao   
 * @date: 2018年9月9日 下午3:57:27 
 */
package com.eddc.method;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import com.eddc.model.CommodityPrices;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.model.Craw_keywords_delivery_place;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.service.CrawlerPublicClassService;
import com.eddc.task.Jd_DataCrawlTask;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.SpringContextUtil;
import com.eddc.util.Validation;
import com.eddc.util.publicClass;
import com.github.pagehelper.util.StringUtil;

/**   
 *    
 * 项目名称：Price_monitoring_crawler   
 * 类名称：JdData   
 * 类描述：   
 * 创建人：jack.zhao   
 * 创建时间：2018年9月9日 下午3:57:27   
 * 修改人：jack.zhao   
 * 修改时间：2018年9月9日 下午3:57:27   
 * 修改备注：   
 * @version    
 *    
 */
@Component
public class JdData {
	@Autowired
	private CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	private ApplicationContext contexts = SpringContextUtil.getApplicationContext(); 
	private static Logger _log = Logger.getLogger(JdData.class);

	//启动爬虫
	public void startCrawlerJd(List<QrtzCrawlerTable> listjobName, String tableName, String count, int pageTop) throws InterruptedException, ExecutionException {
		_log.info(">>> Invoke Jd_Job startCrawlerJd method >>>");
		String time = SimpleDate.SimpleDateFormatData().format(new Date());
		//从数据库获取城市code抓取库存
		List<Craw_keywords_delivery_place> List_Code = crawlerPublicClassService.Keywords_Delivery_Place(listjobName, listjobName.get(0).getDatabases());
		try {
			if (listjobName.size() > 0) {
				if (List_Code.size() > 0) {
					for (Craw_keywords_delivery_place place : List_Code) {
						try {
							crawAndParseInfoAndPriceJD(listjobName, time, place.getDelivery_place_code(), tableName, count, pageTop,place.getDelivery_place_name());//启动爬虫
							if (listjobName.get(0).getClient().equals("ALL") || listjobName.get(0).getClient().equals("all")) {
								CommodityPricesJDData(listjobName, time, tableName, pageTop);//获取商品价格
							}	
						} catch (Exception e) {
							_log.error("获取多个地区失败》》》》》》》》》》》》"+e);
						}

					}
				} else {
					crawAndParseInfoAndPriceJD(listjobName, time, null, tableName, count, pageTop,"北京");//启动爬虫
					if (listjobName.get(0).getClient().equals("ALL") || listjobName.get(0).getClient().equals("all")) {
						CommodityPricesJDData(listjobName, time, tableName, pageTop);//获取商品价格
					}
				}
			} else {
				_log.info(">>> List<QrtzCrawlerTable> is empty,please Check! >>>");
			}
		} catch (Exception e) {
			_log.error(e);
		}finally{
			crawlerPublicClassService.updateCrawKeywordHistory(listjobName, listjobName.get(0).getDatabases());//更新已经抓取商品的状态
			crawlerPublicClassService.SynchronousData(listjobName.get(0).getPlatform(), listjobName.get(0).getDatabases(), listjobName.get(0).getUser_Id(), tableName);//同步数据
		}

	}

	//商品详情
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void crawAndParseInfoAndPriceJD(List<QrtzCrawlerTable> listjobName, String time, String place_code, String tableName, String count, int pageTop, String delivery) throws InterruptedException, ExecutionException {
		_log.info(">>> Invoke Jd_Job crawAndParseInfoAndPriceJD method >>>");
		long start = System.currentTimeMillis();
		ArrayList<Future> futureList = new ArrayList<Future>();
		int jj = 0;String dataCookie="";
		//final long awaitTime = Integer.valueOf(listjobName.get(0).getDocker()) * 1000 * 60;
		//
		ExecutorService taskExecutor = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());  //初始化线程池
		CompletionService completion=new ExecutorCompletionService(taskExecutor);
		List<CrawKeywordsInfo> list = crawlerPublicClassService.crawAndParseInfo(listjobName, listjobName.get(0).getDatabases(), tableName, count, pageTop);//开始解析数据
		for (CrawKeywordsInfo accountInfo : list) {
			jj++;
			Map<String, Object> jdMap = new HashMap<String, Object>();
			Jd_DataCrawlTask dataCrawlTask = (Jd_DataCrawlTask) SpringContextUtil.getApplicationContext().getBean(Jd_DataCrawlTask.class);
			jdMap.put("delivery", delivery);
			dataCrawlTask.setDataType(listjobName.get(0).getPlatform());
			dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
			dataCrawlTask.setStorage(listjobName.get(0).getStorage());
			dataCrawlTask.setTimeDate(time);
			dataCrawlTask.setSum("" + jj + "/" + list.size() + "");
			dataCrawlTask.setType(Fields.STYPE_1);
			dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id());
			dataCrawlTask.setCrawKeywordsInfo(accountInfo);
			dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource()));
			dataCrawlTask.setCookie(dataCookie);
			dataCrawlTask.setProxy(listjobName.get(0).getIP());
			dataCrawlTask.setPlace_code(place_code);
			dataCrawlTask.setJdMap(jdMap);
			dataCrawlTask.setPage(Fields.STATUS_OFF);
			dataCrawlTask.setStatus_type(String.valueOf(listjobName.get(0).getPromotion_status()));
			if (Validation.isEmpty(dataCookie)) {
				dataCrawlTask.setSetUrl(Fields.JD_URL_APP + accountInfo.getCust_keyword_name() + ".html");
			} else {
				dataCrawlTask.setSetUrl(Fields.JD_URL_APP_INTERNATIONL + accountInfo.getCust_keyword_name());
			}
			Future<Map<String, Object>> future = completion.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值
			futureList.add(future);
			_log.info(">>> " + dataCrawlTask.getSetUrl() + " finish run >>>");
		}
		taskExecutor.shutdown();
		_log.info(">>> Jd_Job Future List prepare finished >>>");
		try {
				_log.info(">>> START COUNT = 0 >>>");
				while (true) {
					if (((ExecutorService) taskExecutor).isTerminated()) {
						_log.info(">>> Start batch insert crawler data >>>");
						if(!listjobName.get(0).getStorage().contains(Fields.COUNT_4)){
							crawlerPublicClassService.bulkInsertData(futureList,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据
						}
						_log.info(">>> Finish check failure crawler info >>>");
						break;
					}
				}
		} catch (Exception e) {
			_log.error("crawAndParseInfoAndPriceJD fail,please check!");
			_log.error(e.getMessage());
			taskExecutor.shutdownNow();// 超时的时候向线程池中所有的线程发出中断(interrupted)。
		}
		long end = System.currentTimeMillis();
		_log.info(" Finish crawAndParseInfoAndPriceJD cost : " + (end - start) + " ms");
	}

	///检查失败的item数据
	@SuppressWarnings("rawtypes")
	public void crawAndParseInfoAndPricefailure(String time, List<QrtzCrawlerTable> listjobName, String place_code, String tableName, String counts, int pageTop) throws InterruptedException {
		int jj = 0;
		int count = 0;
		ArrayList<Future> futureList = new ArrayList<Future>();
		ExecutorService taskExecutor = Executors.newFixedThreadPool(Integer.valueOf(listjobName.get(0).getThread_sum()));  //初始化线程池
		String dataCookie = crawlerPublicClassService.dataCookie(listjobName.get(0).getUser_Id(),listjobName.get(0).getDatabases(), listjobName.get(0).getPlatform());//获取cookie
		do {
			List<CrawKeywordsInfo> list = crawlerPublicClassService.crawAndGrabFailureGoodsData(listjobName, listjobName.get(0).getDatabases(), tableName, pageTop);//开始解析数据
			for (CrawKeywordsInfo accountInfo : list) {
				jj++;
				Map<String, Object> jdMap = new HashMap<String, Object>();
				Jd_DataCrawlTask dataCrawlTask = (Jd_DataCrawlTask) contexts.getBean(Jd_DataCrawlTask.class);
				dataCrawlTask.setDataType(listjobName.get(0).getPlatform());
				dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
				dataCrawlTask.setStorage(listjobName.get(0).getStorage());
				dataCrawlTask.setTimeDate(time);
				dataCrawlTask.setSum("" + jj + "/" + list.size() + "");
				dataCrawlTask.setType(Fields.STYPE_1);
				dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id());
				dataCrawlTask.setCrawKeywordsInfo(accountInfo);
				dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource()));
				dataCrawlTask.setCookie(dataCookie);
				dataCrawlTask.setJdMap(jdMap);
				dataCrawlTask.setProxy(listjobName.get(0).getIP());
				dataCrawlTask.setPlace_code(place_code);
				dataCrawlTask.setPage(Fields.STATUS_OFF);
				dataCrawlTask.setStatus_type(String.valueOf(listjobName.get(0).getPromotion_status()));
				if (Validation.isEmpty(dataCookie)) {
					dataCrawlTask.setSetUrl(Fields.JD_URL_APP + accountInfo.getCust_keyword_name() + ".html");
				} else {
					dataCrawlTask.setSetUrl(Fields.JD_URL_APP_INTERNATIONL + accountInfo.getCust_keyword_name());
				}
				Future<Map<String, Object>> future = taskExecutor.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值
				futureList.add(future);
				_log.info(">>> " + dataCrawlTask.getSetUrl() + " finish run >>>");
				//_log.info(Thread.currentThread().getName() + Runtime.getRuntime().availableProcessors());
			}
			//taskExecutor.shutdown();
			_log.info(">>> Failure future List prepare finished >>>");
			Thread.sleep(10000);
			while (true) {
				if (((ExecutorService) taskExecutor).isTerminated()) {
					taskExecutor.shutdownNow();
					_log.info(">>> Start batch insert failure crawler data >>>");
					crawlerPublicClassService.bulkInsertData(futureList, listjobName.get(0).getDatabases(), listjobName.get(0).getStorage());//批量插入数据
					_log.info(">>> Finish batch insert failure crawler data >>>");
					break;
				}
			}
			jj = 0;
			count++;
			Thread.sleep(5000);
			if (count > 5) {
				_log.info(">>> retry invoke Jd_Job crawAndParseInfoAndPricefailure method : " + count + " >>>");
				Thread.sleep(5000);
				return;
			}
		} while (true);
	}

	//商品价格PC端
	@SuppressWarnings("rawtypes")
	public void CommodityPricesJDData(List<QrtzCrawlerTable> listjobName, String time, String tableName, int pageTop) {
		_log.info(">>> Invoke Jd_Job CommodityPricesJDData method >>>");
		int ii = 0;
		ArrayList<Future> futureList = new ArrayList<Future>();
		ExecutorService taskExecutor = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());  //初始化线程池
		List<CommodityPrices> listPriceJd = crawlerPublicClassService.CommodityPricesData(listjobName, listjobName.get(0).getDatabases(), tableName, pageTop);
		String dataCookie = crawlerPublicClassService.dataCookie(listjobName.get(0).getUser_Id(),listjobName.get(0).getDatabases(), listjobName.get(0).getPlatform());//获取cookie
		for (CommodityPrices accountInfo : listPriceJd) {
			ii++;
			Map<String, Object> jdMap = new HashMap<String, Object>();
			Jd_DataCrawlTask dataCrawlTask = (Jd_DataCrawlTask) contexts.getBean(Jd_DataCrawlTask.class);
			dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
			dataCrawlTask.setStorage(listjobName.get(0).getStorage());
			accountInfo.setBatch_time(time);
			dataCrawlTask.setDataType(listjobName.get(0).getPlatform());
			dataCrawlTask.setTimeDate(time);
			dataCrawlTask.setSum("" + ii + "/" + listPriceJd.size() + "");
			accountInfo.setPlatform_name_en(listjobName.get(0).getPlatform());
			dataCrawlTask.setCommodityPricesJD(accountInfo);
			dataCrawlTask.setType(Fields.STYPE_6);
			dataCrawlTask.setProxy(listjobName.get(0).getIP());
			dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id());
			dataCrawlTask.setCookie(dataCookie);
			dataCrawlTask.setJdMap(jdMap);
			dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource()));
			Future<Map<String, Object>> future = taskExecutor.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值
			futureList.add(future);
			_log.info(Thread.currentThread().getName() + Runtime.getRuntime().availableProcessors());
		}
		//taskExecutor.shutdown();
		while (true) {
			if (((ExecutorService) taskExecutor).isTerminated()) {
				try {// 递归补漏
					taskExecutor.shutdownNow();
					_log.info(">>> Start batch insert crawler price data >>>");
					crawlerPublicClassService.bulkInsertData(futureList, listjobName.get(0).getDatabases(), listjobName.get(0).getStorage());//批量插入数据
					_log.info(">>> Finish batch insert crawler price data >>>");
					RecursionFailureGoods(time, listjobName, tableName, pageTop);
				} catch (Exception e) {
					_log.info("RecursionFailureGoods fail,please check!");
					_log.error(e.getMessage());
				}
				break;
			}
		}
		ii = 0;
		_log.info(">>> Finish invoke Jd_Job CommodityPricesJDData method >>>");
	}

	/**
	 * 递归检查获取没有成功抓取商品的价格
	 *
	 * @throws InterruptedException
	 */
	@SuppressWarnings("rawtypes")
	public void RecursionFailureGoods(String time, List<QrtzCrawlerTable> listjobName, String tableName, int pageTop) throws InterruptedException {
		_log.info(">>> Invoke Jd_Job RecursionFailureGoods method >>>");
		int ii = 0;
		int count = 0;
		ExecutorService taskExecutor = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());  //初始化线程池
		String dataCookie = crawlerPublicClassService.dataCookie(listjobName.get(0).getUser_Id(),listjobName.get(0).getDatabases(), listjobName.get(0).getPlatform());//获取cookie
		ArrayList<Future> futureList = new ArrayList<Future>();
		do { 
			List<CommodityPrices> listPriceJd = crawlerPublicClassService.RecursionFailureGoods(listjobName, listjobName.get(0).getDatabases(), tableName, pageTop);
			if (listPriceJd.size() > 0) {
				for (CommodityPrices accountInfo : listPriceJd) {
					ii++;
					Map<String, Object> jdMap = new HashMap<String, Object>();
					Jd_DataCrawlTask dataCrawlTask = (Jd_DataCrawlTask) contexts.getBean(Jd_DataCrawlTask.class);
					dataCrawlTask.setDataType(listjobName.get(0).getPlatform());
					dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
					dataCrawlTask.setStorage(listjobName.get(0).getStorage());
					dataCrawlTask.setTimeDate(time);
					dataCrawlTask.setSum("" + ii + "/" + listPriceJd.size() + "");
					accountInfo.setPlatform_name_en(listjobName.get(0).getPlatform());
					dataCrawlTask.setCommodityPricesJD(accountInfo);
					dataCrawlTask.setType(Fields.STYPE_6);
					dataCrawlTask.setProxy(listjobName.get(0).getIP());
					dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id());
					dataCrawlTask.setCookie(dataCookie);
					dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource()));
					dataCrawlTask.setJdMap(jdMap);
					Future<Map<String, Object>> future = taskExecutor.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值
					futureList.add(future);
				}
			}
			//taskExecutor.shutdown();

			while (true) {
				if (taskExecutor.isTerminated()) {
					taskExecutor.shutdownNow();
					crawlerPublicClassService.bulkInsertData(futureList, listjobName.get(0).getDatabases(), listjobName.get(0).getStorage());//批量插入数据
					break;
				}
			}
			ii = 0;
			count++;
			if (count > 8) {
				Thread.sleep(5000);
				_log.info(">>> retry invoke Jd_Job RecursionFailureGoods method : " + count + " >>>");
				return;
			}
		} while (true);

	}

}
