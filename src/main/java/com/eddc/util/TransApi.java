/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.job 
 * @author: jack.zhao   
 * @date: 2018年4月26日 下午3:43:16 
 */
package com.eddc.util;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
/**   
 *    
 * 项目名称：Price_monitoring_crawler   
 * 类名称：TransApi   
 * 类描述：   
 * 创建人：jack.zhao   
 * 创建时间：2018年4月26日 下午3:43:16   
 * 修改人：jack.zhao   
 * 修改时间：2018年4月26日 下午3:43:16   
 * 修改备注：   
 * @version    
 *    
 */
public class TransApi {
	private static final String TRANS_API_HOST = "http://api.fanyi.baidu.com/api/trans/vip/translate";
	private static final String appid = ResourceBundle.getBundle("docker").getString("APP_ID").trim().toString();
	private static final String securityKey = ResourceBundle.getBundle("docker").getString("SECURITY_KEY").trim().toString();
	/** 
	 * @Title:TransApi
	 * @Description:TODO 
	 * @param appId2
	 * @param securityKey2 
	 * @throws UnsupportedEncodingException 
	 */  

	public String getTransResult(String query, String from, String to) throws UnsupportedEncodingException {
		Map<String, String> params = buildParams(query, from, to);
		return ClientHttpUtil.get(TRANS_API_HOST, params);
	}
	private Map<String, String> buildParams(String query, String from, String to) throws UnsupportedEncodingException {
		Map<String, String> params = new HashMap<String, String>();
		params.put("q", query);
		params.put("from", from);
		params.put("to", to);
		params.put("appid", appid);
		// 随机数
		String salt = String.valueOf(System.currentTimeMillis());
		params.put("salt", salt);
		// 签名
		String src = appid + query + salt + securityKey; // 加密前的原文
		params.put("sign", MD5.md5(src));
		return params;
	}
}
