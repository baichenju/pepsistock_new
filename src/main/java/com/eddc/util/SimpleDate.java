/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.util 
 * @author: jack.zhao   
 * @date: 2018年5月15日 下午5:16:09 
 */
package com.eddc.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.springframework.stereotype.Component;

/**   
 *    
 * 项目名称：Price_monitoring_crawler   
 * 类名称：SimpleDate   
 * 类描述： 日期归类  
 * 创建人：jack.zhao   
 * 创建时间：2018年5月15日 下午5:16:09   
 * 修改人：jack.zhao   
 * 修改时间：2018年5月15日 下午5:16:09   
 * 修改备注：   
 * @version    
 *    
 */
@Component
public class SimpleDate {

	//时间格式 yyyy-MM-dd HH:mm:ss
	public static SimpleDateFormat SimpleDateFormatData(){
		SimpleDateFormat data=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return data;
	}
	//时间格式 yyyyMMddHHmmss
	public static SimpleDateFormat SimpleDateFormatDataTime(){
		SimpleDateFormat data=new SimpleDateFormat("yyyyMMddHHmmss");
		return data;
	}
	//时间格式 yyyyMMddHHmmss
		public static SimpleDateFormat SimpleDateFormatDataMinute(){
			SimpleDateFormat data=new SimpleDateFormat("yyyyMMddHHmm");
			return data;
		}

	//时间格式 yyyyMMddHHmmss
	public static SimpleDateFormat SimpleDateFormatDataHour(){
		SimpleDateFormat data=new SimpleDateFormat("yyyyMMddHH");
		return data;
	}

	//时间格式 yyyy-MM-dd 
	public static SimpleDateFormat SimpleDateData(){
		SimpleDateFormat data=new SimpleDateFormat("yyyy-MM-dd");
		return data;
	}
	//时间格式 yyyyMMdd 
	public static SimpleDateFormat SimpleDateTime(){
		SimpleDateFormat data=new SimpleDateFormat("yyyyMMdd");
		return data;
	}
	//时间格式 yyyyMMdd 
	public static SimpleDateFormat SimpleDateFormatData_zn(){
		SimpleDateFormat fort=new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
		return fort;
	}
	//时间格式 H
	public static SimpleDateFormat SimpleDateFormatDataHours(){
		SimpleDateFormat fort=new SimpleDateFormat("HH");
		return fort;
	}
	//获取当前周几
	 public static int StringWeekday(){
		Date today = new Date();
		Calendar c=Calendar.getInstance();
		c.setTime(today);
		int weekday=c.get(Calendar.DAY_OF_WEEK);
		return weekday-1;
	}
	
	 /** 
	  *  
	  * @param 要转换的秒数 
	  * @return 该毫秒数转换为 * days * hours * minutes * seconds 后的格式 
	  * @author fy.zhang 
	  */  
	 public static String formatDuring(long now) {  
		 Calendar calendar = Calendar.getInstance();
		 calendar.setTimeInMillis(now*1000);
		 System.out.println(now + " = " + SimpleDateData().format(calendar.getTime()));
		 return SimpleDateData().format(calendar.getTime());
	 } 
	 
	 /** 
	  *  
	  * @param 要转换的毫秒数 
	  * @return 该毫秒数转换为 * days * hours * minutes * seconds 后的格式 
	  * @author fy.zhang 
	  */  
	 public static String formatMillisecond(long now) {  
		 Calendar calendar = Calendar.getInstance();
		 calendar.setTimeInMillis(now);
		// System.out.println(now + " = " + SimpleDateData().format(calendar.getTime()));
		 return SimpleDateData().format(calendar.getTime());
	 }  
	 /*
	  * 日期加减
	  */
	 public static long dateSubtract(String Start,String end ) throws ParseException {  
		 Date ends = SimpleDateData().parse(end);
		 Date Starts = SimpleDateData().parse(Start);
		 long diff = ends.getTime() - Starts.getTime();//这样得到的差值是微秒级别
		 long days = diff / (1000 * 60 * 60 * 24);
		 return days;
	 } 

}