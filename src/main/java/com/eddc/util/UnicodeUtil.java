/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.util 
 * @author: jack.zhao   
 * @date: 2018年7月9日 上午11:58:15 
 */
package com.eddc.util;
import java.util.Date;
import java.util.Map;
import com.alibaba.druid.util.StringUtils;
import com.google.gson.Gson;

/**   
 *    
 * 项目名称：selection_sibrary_crawler   
 * 类名称：UnicodeUtil   
 * 类描述：   Unicode 转换
 * 创建人：jack.zhao   
 * 创建时间：2018年7月9日 上午11:58:15   
 * 修改人：jack.zhao   
 * 修改时间：2018年7月9日 上午11:58:15   
 * 修改备注：   
 * @version    
 *    
 */
public class UnicodeUtil {
	/**
	 * asciicode 转为中文
	 *
	 * @param asciicode eg:{"code":400002,"msg":"\u7b7e\u540d\u9519\u8bef"}
	 * @return eg:{"code":400002,"msg":"签名错误"}
	 */
	public static String ascii2native(String asciicode) {
		String[] asciis = asciicode.split("\\\\u");
		String nativeValue = asciis[0];
		try {
			for (int i = 1; i < asciis.length; i++) {
				String code = asciis[i];
				nativeValue += (char) Integer.parseInt(code.substring(0, 4), 16);
				if (code.length() > 4) {
					nativeValue += code.substring(4, code.length());
				}
			}
		} catch (NumberFormatException e) {
			return asciicode;
		}
		return nativeValue;
	}

}
