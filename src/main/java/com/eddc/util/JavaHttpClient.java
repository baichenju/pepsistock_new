package com.eddc.util;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.io.IOException;
import java.net.Authenticator;
import java.net.InetSocketAddress;
import java.net.PasswordAuthentication;
import java.net.Proxy;
import java.net.URI;
import java.util.Arrays;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpStatus;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.config.AuthSchemes;
import org.apache.http.client.entity.GzipDecompressingEntity;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.LayeredConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.ProxyAuthenticationStrategy;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.NameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.eddc.job.Jd_Job;
import com.eddc.service.CrawlerPublicClassService;
import com.eddc.service.Search_DataCrawlService;
import com.google.common.collect.Lists;
/**   
 *    
 * 项目名称：Price_monitoring_crawler   
 * 类名称：JavaHttpClient   
 * 类描述：   
 * 创建人：jack.zhao   
 * 创建时间：2018年5月16日 下午3:41:21   
 * 修改人：jack.zhao   
 * 修改时间：2018年5月16日 下午3:41:21   
 * 修改备注：   
 * @version    
 *    
 */
@Component
@SuppressWarnings({ "unused", "rawtypes", "unchecked" })
public class JavaHttpClient {
	private static PoolingHttpClientConnectionManager cm = null;
	private static HttpRequestRetryHandler httpRequestRetryHandler = null;
	private static HttpHost proxy = null;
	private static CredentialsProvider credsProvider = null;
	private static RequestConfig reqConfig = null;
	private static Logger logger = Logger.getLogger(JavaHttpClient.class);
	@Autowired
	private BatchInsertData batchInsertData;
	@Autowired
	private CrawlerPublicClassService crawlerPublicClassService;
	static {
		String ProxyUser=ResourceBundle.getBundle("docker").getString("ProxyUser");
		String ProxyPass=ResourceBundle.getBundle("docker").getString("ProxyPass");
		String ProxyHost=ResourceBundle.getBundle("docker").getString("ProxyHost");
		int ProxyPort=Integer.valueOf(ResourceBundle.getBundle("docker").getString("ProxyPort"));
		ConnectionSocketFactory plainsf = PlainConnectionSocketFactory.getSocketFactory();
		LayeredConnectionSocketFactory sslsf = SSLConnectionSocketFactory.getSocketFactory();
		Registry registry = RegistryBuilder.create().register("http", plainsf).register("https", sslsf).build();
		cm = new PoolingHttpClientConnectionManager(registry);
		cm.setMaxTotal(300);
		cm.setDefaultMaxPerRoute(50);
		proxy = new HttpHost(ProxyHost, ProxyPort, "https");
		credsProvider = new BasicCredentialsProvider();
		credsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(ProxyUser, ProxyPass));
		reqConfig = RequestConfig.custom()
				.setConnectionRequestTimeout(5000)
				.setConnectTimeout(5000)
				.setSocketTimeout(5000)
				.setExpectContinueEnabled(false)
				.setProxy(new HttpHost(ProxyHost, ProxyPort))
				.build();
	}

	public  String doRequest(HttpRequestBase httpReq,Map<String,String>map,String database) throws Exception {
		CloseableHttpResponse httpResp = null;int statusCode=200;
		CloseableHttpResponse httpResps = null;
		String result="";
		Map<String,Object> insertItem=new HashMap<String,Object>();
		List<Map<String,Object>> insertList = Lists.newArrayList();
		try {
			setHeaders(httpReq,map);
			httpReq.setConfig(reqConfig);
			CloseableHttpClient httpClient = HttpClients.custom().
					setConnectionManager(cm).
					setDefaultCredentialsProvider(credsProvider).build();
			AuthCache authCache = new BasicAuthCache();
			authCache.put(proxy, new BasicScheme());
			HttpClientContext localContext = HttpClientContext.create();
			localContext.setAuthCache(authCache);
			try {
				httpResp = httpClient.execute(httpReq, localContext);
				statusCode = httpResp.getStatusLine().getStatusCode();
			} catch (Exception e) {
				logger.info("请求超时");statusCode=404;}
			if (statusCode ==HttpStatus.SC_OK) {
				HttpEntity entity = httpResp.getEntity();  
				if (entity != null) {  
					result = EntityUtils.toString(entity,map.get("coding")); 
					if(result.contains(Fields.MESS_CODE)){//处理乱码
						httpResps= httpClient.execute(httpReq);//请求数据
						result = EntityUtils.toString(httpResps.getEntity(),Fields.GBK);	
						EntityUtils.consume(httpResps.getEntity());
						httpResps.close();	
					}
					EntityUtils.consume(httpResp.getEntity());
					httpResp.close();  
					return result;  
				}
			}else if(statusCode ==429){
				if(map.get("platform").equalsIgnoreCase(Fields.PLATFORM_JD)){
					try {
						HttpClientContext context = HttpClientContext.create();
						HttpGet httpGets = new HttpGet(map.get("url"));
						httpResp = httpClient.execute(httpGets, context);
						List<URI>redirectLocations = context.getRedirectLocations();
						httpReq.setHeader("host",redirectLocations.get(0).toASCIIString());
						HttpEntity entity = httpResp.getEntity();  
						if (entity != null) {  
							result = EntityUtils.toString(entity);  
							httpResp.close(); 
							return result;  
						}
					} catch (Exception e2) {
						e2.printStackTrace();
					}	
				}
			}else if (statusCode == HttpStatus.SC_MOVED_TEMPORARILY) {
				logger.info("----->> HttpClient request fail status code 302 : " +map.get("url").toString());
				//insertItem= BeanMapUtil.convertBean2MapWithUnderscoreName(crawlerPublicClassService.proxyIpData(map.get("platform"),0,statusCode));
			} else if (statusCode == HttpStatus.SC_NOT_FOUND) {
				logger.info("----->> HttpClient request fail status code 404 : " +map.get("url").toString());
				//insertItem= BeanMapUtil.convertBean2MapWithUnderscoreName(crawlerPublicClassService.proxyIpData(map.get("platform"),0,statusCode));
			}else{
				logger.error("----->> HttpClient request fail status code " + statusCode + " : " +map.get("url").toString());
				//insertItem= BeanMapUtil.convertBean2MapWithUnderscoreName(crawlerPublicClassService.proxyIpData(map.get("platform"),0,statusCode));
			}
		}catch(ClientProtocolException e){
			//insertItem= BeanMapUtil.convertBean2MapWithUnderscoreName(crawlerPublicClassService.proxyIpData(map.get("platform"),0,statusCode));
			e.printStackTrace(); 
		} catch (IOException e) {
			//insertItem= BeanMapUtil.convertBean2MapWithUnderscoreName(crawlerPublicClassService.proxyIpData(map.get("platform"),0,statusCode));
			e.printStackTrace();
		} catch (Exception e) {
			//insertItem= BeanMapUtil.convertBean2MapWithUnderscoreName(crawlerPublicClassService.proxyIpData(map.get("platform"),0,statusCode));
			e.printStackTrace();
		} finally {
			/*try {
				if (httpResp != null) {
					httpResp.close();
				}
				insertList.add(insertItem);
				if(insertList!=null && insertList.size()>0){
					batchInsertData.insertIntoData(insertList,database,Fields.PROXY_IP_MONITORING);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}*/
		}
		return result; 
	}

	/**
	 * 设置请求头
	 *
	 * @param httpReq
	 */
	private static void setHeaders(HttpRequestBase httpReq,Map<String,String>map) {
		httpReq.setHeader("Host",HttpCrawlerUtil.host(map.get("url").toString()));
		httpReq.setHeader("Accept-Language", "zh-cn,zh;q=0.8,en-us;q=0.5,en;q=0.3");
		httpReq.setHeader("Connection", "keep-alive");
		//httpReq.setHeader("Content-Type", "application/x-javascript;charset="+map.get("coding"));
		httpReq.setHeader("Accept", "*/*");
		httpReq.setHeader("Cache-Control", "max-age=0");
		httpReq.setHeader("Pragma", "no-cache");
		httpReq.setHeader("X-Requested-With", "XMLHttpRequest");
		httpReq.setHeader("Accept-Encoding", "gzip, deflate, sdch, br");
		httpReq.setHeader("Cache-Control", "no-cache");
		httpReq.setHeader("Upgrade-insecure-Requests", "1");
		httpReq.setHeader("User-Agent", HttpCrawlerUtil.CrawlerAttribute());
		httpReq.setHeader("Cookie", map.get("cookie")==null ? "" :map.get("cookie").toString());
		httpReq.setHeader("Referer", map.get("urlRef")==null ? "":map.get("urlRef").toString());
	}

	public  String doGetRequest(Map<String,String>map,String database) {
		// 要访问的目标页面
		String item="";
		try {
			HttpGet httpGet = new HttpGet(map.get("url"));
			item=doRequest(httpGet,map,database);
			 
		} catch (Exception e) {
			logger.info("请求数据失败"+e.getMessage());
		}
		return item;
	}

	public static String getUrlProxyContent(String url,String referer){
		String item="";
		String ProxyUser=ResourceBundle.getBundle("docker").getString("ProxyUser");
		String ProxyPass=ResourceBundle.getBundle("docker").getString("ProxyPass");
		String ProxyHost=ResourceBundle.getBundle("docker").getString("ProxyHost");
		int ProxyPort=Integer.valueOf(ResourceBundle.getBundle("docker").getString("ProxyPort"));
		Authenticator.setDefault(new Authenticator() {
			public PasswordAuthentication getPasswordAuthentication(){
				return new PasswordAuthentication(ProxyUser, ProxyPass.toCharArray());
			}
		});
		Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(ProxyHost, ProxyPort));
		try{
			// 此处自己处理异常、其他参数等
			Document doc = Jsoup.connect(url).referrer("ref").timeout(3000).proxy(proxy).get();
			if(doc != null) {
				item=doc.body().html();
			}
		}catch (IOException e){
			logger.info("请求数据失败"+e.getMessage());
		}

		return item;
	}
	public static void doPostRequest() {
		try {
			// 要访问的目标页面
			HttpPost httpPost = new HttpPost("https://www.baidu.com");

			// 设置表单参数
			List params = new ArrayList();
			params.add(new BasicNameValuePair("method", "next"));
			params.add(new BasicNameValuePair("params", "{\"broker\":\"abuyun\":\"site\":\"https://www.abuyun.com\"}"));
			httpPost.setEntity(new UrlEncodedFormEntity(params, "utf-8"));
			//doRequest(httpPost);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static void main(String[] args) throws InterruptedException, UnsupportedEncodingException {
		Map<String,String>map=new HashMap<String, String>();
		map.put("url", "http://acs.m.taobao.com/h5/mtop.taobao.detail.getdetail/6.0/?data=%7B%22itemNumId%22%3A%22547753800706%22%7D&qq-pf-to=pcqq.group&areaId=310100");
		map.put("urlRef", "http://acs.m.taobao.com/h5/mtop.taobao.detail.getdetail/6.0/?data=%7B%22itemNumId%22%3A%22547753800706%22%7D&qq-pf-to=pcqq.group&areaId=310100");
		map.put("coding", "GET"); 
//		for(int i=0;i<500;i++){ 
//			String item=doGetRequest(map);
//			Thread.sleep(2000);
//			String str=new String(item.getBytes("ISO-8859-1"),"UTF-8");
//			System.out.println(item.toString());
//		}
	}
}
