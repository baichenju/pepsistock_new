/**   
 * Copyright © 2019 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: util 
 * @author: jack.zhao   
 * @date: 2019年6月14日 下午3:17:57 
 */
package com.eddc.util;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.*;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.config.SocketConfig;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.Connection.Method;
import org.jsoup.Connection.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import com.eddc.util.HttpCrawlerUtil;
import com.eddc.util.Validation;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLHandshakeException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.PrintStream;
import java.net.UnknownHostException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;;
/**   
 *    
 * 项目名称：gnpd_crawler   
 * 类名称：HttpClientUtils   
 * @author：jack.zhao       
 * 创建时间：2019年6月14日 下午3:17:57   
 * 类描述：   
 * @version    
 *    
 */
@Component
public class HttpClientUtils {
	private final static int MAX_TIMEOUT = 10000;
	private final static ThreadLocal<CloseableHttpClient> clients = new ThreadLocal<CloseableHttpClient>();
	private final static int RETRY_TIMES = 3;
	private static final String HTTP = "http";
	private static final String HTTPS = "https";
	private static SSLConnectionSocketFactory sslsf = null;
	private static Registry<ConnectionSocketFactory> registry = null;
	private static Logger log=LoggerFactory.getLogger(ClientHttpDataUtil.class);
	static {
		try {
			SSLContextBuilder builder = new SSLContextBuilder();
			// 全部信任 不做身份鉴定
			builder.loadTrustMaterial(null, new TrustStrategy() {
				@Override
				public boolean isTrusted(X509Certificate[] x509Certificates, String s) {
					return true;
				}
			});//"SSLv2Hello", 
			sslsf = new SSLConnectionSocketFactory(builder.build(), new String[]{"SSLv3", "TLSv1", "TLSv1.2","TLSv1"}, null, NoopHostnameVerifier.INSTANCE);
			registry = RegistryBuilder.<ConnectionSocketFactory>create()
					.register(HTTP, new PlainConnectionSocketFactory())
					.register(HTTPS, sslsf)
					.build();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("resource")
	public CloseableHttpClient getHttpClient() {
		CloseableHttpClient closeableHttpClient = clients.get();
		org.apache.http.config.SocketConfig.Builder sobuider = SocketConfig.custom().setSoTimeout(MAX_TIMEOUT);
		if (closeableHttpClient == null) {
			PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager(registry);
			//PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
			cm.setMaxTotal(Integer.MAX_VALUE);
			cm.setDefaultSocketConfig(sobuider.build());
			cm.setDefaultMaxPerRoute(Integer.MAX_VALUE);
			HttpRequestRetryHandler httpRequestRetryHandler = (exception, executionCount, context) -> {
				if (executionCount >= RETRY_TIMES) {
					return false;
				}
				if (exception instanceof NoHttpResponseException) {
					return true;
				}
				if (exception instanceof SSLHandshakeException) {
					return false;
				}
				if (exception instanceof InterruptedIOException) {
					return false;
				}
				if (exception instanceof UnknownHostException) {
					return false;
				}
				if (exception instanceof ConnectTimeoutException) {
					return false;
				}
				if (exception instanceof SSLException) {
					return false;
				}
				HttpClientContext clientContext = HttpClientContext
						.adapt(context);
				HttpRequest request = clientContext.getRequest();
				/* 如果请求是幂等的就再次尝试*/
				if (!(request instanceof HttpEntityEnclosingRequest)) {
					return true;
				}
				return false;
			};

			CredentialsProvider credsProvider = new BasicCredentialsProvider();
			credsProvider.setCredentials(AuthScope.ANY,new UsernamePasswordCredentials("379862802","infopower"));
			closeableHttpClient = HttpClients.custom()
					.setSSLSocketFactory(sslsf)
					.setConnectionManager(cm)
					.setConnectionManagerShared(true)
					.setRetryHandler(httpRequestRetryHandler)
					.setDefaultCredentialsProvider(credsProvider)
					.build();
			clients.set(closeableHttpClient);
		}
		return closeableHttpClient;

	}

	/**
	 * httpClient get请求
	 *
	 * @param url       请求url
	 * @param proxyIp   代理ip
	 * @param proxyPort 代理端口
	 * @param header    头部信息
	 * @return
	 */
	public  String doGet(Map<String,String>mapMessage) {
		String url=mapMessage.get("url");
		String charset=mapMessage.get("coding");
		String proxyIp=mapMessage.get("ip");
		int proxyPort=Integer.valueOf(mapMessage.get("port"));
		String urlRef=url;
		String cookie="";String result = "";
		if(StringUtils.isNotEmpty(mapMessage.get("cookie"))){
			cookie=mapMessage.get("cookie");
			//cookie = "hng=US%7Czh-CN%7CUSD%7C840; enc=w0GnFeljlJBszKqiviVNm3gAArGbd9DfylMNq4aXzMDLaHgyHe0t8%2FBDnJtAt8M%2FPSFwxyjgk%2BhADuBcWXM%2BZQ%3D%3D; t=b78fdd3e27d098d999b2603695a89995; cna=M2toF58X2HECARd2L8G3UYZh; lgc=chaseju; tracknick=chaseju; mt=ci=-1_0; cookie2=2cda4816cd1db15b48a3b572ee2e31b5; _tb_token_=f7d53e3ebe1a5; _samesite_flag_=true; dnk=chaseju; sgcookie=E100fyBDCxiNATd4DRiLgnfNMivg%2BcUID%2FtP6rampR39%2FC1NxG2%2FRCtQggE25Sy7wL9Xg8WFe%2BxBAfMpD4h%2F5hWZlg%3D%3D; uc3=id2=UUpgT7m%2BqEmi%2FQ%3D%3D&vt3=F8dCuAMkPtp8%2Bydjps4%3D&lg2=VFC%2FuZ9ayeYq2g%3D%3D&nk2=AHLWll6xkA%3D%3D; csg=c66c9b99; skt=bc11a1e57a07ef06; existShop=MTYwOTI0OTI4Mg%3D%3D; uc4=nk4=0%40AhyAlqaIga8aHqQvEk9tR27J&id4=0%40U2gqwAZ6CN3vTK2EPUApQ9haHvvv; _cc_=WqG3DMC9EA%3D%3D; uc1=existShop=false&cookie16=WqG3DMC9UpAPBHGz5QBErFxlCA%3D%3D&pas=0&cookie21=U%2BGCWk%2F7pY%2FF&cookie14=Uoe0ZNTQoHf%2F7Q%3D%3D";
		}
		if(StringUtils.isNotEmpty(mapMessage.get("urlRef"))){
			urlRef=mapMessage.get("urlRef");
			//urlRef = "https://world.tmall.com/";
		}

		CloseableHttpClient httpClient = null;
		Map<String, String> headerParas=header(url, charset, cookie, urlRef);
		try {
			HttpHost proxy = null;
			if (StringUtils.isNotEmpty(proxyIp) && proxyPort > 0) {
				log.info("invoke HttpClientUtil doGet , Proxy IP = " + proxyIp + " Port = " + proxyPort);
				proxy = new HttpHost(proxyIp, proxyPort, "http");
			}
			RequestConfig config = RequestConfig.custom()
					.setSocketTimeout(MAX_TIMEOUT)
					.setConnectTimeout(MAX_TIMEOUT)
					.setConnectionRequestTimeout(MAX_TIMEOUT)//.setProxy(proxy)
					.build();
			httpClient = getHttpClient();
			HttpGet httpGet = new HttpGet(url);

			httpGet.setConfig(config);
			// 设置头信息
			if (headerParas != null) {
				for (Map.Entry<String, String> entry : headerParas.entrySet()) {
					httpGet.addHeader(entry.getKey(), entry.getValue());
				}
			}

			HttpResponse httpResponse = httpClient.execute(httpGet);
			int statusCode = httpResponse.getStatusLine().getStatusCode();
			if (statusCode == HttpStatus.SC_OK) {
				log.info("invoke HttpClientUtil doGet : statusCode = " + statusCode);
				HttpEntity resEntity = httpResponse.getEntity();
				result = EntityUtils.toString(resEntity,charset);
				//log.info(result);
			} else {
				log.info("invoke HttpClientUtil doGet : status code != 200 and status code = " + statusCode);
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			e.printStackTrace(new PrintStream(baos));
			String exception = baos.toString();	
			if(exception.contains("http://err.tmall.com/m-error1.html")) {
				result= "当前商品已经下架,很抱歉，您查看的宝贝不存在，可能已下架或被转移";
			}
		} finally {
			if (httpClient != null) {
				try {
					httpClient.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return result;
	}
	
	
    public  String doGetIP(String url) { 
        String result = "";
        CloseableHttpClient httpClient = null;
        try {
            RequestConfig config = RequestConfig.custom ( )
                    .setSocketTimeout (MAX_TIMEOUT).setConnectTimeout (MAX_TIMEOUT)
                    .setConnectionRequestTimeout (MAX_TIMEOUT).build ( );

            httpClient = getHttpClient ();
            HttpGet httpGet = new HttpGet (url);
            httpGet.setConfig (config);
            HttpResponse httpResponse = httpClient.execute (httpGet);
            int statusCode = httpResponse.getStatusLine ( ).getStatusCode ( );
            if (statusCode == HttpStatus.SC_OK) {
                log.info ("invoke HttpClientUtil doGet : statusCode = " + statusCode);
                HttpEntity resEntity = httpResponse.getEntity ( );
                result = EntityUtils.toString (resEntity,"utf-8");
            } else {
                log.info ("invoke HttpClientUtil doGet : status code != 200 and status code = " + statusCode);
            }
        } catch (Exception e) {
            log.error (e.getMessage ( ));
            e.printStackTrace ( );
        } finally {
            if (httpClient != null) {
                try {
                    httpClient.close ( );
                } catch (IOException e) {
                    e.printStackTrace ( );
                }
            }
        }
        return result;
    }

    
	/**请求代理Ip */
	public   String  getIpResult(String url){
		String priceString="";
		try{
			Connection conn =Jsoup.connect(url).timeout(600000).ignoreContentType(true).maxBodySize(0);
			conn.header("Accept", "*/*");
	        conn.header("Accept-Charset", "UTF-8,*;q=0.5");
	        conn.header("Accept-Encoding", "gzip, deflate, sdch");
	        conn.header("Accept-Language", "zh-cn,zh;q=0.8,en-us;q=0.5,en;q=0.3");
	        conn.header("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
	        conn.header("Cache-Control", "no-cache");
	        conn.header("Connection", "keep-alive");
	        conn.header("Pragma", "no-cache");
	        conn.header("Host",host(url));
	        conn.header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36");
	        conn.header("Referer", url);
			Response response= conn.method(Method.GET).execute();
			priceString=response.body();
		} catch (IOException e) {
			log.info("==>>请求代理Ip发送异常："+e+"<<==");
		}
		return priceString;
	}
	public  Map<String,String>header(String url,String charset,String cookie,String urlRef){
		Map<String,String>headerParm=new HashMap<String, String>();

		headerParm.put("http.socket.timeout",String.valueOf(MAX_TIMEOUT));
		headerParm.put("Host",host(url));
		headerParm.put("Accept-Language", "zh-cn,zh;q=0.8,en-us;q=0.5,en;q=0.3");
		headerParm.put("Connection", "keep-alive");
		headerParm.put("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3");
		headerParm.put("Cache-Control", "max-age=0");
		headerParm.put("Pragma", "no-cache");
		headerParm.put("X-Requested-With", "XMLHttpRequest");
		//headerParm.put("Accept-Encoding", "gzip,deflate,sdch,deflate,br");// br , 
		headerParm.put("Cache-Control", "no-cache");
		headerParm.put("Upgrade-insecure-Requests", "1"); 
		//headerParm.put("Origin", "http://www.xinouhui.com");
		headerParm.put("User-Agent",HttpCrawlerUtil.CrawlerAttribute());
		if (!Validation.isEmpty(cookie)) {
			headerParm.put("Cookie",cookie);
		}
		if (!Validation.isEmpty(urlRef)) {
			headerParm.put("Referer", urlRef);
		}
		return headerParm;
	}
	public static String host(String url){
		String HOST=null;
		String spitString[]=url.split("/");
		if(!Validation.isEmpty(url)){
			HOST=spitString[2];
		}
		return HOST;
	}
}
