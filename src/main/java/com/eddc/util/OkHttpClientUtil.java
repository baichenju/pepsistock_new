package com.eddc.util;
import java.io.IOException;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.SocketTimeoutException;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSONObject;
import okhttp3.Authenticator;
import okhttp3.ConnectionPool;
import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.Route;
public class OkHttpClientUtil {
	private static Logger logger=Logger.getLogger(OkHttpClientUtil.class);
	@SuppressWarnings("deprecation")
	public  String  okHttpClient(String url ,Map<String, String> mapData) {
		HttpClientUtils httpClientUtils=new HttpClientUtils();
		String item=""; 
		String proxyadder=ResourceBundle.getBundle("docker").getString("IP_ADDRES");
		try {
			OkHttpClient.Builder builder = new OkHttpClient.Builder();
			Authenticator proxyAuthenticator = new Authenticator() {
				@Override
				public Request authenticate(Route route, Response response) throws IOException {
					String credential = Credentials.basic("379862802", "infopower");
					return response.request().newBuilder()
							.header("Proxy-Authorization", credential)
							.build();
				}
			};
			String proxyIp = httpClientUtils.doGetIP(proxyadder);
			if(StringUtils.isNoneBlank(proxyIp)) {
				JSONObject jsonObject = JSONObject.parseObject(proxyIp);
				builder.proxyAuthenticator(proxyAuthenticator);
				builder.proxy(new Proxy(Proxy.Type.HTTP, new InetSocketAddress(jsonObject.getString("ip"), jsonObject.getInteger("port"))));
			}

			builder	.connectTimeout(10, TimeUnit.SECONDS);

			// 解决内存溢出问题
			builder.connectionPool(new ConnectionPool(10, 3, TimeUnit.SECONDS));
			builder.readTimeout(10, TimeUnit.SECONDS);
			builder.sslSocketFactory(new XZTLSSocketFactory());
			Request.Builder requests = new Request.Builder();
			if (mapData != null) {
				for (Map.Entry<String, String> entry : mapData.entrySet()) {
						if(mapData.containsKey("cookie")) {
							requests.addHeader(entry.getKey(), entry.getValue());
						}
				}
			}
			requests.addHeader("user-agent", HttpCrawlerUtil.CrawlerAttribute());
			requests.url(url);
			requests.method("GET", null);
			Response response = builder.build().newCall(requests.build()).execute();
			ResponseBody rb = response.body();
			item=rb.string();
		}catch(Exception e) {
			if(e instanceof SocketTimeoutException){
				logger.info("==>>Data request timeout..........<<==");
			}
			if(e instanceof ConnectException) {
				logger.info("==>>Connection timed out: connect..........<<==");
			}
		}
		return item;
	}



	@SuppressWarnings("deprecation")
	public  String  getOkHttp(String url,Map<String, String> mapData){
		String item=""; 
		HttpClientUtils httpClientUtils=new HttpClientUtils();
		OkHttpClient.Builder builder = new OkHttpClient.Builder();
		String proxyadder=ResourceBundle.getBundle("docker").getString("IP_ADDRES");
		String proxyIp = httpClientUtils.doGetIP(proxyadder);
		if(StringUtils.isNoneBlank(proxyIp)) {
			JSONObject jsonObject = JSONObject.parseObject(proxyIp);
			builder.proxy(new Proxy(Proxy.Type.HTTP, new InetSocketAddress(jsonObject.getString("ip"), jsonObject.getInteger("port"))));
		}
		builder	.connectTimeout(10, TimeUnit.SECONDS);

		// 解决内存溢出问题
		builder.connectionPool(new ConnectionPool(15, 3, TimeUnit.SECONDS));
		builder.readTimeout(15, TimeUnit.SECONDS);
		try {
			builder.sslSocketFactory(new XZTLSSocketFactory());
			Request.Builder requests = new Request.Builder();

			requests.addHeader("user-agent",HttpCrawlerUtil.CrawlerAttribute());
			requests.url(url);
			requests.method("GET", null);	
			if(mapData.containsKey("cookie")) {
				requests.addHeader("cookie", mapData.get("cookie").toString() ) ;
			}
			Response response = builder.build().newCall(requests.build()).execute();
			ResponseBody rb = response.body();
			item=rb.string();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return item;


	}





}
