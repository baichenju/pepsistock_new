package com.eddc.model;

import java.io.Serializable;

public class Craw_goods_Info_Failure implements Serializable {
	private int cust_keyword_id;
	private String goodsid;
	private String egoodsid;
	private String platform_name;
	private String goods_url;
	private int goods_status;
	private String update_time;
	/**
	 * @return the cust_keyword_id
	 */
	public int getCust_keyword_id() {
		return cust_keyword_id;
	}
	/**
	 * @param cust_keyword_id the cust_keyword_id to set
	 */
	public void setCust_keyword_id(int cust_keyword_id) {
		this.cust_keyword_id = cust_keyword_id;
	}
	/**
	 * @return the goodsid
	 */
	public String getGoodsid() {
		return goodsid;
	}
	/**
	 * @param goodsid the goodsid to set
	 */
	public void setGoodsid(String goodsid) {
		this.goodsid = goodsid;
	}
	/**
	 * @return the egoodsid
	 */
	public String getEgoodsid() {
		return egoodsid;
	}
	/**
	 * @param egoodsid the egoodsid to set
	 */
	public void setEgoodsid(String egoodsid) {
		this.egoodsid = egoodsid;
	}
	/**
	 * @return the platform_name
	 */
	public String getPlatform_name() {
		return platform_name;
	}
	/**
	 * @param platform_name the platform_name to set
	 */
	public void setPlatform_name(String platform_name) {
		this.platform_name = platform_name;
	}
	/**
	 * @return the goods_url
	 */
	public String getGoods_url() {
		return goods_url;
	}
	/**
	 * @param goods_url the goods_url to set
	 */
	public void setGoods_url(String goods_url) {
		this.goods_url = goods_url;
	}
	/**
	 * @return the goods_status
	 */
	public int getGoods_status() {
		return goods_status;
	}
	/**
	 * @param goods_status the goods_status to set
	 */
	public void setGoods_status(int goods_status) {
		this.goods_status = goods_status;
	}
	/**
	 * @return the update_time
	 */
	public String getUpdate_time() {
		return update_time;
	}
	/**
	 * @param update_time the update_time to set
	 */
	public void setUpdate_time(String update_time) {
		this.update_time = update_time;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Craw_goods_Info_Failure [cust_keyword_id=" + cust_keyword_id
				+ ", goodsid=" + goodsid + ", egoodsid=" + egoodsid
				+ ", platform_name=" + platform_name + ", goods_url="
				+ goods_url + ", goods_status=" + goods_status
				+ ", update_time=" + update_time + "]";
	}
	
}
