/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.model 
 * @author: jack.zhao   
 * @date: 2018年4月23日 下午5:28:41 
 */
package com.eddc.model;

import java.io.Serializable;

/**   
*    
* 项目名称：Price_monitoring_crawler   
* 类名称：Craw_goods_Fixed_Info   
* 类描述：  获取商品上架时间
* 创建人：jack.zhao   
* 创建时间：2018年4月23日 下午5:28:41   
* 修改人：jack.zhao   
* 修改时间：2018年4月23日 下午5:28:41   
* 修改备注：   
* @version    
*    
*/
public class Craw_goods_Fixed_Info implements Serializable {
private int cust_account_id;
private int cust_keyword_id;
private String platform_name_en;
private String egoodsid;
private String goodsId;
private String first_reviewtime;//---第一次评论时间
private  String update_time;
private   int  fixed_status;//1 商品有上下架时间 0 无

/**
 * @return the cust_account_id
 */
public int getCust_account_id() {
	return cust_account_id;
}
/**
 * @param cust_account_id the cust_account_id to set
 */
public void setCust_account_id(int cust_account_id) {
	this.cust_account_id = cust_account_id;
}
/**
 * @return the cust_keyword_id
 */
public int getCust_keyword_id() {
	return cust_keyword_id;
}
/**
 * @param cust_keyword_id the cust_keyword_id to set
 */
public void setCust_keyword_id(int cust_keyword_id) {
	this.cust_keyword_id = cust_keyword_id;
}
/**
 * @return the platform_name_en
 */
public String getPlatform_name_en() {
	return platform_name_en;
}
/**
 * @param platform_name_en the platform_name_en to set
 */
public void setPlatform_name_en(String platform_name_en) {
	this.platform_name_en = platform_name_en;
}
/**
 * @return the egoodsid
 */
public String getEgoodsid() {
	return egoodsid;
}
/**
 * @param egoodsid the egoodsid to set
 */
public void setEgoodsid(String egoodsid) {
	this.egoodsid = egoodsid;
}
/**
 * @return the first_reviewtime
 */
public String getFirst_reviewtime() {
	return first_reviewtime;
}
/**
 * @param first_reviewtime the first_reviewtime to set
 */
public void setFirst_reviewtime(String first_reviewtime) {
	this.first_reviewtime = first_reviewtime;
}
/**
 * @return the update_time
 */
public String getUpdate_time() {
	return update_time;
}
/**
 * @param update_time the update_time to set
 */
public void setUpdate_time(String update_time) {
	this.update_time = update_time;
}
/**
 * @return the goodsId
 */
public String getGoodsId() {
	return goodsId;
}
/**
 * @param goodsId the goodsId to set
 */
public void setGoodsId(String goodsId) {
	this.goodsId = goodsId;
}
/**
 * @return the fixed_status
 */
public int getFixed_status() {
	return fixed_status;
}
/**
 * @param fixed_status the fixed_status to set
 */
public void setFixed_status(int fixed_status) {
	this.fixed_status = fixed_status;
}

}
