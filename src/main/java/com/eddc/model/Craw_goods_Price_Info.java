package com.eddc.model;

import java.io.Serializable;

public class Craw_goods_Price_Info implements Serializable {
private static final long serialVersionUID = 1L;
private String cust_keyword_id;
private String goodsid;
private String SKUid;
private String channel;
private String original_price;
private String current_price;
private String promotion;
private String update_time;
private String update_date;
private String batch_time;
private String sale_qty;//销量
private String inventory;//库存
private String deposit;//定金
private String to_use_amount;//抵用金额
private String reserve_num;//预定件数
private String delivery_place;
private String platform_name_en;
private String sku_name;

	public String getPlatform_name_en() {
		return platform_name_en;
	}

	public void setPlatform_name_en(String platform_name_en) {
		this.platform_name_en = platform_name_en;
	}

	public String getSku_name() {
	return sku_name;
}
public void setSku_name(String sku_name) {
	this.sku_name = sku_name;
}
/**
 * @return the delivery_place
 */
public String getDelivery_place() {
	return delivery_place;
}
/**
 * @param delivery_place the delivery_place to set
 */
public void setDelivery_place(String delivery_place) {
	this.delivery_place = delivery_place;
}
/**
 * @return the cust_keyword_id
 */
public String getCust_keyword_id() {
	return cust_keyword_id;
}
/**
 * @param cust_keyword_id the cust_keyword_id to set
 */
public void setCust_keyword_id(String cust_keyword_id) {
	this.cust_keyword_id = cust_keyword_id;
}
/**
 * @return the goodsid
 */
public String getGoodsid() {
	return goodsid;
}
/**
 * @param goodsid the goodsid to set
 */
public void setGoodsid(String goodsid) {
	this.goodsid = goodsid;
}
/**
 * @return the sKUid
 */
public String getSKUid() {
	return SKUid;
}
/**
 * @param sKUid the sKUid to set
 */
public void setSKUid(String sKUid) {
	SKUid = sKUid;
}
/**
 * @return the channel
 */
public String getChannel() {
	return channel;
}
/**
 * @param channel the channel to set
 */
public void setChannel(String channel) {
	this.channel = channel;
}
/**
 * @return the original_price
 */
public String getOriginal_price() {
	return original_price;
}
/**
 * @param original_price the original_price to set
 */
public void setOriginal_price(String original_price) {
	this.original_price = original_price;
}
/**
 * @return the current_price
 */
public String getCurrent_price() {
	return current_price;
}
/**
 * @param current_price the current_price to set
 */
public void setCurrent_price(String current_price) {
	this.current_price = current_price;
}
/**
 * @return the promotion
 */
public String getPromotion() {
	return promotion;
}
/**
 * @param promotion the promotion to set
 */
public void setPromotion(String promotion) {
	this.promotion = promotion;
}
/**
 * @return the update_time
 */
public String getUpdate_time() {
	return update_time;
}
/**
 * @param update_time the update_time to set
 */
public void setUpdate_time(String update_time) {
	this.update_time = update_time;
}
/**
 * @return the update_date
 */
public String getUpdate_date() {
	return update_date;
}
/**
 * @param update_date the update_date to set
 */
public void setUpdate_date(String update_date) {
	this.update_date = update_date;
}
/**
 * @return the batch_time
 */
public String getBatch_time() {
	return batch_time;
}
/**
 * @param batch_time the batch_time to set
 */
public void setBatch_time(String batch_time) {
	this.batch_time = batch_time;
}

/**
 * @return the sale_qty
 */
public String getSale_qty() {
	return sale_qty;
}
/**
 * @param sale_qty the sale_qty to set
 */
public void setSale_qty(String sale_qty) {
	this.sale_qty = sale_qty;
}
/**
 * @return the inventory
 */
public String getInventory() {
	return inventory;
}
/**
 * @param inventory the inventory to set
 */
public void setInventory(String inventory) {
	this.inventory = inventory;
}
/**
 * @return the deposit
 */
public String getDeposit() {
	return deposit;
}
/**
 * @param deposit the deposit to set
 */
public void setDeposit(String deposit) {
	this.deposit = deposit;
}
/**
 * @return the to_use_amount
 */
public String getTo_use_amount() {
	return to_use_amount;
}
/**
 * @param to_use_amount the to_use_amount to set
 */
public void setTo_use_amount(String to_use_amount) {
	this.to_use_amount = to_use_amount;
}
/**
 * @return the reserve_num
 */
public String getReserve_num() {
	return reserve_num;
}
/**
 * @param reserve_num the reserve_num to set
 */
public void setReserve_num(String reserve_num) {
	this.reserve_num = reserve_num;
}
/* (non Javadoc) 
 * @Title: toString
 * @Description: TODO
 * @return 
 * @see java.lang.Object#toString() 
 */ 
@Override
public String toString() {
	return "Craw_goods_Price_Info [cust_keyword_id=" + cust_keyword_id + ", goodsid=" + goodsid + ", SKUid=" + SKUid
			+ ", channel=" + channel + ", original_price=" + original_price + ", current_price=" + current_price
			+ ", promotion=" + promotion + ", update_time=" + update_time + ", update_date=" + update_date
			+ ", batch_time=" + batch_time + ", sale_qty=" + sale_qty + ", inventory=" + inventory + ", deposit="
			+ deposit + ", to_use_amount=" + to_use_amount + ", reserve_num=" + reserve_num + "]";
}


}
