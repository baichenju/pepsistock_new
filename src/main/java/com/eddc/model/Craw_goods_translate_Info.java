/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.model 
 * @author: jack.zhao   
 * @date: 2018年4月26日 下午4:27:33 
 */
package com.eddc.model;

import java.io.Serializable;

/**   
*    
* 项目名称：Price_monitoring_crawler   
* 类名称：Craw_goods_translate_Info   
* 类描述：   
* 创建人：jack.zhao   
* 创建时间：2018年4月26日 下午4:27:33   
* 修改人：jack.zhao   
* 修改时间：2018年4月26日 下午4:27:33   
* 修改备注：   
* @version    
*    
*/
public class Craw_goods_translate_Info implements Serializable {
private int  cust_account_id;
private int cust_keyword_id;
private String platform_name_en;
private String egoodsid;
private String platform_goods_name_en;
private String platform_goods_name_cn;
private String update_time;
/**
 * @return the cust_account_id
 */
public int getCust_account_id() {
	return cust_account_id;
}
/**
 * @param cust_account_id the cust_account_id to set
 */
public void setCust_account_id(int cust_account_id) {
	this.cust_account_id = cust_account_id;
}
/**
 * @return the cust_keyword_id
 */
public int getCust_keyword_id() {
	return cust_keyword_id;
}
/**
 * @param cust_keyword_id the cust_keyword_id to set
 */
public void setCust_keyword_id(int cust_keyword_id) {
	this.cust_keyword_id = cust_keyword_id;
}
/**
 * @return the platform_name_en
 */
public String getPlatform_name_en() {
	return platform_name_en;
}
/**
 * @param platform_name_en the platform_name_en to set
 */
public void setPlatform_name_en(String platform_name_en) {
	this.platform_name_en = platform_name_en;
}
/**
 * @return the egoodsid
 */
public String getEgoodsid() {
	return egoodsid;
}
/**
 * @param egoodsid the egoodsid to set
 */
public void setEgoodsid(String egoodsid) {
	this.egoodsid = egoodsid;
}
/**
 * @return the platform_goods_name_en
 */
public String getPlatform_goods_name_en() {
	return platform_goods_name_en;
}
/**
 * @param platform_goods_name_en the platform_goods_name_en to set
 */
public void setPlatform_goods_name_en(String platform_goods_name_en) {
	this.platform_goods_name_en = platform_goods_name_en;
}
/**
 * @return the platform_goods_name_cn
 */
public String getPlatform_goods_name_cn() {
	return platform_goods_name_cn;
}
/**
 * @param platform_goods_name_cn the platform_goods_name_cn to set
 */
public void setPlatform_goods_name_cn(String platform_goods_name_cn) {
	this.platform_goods_name_cn = platform_goods_name_cn;
}
/**
 * @return the update_time
 */
public String getUpdate_time() {
	return update_time;
}
/**
 * @param update_time the update_time to set
 */
public void setUpdate_time(String update_time) {
	this.update_time = update_time;
}

}
