/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.model 
 * @author: jack.zhao   
 * @date: 2018年5月30日 下午3:37:55 
 */
package com.eddc.model;

import java.io.Serializable;

/**   
*    
* 项目名称：Price_monitoring_crawler   
* 类名称：Craw_goods_crowdfunding_price_Info   
* 类描述：   
* 创建人：jack.zhao   
* 创建时间：2018年5月30日 下午3:37:55   
* 修改人：jack.zhao   
* 修改时间：2018年5月30日 下午3:37:55   
* 修改备注：   
* @version    
*    
*/
@SuppressWarnings("serial")
public class Craw_goods_crowdfunding_price_Info implements Serializable{
private int cust_keyword_id;//
private String goodsId;//
private String egoodsId;//
private String source_egoodsid;
private String platform_goods_name;//回报商品内容
private String platform_goods_remark;//回报商品内容的进一步说明
private String platform_name_en;//平台
private Float curr_price;//众筹价
private int support_count;//支持人数
private int limit_total_count;//限制人数
private int remain_count;//剩余人数
private String exp_shiptime_remark;//预计回报说明
private String exp_postfee_remark;//邮费说明
private String goods_url;//商品页面链接
private String goods_pic_url;//回报商品的商品小图片链接
private Float source_curr_amount;
private int source_support_count;
private int source_focus_count;
private Float source_finish_per;
private String source_goods_status;
private String update_time;//
private String update_date;//
private String batch_time;//

/**
 * @return the source_curr_amount
 */
public Float getSource_curr_amount() {
	return source_curr_amount;
}
/**
 * @param source_curr_amount the source_curr_amount to set
 */
public void setSource_curr_amount(Float source_curr_amount) {
	this.source_curr_amount = source_curr_amount;
}
/**
 * @return the source_support_count
 */
public int getSource_support_count() {
	return source_support_count;
}
/**
 * @param source_support_count the source_support_count to set
 */
public void setSource_support_count(int source_support_count) {
	this.source_support_count = source_support_count;
}
/**
 * @return the source_focus_count
 */
public int getSource_focus_count() {
	return source_focus_count;
}
/**
 * @param source_focus_count the source_focus_count to set
 */
public void setSource_focus_count(int source_focus_count) {
	this.source_focus_count = source_focus_count;
}
/**
 * @return the source_finish_per
 */
public Float getSource_finish_per() {
	return source_finish_per;
}
/**
 * @param source_finish_per the source_finish_per to set
 */
public void setSource_finish_per(Float source_finish_per) {
	this.source_finish_per = source_finish_per;
}
/**
 * @return the source_goods_status
 */
public String getSource_goods_status() {
	return source_goods_status;
}
/**
 * @param source_goods_status the source_goods_status to set
 */
public void setSource_goods_status(String source_goods_status) {
	this.source_goods_status = source_goods_status;
}
/**
 * @return the source_egoodsid
 */
public String getSource_egoodsid() {
	return source_egoodsid;
}
/**
 * @param source_egoodsid the source_egoodsid to set
 */
public void setSource_egoodsid(String source_egoodsid) {
	this.source_egoodsid = source_egoodsid;
}
/**
 * @return the cust_keyword_id
 */
public int getCust_keyword_id() {
	return cust_keyword_id;
}
/**
 * @param cust_keyword_id the cust_keyword_id to set
 */
public void setCust_keyword_id(int cust_keyword_id) {
	this.cust_keyword_id = cust_keyword_id;
}
/**
 * @return the goodsId
 */
public String getGoodsId() {
	return goodsId;
}
/**
 * @param goodsId the goodsId to set
 */
public void setGoodsId(String goodsId) {
	this.goodsId = goodsId;
}
/**
 * @return the egoodsId
 */
public String getEgoodsId() {
	return egoodsId;
}
/**
 * @param egoodsId the egoodsId to set
 */
public void setEgoodsId(String egoodsId) {
	this.egoodsId = egoodsId;
}
/**
 * @return the platform_goods_name
 */
public String getPlatform_goods_name() {
	return platform_goods_name;
}
/**
 * @param platform_goods_name the platform_goods_name to set
 */
public void setPlatform_goods_name(String platform_goods_name) {
	this.platform_goods_name = platform_goods_name;
}
/**
 * @return the platform_goods_remark
 */
public String getPlatform_goods_remark() {
	return platform_goods_remark;
}
/**
 * @param platform_goods_remark the platform_goods_remark to set
 */
public void setPlatform_goods_remark(String platform_goods_remark) {
	this.platform_goods_remark = platform_goods_remark;
}
/**
 * @return the platform_name_en
 */
public String getPlatform_name_en() {
	return platform_name_en;
}
/**
 * @param platform_name_en the platform_name_en to set
 */
public void setPlatform_name_en(String platform_name_en) {
	this.platform_name_en = platform_name_en;
}
/**
 * @return the curr_price
 */
public Float getCurr_price() {
	return curr_price;
}
/**
 * @param curr_price the curr_price to set
 */
public void setCurr_price(Float curr_price) {
	this.curr_price = curr_price;
}
/**
 * @return the support_count
 */
public int getSupport_count() {
	return support_count;
}
/**
 * @param support_count the support_count to set
 */
public void setSupport_count(int support_count) {
	this.support_count = support_count;
}
/**
 * @return the limit_total_count
 */
public int getLimit_total_count() {
	return limit_total_count;
}
/**
 * @param limit_total_count the limit_total_count to set
 */
public void setLimit_total_count(int limit_total_count) {
	this.limit_total_count = limit_total_count;
}
/**
 * @return the remain_count
 */
public int getRemain_count() {
	return remain_count;
}
/**
 * @param remain_count the remain_count to set
 */
public void setRemain_count(int remain_count) {
	this.remain_count = remain_count;
}
/**
 * @return the exp_shiptime_remark
 */
public String getExp_shiptime_remark() {
	return exp_shiptime_remark;
}
/**
 * @param exp_shiptime_remark the exp_shiptime_remark to set
 */
public void setExp_shiptime_remark(String exp_shiptime_remark) {
	this.exp_shiptime_remark = exp_shiptime_remark;
}
/**
 * @return the exp_postfee_remark
 */
public String getExp_postfee_remark() {
	return exp_postfee_remark;
}
/**
 * @param exp_postfee_remark the exp_postfee_remark to set
 */
public void setExp_postfee_remark(String exp_postfee_remark) {
	this.exp_postfee_remark = exp_postfee_remark;
}
/**
 * @return the goods_url
 */
public String getGoods_url() {
	return goods_url;
}
/**
 * @param goods_url the goods_url to set
 */
public void setGoods_url(String goods_url) {
	this.goods_url = goods_url;
}
/**
 * @return the goods_pic_url
 */
public String getGoods_pic_url() {
	return goods_pic_url;
}
/**
 * @param goods_pic_url the goods_pic_url to set
 */
public void setGoods_pic_url(String goods_pic_url) {
	this.goods_pic_url = goods_pic_url;
}
/**
 * @return the update_time
 */
public String getUpdate_time() {
	return update_time;
}
/**
 * @param update_time the update_time to set
 */
public void setUpdate_time(String update_time) {
	this.update_time = update_time;
}
/**
 * @return the update_date
 */
public String getUpdate_date() {
	return update_date;
}
/**
 * @param update_date the update_date to set
 */
public void setUpdate_date(String update_date) {
	this.update_date = update_date;
}
/**
 * @return the batch_time
 */
public String getBatch_time() {
	return batch_time;
}
/**
 * @param batch_time the batch_time to set
 */
public void setBatch_time(String batch_time) {
	this.batch_time = batch_time;
}




}
