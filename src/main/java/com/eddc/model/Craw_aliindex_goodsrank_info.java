/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.model 
 * @author: jack.zhao   
 * @date: 2018年5月11日 下午1:33:34 
 */
package com.eddc.model;

import java.io.Serializable;

/**   
*    
* 项目名称：Price_monitoring_crawler   
* 类名称：Craw_aliindex_goodsrank_info   
* 类描述：   
* 创建人：jack.zhao   
* 创建时间：2018年5月11日 下午1:33:34   
* 修改人：jack.zhao   
* 修改时间：2018年5月11日 下午1:33:34   
* 修改备注：   
* @version    
*    
*/
public class Craw_aliindex_goodsrank_info implements Serializable {
private int  category_id;//类别id 例如 122312006　
private int rank_dimention_id;//---1: trade; 2: flow
private int rank_period_id;//7: week; 30: month
private int rank_type_id;//1 hot; 2: latest; 3: rise
private int rank_date_id;//-20180510 排行日期
private int goods_rank;//从零开始 排行榜
private String egoods_id;//商品ID
private String goods_name;///商品名称
private String image_url;//商品图片 　
private String company_name;//公司名称	　
private String company_url;//公司链接
private String comment_count;//商品评论数
private Float goods_price;//商品价格
private int  goods_trade;//商品交易指数
private int goods_flow;//商品流量指数
private String rank_date;//排行日期
private String insert_time;//-插入时间
private String update_time;//更新时间
private String samegoods_source_url;//相似商品url
/**
 * @return the category_id
 */
public int getCategory_id() {
	return category_id;
}
/**
 * @param category_id the category_id to set
 */
public void setCategory_id(int category_id) {
	this.category_id = category_id;
}
/**
 * @return the rank_dimention_id
 */
public int getRank_dimention_id() {
	return rank_dimention_id;
}
/**
 * @param rank_dimention_id the rank_dimention_id to set
 */
public void setRank_dimention_id(int rank_dimention_id) {
	this.rank_dimention_id = rank_dimention_id;
}
/**
 * @return the rank_period_id
 */
public int getRank_period_id() {
	return rank_period_id;
}
/**
 * @param rank_period_id the rank_period_id to set
 */
public void setRank_period_id(int rank_period_id) {
	this.rank_period_id = rank_period_id;
}
/**
 * @return the rank_type_id
 */
public int getRank_type_id() {
	return rank_type_id;
}
/**
 * @param rank_type_id the rank_type_id to set
 */
public void setRank_type_id(int rank_type_id) {
	this.rank_type_id = rank_type_id;
}
/**
 * @return the rank_date_id
 */
public int getRank_date_id() {
	return rank_date_id;
}
/**
 * @param rank_date_id the rank_date_id to set
 */
public void setRank_date_id(int rank_date_id) {
	this.rank_date_id = rank_date_id;
}
/**
 * @return the goods_rank
 */
public int getGoods_rank() {
	return goods_rank;
}
/**
 * @param goods_rank the goods_rank to set
 */
public void setGoods_rank(int goods_rank) {
	this.goods_rank = goods_rank;
}
/**
 * @return the egoods_id
 */
public String getEgoods_id() {
	return egoods_id;
}
/**
 * @param egoods_id the egoods_id to set
 */
public void setEgoods_id(String egoods_id) {
	this.egoods_id = egoods_id;
}
/**
 * @return the goods_name
 */
public String getGoods_name() {
	return goods_name;
}
/**
 * @param goods_name the goods_name to set
 */
public void setGoods_name(String goods_name) {
	this.goods_name = goods_name;
}
/**
 * @return the image_url
 */
public String getImage_url() {
	return image_url;
}
/**
 * @param image_url the image_url to set
 */
public void setImage_url(String image_url) {
	this.image_url = image_url;
}
/**
 * @return the company_name
 */
public String getCompany_name() {
	return company_name;
}
/**
 * @param company_name the company_name to set
 */
public void setCompany_name(String company_name) {
	this.company_name = company_name;
}
/**
 * @return the company_url
 */
public String getCompany_url() {
	return company_url;
}
/**
 * @param company_url the company_url to set
 */
public void setCompany_url(String company_url) {
	this.company_url = company_url;
}
/**
 * @return the comment_count
 */
public String getComment_count() {
	return comment_count;
}
/**
 * @param comment_count the comment_count to set
 */
public void setComment_count(String comment_count) {
	this.comment_count = comment_count;
}
/**
 * @return the goods_price
 */
public Float getGoods_price() {
	return goods_price;
}
/**
 * @param goods_price the goods_price to set
 */
public void setGoods_price(Float goods_price) {
	this.goods_price = goods_price;
}
/**
 * @return the goods_trade
 */
public int getGoods_trade() {
	return goods_trade;
}
/**
 * @param goods_trade the goods_trade to set
 */
public void setGoods_trade(int goods_trade) {
	this.goods_trade = goods_trade;
}
/**
 * @return the goods_flow
 */
public int getGoods_flow() {
	return goods_flow;
}
/**
 * @param goods_flow the goods_flow to set
 */
public void setGoods_flow(int goods_flow) {
	this.goods_flow = goods_flow;
}
/**
 * @return the rank_date
 */
public String getRank_date() {
	return rank_date;
}
/**
 * @param rank_date the rank_date to set
 */
public void setRank_date(String rank_date) {
	this.rank_date = rank_date;
}
/**
 * @return the insert_time
 */
public String getInsert_time() {
	return insert_time;
}
/**
 * @param insert_time the insert_time to set
 */
public void setInsert_time(String insert_time) {
	this.insert_time = insert_time;
}
/**
 * @return the update_time
 */
public String getUpdate_time() {
	return update_time;
}
/**
 * @param update_time the update_time to set
 */
public void setUpdate_time(String update_time) {
	this.update_time = update_time;
}
/**
 * @return the samegoods_source_url
 */
public String getSamegoods_source_url() {
	return samegoods_source_url;
}
/**
 * @param samegoods_source_url the samegoods_source_url to set
 */
public void setSamegoods_source_url(String samegoods_source_url) {
	this.samegoods_source_url = samegoods_source_url;
}


}
