/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.model 
 * @author: jack.zhao   
 * @date: 2018年5月7日 下午6:00:12 
 */
package com.eddc.model;

import java.io.Serializable;

/**   
*    
* 项目名称：Price_monitoring_crawler   
* 类名称：Craw_goods_crowdfunding_Info   
* 类描述：   
* 创建人：jack.zhao   
* 创建时间：2018年5月7日 下午6:00:12   
* 修改人：jack.zhao   
* 修改时间：2018年5月7日 下午6:00:12   
* 修改备注：   
* @version    
*    
*/
@SuppressWarnings("serial")
public class Craw_goods_crowdfunding_Info implements Serializable{
private  int cust_keyword_id;
private String goodsId;
private String egoodsId;
private String platform_goods_name;//商品名称
private String platform_goods_detail;//商品详情
private String platform_name_en;//平台
private String platform_category;//所在类
private String platform_sellername;//卖家名称
private String platform_sellerdetail;//卖家详情
private float curr_amount;//已筹金额
private float target_amount;//目标金额
private float finish_per;//达成率
private  int support_count;//支持人数
private int focus_count;//喜欢人数
private  int remain_day;//剩余天数
private String begin_date;//开始日期
private String end_date;//众筹结束日期
private String goods_status;//众筹状态
private String goods_url;//商品详情链接
private String goods_pic_url;//商品图片
private int price_status;
private String update_time;
private String update_date;
private String batch_time;

/**
 * @return the price_status
 */
public int getPrice_status() {
	return price_status;
}
/**
 * @param price_status the price_status to set
 */
public void setPrice_status(int price_status) {
	this.price_status = price_status;
}
/**
 * @return the cust_keyword_id
 */
public int getCust_keyword_id() {
	return cust_keyword_id;
}
/**
 * @param cust_keyword_id the cust_keyword_id to set
 */
public void setCust_keyword_id(int cust_keyword_id) {
	this.cust_keyword_id = cust_keyword_id;
}
/**
 * @return the goodsId
 */
public String getGoodsId() {
	return goodsId;
}
/**
 * @param goodsId the goodsId to set
 */
public void setGoodsId(String goodsId) {
	this.goodsId = goodsId;
}
/**
 * @return the egoodsId
 */
public String getEgoodsId() {
	return egoodsId;
}
/**
 * @param egoodsId the egoodsId to set
 */
public void setEgoodsId(String egoodsId) {
	this.egoodsId = egoodsId;
}
/**
 * @return the platform_goods_name
 */
public String getPlatform_goods_name() {
	return platform_goods_name;
}
/**
 * @param platform_goods_name the platform_goods_name to set
 */
public void setPlatform_goods_name(String platform_goods_name) {
	this.platform_goods_name = platform_goods_name;
}
/**
 * @return the platform_goods_detail
 */
public String getPlatform_goods_detail() {
	return platform_goods_detail;
}
/**
 * @param platform_goods_detail the platform_goods_detail to set
 */
public void setPlatform_goods_detail(String platform_goods_detail) {
	this.platform_goods_detail = platform_goods_detail;
}
/**
 * @return the platform_name_en
 */
public String getPlatform_name_en() {
	return platform_name_en;
}
/**
 * @param platform_name_en the platform_name_en to set
 */
public void setPlatform_name_en(String platform_name_en) {
	this.platform_name_en = platform_name_en;
}
/**
 * @return the platform_category
 */
public String getPlatform_category() {
	return platform_category;
}
/**
 * @param platform_category the platform_category to set
 */
public void setPlatform_category(String platform_category) {
	this.platform_category = platform_category;
}
/**
 * @return the platform_sellername
 */
public String getPlatform_sellername() {
	return platform_sellername;
}
/**
 * @param platform_sellername the platform_sellername to set
 */
public void setPlatform_sellername(String platform_sellername) {
	this.platform_sellername = platform_sellername;
}
/**
 * @return the platform_sellerdetail
 */
public String getPlatform_sellerdetail() {
	return platform_sellerdetail;
}
/**
 * @param platform_sellerdetail the platform_sellerdetail to set
 */
public void setPlatform_sellerdetail(String platform_sellerdetail) {
	this.platform_sellerdetail = platform_sellerdetail;
}
/**
 * @return the curr_amount
 */
public float getCurr_amount() {
	return curr_amount;
}
/**
 * @param curr_amount the curr_amount to set
 */
public void setCurr_amount(float curr_amount) {
	this.curr_amount = curr_amount;
}
/**
 * @return the target_amount
 */
public float getTarget_amount() {
	return target_amount;
}
/**
 * @param target_amount the target_amount to set
 */
public void setTarget_amount(float target_amount) {
	this.target_amount = target_amount;
}
/**
 * @return the finish_per
 */
public float getFinish_per() {
	return finish_per;
}
/**
 * @param finish_per the finish_per to set
 */
public void setFinish_per(float finish_per) {
	this.finish_per = finish_per;
}
/**
 * @return the support_count
 */
public int getSupport_count() {
	return support_count;
}
/**
 * @param support_count the support_count to set
 */
public void setSupport_count(int support_count) {
	this.support_count = support_count;
}
/**
 * @return the focus_count
 */
public int getFocus_count() {
	return focus_count;
}
/**
 * @param focus_count the focus_count to set
 */
public void setFocus_count(int focus_count) {
	this.focus_count = focus_count;
}
/**
 * @return the remain_day
 */
public int getRemain_day() {
	return remain_day;
}
/**
 * @param remain_day the remain_day to set
 */
public void setRemain_day(int remain_day) {
	this.remain_day = remain_day;
}
/**
 * @return the begin_date
 */
public String getBegin_date() {
	return begin_date;
}
/**
 * @param begin_date the begin_date to set
 */
public void setBegin_date(String begin_date) {
	this.begin_date = begin_date;
}
/**
 * @return the end_date
 */
public String getEnd_date() {
	return end_date;
}
/**
 * @param end_date the end_date to set
 */
public void setEnd_date(String end_date) {
	this.end_date = end_date;
}
/**
 * @return the goods_status
 */
public String getGoods_status() {
	return goods_status;
}
/**
 * @param goods_status the goods_status to set
 */
public void setGoods_status(String goods_status) {
	this.goods_status = goods_status;
}
/**
 * @return the goods_url
 */
public String getGoods_url() {
	return goods_url;
}
/**
 * @param goods_url the goods_url to set
 */
public void setGoods_url(String goods_url) {
	this.goods_url = goods_url;
}
/**
 * @return the goods_pic_url
 */
public String getGoods_pic_url() {
	return goods_pic_url;
}
/**
 * @param goods_pic_url the goods_pic_url to set
 */
public void setGoods_pic_url(String goods_pic_url) {
	this.goods_pic_url = goods_pic_url;
}
/**
 * @return the update_time
 */
public String getUpdate_time() {
	return update_time;
}
/**
 * @param update_time the update_time to set
 */
public void setUpdate_time(String update_time) {
	this.update_time = update_time;
}
/**
 * @return the update_date
 */
public String getUpdate_date() {
	return update_date;
}
/**
 * @param update_date the update_date to set
 */
public void setUpdate_date(String update_date) {
	this.update_date = update_date;
}
/**
 * @return the batch_time
 */
public String getBatch_time() {
	return batch_time;
}
/**
 * @param batch_time the batch_time to set
 */
public void setBatch_time(String batch_time) {
	this.batch_time = batch_time;
}
}
