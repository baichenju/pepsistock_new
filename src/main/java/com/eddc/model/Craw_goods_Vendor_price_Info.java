/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.model 
 * @author: jack.zhao   
 * @date: 2018年4月16日 下午3:22:26 
 */
package com.eddc.model;

import java.io.Serializable;

/**   
*    
* 项目名称：Price_monitoring_crawler   
* 类名称：Craw_goods_Vendor_price_Info   
* 类描述：   
* 创建人：jack.zhao   
* 创建时间：2018年4月16日 下午3:22:26   
* 修改人：jack.zhao   
* 修改时间：2018年4月16日 下午3:22:26   
* 修改备注：   
* @version    
*    
*/
public class Craw_goods_Vendor_price_Info implements Serializable {
private int cust_keyword_id;//
private String goodsid;
private String egoodsid;
private String purchase_amount;//采购起批量
private String purchase_unit;//采购单位
private String purchase_price;//采购价格
private String purchase_discount;//采购促销价
private String channel;//
private String update_time;
private String update_date;
private String batch_time;
/**
 * @return the cust_keyword_id
 */
public int getCust_keyword_id() {
	return cust_keyword_id;
}
/**
 * @param cust_keyword_id the cust_keyword_id to set
 */
public void setCust_keyword_id(int cust_keyword_id) {
	this.cust_keyword_id = cust_keyword_id;
}
/**
 * @return the goodsid
 */
public String getGoodsid() {
	return goodsid;
}
/**
 * @param goodsid the goodsid to set
 */
public void setGoodsid(String goodsid) {
	this.goodsid = goodsid;
}
/**
 * @return the egoodsid
 */
public String getEgoodsid() {
	return egoodsid;
}
/**
 * @param egoodsid the egoodsid to set
 */
public void setEgoodsid(String egoodsid) {
	this.egoodsid = egoodsid;
}
/**
 * @return the purchase_amount
 */
public String getPurchase_amount() {
	return purchase_amount;
}
/**
 * @param purchase_amount the purchase_amount to set
 */
public void setPurchase_amount(String purchase_amount) {
	this.purchase_amount = purchase_amount;
}
/**
 * @return the purchase_unit
 */
public String getPurchase_unit() {
	return purchase_unit;
}
/**
 * @param purchase_unit the purchase_unit to set
 */
public void setPurchase_unit(String purchase_unit) {
	this.purchase_unit = purchase_unit;
}
/**
 * @return the purchase_price
 */
public String getPurchase_price() {
	return purchase_price;
}
/**
 * @param purchase_price the purchase_price to set
 */
public void setPurchase_price(String purchase_price) {
	this.purchase_price = purchase_price;
}
/**
 * @return the purchase_discount
 */
public String getPurchase_discount() {
	return purchase_discount;
}
/**
 * @param purchase_discount the purchase_discount to set
 */
public void setPurchase_discount(String purchase_discount) {
	this.purchase_discount = purchase_discount;
}
/**
 * @return the channel
 */
public String getChannel() {
	return channel;
}
/**
 * @param channel the channel to set
 */
public void setChannel(String channel) {
	this.channel = channel;
}
/**
 * @return the update_time
 */
public String getUpdate_time() {
	return update_time;
}
/**
 * @param update_time the update_time to set
 */
public void setUpdate_time(String update_time) {
	this.update_time = update_time;
}
/**
 * @return the update_date
 */
public String getUpdate_date() {
	return update_date;
}
/**
 * @param update_date the update_date to set
 */
public void setUpdate_date(String update_date) {
	this.update_date = update_date;
}
/**
 * @return the batch_time
 */
public String getBatch_time() {
	return batch_time;
}
/**
 * @param batch_time the batch_time to set
 */
public void setBatch_time(String batch_time) {
	this.batch_time = batch_time;
}
/* (non Javadoc) 
 * @Title: toString
 * @Description: TODO
 * @return 
 * @see java.lang.Object#toString() 
 */ 
@Override
public String toString() {
	return "Craw_goods_Vendor_price_Info [cust_keyword_id=" + cust_keyword_id + ", goodsid=" + goodsid + ", egoodsid="
			+ egoodsid + ", purchase_amount=" + purchase_amount + ", purchase_unit=" + purchase_unit
			+ ", purchase_price=" + purchase_price + ", purchase_discount=" + purchase_discount + ", channel=" + channel
			+ ", update_time=" + update_time + ", update_date=" + update_date + ", batch_time=" + batch_time + "]";
}
/** 
 * @Title:Craw_goods_Vendor_price_Info
 * @Description:TODO 
 * @param cust_keyword_id
 * @param goodsid
 * @param egoodsid
 * @param purchase_amount
 * @param purchase_unit
 * @param purchase_price
 * @param purchase_discount
 * @param channel
 * @param update_time
 * @param update_date
 * @param batch_time 
 */  
public Craw_goods_Vendor_price_Info(int cust_keyword_id, String goodsid, String egoodsid, String purchase_amount,
		String purchase_unit, String purchase_price, String purchase_discount, String channel, String update_time,
		String update_date, String batch_time) {
	super();
	this.cust_keyword_id = cust_keyword_id;
	this.goodsid = goodsid;
	this.egoodsid = egoodsid;
	this.purchase_amount = purchase_amount;
	this.purchase_unit = purchase_unit;
	this.purchase_price = purchase_price;
	this.purchase_discount = purchase_discount;
	this.channel = channel;
	this.update_time = update_time;
	this.update_date = update_date;
	this.batch_time = batch_time;
}
/** 
 * @Title:Craw_goods_Vendor_price_Info
 * @Description:TODO  
 */  
public Craw_goods_Vendor_price_Info() {
	super();
	// TODO Auto-generated constructor stub
}

}
