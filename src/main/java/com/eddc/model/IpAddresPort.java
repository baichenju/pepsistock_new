package com.eddc.model;

import java.io.Serializable;

public class IpAddresPort implements Serializable {
private int id;
private String ipAddress;
private String port;
private int number;
private String cookie;
public int getId() {
	return id;
}
public String getIpAddress() {
	return ipAddress;
}
public void setId(int id) {
	this.id = id;
}
public void setIpAddress(String ipAddress) {
	this.ipAddress = ipAddress;
}
public String getPort() {
	return port;
}
public void setPort(String port) {
	this.port = port;
}
public int getNumber() {
	return number;
}
public void setNumber(int number) {
	this.number = number;
}
/**
 * @return the cookie
 */
public String getCookie() {
	return cookie;
}
/**
 * @param cookie the cookie to set
 */
public void setCookie(String cookie) {
	this.cookie = cookie;
}

}
