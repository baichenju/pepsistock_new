/**   
 * Copyright © 2017 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.model 
 * @author: jack.zhao   
 * @date: 2017年11月16日 下午3:44:48 
 */
package com.eddc.model;

import java.io.Serializable;

/**   
 *    
 * 项目名称：Price_monitoring_crawler   
 * 类名称：craw_customerweb_goods_info
 * 创建人：jack.zhao   
 * 创建时间：2017年11月16日 下午3:44:48   
 * 修改人：jack.zhao   
 * 修改时间：2017年11月16日 下午3:44:48   
 * 修改备注：   
 * @version    
 *    
 */
public class craw_customerweb_goods_info implements Serializable{
private int Userid ;
private String series ;
private String categories ;
private String  goods_name;
private String goods_url;
private String  goods_pic_url;
private String goods_price;
private String update_time;
private String update_date;
private String egoods_id;
/**
 * @return the userid
 */
public int getUserid() {
	return Userid;
}
/**
 * @param userid the userid to set
 */
public void setUserid(int userid) {
	Userid = userid;
}
/**
 * @return the series
 */
public String getSeries() {
	return series;
}
/**
 * @param series the series to set
 */
public void setSeries(String series) {
	this.series = series;
}
/**
 * @return the categories
 */
public String getCategories() {
	return categories;
}
/**
 * @param categories the categories to set
 */
public void setCategories(String categories) {
	this.categories = categories;
}
/**
 * @return the goods_name
 */
public String getGoods_name() {
	return goods_name;
}
/**
 * @param goods_name the goods_name to set
 */
public void setGoods_name(String goods_name) {
	this.goods_name = goods_name;
}
/**
 * @return the goods_url
 */
public String getGoods_url() {
	return goods_url;
}
/**
 * @param goods_url the goods_url to set
 */
public void setGoods_url(String goods_url) {
	this.goods_url = goods_url;
}
/**
 * @return the goods_pic_url
 */
public String getGoods_pic_url() {
	return goods_pic_url;
}
/**
 * @param goods_pic_url the goods_pic_url to set
 */
public void setGoods_pic_url(String goods_pic_url) {
	this.goods_pic_url = goods_pic_url;
}
/**
 * @return the goods_price
 */
public String getGoods_price() {
	return goods_price;
}
/**
 * @param goods_price the goods_price to set
 */
public void setGoods_price(String goods_price) {
	this.goods_price = goods_price;
}
/**
 * @return the update_time
 */
public String getUpdate_time() {
	return update_time;
}
/**
 * @param update_time the update_time to set
 */
public void setUpdate_time(String update_time) {
	this.update_time = update_time;
}
/**
 * @return the update_date
 */
public String getUpdate_date() {
	return update_date;
}
/**
 * @param update_date the update_date to set
 */
public void setUpdate_date(String update_date) {
	this.update_date = update_date;
}
/**
 * @return the egoods_id
 */
public String getEgoods_id() {
	return egoods_id;
}
/**
 * @param egoods_id the egoods_id to set
 */
public void setEgoods_id(String egoods_id) {
	this.egoods_id = egoods_id;
}

}
