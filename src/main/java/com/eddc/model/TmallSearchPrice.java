package com.eddc.model;

import java.io.Serializable;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class TmallSearchPrice  implements Serializable{ 
private String cust_keyword_id;
private String egoodsId;
private String platform_goods_name;
public String getCust_keyword_id() {
	return cust_keyword_id;
}
public String getEgoodsId() {
	return egoodsId;
}
public String getPlatform_goods_name() {
	return platform_goods_name;
}
public void setCust_keyword_id(String cust_keyword_id) {
	this.cust_keyword_id = cust_keyword_id;
}
public void setEgoodsId(String egoodsId) {
	this.egoodsId = egoodsId;
}
public void setPlatform_goods_name(String platform_goods_name) {
	this.platform_goods_name = platform_goods_name;
}

}
