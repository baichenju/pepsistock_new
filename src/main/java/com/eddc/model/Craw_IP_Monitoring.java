/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.model 
 * @author: jack.zhao   
 * @date: 2018年3月13日 下午1:56:49 
 */
package com.eddc.model;

import java.io.Serializable;

/**   
*    
* 项目名称：Price_monitoring_crawler   
* 类名称：Craw_IP_Monitoring   
* 类描述：   
* 创建人：jack.zhao   
* 创建时间：2018年3月13日 下午1:56:49   
* 修改人：jack.zhao   
* 修改时间：2018年3月13日 下午1:56:49   
* 修改备注：   
* @version    
*    
*/
public class Craw_IP_Monitoring implements Serializable {
private int id;
private String ip_library;
private int iP_number;
private String inset_time;
private String platform;
private int cust_account_id;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getIp_library() {
	return ip_library;
}
public void setIp_library(String ip_library) {
	this.ip_library = ip_library;
}
public int getiP_number() {
	return iP_number;
}
public void setiP_number(int iP_number) {
	this.iP_number = iP_number;
}
public String getInset_time() {
	return inset_time;
}
public void setInset_time(String inset_time) {
	this.inset_time = inset_time;
}
public String getPlatform() {
	return platform;
}
public void setPlatform(String platform) {
	this.platform = platform;
}
public int getCust_account_id() {
	return cust_account_id;
}
public void setCust_account_id(int cust_account_id) {
	this.cust_account_id = cust_account_id;
}
@Override
public String toString() {
	return "Craw_IP_Monitoring [id=" + id + ", ip_library=" + ip_library + ", iP_number=" + iP_number + ", inset_time="
			+ inset_time + ", platform=" + platform + ", cust_account_id=" + cust_account_id + "]";
}



}
