/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.model 
 * @author: jack.zhao   
 * @date: 2018年5月23日 下午1:33:39 
 */
package com.eddc.model;

/**   
*    
* 项目名称：Price_monitoring_crawler   
* 类名称：Ftp   
* 类描述：   
* 创建人：jack.zhao   
* 创建时间：2018年5月23日 下午1:33:39   
* 修改人：jack.zhao   
* 修改时间：2018年5月23日 下午1:33:39   
* 修改备注：   
* @version    
*    
*/
public class Ftp {
private String ipAddr;
private String userName;
private String pwd;
private int port;
private String path;
/**
 * @return the ipAddr
 */
public String getIpAddr() {
	return ipAddr;
}
/**
 * @param ipAddr the ipAddr to set
 */
public void setIpAddr(String ipAddr) {
	this.ipAddr = ipAddr;
}
/**
 * @return the userName
 */
public String getUserName() {
	return userName;
}
/**
 * @param userName the userName to set
 */
public void setUserName(String userName) {
	this.userName = userName;
}
/**
 * @return the pwd
 */
public String getPwd() {
	return pwd;
}
/**
 * @param pwd the pwd to set
 */
public void setPwd(String pwd) {
	this.pwd = pwd;
}
/**
 * @return the port
 */
public int getPort() {
	return port;
}
/**
 * @param port the port to set
 */
public void setPort(int port) {
	this.port = port;
}

/**
 * @return the path
 */
public String getPath() {
	return path;
}
/**
 * @param path the path to set
 */
public void setPath(String path) {
	this.path = path;
}
/* (non Javadoc) 
 * @Title: toString
 * @Description: TODO
 * @return 
 * @see java.lang.Object#toString() 
 */ 
@Override
public String toString() {
	return "Ftp [ipAddr=" + ipAddr + ", userName=" + userName + ", pwd=" + pwd + ", port=" + port + ", path=" + path
			+ "]";
}

}
