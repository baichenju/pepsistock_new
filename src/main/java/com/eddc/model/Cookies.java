/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.model 
 * @author: jack.zhao   
 * @date: 2018年3月26日 下午4:53:14 
 */
package com.eddc.model;

/**   
*    
* 项目名称：Price_monitoring_crawler   
* 类名称：Cookie   
* 类描述：   
* 创建人：jack.zhao   
* 创建时间：2018年3月26日 下午4:53:14   
* 修改人：jack.zhao   
* 修改时间：2018年3月26日 下午4:53:14   
* 修改备注：   
* @version    
*    
*/
public class Cookies {
private int id;
private String ip;
private String cookie;
/**
 * @return the id
 */
public int getId() {
	return id;
}
/**
 * @param id the id to set
 */
public void setId(int id) {
	this.id = id;
}
/**
 * @return the ip
 */
public String getIp() {
	return ip;
}
/**
 * @param ip the ip to set
 */
public void setIp(String ip) {
	this.ip = ip;
}
/**
 * @return the cookie
 */
public String getCookie() {
	return cookie;
}
/**
 * @param cookie the cookie to set
 */
public void setCookie(String cookie) {
	this.cookie = cookie;
}


}