/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.model 
 * @author: jack.zhao   
 * @date: 2018年6月21日 下午2:11:35 
 */
package com.eddc.model;

import java.io.Serializable;

/**   
*    
* 项目名称：selection_sibrary_crawler   
* 类名称：Craw_goods_bsr_Info   
* 类描述：   
* 创建人：jack.zhao   
* 创建时间：2018年6月21日 下午2:11:35   
* 修改人：jack.zhao   
* 修改时间：2018年6月21日 下午2:11:35   
* 修改备注：   
* @version    
*    
*/
@SuppressWarnings("serial")
public class Craw_goods_bsr_Info implements Serializable{
private  int cust_keyword_id;
private String goodsId;
private String egoodsId;
private String platform_name_en;
private String bsr_rank;
private String bsr_category;
private String update_time;
private String update_date;
private String batch_time;
/**
 * @return the cust_keyword_id
 */
public int getCust_keyword_id() {
	return cust_keyword_id;
}
/**
 * @param cust_keyword_id the cust_keyword_id to set
 */
public void setCust_keyword_id(int cust_keyword_id) {
	this.cust_keyword_id = cust_keyword_id;
}
/**
 * @return the goodsId
 */
public String getGoodsId() {
	return goodsId;
}
/**
 * @param goodsId the goodsId to set
 */
public void setGoodsId(String goodsId) {
	this.goodsId = goodsId;
}
/**
 * @return the egoodsId
 */
public String getEgoodsId() {
	return egoodsId;
}
/**
 * @param egoodsId the egoodsId to set
 */
public void setEgoodsId(String egoodsId) {
	this.egoodsId = egoodsId;
}
/**
 * @return the platform_name_en
 */
public String getPlatform_name_en() {
	return platform_name_en;
}
/**
 * @param platform_name_en the platform_name_en to set
 */
public void setPlatform_name_en(String platform_name_en) {
	this.platform_name_en = platform_name_en;
}
/**
 * @return the bsr_rank
 */
public String getBsr_rank() {
	return bsr_rank;
}
/**
 * @param bsr_rank the bsr_rank to set
 */
public void setBsr_rank(String bsr_rank) {
	this.bsr_rank = bsr_rank;
}
/**
 * @return the bsr_category
 */
public String getBsr_category() {
	return bsr_category;
}
/**
 * @param bsr_category the bsr_category to set
 */
public void setBsr_category(String bsr_category) {
	this.bsr_category = bsr_category;
}
/**
 * @return the update_time
 */
public String getUpdate_time() {
	return update_time;
}
/**
 * @param update_time the update_time to set
 */
public void setUpdate_time(String update_time) {
	this.update_time = update_time;
}
/**
 * @return the update_date
 */
public String getUpdate_date() {
	return update_date;
}
/**
 * @param update_date the update_date to set
 */
public void setUpdate_date(String update_date) {
	this.update_date = update_date;
}
/**
 * @return the batch_time
 */
public String getBatch_time() {
	return batch_time;
}
/**
 * @param batch_time the batch_time to set
 */
public void setBatch_time(String batch_time) {
	this.batch_time = batch_time;
}


}
