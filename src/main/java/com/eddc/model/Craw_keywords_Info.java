package com.eddc.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Craw_keywords_Info implements Serializable {
private int id;
private int cust_account_id;
private String cust_account_name;
private String platform_name;
private String cust_keyword_id;
private String cust_keyword_name;
private String cust_keyword_type;
private String feature_1;
private int crawling_status;
private String crawling_fre;
private String update_time;
private String update_date;
private String always_abnormal ;
private String is_presell;
private String cust_keyword_ref;
private int count;
private Craw_keywords_delivery_place crawKeywordsDeliveryPlace;

	public Craw_keywords_delivery_place getCrawKeywordsDeliveryPlace() {
		return crawKeywordsDeliveryPlace;
	}

	public void setCrawKeywordsDeliveryPlace(Craw_keywords_delivery_place crawKeywordsDeliveryPlace) {
		this.crawKeywordsDeliveryPlace = crawKeywordsDeliveryPlace;
	}

	/**
 * @return the cust_keyword_ref
 */
public String getCust_keyword_ref() {
	return cust_keyword_ref;
}
/**
 * @param cust_keyword_ref the cust_keyword_ref to set
 */
public void setCust_keyword_ref(String cust_keyword_ref) {
	this.cust_keyword_ref = cust_keyword_ref;
}
/**
 * @return the id
 */
public int getId() {
	return id;
}
/**
 * @param id the id to set
 */
public void setId(int id) {
	this.id = id;
}
/**
 * @return the cust_account_id
 */
public int getCust_account_id() {
	return cust_account_id;
}
/**
 * @param cust_account_id the cust_account_id to set
 */
public void setCust_account_id(int cust_account_id) {
	this.cust_account_id = cust_account_id;
}
/**
 * @return the cust_account_name
 */
public String getCust_account_name() {
	return cust_account_name;
}
/**
 * @param cust_account_name the cust_account_name to set
 */
public void setCust_account_name(String cust_account_name) {
	this.cust_account_name = cust_account_name;
}
/**
 * @return the platform_name
 */
public String getPlatform_name() {
	return platform_name;
}
/**
 * @param platform_name the platform_name to set
 */
public void setPlatform_name(String platform_name) {
	this.platform_name = platform_name;
}
/**
 * @return the cust_keyword_id
 */
public String getCust_keyword_id() {
	return cust_keyword_id;
}
/**
 * @param cust_keyword_id the cust_keyword_id to set
 */
public void setCust_keyword_id(String cust_keyword_id) {
	this.cust_keyword_id = cust_keyword_id;
}
/**
 * @return the cust_keyword_name
 */
public String getCust_keyword_name() {
	return cust_keyword_name;
}
/**
 * @param cust_keyword_name the cust_keyword_name to set
 */
public void setCust_keyword_name(String cust_keyword_name) {
	this.cust_keyword_name = cust_keyword_name;
}
/**
 * @return the cust_keyword_type
 */
public String getCust_keyword_type() {
	return cust_keyword_type;
}
/**
 * @param cust_keyword_type the cust_keyword_type to set
 */
public void setCust_keyword_type(String cust_keyword_type) {
	this.cust_keyword_type = cust_keyword_type;
}
/**
 * @return the feature_1
 */
public String getFeature_1() {
	return feature_1;
}
/**
 * @param feature_1 the feature_1 to set
 */
public void setFeature_1(String feature_1) {
	this.feature_1 = feature_1;
}
/**
 * @return the crawling_status
 */
public int getCrawling_status() {
	return crawling_status;
}
/**
 * @param crawling_status the crawling_status to set
 */
public void setCrawling_status(int crawling_status) {
	this.crawling_status = crawling_status;
}
/**
 * @return the crawling_fre
 */
public String getCrawling_fre() {
	return crawling_fre;
}
/**
 * @param crawling_fre the crawling_fre to set
 */
public void setCrawling_fre(String crawling_fre) {
	this.crawling_fre = crawling_fre;
}
/**
 * @return the update_time
 */
public String getUpdate_time() {
	return update_time;
}
/**
 * @param update_time the update_time to set
 */
public void setUpdate_time(String update_time) {
	this.update_time = update_time;
}
/**
 * @return the update_date
 */
public String getUpdate_date() {
	return update_date;
}
/**
 * @param update_date the update_date to set
 */
public void setUpdate_date(String update_date) {
	this.update_date = update_date;
}
/**
 * @return the always_abnormal
 */
public String getAlways_abnormal() {
	return always_abnormal;
}
/**
 * @param always_abnormal the always_abnormal to set
 */
public void setAlways_abnormal(String always_abnormal) {
	this.always_abnormal = always_abnormal;
}
/**
 * @return the is_presell
 */
public String getIs_presell() {
	return is_presell;
}
/**
 * @param is_presell the is_presell to set
 */
public void setIs_presell(String is_presell) {
	this.is_presell = is_presell;
}
/**
 * @return the count
 */
public int getCount() {
	return count;
}
/**
 * @param count the count to set
 */
public void setCount(int count) {
	this.count = count;
}


}