package com.eddc.task;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.eddc.model.CommodityPrices;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.model.Craw_goods_translate_Info;
import com.eddc.service.Amazon_DataCrawlService;
import com.eddc.service.CrawlerPublicClassService;
import com.eddc.service.HttpClientService;
import com.eddc.util.BatchInsertData;
import com.eddc.util.BeanMapUtil;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.StringHelper;
import com.eddc.util.TransApi;
import com.eddc.util.WhetherShelves;
import com.eddc.util.publicClass;
import com.google.common.collect.Lists;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
@Component
@Scope("prototype")
public class Amazon_DataCrawlTask implements  Callable<Map<String, Object>> {
	Logger logger = Logger.getLogger(Amazon_DataCrawlTask.class);
	@Autowired
	CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	HttpClientService httpClientService;
	@Autowired
	private Amazon_DataCrawlService amazon_DataCrawlService;
	@Autowired
	BatchInsertData batchInsertData;
	@Autowired
	WhetherShelves whetherShelves;
	private CommodityPrices commodityPrices;
	private CrawKeywordsInfo crawKeywordsInfo;
	private Map<String,Object>amazon;
	private String accountId;
	private String dataType;
	private String setUrl;
	private String IPPROXY;
	private String Sum;
	private String timeDate;
	private String type;
	private String codeCookice;
	private String status_type;
	private String page;
	private String ip;
	private String database;
	private String storage;
	public Map<String, Object> call() throws Exception {//call解析数据
		Map<String,String>Map=new HashMap<String, String>();
		Map=publicClass.parameterMap(setUrl,accountId,dataType,timeDate,IPPROXY);
		if(Fields.STYPE_1.equals(type)){//解析数据
			//logger.info("亚马逊平台开始解析数据"+"-------"+this.Sum+"---------------"+SimpleDate.SimpleDateFormatData().format(new Date()));
			try { 
				String goodsId=StringHelper.encryptByString(crawKeywordsInfo.getCust_keyword_name()+Fields.PLATFORM_AMAZON_UN);//生成goodSId
				Map.put("goodsId", goodsId);
				Map.put("egoodsId", crawKeywordsInfo.getCust_keyword_name());
				Map.put("keywordId",crawKeywordsInfo.getCust_keyword_id());
				Map.put("status_type", status_type);
				String StringMessage=httpClientService.interfaceSwitch(ip, Map,database);//请求数据
				String status =whetherShelves.parseGoodsStatusByItemPage(StringMessage);//判断商品是否下架
				Map<String, Object> sum_status=crawlerPublicClassService.InsertDatStatus(status,database,crawKeywordsInfo,goodsId,timeDate,dataType,setUrl,storage);
				if(Integer.valueOf(sum_status.get("status").toString())==Integer.valueOf(Fields.STATUS_OFF) || Integer.valueOf(sum_status.get("status").toString())==Integer.valueOf(Fields.STATUS_EXCEPTION)){
					amazon.putAll(sum_status);
					logger.info(crawKeywordsInfo.getPlatform_name()+"The goods have been removed from the shelves_"+Map.get("egoodsId")+"----------SOLD OUT------>>>>>>>>>>----"+Sum);
					return amazon; 
				}
				Map<String,Object>mapData=amazon_DataCrawlService.amazonParseItemPage(StringMessage,Map,database,storage,crawKeywordsInfo);//解析数据
				amazon.putAll(mapData);//返回信息;
				amazon.putAll(Map);
				logger.info(crawKeywordsInfo.getPlatform_name()+"Goods that have been seized_EGOODSID："+crawKeywordsInfo.getCust_keyword_name()+"-----SUCCESS--->>>>>>>>>----"+Sum);
			} catch (Exception e) { 
				e.printStackTrace();
				logger.info("解析亚马逊数据失败-"+SimpleDate.SimpleDateFormatData().format(new Date())+e.toString());
			}  
		}
		return amazon;
	}
	//商品英文名称切换成中文
	@SuppressWarnings("unchecked")
	public void nameCommoditySwitch(CrawKeywordsInfo info,String goodsName,String database){
		//判断需要翻译的商品是否已经存在，如果不存在调用此方法
		Map<String,Object> insertItemTime=new HashMap<String,Object>();
		List<Map<String,Object>> insert = Lists.newArrayList();
		Pattern p = Pattern.compile("[\u4e00-\u9fa5]");
		Matcher m = p.matcher(goodsName);
		if(!m.find()){//如果商品名称是中文无需翻译
			List<Craw_goods_translate_Info>list=crawlerPublicClassService.nameCommoditySwitchMessage(info,database);
			try {
				if(list.size()==0){
					TransApi api = new TransApi();//美国亚马逊商品名称翻译
					JSONObject currPriceJson = JSONObject.fromObject(api.getTransResult(goodsName.replace("\"", ""), "auto", "zh"));
					JSONArray priceInfoJson = currPriceJson.getJSONArray("trans_result");
					if(priceInfoJson.size()>0){
						logger.info(JSONObject.fromObject(priceInfoJson.toArray()[0]).getString("dst")+">>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
						Craw_goods_translate_Info  translate=new Craw_goods_translate_Info();
						translate.setCust_account_id(Integer.valueOf(accountId));
						translate.setCust_keyword_id(Integer.valueOf(info.getCust_keyword_id()));
						translate.setEgoodsid(info.getCust_keyword_name());
						translate.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
						translate.setPlatform_name_en(info.getPlatform_name());
						translate.setPlatform_goods_name_cn(JSONObject.fromObject(priceInfoJson.toArray()[0]).getString("dst"));
						translate.setPlatform_goods_name_en(goodsName);
						insertItemTime=BeanMapUtil.convertBean2MapWithUnderscoreName(translate);
						insert.add(insertItemTime); 
						batchInsertData.insertIntoData(insert,database,Fields.TABLE_CRAW_GOODS_TRANSLATE_INFO);//插入商品中文名称
					}
				}
			} catch (Exception e) {
				logger.info("百度翻译商品名称失败"+e.toString());
			}
		}
	}	
	public CrawKeywordsInfo getCrawKeywordsInfo() {
		return crawKeywordsInfo;
	}
	public void setCrawKeywordsInfo(CrawKeywordsInfo crawKeywordsInfo) {
		this.crawKeywordsInfo = crawKeywordsInfo;
	}

	public String getSum() {
		return Sum;
	}
	
	public void setSum(String sum) {
		Sum = sum;
	}
	
	public String getTimeDate() {
		return timeDate;
	}
	
	public void setTimeDate(String timeDate) {
		this.timeDate = timeDate;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public CrawlerPublicClassService getCrawlerPublicClassService() {
		return crawlerPublicClassService;
	}
	
	public void setCrawlerPublicClassService(
			CrawlerPublicClassService crawlerPublicClassService) {
		this.crawlerPublicClassService = crawlerPublicClassService;
	}
	public String getAccountId() {
		return accountId;
	}
	
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	
	public String getDataType() {
		return dataType;
	}
	
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	
	public String getSetUrl() {
		return setUrl;
	}
	
	public void setSetUrl(String setUrl) {
		this.setUrl = setUrl;
	}
	
	public String getIPPROXY() {
		return IPPROXY;
	}
	
	public void setIPPROXY(String iPPROXY) {
		IPPROXY = iPPROXY;
	}
	public CommodityPrices getCommodityPrices() {
		return commodityPrices;
	}
	public void setCommodityPrices(CommodityPrices commodityPrices) {
		this.commodityPrices = commodityPrices;
	}
	public String getCodeCookice() {
		return codeCookice;
	}
	public void setCodeCookice(String codeCookice) {
		this.codeCookice = codeCookice;
	}
	public String getStatus_type() {
		return status_type;
	}
	public void setStatus_type(String status_type) {
		this.status_type = status_type;
	}
	public String getPage() {
		return page;
	}
	public void setPage(String page) {
		this.page = page;
	}
	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Map<String, Object> getAmazon() {
		return amazon;
	}

	public void setAmazon(Map<String, Object> amazon) {
		this.amazon = amazon;
	}
	public String getDatabase() {
		return database;
	}
	public void setDatabase(String database) {
		this.database = database;
	}
	public String getStorage() {
		return storage;
	}
	public void setStorage(String storage) {
		this.storage = storage;
	}
	
}
