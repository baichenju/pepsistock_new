package com.eddc.task;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.eddc.model.CommodityPrices;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.model.Craw_goods_InfoVO;
import com.eddc.model.Craw_goods_Price_Info;
import com.eddc.service.CrawlerPublicClassService;
import com.eddc.service.HttpClientService;
import com.eddc.service.Yhd_DataCrawlService;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.StringHelper;
import com.eddc.util.WhetherShelves;
import com.eddc.util.publicClass;
import com.enterprise.support.utility.Validation;
import com.github.pagehelper.util.StringUtil;
@Component
@Scope("prototype")
public class Yhd_DataCrawlTask  implements Callable<Map<String, Object>>{ 
	Logger logger = Logger.getLogger(Yhd_DataCrawlTask.class);
	@Autowired
	CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	HttpClientService httpClientService;
	@Autowired
	private Yhd_DataCrawlService yhd_DataCrawlService;
	@Autowired
	private WhetherShelves whetherShelves;
	private CommodityPrices commodityPrices;
	private CrawKeywordsInfo crawKeywordsInfo;
	private String accountId;
	private String dataType;
	private String setUrl;
	private String IPPROXY;
	private String Sum; 
	private String timeDate;
	private String type;
	private String codeCookice;
	private Map<String,Object>yhdMap;
	private int promotion_status;
	private String ip;
	private String database;
	private String storage;
	public Map<String, Object> call() {
		Map<String,String>Map=new HashMap<String, String>();
		Map=publicClass.parameterMap(setUrl,accountId,dataType,timeDate,IPPROXY);
		Map.put("ip", ip);
		if(Fields.STYPE_1.equals(type)){//解析数据
			logger.info("一号店平台开始解析数据"+"-------"+this.Sum+"---------------"+SimpleDate.SimpleDateFormatData().format(new Date()));
			try { 
				String goodsId=StringHelper.encryptByString(crawKeywordsInfo.getCust_keyword_name()+Fields.PLATFORM_YHD);//生成goodSId
				Map.put("egoodsId", crawKeywordsInfo.getCust_keyword_name());
				Map.put("keywordId",crawKeywordsInfo.getCust_keyword_id());
				Map.put("goodsId", goodsId);
				if(StringUtil.isNotEmpty(codeCookice)){
					Map.put("cookie", codeCookice); 
				}
				Thread.sleep(1000); 
				String StringMessage=httpClientService.interfaceSwitch(ip, Map,database);//请求数据
				String status =whetherShelves.parseGoodsStatusByItemPage(StringMessage);//判断商品是否下架
				Map<String, Object> sum_status=crawlerPublicClassService.InsertDatStatus(status,database,crawKeywordsInfo,goodsId,timeDate,dataType,setUrl,storage);
				if(Integer.valueOf(sum_status.get("status").toString())==Integer.valueOf(Fields.STATUS_OFF) || Integer.valueOf(sum_status.get("status").toString())==Integer.valueOf(Fields.STATUS_EXCEPTION)){
					yhdMap.putAll(sum_status);
					return yhdMap; 
				}
				Map.put("promotion_status",String.valueOf(promotion_status));
				Map<String, String> itemFields=yhd_DataCrawlService.YhdParseItemPage(StringMessage,Map,database);//解析数据
				if(!Validation.isEmpty(itemFields.get(Fields.SHOPNAME))){//店铺不能为空
					Map.putAll(itemFields);
					Craw_goods_InfoVO info=crawlerPublicClassService.itemFieldsData(Map,database,storage); //商品详情插入数据库 
					Craw_goods_Price_Info price=crawlerPublicClassService.parseItemPrice(Map,crawKeywordsInfo.getCust_keyword_id(),goodsId,timeDate,dataType,Fields.STATUS_COUNT_1,database,storage);//APP端价格插入数据库
					yhdMap.put(Fields.TABLE_CRAW_GOODS_INFO, info);
					yhdMap.put(Fields.TABLE_CRAW_GOODS_PRICE_INFO, price);
					yhdMap.putAll(Map);
				}   
			} catch (Exception e) { 
				logger.info("解析一号店数据失败-"+SimpleDate.SimpleDateFormatData().format(new Date())+e.toString());
			}
		}else if(Fields.STYPE_6.equals(type)){
			try {
				Map.put("egoodsId", commodityPrices.getEgoodsId());
				Map<String,Object> pricedata=yhd_DataCrawlService.dataCrawYHDPriceP_PC(Map, commodityPrices);//PC价格
				if(pricedata.toString().contains("currentPrice")){
					yhdMap.putAll(pricedata);
				}
			} catch (Exception e) {
				logger.info("解析一号店PC价格解析失败------------"+e.toString()+"---------"+SimpleDate.SimpleDateFormatData().format(new Date()));
			}
		}
		return yhdMap;  
	} 
	public CrawKeywordsInfo getCrawKeywordsInfo() {
		return crawKeywordsInfo;
	}
	public void setCrawKeywordsInfo(CrawKeywordsInfo crawKeywordsInfo) {
		this.crawKeywordsInfo = crawKeywordsInfo;
	}

	public String getSum() {
		return Sum;
	}

	public void setSum(String sum) {
		Sum = sum;
	}

	public String getTimeDate() {
		return timeDate;
	}

	public void setTimeDate(String timeDate) {
		this.timeDate = timeDate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public CrawlerPublicClassService getCrawlerPublicClassService() {
		return crawlerPublicClassService;
	}

	public void setCrawlerPublicClassService(
			CrawlerPublicClassService crawlerPublicClassService) {
		this.crawlerPublicClassService = crawlerPublicClassService;
	}
	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getSetUrl() {
		return setUrl;
	}

	public void setSetUrl(String setUrl) {
		this.setUrl = setUrl;
	}

	public String getIPPROXY() {
		return IPPROXY;
	}

	public void setIPPROXY(String iPPROXY) {
		IPPROXY = iPPROXY;
	}
	public CommodityPrices getCommodityPrices() {
		return commodityPrices;
	}
	public void setCommodityPrices(CommodityPrices commodityPrices) {
		this.commodityPrices = commodityPrices;
	}
	public String getCodeCookice() {
		return codeCookice;
	}
	public void setCodeCookice(String codeCookice) {
		this.codeCookice = codeCookice;
	}
	public int getPromotion_status() {
		return promotion_status;
	}
	public void setPromotion_status(int promotion_status) {
		this.promotion_status = promotion_status;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public Map<String, Object> getYhdMap() {
		return yhdMap;
	}
	public void setYhdMap(Map<String, Object> yhdMap) {
		this.yhdMap = yhdMap;
	}
	public String getDatabase() {
		return database;
	}
	public void setDatabase(String database) {
		this.database = database;
	}
	/**
	 * @return the storage
	 */
	public String getStorage() {
		return storage;
	}
	/**
	 * @param storage the storage to set
	 */
	public void setStorage(String storage) {
		this.storage = storage;
	}

}
