package com.eddc.task;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.eddc.model.CommodityPrices;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.model.Craw_goods_Info;
import com.eddc.service.CrawlerPublicClassService;
import com.eddc.service.HttpClientService;
import com.eddc.service.Jd_DataCrawlService;
import com.eddc.util.Fields;
import com.eddc.util.publicClass;
import com.github.pagehelper.util.StringUtil;
@Component
@Scope("prototype")
public class JdPrice_DataCrawlTask implements Callable<Map<String, Object>> {
    Logger logger = Logger.getLogger(JdPrice_DataCrawlTask.class);
    @Autowired
    CrawlerPublicClassService crawlerPublicClassService;
    @Autowired
    HttpClientService httpClientService;
    @Autowired
    private Jd_DataCrawlService jd_DataCrawlService;
    private CommodityPrices commodityPricesJD;
    private CrawKeywordsInfo crawKeywordsInfo;
    private Craw_goods_Info craw_goods_Info;
    private Map<String, Object> jdMap;
    private String page;
    private String accountId;
    private String dataType;
    private String setUrl;
    private String IPPROXY;
    private String Sum;
    private String timeDate;
    private String type;
    private String cookie;
    private String cityCode;
    private String place_code;
    private String status_type;
    private String ip;
    private String database;
    private String storage;
    private String keyworId;
    private String dataEgoodsId;
    public Map<String, Object> call() throws Exception {//call解析数据
        Map<String, String> Map = new HashMap<String, String>();
        Map = publicClass.parameterMap(setUrl, accountId, dataType, timeDate, IPPROXY);
         if(Fields.STYPE_9.equals(type)){ 
        	 System.out.println(setUrl);
        	String StringMessage = httpClientService.interfaceSwitch(ip, Map,database);//请求数据	
        	if(StringUtil.isNotEmpty(StringMessage) && StringMessage.contains("op")){
        		Map<String,Object> priceData=jd_DataCrawlService.serchPrice(timeDate, dataType,StringMessage,database,keyworId,dataEgoodsId);//搜索商品价格
        		jdMap.putAll(priceData);
        		logger.info(" >>> get current price finish >>>>>>>>>>>>>>>SUCCESS>>>>>>>>>>>>>>>>>>>>>>>"+Sum);
        	}else{
        		 logger.error(">>> " + setUrl + ": Analysis JD Price Data Fail,please check! >>>");
        	}
        	
        }
        return jdMap;
    }
    public CrawKeywordsInfo getCrawKeywordsInfo() {
        return crawKeywordsInfo;
    }
    public void setCrawKeywordsInfo(CrawKeywordsInfo crawKeywordsInfo) {
        this.crawKeywordsInfo = crawKeywordsInfo;
    }
    public String getSum() {
        return Sum;
    }

    public void setSum(String sum) {
        Sum = sum;
    }

    public String getTimeDate() {
        return timeDate;
    }

    public void setTimeDate(String timeDate) {
        this.timeDate = timeDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public CrawlerPublicClassService getCrawlerPublicClassService() {
        return crawlerPublicClassService;
    }

    public void setCrawlerPublicClassService(
            CrawlerPublicClassService crawlerPublicClassService) {
        this.crawlerPublicClassService = crawlerPublicClassService;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getSetUrl() {
        return setUrl;
    }

    public void setSetUrl(String setUrl) {
        this.setUrl = setUrl;
    }

    public String getIPPROXY() {
        return IPPROXY;
    }

    public void setIPPROXY(String iPPROXY) {
        IPPROXY = iPPROXY;
    }

    public CommodityPrices getCommodityPricesJD() {
        return commodityPricesJD;
    }

    public void setCommodityPricesJD(CommodityPrices commodityPricesJD) {
        this.commodityPricesJD = commodityPricesJD;
    }

    public String getCookie() {
        return cookie;
    }

    public void setCookie(String cookie) {
        this.cookie = cookie;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getPlace_code() {
        return place_code;
    }

    public void setPlace_code(String place_code) {
        this.place_code = place_code;
    }

    public Craw_goods_Info getCraw_goods_Info() {
        return craw_goods_Info;
    }

    public void setCraw_goods_Info(Craw_goods_Info craw_goods_Info) {
        this.craw_goods_Info = craw_goods_Info;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getStatus_type() {
        return status_type;
    }

    public void setStatus_type(String status_type) {
        this.status_type = status_type;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Map<String, Object> getJdMap() {
        return jdMap;
    }

    public void setJdMap(Map<String, Object> jdMap) {
        this.jdMap = jdMap;
    }
    public String getDatabase() {
        return database;
    }
    public void setDatabase(String database) {
        this.database = database;
    }
	public String getKeyworId() {
		return keyworId;
	}
	public void setKeyworId(String keyworId) {
		this.keyworId = keyworId;
	}
	public String getStorage() {
		return storage;
	}
	public void setStorage(String storage) {
		this.storage = storage;
	}
	public String getDataEgoodsId() {
		return dataEgoodsId;
	}
	public void setDataEgoodsId(String dataEgoodsId) {
		this.dataEgoodsId = dataEgoodsId;
	}


}
