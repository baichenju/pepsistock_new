/**   
 * Copyright © 2017 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.task 
 * @author: jack.zhao   
 * @date: 2017年11月23日 下午4:00:14 
 */
package com.eddc.task;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.eddc.model.CommodityPrices;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.service.CrawlerPublicClassService;
import com.eddc.service.HttpClientService;
import com.eddc.service.Kaola_DataCrawlService;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.StringHelper;
import com.eddc.util.WhetherShelves;
import com.eddc.util.publicClass;
import com.enterprise.support.utility.Validation;
import com.github.pagehelper.util.StringUtil;

/**   
 *    
 * 项目名称：Price_monitoring_crawler   
 * 类名称：Kaolao_DataCrawlTask   
 * 类描述：   
 * 创建人：jack.zhao   
 * 创建时间：2017年11月23日 下午4:00:14   
 * 修改人：jack.zhao   
 * 修改时间：2017年11月23日 下午4:00:14   
 * 修改备注：   
 * @version    
 *    
 */
@Component
@Scope("prototype")
public class Kaolao_DataCrawlTask implements  Callable<Map<String, Object>>{
	Logger logger = Logger.getLogger(Kaolao_DataCrawlTask.class);
	@Autowired
	CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	HttpClientService httpClientService;
	@Autowired
	private WhetherShelves whetherShelves;
	@Autowired
	private Kaola_DataCrawlService kaola_DataCrawlService;
	private CommodityPrices commodityPrices;
	private CrawKeywordsInfo crawKeywordsInfo;
	private Map<String,Object>kaolaData;
	private String accountId;
	private String dataType;
	private String setUrl;
	private String IPPROXY;
	private String Sum; 
	private String timeDate;
	private String type;
	private String ip;
	private String database;
	private String storage;
	public Map<String, Object> call() throws Exception {//call解析数据
		Map<String,String>Map=new HashMap<String, String>();
		Map=publicClass.parameterMap(setUrl,accountId,dataType,timeDate,IPPROXY);
		if(Fields.STYPE_1.equals(type)){//解析数据
			logger.info("考拉平台开始解析数据"+"-------"+this.Sum+"---------------"+SimpleDate.SimpleDateFormatData().format(new Date()));
			try {
				String goodsId=StringHelper.encryptByString(crawKeywordsInfo.getCust_keyword_name()+Fields.PLATFORM_TMALL_EN);//生成goodSId
				Map.put("egoodsId", crawKeywordsInfo.getCust_keyword_name());
				Map.put("keywordId",crawKeywordsInfo.getCust_keyword_id());
				Map.put("goodsId", goodsId);
				String StringMessage=httpClientService.interfaceSwitch(ip, Map,database);//请求数据
				//String status =whetherShelves.parseGoodsStatusVip(StringMessage);//判断商品是否下架 
				Map<String, Object> sum_status=crawlerPublicClassService.InsertDatStatus(Fields.STATUS_ON,database,crawKeywordsInfo,goodsId,timeDate,dataType,setUrl,storage);
				if(Integer.valueOf(sum_status.get("status").toString())==Integer.valueOf(Fields.STATUS_OFF) || Integer.valueOf(sum_status.get("status").toString())==Integer.valueOf(Fields.STATUS_EXCEPTION)){
					kaolaData.putAll(sum_status);
					return kaolaData; 
				}
				Map<String, Object> kaolaDataItem=kaola_DataCrawlService.KaolaParseItemPage(StringMessage,crawKeywordsInfo,Map,database,storage);//解析数据
				kaolaData.putAll(kaolaDataItem);
			} catch (Exception e) { 
				logger.info("解析考拉数据失败-"+e.getMessage()+SimpleDate.SimpleDateFormatData().format(new Date()));
			}

		}else if(Fields.STYPE_6.equals(type)){
			try {
				Thread.sleep(1000); 
				Map.put("cookie",null);
				Map.put("egoodsId", commodityPrices.getEgoodsId());
				String PC_price_Message=httpClientService.interfaceSwitch(ip, Map,database);//请求数据
				if(Validation.isEmpty(PC_price_Message)||PC_price_Message.toString().contains(String.valueOf(HttpStatus.SC_NOT_FOUND))){
					PC_price_Message =httpClientService.requestData(Map);//Fields.CLIENT_MOBILE
				}
				commodityPrices.setBatch_time(timeDate);//重新赋值
				Map<String, Object> price=kaola_DataCrawlService.ResolutionCellPhonePrices(commodityPrices, PC_price_Message);//解析数据
				if(price.toString().contains("currentPrice")){
					kaolaData.putAll(Map);
					kaolaData.putAll(price);
				}	
			} catch (InterruptedException e) {
				  logger.info("请求考拉PC数据失败-------------"+e.getMessage()+"------------"+SimpleDate.SimpleDateFormatData().format(new Date()));
			} catch (Exception e) {
				  logger.info("请求考拉PC数据失败-------------"+e.getMessage()+"------------"+SimpleDate.SimpleDateFormatData().format(new Date()));
			}
			
		}
		return kaolaData;
	}
	public CrawlerPublicClassService getCrawlerPublicClassService() {
		return crawlerPublicClassService;
	}
	public void setCrawlerPublicClassService(
			CrawlerPublicClassService crawlerPublicClassService) {
		this.crawlerPublicClassService = crawlerPublicClassService;
	}

	public HttpClientService getHttpClientService() {
		return httpClientService;
	}

	public void setHttpClientService(HttpClientService httpClientService) {
		this.httpClientService = httpClientService;
	}
	public WhetherShelves getWhetherShelves() {
		return whetherShelves;
	}

	public void setWhetherShelves(WhetherShelves whetherShelves) {
		this.whetherShelves = whetherShelves;
	}
	public CommodityPrices getCommodityPrices() {
		return commodityPrices;
	}
	public void setCommodityPrices(CommodityPrices commodityPrices) {
		this.commodityPrices = commodityPrices;
	}
	public CrawKeywordsInfo getCrawKeywordsInfo() {
		return crawKeywordsInfo;
	}
	public void setCrawKeywordsInfo(CrawKeywordsInfo crawKeywordsInfo) {
		this.crawKeywordsInfo = crawKeywordsInfo;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getDataType() {
		return dataType;
	}
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	public String getSetUrl() {
		return setUrl;
	}
	public void setSetUrl(String setUrl) {
		this.setUrl = setUrl;
	}
	public String getIPPROXY() {
		return IPPROXY;
	}
	public void setIPPROXY(String iPPROXY) {
		IPPROXY = iPPROXY;
	}
	public String getSum() {
		return Sum;
	}
	public void setSum(String sum) {
		Sum = sum;
	}
	public String getTimeDate() {
		return timeDate;
	}
	public void setTimeDate(String timeDate) {
		this.timeDate = timeDate;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public Map<String, Object> getKaolaData() {
		return kaolaData;
	}
	public void setKaolaData(Map<String, Object> kaolaData) {
		this.kaolaData = kaolaData;
	}
	public String getDatabase() {
		return database;
	}
	public void setDatabase(String database) {
		this.database = database;
	}
	/**
	 * @return the storage
	 */
	public String getStorage() {
		return storage;
	}
	/**
	 * @param storage the storage to set
	 */
	public void setStorage(String storage) {
		this.storage = storage;
	}

}
