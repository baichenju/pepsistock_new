/**   
 * Copyright © 2017 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.task 
 * @author: jack.zhao   
 * @date: 2017年11月23日 下午4:00:14 
 */
package com.eddc.task;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.service.CrawlerPublicClassService;
import com.eddc.service.HttpClientService;
import com.eddc.service.Search_DataCrawlService;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.StringHelper;
import com.eddc.util.Validation;
import com.eddc.util.WhetherShelves;
import com.eddc.util.publicClass;
import com.github.pagehelper.util.StringUtil;
/**   
 *    
 * 项目名称：Price_monitoring_crawler   
 * 类名称：Pepsi_DataCrawlTask   
 * 类描述：   
 * 创建人：jack.zhao   
 * 创建时间：2018年03月14日 下午4:00:14   
 * 修改人：jack.zhao   
 * 修改时间：2018年03月14日 下午4:00:14   
 * 修改备注：   
 * @version    
 *    
 */
@Component
@Scope("prototype")
public class Search_DataCrawlTask implements Callable<Map<String, Object>> {
	Logger logger = Logger.getLogger(Search_DataCrawlTask.class);
	@Autowired
	CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	HttpClientService httpClientService;
	@Autowired
	private WhetherShelves whetherShelves;
	@Autowired
	private Search_DataCrawlService pepsi_DataCrawlService;
	private CrawKeywordsInfo crawKeywordsInfo;
	private  Map<String, Object>searchData;
	private String accountId;
	private String dataType;
	private String setUrl;
	private String IPPROXY;
	private String timeDate;
	private String type;
	private String codeCookie;
	private int status;
	private String status_type;
	private String oriUrl="";
	private String ref="";
	private String  ip;
	private String database;
	private String place_name;
	public Map<String, Object> call() throws Exception {
		//synchronized(this) {
			Map<String,String>Map=new HashMap<String, String>();
			Map=publicClass.parameterMap(setUrl,accountId,dataType,timeDate,IPPROXY);
			Map.put("status", String.valueOf(status));
			Map.put("status_type",status_type);
			Map.put("database", database);
			Map.put("cust_keyword_id", crawKeywordsInfo.getCust_keyword_id());
			Map.put("place_name", place_name);
			String StringMessage="";
			if(Fields.STYPE_1.equals(type)){
				try {
					Thread.sleep(1000);
					Map.put("egoodsId", crawKeywordsInfo.getCust_keyword_name());
					Map.put("accountId", accountId);
					Map.put("ip",ip);
					for(int i=1;i<=crawKeywordsInfo.getSearch_page_nums();i++){//天猫
						if(crawKeywordsInfo.getPlatform_name().equals(Fields.PLATFORM_TMALL_EN)||crawKeywordsInfo.getPlatform_name().equals(Fields.PLATFORM_TMALL_GLOBAL_EN)||crawKeywordsInfo.getPlatform_name().equals(Fields.PLATFORM_TAOBAO_EN)){
							Map<String,String>tmall=tmallData(Map,crawKeywordsInfo,i);
							Map.put("status", Fields.STATUS_ON);
							Map.putAll(tmall);

						}else if(crawKeywordsInfo.getPlatform_name().equals(Fields.PLATFORM_JD)){//京东
							Map<String,String>jd=jdData(Map,crawKeywordsInfo,i);
							Map.put("status", Fields.STATUS_ON);
							Map.putAll(jd);
						}else if(crawKeywordsInfo.getPlatform_name().equals(Fields.PLATFORM_YHD)){//一号店
							Map<String,String>yhd=yhdData(Map,crawKeywordsInfo,i,database);
							Map.put("status", Fields.STATUS_OFF);
							Map.putAll(yhd);
							if(yhd.get(Fields.DISABLE).contains(Fields.DISABLE)){
								return searchData;
							}
						}else if(crawKeywordsInfo.getPlatform_name().equals(Fields.PLATFORM_SUNING)){//苏宁
							Map<String,String>suning=suningdData(Map,crawKeywordsInfo,i,database);
							Map.put("status", Fields.STATUS_OFF);
							Map.putAll(suning);
							if(suning.get(Fields.DISABLE).contains(Fields.DISABLE)){
								return searchData;
							}
						}else if(crawKeywordsInfo.getPlatform_name().equals(Fields.PLATFORM_AMAZON)||crawKeywordsInfo.getPlatform_name().equals(Fields.PLATFORM_AMAZON_UN)){
							Map<String,String>amazon=amazonData(Map,crawKeywordsInfo,i);//亚马逊
							Map.put("status", Fields.STATUS_ON);
							Map.putAll(amazon);
						}else if(crawKeywordsInfo.getPlatform_name().equals(Fields.platform_1688)){
							if(crawKeywordsInfo.getCust_keyword_type().contains("category")){
								Map<String,String>platform1688=platform1688_Data(Map,crawKeywordsInfo,i);
								Map.put("status", Fields.STATUS_OFF);
								Map.putAll(platform1688);
								if(Map.get(Fields.DISABLE).contains(Fields.DISABLE)){
									return searchData; 
								}
							}else{ 
								Map<String,String>platform1688=platform1688_Data(Map,crawKeywordsInfo,i);
								Map.put("status", Fields.STATUS_ON);
								Map.putAll(platform1688);
							}
						}else if(crawKeywordsInfo.getPlatform_name().equals(Fields.PLATFORM_TAOBAO_ZC)){//淘宝众筹
							Map<String,String>taobazc=platformTaobaoZc_Data(Map,crawKeywordsInfo,i);
							Map.put("status", Fields.STATUS_ON);
							Map.putAll(taobazc);
						}else if(crawKeywordsInfo.getPlatform_name().equals(Fields.PLATFORM_JD_ZC)){//京东众筹
							Map<String,String>jdzc=platformJdZc_Data(Map,crawKeywordsInfo,i);
							Map.put("status", Fields.STATUS_ON);
							Map.putAll(jdzc);
						}else if(crawKeywordsInfo.getPlatform_name().equals(Fields.PLATFORM_KICKSTARTER)){//KICKSTARTER众筹
							Map<String,String>kickstarter=platformKickstarter_Data(Map,crawKeywordsInfo,i);
							Map.put("status", Fields.STATUS_ON);
							Map.putAll(kickstarter);
						}

						Thread.sleep(5000);
						if(Map.get("status").contains(Fields.STATUS_ON)){
							Map.put("count",String.valueOf(i));
							try {
								StringMessage=httpClientService.interfaceSwitch(ip, Map,database);//请求数据
								if(StringUtil.isEmpty(StringMessage)){
									StringMessage=httpClientService.GoodsProduct(Map,database);//抓取Itemyem
									if(StringUtil.isEmpty(StringMessage)){
										StringMessage=httpClientService.getJsoupResultMessage(Map);	
									}
									if(StringUtil.isEmpty(StringMessage)){
										StringMessage=httpClientService.GoodsProduct(Map,database);	
									}
								}
							} catch (Exception e) {
								StringMessage=httpClientService.GoodsProduct(Map,database);//抓取Itemyem
							}
							String disable =pepsi_DataCrawlService.ParseItemPage(StringMessage,crawKeywordsInfo,Map);//解析数据
							if(disable.contains(Fields.DISABLE)){
								return searchData;  
							}
						}
					}
				} catch (Exception e) { 
					e.printStackTrace();
					logger.info("解析"+dataType+"数据失败-"+e.getMessage()+SimpleDate.SimpleDateFormatData_zn().format(new Date()));
				}
			} 
		//}
		return searchData;
	}
	//天猫
	public  Map<String,String>tmallData(Map<String,String>Map,CrawKeywordsInfo crawKeywordsInfo, int i) throws UnsupportedEncodingException{
		if(crawKeywordsInfo.getCust_keyword_remark().contains(Fields.FLAGSHIP_STORE)){//旗舰店
			String SEARCH="";
			if(Validation.isNotEmpty(crawKeywordsInfo.getCust_keyword_name())){
				String keyWord=URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "GBK");
				SEARCH="&q="+keyWord;
			}
			Map.put("coding", "GBK");
			
			if(Validation.isEmpty(crawKeywordsInfo.getCust_keyword_ref())){
				String keyWord=URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");
				Map.put("url", crawKeywordsInfo.getCust_keyword_url().replace("PAGE", String.valueOf(i)).replace("keyword", keyWord));
				Map.put("urlRef",crawKeywordsInfo.getCust_keyword_url().replace("PAGE", String.valueOf(i)).replace("keyword", keyWord));
			}else{
				Map.put("url", crawKeywordsInfo.getCust_keyword_url().replace("PAGE", String.valueOf(i))+SEARCH);
				Map.put("urlRef",crawKeywordsInfo.getCust_keyword_ref().replace("PAGE", String.valueOf(i))+SEARCH);
			}
			Map.put("cookie",null);
		}else{
			if(crawKeywordsInfo.getCust_keyword_url().contains("pepsico.tmall.com")|| crawKeywordsInfo.getCust_keyword_url().contains("guigeshipin.tmall.com")){
				Map.put("url", crawKeywordsInfo.getCust_keyword_url().split("/")[0]+"//"+crawKeywordsInfo.getCust_keyword_url().split("/")[2]+Fields.PEPSI_KEYWORDS_DETAILS_URL +"&path=/category-"+crawKeywordsInfo.getCust_keyword_name()+".htm&catId="+crawKeywordsInfo.getCust_keyword_name()+"&scid="+crawKeywordsInfo.getCust_keyword_name()+"&pageNo="+i); 
				Map.put("coding", "GBK"); 
				Map.put("cookie",null);
				Map.put("urlRef", crawKeywordsInfo.getCust_keyword_url());
			}else if(crawKeywordsInfo.getCust_keyword_url().contains("list.tmall.com") && crawKeywordsInfo.getCust_keyword_type().contains("keyword")){
				if(i!=1){
					setUrl=setUrl+"&s="+40*i; 
				}
				Map.put("coding", "GBK");
				Map.put("url", setUrl);  
			}else if(crawKeywordsInfo.getCust_keyword_url().contains("search_radio_tmall")||crawKeywordsInfo.getCust_keyword_url().contains("filter_tianmao=tmall")||crawKeywordsInfo.getCust_keyword_url().contains("search_radio_all")){
				String keyWord=URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");
				String region="";
				if(Validation.isEmpty(Map.get("place_name"))){
					region="&loc="+URLEncoder.encode(Fields.SHANGHAI, "UTF-8");
				}else{
					region="&loc="+URLEncoder.encode(Map.get("place_name").toString(), "UTF-8");
				}
				if(i==1){
					setUrl=crawKeywordsInfo.getCust_keyword_url().replace("SEARCH", keyWord).replace("PAGE",String.valueOf(i-1))+region;
				}else{
					setUrl=crawKeywordsInfo.getCust_keyword_url().replace("SEARCH", keyWord).replace("PAGE",String.valueOf(i*44))+region;
				}
				Map.put("url", setUrl);
			}else if(crawKeywordsInfo.getCust_keyword_url().contains("list.tmall.com")&& crawKeywordsInfo.getCust_keyword_type().contains("category")){//天猫指定类目搜索
				Map.put("url", setUrl.replace("PAGE",String.valueOf((i*60))).replace("CODE", codeCookie));  
				Map.put("coding", "GBK");
			}else if(crawKeywordsInfo.getCust_keyword_url().contains("s.m.taobao.com/search")){
				String keyWord=URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");
				String region="";
				if(Validation.isEmpty(Map.get("place_name"))){
					region=Fields.SHANGHAI;
				}else{
					region=Map.get("place_name").toString();
				}
				Map.put("url", crawKeywordsInfo.getCust_keyword_url().replace("SEARCH", keyWord).replace("PAGE", String.valueOf(i)).replace("REGION",URLEncoder.encode(region, "UTF-8")));
			}
		}
		return Map;
	}
	//京东
	public  Map<String,String>jdData(Map<String,String>Map,CrawKeywordsInfo crawKeywordsInfo, int i)
			throws UnsupportedEncodingException{
		int mod =i%2;
		if(crawKeywordsInfo.getCust_keyword_url().contains("search.jd.com")){//京东搜索PC
			String keyWord=URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");
			if(mod==0){//https://search.jd.com/s_new.php?keyword=KEYWORD&enc=utf-8&qrst=1&rt=1&stop=1&vt=2&wq=KEYWORD&page=PAGE&s=SUM
				oriUrl=crawKeywordsInfo.getCust_keyword_url().replace("KEYWORD", keyWord).replace("PAGE",String.valueOf(i)).replace("SUM",String.valueOf(((i-1)*26)));
				//oriUrl=Fields.JD_SEARCH_URL.replace("KEYWORD", keyWord).replace("PAGE",String.valueOf(i)).replace("SUM",String.valueOf(((i-1)*26)))+"&scrolling=y&log_id=1524643976.14897&tpl=3_M";
			}else {
				oriUrl=crawKeywordsInfo.getCust_keyword_url().replace("KEYWORD", keyWord).replace("PAGE",String.valueOf(i)).replace("SUM",String.valueOf(((i-1)*26)));
				ref=crawKeywordsInfo.getCust_keyword_ref().replace("KEYWORD", keyWord).replace("PAGE",String.valueOf(i)).replace("SUM",String.valueOf(((i-1)*26)));
				//oriUrl=Fields.JD_SEARCH_URL.replace("KEYWORD", keyWord).replace("PAGE",String.valueOf(i)).replace("SUM",String.valueOf(((i-1)*26)))+"&click=2";
				//oriUrl=Fields.JD_SEARCH_URL+keyWord+"&enc=utf-8&qrst=1&rt=1&stop=1&vt=2&stock=1&page="+i+"&s="+((i-1)*26)+"&click=2&qrst=unchange";
				//ref=Fields.JD_SEARCH_REF_URL+keyWord+"&enc=utf-8&qrst=1&rt=1&stop=1&vt=2&stock=1&page="+i+"&s="+((i-1)*26)+"&click=2&qrst=unchange";
			}  
			Map.put("url", oriUrl);
			Map.put("urlRef", ref);
			if(StringUtil.isNotEmpty(codeCookie)){
				Map.put("cookie",codeCookie);
			}
		}else if(crawKeywordsInfo.getCust_keyword_url().contains("so.m.jd.com")){//京东APP
			if(crawKeywordsInfo.getCust_keyword_type().equals("category")){//categoryId=CATEGORYID&c1=C1&c2=C2
				Map.put("url", Fields.JD_SEARCH_REF_URL_APP.replace("PAGE",String.valueOf(i)).replace("C1", crawKeywordsInfo.getCust_keyword_name().split("-")[0]).replace("C2", crawKeywordsInfo.getCust_keyword_name().split("-")[1]).replace("CATEGORYID", crawKeywordsInfo.getCust_keyword_name().split("-")[2]));	
			}else if(crawKeywordsInfo.getCust_keyword_type().equals("keyword") ){
				String keyWord=URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");
				Map.put("url", Fields.JD_SEARCH_REF_URL_APP_KEYWORD.replace("PAGE",String.valueOf(i)).replace("KEYWORD", keyWord));	
			}else if(crawKeywordsInfo.getCust_keyword_type().equals("keywordcategory")){
				String keyWord=URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");
				String keyWord2=URLEncoder.encode(crawKeywordsInfo.getCategory_name(), "UTF-8");
				if(crawKeywordsInfo.getCust_account_name().contains(Fields.JD_LOGISTICS)){
					Map.put("url", Fields.JD_SEARCH_REF_URL_APP_KEYWORD.replace("PAGE",String.valueOf(i)).replace("KEYWORD", keyWord2)+"&expressionKey=%5B%7B%22value%22%3A%22"+keyWord+"%22%2C%22key%22%3A%22%E5%93%81%E7%89%8C%22%7D%5D&configuredFilters=%5B%7B%22bodyValues%22%3A%221%22%2C%22bodyKey%22%3A%22stock%22%7D%2C%7B%22bodyValues%22%3A%221%22%2C%22bodyKey%22%3A%22self%22%7D%5D");		
				}else{
					Map.put("url", Fields.JD_SEARCH_REF_URL_APP_KEYWORD.replace("PAGE",String.valueOf(i)).replace("KEYWORD", keyWord2)+"&expressionKey=%5B%7B%22value%22%3A%22"+keyWord+"%22%2C%22key%22%3A%22%E5%93%81%E7%89%8C%22%7D%5D&configuredFilters=%5B%7B%22bodyValues%22%3A%221%22%2C%22bodyKey%22%3A%22stock%22%7D%5D");	
				}
			}
			Map.put("urlRef", setUrl);
		}else if(crawKeywordsInfo.getCust_keyword_url().contains("list.jd.com")){//京东指定类目
			if(StringUtil.isNotEmpty(codeCookie)){
				Map.put("cookie",codeCookie);
			}
			Map.put("url",crawKeywordsInfo.getCust_keyword_url().replace("PAGE", String.valueOf(i)));	 
			Map.put("urlRef", crawKeywordsInfo.getCust_keyword_url().replace("PAGE", String.valueOf(i)));
		}
		return Map; 
	}
	//一号店
	public  Map<String,String>yhdData(Map<String,String>Map,CrawKeywordsInfo crawKeywordsInfo, int i,String database)
			throws Exception{
		String StringMessage=""; String disable="";
		if(crawKeywordsInfo.getCust_keyword_url().contains("search.m.yhd.com")){//一号店手机端搜索
			String keyWord=URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");//+
			if (crawKeywordsInfo.getCust_keyword_type().contains("category")) {
				Map.put("url", Fields.YHD_SEARCH_REF_URL_APP.replace("A",crawKeywordsInfo.getCategory_id() ).replace("P1","p"+String.valueOf(i)));
			}else if(crawKeywordsInfo.getCust_keyword_type().contains("keyword")){
				Map.put("url", Fields.YHD_SEARCH_REF_URL_APP_KEYWORD.replace("KEYWORD","k"+keyWord).replace("P1","p"+String.valueOf(i)));
			}
			Map.put("urlRef", setUrl);
			if(StringUtil.isNotEmpty(codeCookie)){
				Map.put("cookie",codeCookie);
			}  
		}else if(crawKeywordsInfo.getCust_keyword_url().contains("search.yhd.com")){//PC端搜索
			/*String keyWord=URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");//+
			String urlpc=Fields.YHD_SEARCH_REF_URL_APP_KEYWORD_PC.replace("PAGE","p"+String.valueOf(i)).replace("KSEARCH", "k"+keyWord);
			String nnforiUrls = "&isGetMoreProducts=1&moreProductsDefaultTemplate=0&isLargeImg=0";
			Map.put("urlRef",crawKeywordsInfo.getCust_keyword_url().replace("KSEARCH", "k"+keyWord));
			if(crawKeywordsInfo.getCust_keyword_type().equals("categorybrand")){
				urlpc=crawKeywordsInfo.getCust_keyword_url().replace("MBNAME", keyWord).replace("PA","p"+String.valueOf(i));
				Map.put("urlRef",Fields.YHD_SEARCH_PC_REF.replace("MBNAME", keyWord));
			}else if(crawKeywordsInfo.getCust_keyword_type().equals("keywordcategory")){
				String code[]=crawKeywordsInfo.getCust_keyword_url().split("/");
				urlpc=Fields.YHD_SEARCH_REF_URL_CATEGORY_KEYWORD_PC.replace("CATEGORY", code[3]).replace("CODE", code[4]).replace("PA","p"+String.valueOf(i));
				Map.put("urlRef",crawKeywordsInfo.getCust_keyword_url());	
			}
			Map.put("url",urlpc);
			if(StringUtil.isNotEmpty(codeCookie)){
				Map.put("cookie",codeCookie);
			}*/  
			String keyWord=URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");//+
			String urlpc=crawKeywordsInfo.getCust_keyword_ref().replace("PAGE",String.valueOf(i)).replace("KSEARCH", "k"+keyWord);
			for(int kk=0;kk<2;kk++){
				if(kk==0){
					Map.put("url",urlpc);	
				}else{
					Map.put("url",crawKeywordsInfo.getCust_keyword_url().replace("PAGE","p"+String.valueOf(i)).replace("KSEARCH", "k"+keyWord));		
				} 
				Map.put("urlRef",urlpc);
				Map.put("count",String.valueOf(i));
				Map.put("kk", String.valueOf(kk));
				logger.info(crawKeywordsInfo.getCust_keyword_name()+"----------当前商品第："+i+"页--------"+Map.get("url"));
				StringMessage=httpClientService.interfaceSwitch(Map.get("ip"), Map,database);//请求数据
				if(StringUtil.isEmpty(StringMessage)||StringMessage.contains(Fields.RETURNURL_FLAG)){
					StringMessage=httpClientService.GoodsProduct(Map,database);//抓取Itemyem 
				}
				disable =pepsi_DataCrawlService.ParseItemPage(StringMessage,crawKeywordsInfo,Map);//解析数据
				if(disable.contains(Fields.DISABLE)){
					Map.put(Fields.DISABLE, Fields.DISABLE);
					return Map;
				}
			}
		}
		Map.put(Fields.DISABLE, disable);
		return Map;
	}
	//1688供应商商品PC
	public  Map<String,String>platform1688_Data(Map<String,String>Map,CrawKeywordsInfo crawKeywordsInfo, int i)
			throws Exception{
		int mi=1000*(int)(1+Math.random()*(9-1+1));
		Thread.sleep(mi);String keyWord="";
		try {
			if(crawKeywordsInfo.getCust_keyword_type().contains("keyword")){
				if(crawKeywordsInfo.getCust_account_id().contains(Fields.ACCOUNTID_60)){
					keyWord=crawKeywordsInfo.getCust_keyword_name().replace(Fields.MESSAGE, "").trim();
					if(keyWord.contains("/")){
						keyWord=keyWord.split("/")[0].toString().trim();
					}else{
						if(crawKeywordsInfo.getCust_keyword_name().length()>8){
							keyWord=crawKeywordsInfo.getCust_keyword_name().substring(0,8);
						}	
					}
					keyWord=URLEncoder.encode(keyWord, "GBK");	
				}else{
					keyWord=URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "GBK");
				}	
				Map.put("urlRef",crawKeywordsInfo.getCust_keyword_url()+keyWord+"&beginPage="+i+"");
				Map.put("count",String.valueOf(i));
				Map.put("url", crawKeywordsInfo.getCust_keyword_url()+keyWord+"&beginPage="+i);
			}else if(crawKeywordsInfo.getCust_keyword_type().contains("category")){
				Map.put("count",String.valueOf(i));
				keyWord=URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "GBK");
				Map.put("url","https://s.1688.com/selloffer/rpc_async_render.jsonp?keywords="+keyWord+"&startIndex=0&sug=2_0&n=y&pageSize=60&rpcflag=new&async=true&templateConfigName=marketOfferresult&enableAsync=true&qrwRedirectEnabled=false&asyncCount=20&_pageName_=market&offset=9&uniqfield=pic_tag_id&beginPage="+i+"");	
				Map.put("urlRef", crawKeywordsInfo.getCust_keyword_url()+keyWord+"&beginPage="+i);
				Map.put("coding", "GBK");
				Thread.sleep(mi);//程序休眠	 
			}
		} catch (Exception e) {
			logger.info("请求1688数据失败"+e.getMessage());
		}
		return Map;
	}
	//淘宝众筹
	public  Map<String,String>platformTaobaoZc_Data(Map<String,String>Map,CrawKeywordsInfo crawKeywordsInfo, int i)
			throws Exception{
		Map.put("urlRef",crawKeywordsInfo.getCust_keyword_url().replace("PAGE", String.valueOf(i)));
		Map.put("count",String.valueOf(i));
		Map.put("url", crawKeywordsInfo.getCust_keyword_url().replace("PAGE", String.valueOf(i)));	
		return Map;
	}

	//京东众筹
	public  Map<String,String>platformJdZc_Data(Map<String,String>Map,CrawKeywordsInfo crawKeywordsInfo, int i)
			throws Exception{
		Map.put("urlRef",crawKeywordsInfo.getCust_keyword_url().replace("PAGE", String.valueOf(i)));
		Map.put("count",String.valueOf(i));
		Map.put("url", crawKeywordsInfo.getCust_keyword_url().replace("PAGE", String.valueOf(i)));	
		return Map;
	}

	//京东众筹
	public  Map<String,String>platformKickstarter_Data(Map<String,String>Map,CrawKeywordsInfo crawKeywordsInfo, int i)
			throws Exception{
		Map.put("urlRef",crawKeywordsInfo.getCust_keyword_url().replace("PAGE", String.valueOf(i)));
		Map.put("count",String.valueOf(i));
		Map.put("url", crawKeywordsInfo.getCust_keyword_url().replace("PAGE", String.valueOf(i)));	
		return Map;
	}
	//苏宁
	public  Map<String,String>suningdData(Map<String,String>Map,CrawKeywordsInfo crawKeywordsInfo, int i,String database)
			throws Exception{
		String StringMessage=""; String disable="";
		if(crawKeywordsInfo.getCust_keyword_url().contains("search.suning.com")){//苏宁搜索
			String code="021";
			if(StringUtil.isNotEmpty(codeCookie)){
				code=StringHelper.getResultByReg(codeCookie,"cityCode=([0-9]+);");
				Map.put("cookie",codeCookie);
			}  
			for(int kk=0;kk<2;kk++){
				String keyWord=URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");//+
				Map.put("url",Fields.SUNING_SEARCH_PC_REF.replace("KEYWORD", keyWord).replace("PAGE",String.valueOf(i-1)).replace("CITY", code));
				if(kk==1){
					Map.put("url",Fields.SUNING_SEARCH_PC_REF.replace("KEYWORD", keyWord).replace("PAGE",String.valueOf(i-1)).replace("CITY", code)+"&paging=1&sub=0");
				} 
				Map.put("urlRef",setUrl+keyWord+"/");
				Map.put("count",String.valueOf(i)); 
				logger.info(Map.get("url")+"----------当前商品第："+i+"页--------"+Map.get("url"));
				StringMessage=httpClientService.interfaceSwitch(Map.get("ip"), Map,database);//请求数据
				//StringMessage=httpClientService.GoodsProduct(Map);//抓取Itemyem 	
				disable =pepsi_DataCrawlService.ParseItemPage(StringMessage,crawKeywordsInfo,Map);//解析数据
				if(disable.contains(Fields.DISABLE)){
					Map.put(Fields.DISABLE, Fields.DISABLE);
					return Map;
				}
			}
		}
		Map.put(Fields.DISABLE, disable);
		return Map;
	}
	//亚马逊
	public  Map<String,String>amazonData(Map<String,String>Map,CrawKeywordsInfo crawKeywordsInfo, int i)
			throws Exception{
		if(crawKeywordsInfo.getCust_keyword_url().contains("www.amazon.com")){
			if((crawKeywordsInfo.getCust_keyword_type().contains("category"))){
				Map.put("url",crawKeywordsInfo.getCust_keyword_url().replace("PAGE", String.valueOf(i)));
			}else{
				String keyWord=URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");//+
				Map.put("url",Fields.AMAZON_URL_KEYWORD.replace("KEYWORD", keyWord).replace("PAGE", String.valueOf(i)));
			}
		}else if(crawKeywordsInfo.getCust_keyword_url().contains("www.amazon.cn")){
			if((crawKeywordsInfo.getCust_keyword_type().contains("category"))){
				Map.put("url",crawKeywordsInfo.getCust_keyword_url().replace("PAGE", String.valueOf(i)));
			}else{
				String keyWord=URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");//+
				Map.put("url",Fields.AMAZON_URL_KEYWORD_CN.replace("KEYWORD", keyWord).replace("PAGE", String.valueOf(i)));
			}

		}
		return Map;
	}
	public CrawlerPublicClassService getCrawlerPublicClassService() {
		return crawlerPublicClassService;
	}
	public void setCrawlerPublicClassService(
			CrawlerPublicClassService crawlerPublicClassService) {
		this.crawlerPublicClassService = crawlerPublicClassService;
	}

	public HttpClientService getHttpClientService() {
		return httpClientService;
	}

	public void setHttpClientService(HttpClientService httpClientService) {
		this.httpClientService = httpClientService;
	}
	public WhetherShelves getWhetherShelves() {
		return whetherShelves;
	}

	public void setWhetherShelves(WhetherShelves whetherShelves) {
		this.whetherShelves = whetherShelves;
	}
	public CrawKeywordsInfo getCrawKeywordsInfo() {
		return crawKeywordsInfo;
	}
	public void setCrawKeywordsInfo(CrawKeywordsInfo crawKeywordsInfo) {
		this.crawKeywordsInfo = crawKeywordsInfo;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getDataType() {
		return dataType;
	}
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	public String getSetUrl() {
		return setUrl;
	}
	public void setSetUrl(String setUrl) {
		this.setUrl = setUrl;
	}
	public String getIPPROXY() {
		return IPPROXY;
	}
	public void setIPPROXY(String iPPROXY) {
		IPPROXY = iPPROXY;
	}
	public String getTimeDate() {
		return timeDate;
	}
	public void setTimeDate(String timeDate) {
		this.timeDate = timeDate;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getCodeCookie() {
		return codeCookie;
	}
	public void setCodeCookie(String codeCookie) {
		this.codeCookie = codeCookie;
	}
	public String getStatus_type() {
		return status_type;
	}
	public void setStatus_type(String status_type) {
		this.status_type = status_type;
	}
	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getDatabase() {
		return database;
	}
	public void setDatabase(String database) {
		this.database = database;
	}
	public String getPlace_name() {
		return place_name;
	}
	public void setPlace_name(String place_name) {
		this.place_name = place_name;
	}
	public  Map<String, Object> getSearchData() {
		return searchData;
	}
	public void setSearchData( Map<String, Object> searchData) {
		this.searchData = searchData;
	}

}
