package com.eddc.task;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.eddc.model.Craw_goods_crowdfunding_Info;
import com.eddc.service.HttpClientService;
import com.eddc.service.CrowdFunding_DataCrawlService;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.StringHelper;
import com.eddc.util.publicClass;
import com.github.pagehelper.util.StringUtil;
@Component
@Scope("prototype")
public class CrowdFunding_DataCrawlTask  implements Callable<Map<String, Object>>{
	Logger log = Logger.getLogger(CrowdFunding_DataCrawlTask.class);
	@Autowired
	private CrowdFunding_DataCrawlService taobazc_DataCrawlService;
	@Autowired
	private HttpClientService httpClientService;
	private Craw_goods_crowdfunding_Info crowdfunding;
	private Map<String,Object>taobaozcMap;
	private String  dataType;
	private String timeDate;
	private String setUrl; 
	private String type;
	private String accountId;
	private String IPPROXY;
	private int page;
	private String ip;
	private String database;
	private String sum;
	private String storage;
	public Map<String, Object> call() {
		Map<String,String>Map=new HashMap<String, String>();
		Map<String, Object> itemFieldsTmall=new HashMap<String, Object>();
		Map=publicClass.parameterMap(setUrl,accountId,dataType,timeDate,IPPROXY);
		String goodsId=StringHelper.encryptByString(crowdfunding.getEgoodsId()+dataType);//生成goodSId
		Map.put("dataIdentification", Fields.STATUS_OFF);//STATUS_ON
		Map.put("accountId", accountId);
		Map.put("egoodsId", crowdfunding.getEgoodsId());
		Map.put("keywordId",String.valueOf(crowdfunding.getCust_keyword_id()));
		Map.put("goodsId", goodsId);
		Map.put("timeDate", timeDate);
		Map.put("platform_name", dataType); 
		Map.put("goodsUrl", setUrl); 
		if(Fields.STYPE_1.equals(type)){//解析数据
			try {
				String  StringMessage=httpClientService.interfaceSwitch(ip, Map,database);//请求数据
				try { 
					if(StringUtil.isNotEmpty(StringMessage)){
					itemFieldsTmall=taobazc_DataCrawlService.crowdFunding(StringMessage,crowdfunding,database,storage);
					taobaozcMap.putAll(Map);
					taobaozcMap.putAll(itemFieldsTmall);
					log.info(crowdfunding.getPlatform_name_en()+"The goods have been removed from the shelves_"+Map.get("egoodsId")+"----------SUCCESS---->>>>>>>>>------"+sum);
					}
				} catch (Exception e) { 
					e.printStackTrace();
					log.info("解析数据失败当前时间："+SimpleDate.SimpleDateFormatData().format(new Date())+e.getMessage());

				}
			} catch (Exception e) {
				e.printStackTrace();
			} 
		}else if(Fields.STYPE_10.equals(type)){
			try {
				String  StringMessage=httpClientService.interfaceSwitch(ip, Map,database);//请求数据
				try { 
					if(StringUtil.isNotEmpty(StringMessage)){
					itemFieldsTmall=taobazc_DataCrawlService.crowdFundingStatus(StringMessage,crowdfunding,database,storage);
					taobaozcMap.putAll(Map);
					taobaozcMap.putAll(itemFieldsTmall);
					log.info(crowdfunding.getPlatform_name_en()+"The goods have been removed from the shelves_"+Map.get("egoodsId")+"----------SUCCESS---->>>>>>>>>------"+sum);
					}
				} catch (Exception e) { 
					e.printStackTrace();
					log.info("解析数据失败当前时间："+SimpleDate.SimpleDateFormatData().format(new Date())+e.getMessage());

				}
			} catch (Exception e) {
				e.printStackTrace();
			} 
		}
		return taobaozcMap;
	}
	public String getDataType() {
		return dataType;
	}
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	public String getTimeDate() {
		return timeDate;
	}
	public void setTimeDate(String timeDate) {
		this.timeDate = timeDate;
	}
	
	public String getSetUrl() {
		return setUrl;
	}
	public void setSetUrl(String setUrl) {
		this.setUrl = setUrl;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	
	public String getIPPROXY() {
		return IPPROXY;
	}
	public void setIPPROXY(String iPPROXY) {
		IPPROXY = iPPROXY;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}
	public Map<String, Object> getTaobaozcMap() {
		return taobaozcMap;
	}
	public void setTaobaozcMap(Map<String, Object> taobaozcMap) {
		this.taobaozcMap = taobaozcMap;
	}
	public Craw_goods_crowdfunding_Info getCrowdfunding() {
		return crowdfunding;
	}
	public void setCrowdfunding(Craw_goods_crowdfunding_Info crowdfunding) {
		this.crowdfunding = crowdfunding;
	}
	public String getDatabase() {
		return database;
	}
	public void setDatabase(String database) {
		this.database = database;
	}
	public String getSum() {
		return sum;
	}
	public void setSum(String sum) {
		this.sum = sum;
	}
	/**
	 * @return the storage
	 */
	public String getStorage() {
		return storage;
	}
	/**
	 * @param storage the storage to set
	 */
	public void setStorage(String storage) {
		this.storage = storage;
	}
	
}
