package com.eddc.task;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.model.Craw_goods_Info;
import com.eddc.model.TmallSearchPrice;
import com.eddc.service.CrawlerPublicClassService;
import com.eddc.service.HttpClientService;
import com.eddc.service.Tmall_DataCrawlService;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.StringHelper;
import com.eddc.util.WhetherShelves;
import com.eddc.util.publicClass;
import com.github.pagehelper.util.StringUtil;
@Component
@Scope("prototype")
public class Taobao_DataCrawlTask  implements Callable<Map<String, Object>>{
	Logger log = Logger.getLogger(Taobao_DataCrawlTask.class);
	@Autowired
	private Tmall_DataCrawlService tmall_DataCrawlService;
	@Autowired
	private WhetherShelves whetherShelves;
	@Autowired
	CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	HttpClientService httpClientService;
	private TmallSearchPrice tmallSearchPrice;
	private CrawKeywordsInfo accountinfo;
	private Craw_goods_Info craw_goods_Info;
	private Map<String,Object>TaobaoMap;
	private String  dataType;
	private String timeDate;
	private String sum; 
	private String setUrl; 
	private String type;
	private String accountId;
	private String code;
	private String IPPROXY;
	private String client;
	private int page;
	private String ip;
	private int pageTop;
	private String database;
	private String storage;
	private String status_type;
	public Map<String, Object> call() {
		Map<String,String>Map=new HashMap<String, String>();
		Map<String,Object>message=new HashMap<String, Object>();
		Map=publicClass.parameterMap(setUrl,accountId,dataType,timeDate,IPPROXY);
		Map.put("ip",ip);
		Map.put("code",code);
		Map.put("accountId",accountId);
		Map.put("timeDate",timeDate);
		Map.put("dataType",dataType);
		Map.put("storage",storage);
		Map.put("client",client);
		Map.put("status_type",status_type);
		Map.put("dataIdentification", Fields.STATUS_OFF);//STATUS_ON
		Map.put("database", database);
		Map.put("pageTop",String.valueOf(pageTop));
		if(Fields.STYPE_1.equals(type)){//解析数据
			try {
				Thread.sleep(1000);
				message=parseTmallData(Map,accountinfo,null);//解析数据
				if(status_type.equals(Fields.COUNT_4)){
					if(message.toString().contains("present")){
						if(StringUtil.isNotEmpty(message.get("present").toString())){
							message=parseTmallData(Map,accountinfo,message.get("present").toString());//解析数据
						}
					}
				}
			} catch (InterruptedException e) {
				log.info("解析数据失败当前时间："+SimpleDate.SimpleDateFormatData().format(new Date())+e.getMessage());
			} catch (Exception e) {
				e.printStackTrace();
			} 
		}
		if(message.toString().contains("goodsName")){
			TaobaoMap.putAll(message);	
		}
		return TaobaoMap;
	}

	/**  
	 * @Title: parseTmallData  
	 * @Description: TODO(请求数据)  
	 * @param @param map
	 * @param @param accountinfo
	 * @param @param present
	 * @param @return    设定文件  
	 * @return Map<String,Object>    返回类型  
	 * @throws  
	 */  
	public Map<String, Object>parseTmallData(Map<String,String>Map,CrawKeywordsInfo accountinfo,String present){
		String goodsId="";
		Map<String, Object>dataMap=new HashMap<String, Object>();
		try {
			if(StringUtil.isEmpty(present)){
				goodsId=StringHelper.encryptByString(accountinfo.getCust_keyword_name()+accountinfo.getPlatform_name());//生成goodSId	
				Map.put("egoodsId", accountinfo.getCust_keyword_name());
				Map.put("goodsId", goodsId);
				dataMap=analysisTmallData(Map,accountinfo);
			}else{
				if(present.toString().contains(",")){
					String data[]=present.split(",");
					for(int i=0;i<data.length;i++){
						Map.put("egoodsId",data[i]);
						Map.put("url",Fields.TMALL_APP_2.replace("EGOODSID",data[i]).replace("CODE",Map.get("code").toString()));
						Map.put("goodsId", StringHelper.encryptByString(data[i]+accountinfo.getPlatform_name()));
						accountinfo.setCust_keyword_name(data[i]);
						dataMap=analysisTmallData(Map,accountinfo);
					}
				}else{
					Map.put("egoodsId",present);
					Map.put("url",Fields.TMALL_APP_2.replace("EGOODSID",present).replace("CODE",Map.get("code").toString()));
					Map.put("goodsId", StringHelper.encryptByString(present+accountinfo.getPlatform_name()));
					accountinfo.setCust_keyword_name(present);
					dataMap=analysisTmallData(Map,accountinfo);
				}
			}

		} catch (Exception e) {
			log.error(accountinfo.getPlatform_name()+"请求数据失败"+e);
		}

		return dataMap;
	}

	/**  
	 * @Title: analysisTmallData  
	 * @Description: TODO(解析数据)  
	 * @param @param Map
	 * @param @param accountinfo
	 * @param @param present
	 * @param @return    设定文件  
	 * @return Map<String,Object>    返回类型  
	 * @throws  
	 */
	public Map<String, Object>analysisTmallData(Map<String,String>Map,CrawKeywordsInfo accountinfo){
		Map<String, Object> itemFieldsTmall=new HashMap<String, Object>();
		try {
			Map.put("keywordId",accountinfo.getCust_keyword_id());
			String  StringMessage=httpClientService.interfaceSwitch(Map.get("ip").toString(),Map,Map.get("database").toString());//请求数据		
			String status =whetherShelves.parseGoodsStatusByItemPage(StringMessage);//判断商品是否下架	
			Map<String, Object> sum_status=crawlerPublicClassService.InsertDatStatus(status,Map.get("database").toString(),accountinfo,Map.get("goodsId").toString(),Map.get("timeDate"),Map.get("dataType"),Map.get("setUrl"),Map.get("storage"));
			if(Integer.valueOf(sum_status.get("status").toString())==Integer.valueOf(Fields.STATUS_OFF) || Integer.valueOf(sum_status.get("status").toString())==Integer.valueOf(Fields.STATUS_EXCEPTION)){
				log.info( Map.get("egoodsId") + " SOLD OUT : >>>>>>>>>>>>>>>>>>>>>>>" + sum + ">>>");
				return itemFieldsTmall; 
			}
			try {
				itemFieldsTmall.putAll(Map);
				itemFieldsTmall.putAll(sum_status);
				itemFieldsTmall=tmall_DataCrawlService.TmallparseItemH5Message_alibaba(StringMessage,accountinfo,Map);
				itemFieldsTmall.put("goodsUrl", Fields.TMALL_URL+accountinfo.getCust_keyword_name());

			} catch (Exception e){ 
				log.error("解析数据失败"+e);
			}
		} catch (Exception e) {
			log.error("解析数据失败"+e);
		}
		return itemFieldsTmall;
	}

	public CrawKeywordsInfo getAccountinfo() {
		return accountinfo;
	}
	public void setAccountinfo(CrawKeywordsInfo accountinfo) {
		this.accountinfo = accountinfo;
	}
	public String getDataType() {
		return dataType;
	}
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	public String getTimeDate() {
		return timeDate;
	}
	public void setTimeDate(String timeDate) {
		this.timeDate = timeDate;
	}
	public String getSum() {
		return sum;
	}
	public void setSum(String sum) {
		this.sum = sum;
	}
	public String getSetUrl() {
		return setUrl;
	}
	public void setSetUrl(String setUrl) {
		this.setUrl = setUrl;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getIPPROXY() {
		return IPPROXY;
	}
	public void setIPPROXY(String iPPROXY) {
		IPPROXY = iPPROXY;
	}
	public TmallSearchPrice getTmallSearchPrice() {
		return tmallSearchPrice;
	}
	public void setTmallSearchPrice(TmallSearchPrice tmallSearchPrice) {
		this.tmallSearchPrice = tmallSearchPrice;
	}
	public String getClient() {
		return client;
	}
	public void setClient(String client) {
		this.client = client;
	}
	public Craw_goods_Info getCraw_goods_Info() {
		return craw_goods_Info;
	}
	public void setCraw_goods_Info(Craw_goods_Info craw_goods_Info) {
		this.craw_goods_Info = craw_goods_Info;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}
	public Map<String, Object> getTaobaoMap() {
		return TaobaoMap;
	}
	public void setTaobaoMap(Map<String, Object> taobaoMap) {
		TaobaoMap = taobaoMap;
	}

	public int getPageTop() {
		return pageTop;
	}
	public void setPageTop(int pageTop) {
		this.pageTop = pageTop;
	}
	public String getDatabase() {
		return database;
	}
	public void setDatabase(String database) {
		this.database = database;
	}
	public String getStorage() {
		return storage;
	}
	public void setStorage(String storage) {
		this.storage = storage;
	}
	public String getStatus_type() {
		return status_type;
	}
	public void setStatus_type(String status_type) {
		this.status_type = status_type;
	}

}
