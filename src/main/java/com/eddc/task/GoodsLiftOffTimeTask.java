/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.task 
 * @author: jack.zhao   
 * @date: 2018年6月21日 下午2:29:42 
 */
package com.eddc.task;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.eddc.model.CommodityPrices;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.model.Craw_goods_Fixed_Info;
import com.eddc.model.Craw_goods_Info;
import com.eddc.service.Alibaba_DataCrawlService;
import com.eddc.service.Amazon_DataCrawlService;
import com.eddc.service.CrawlerPublicClassService;
import com.eddc.service.HttpClientService;
import com.eddc.service.Jd_DataCrawlService;
import com.eddc.service.Tmall_DataCrawlService;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.StringHelper;
import com.eddc.util.WhetherShelves;
import com.eddc.util.publicClass;
import com.github.pagehelper.util.StringUtil;

import net.sf.json.JSONObject;

/**   
 *    
 * 项目名称：selection_sibrary_crawler   
 * 类名称：GoodsLiftOffTimeTask   
 * 类描述：   
 * 创建人：jack.zhao   
 * 创建时间：2018年6月21日 下午2:29:42   
 * 修改人：jack.zhao   
 * 修改时间：2018年6月21日 下午2:29:42   
 * 修改备注：   
 * @version    
 *    
 */
@Component
@Scope("prototype")
public class GoodsLiftOffTimeTask implements Callable<Map<String, Object>>{
	@Autowired
	CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	HttpClientService httpClientService;
	@Autowired
	private Alibaba_DataCrawlService alibaba_DataCrawlService;
	@Autowired
	private Amazon_DataCrawlService amazon_DataCrawlService;
	@Autowired
	private Tmall_DataCrawlService tmall_DataCrawlService;
	@Autowired
	private Jd_DataCrawlService jd_DataCrawlService;
	@Autowired
	WhetherShelves whetherShelves;
	private CommodityPrices commodityPrices;
	private Craw_goods_Info crawGoodsiInfo;
	private Map<String,Object>goodsMap;
	private String accountId;
	private String dataType;
	private String setUrl;
	private String IPPROXY;
	private String Sum;
	private String timeDate;
	private String type;
	private String ip;
	private String database;
	private String storage;
	private String Status_type;
	private int page;
	private String keyworId;
	private String dataEgoodsId;
	private Logger logger = Logger.getLogger(GoodsLiftOffTimeTask.class);
	@Override
	public Map<String, Object> call() throws Exception {
		Map<String,String>Map=new HashMap<String, String>();
		Map=publicClass.parameterMap(setUrl,accountId,dataType,timeDate,IPPROXY);
		if(type.equalsIgnoreCase(Fields.COUNT_01)){
			String goodsId=StringHelper.encryptByString(crawGoodsiInfo.getEgoodsId()+crawGoodsiInfo.getPlatform_name_en());//生成goodSId
			if(dataType.equalsIgnoreCase(Fields.PLATFORM_AMAZON_UN)){
				try {
					//List<Craw_goods_Fixed_Info>fiexe=crawlerPublicClassService.whetherGoodsShelves(crawGoodsiInfo,database); //判断此商品上架时间是否已经存在
					int page=1;
						//for(int k=0;k<2;k++){
							Map.put("coding","GBK");
							Map.put("urlRef", Fields.PEPSI_AMAZON_EN_URL+crawGoodsiInfo.getEgoodsId());
							Map.put("url",Fields.AMAZONUSA_GOODS_TIME_URL_PC.replace("ITEMID",crawGoodsiInfo.getEgoodsId()).replace("PAGE",String.valueOf(page)));
							String item=httpClientService.interfaceSwitch(ip, Map,database);//请求数据
							if(item.contains("a-pagination")){
								int pageItem=amazon_DataCrawlService.amazonGoodsOnTime(item,database,crawGoodsiInfo,Integer.valueOf(page),crawGoodsiInfo.getCust_account_id(), goodsId);
								if(pageItem!=0){
									if(pageItem>Integer.valueOf(page)){
										Map.put("url",Fields.AMAZONUSA_GOODS_TIME_URL_PC.replace("ITEMID",crawGoodsiInfo.getEgoodsId()).replace("PAGE",String.valueOf(pageItem)));
										item=httpClientService.GoodsProduct(Map,database);
										pageItem=amazon_DataCrawlService.amazonGoodsOnTime(item,database,crawGoodsiInfo,pageItem,crawGoodsiInfo.getCust_account_id(),goodsId);
									}	
								}else{
									crawlerPublicClassService.shelfLifeDoesNotExist(database,crawGoodsiInfo, crawGoodsiInfo.getCust_account_id(), goodsId);
								}
							}else{
								amazon_DataCrawlService.amazonGoodsOnTime(item,database,crawGoodsiInfo,page,crawGoodsiInfo.getCust_account_id(),goodsId);
							}
						//}
				} catch (Exception e) {
					logger.error(">>> " + setUrl + ": Analysis Amazonusa Data Fail,please check! >>>");
					logger.error(e.getMessage());
				}
			}else if(dataType.equalsIgnoreCase(Fields.PLATFORM_TMALL_EN) || dataType.equalsIgnoreCase(Fields.PLATFORM_TAOBAO_EN)){
				try {
					//List<Craw_goods_Fixed_Info>fiexe=crawlerPublicClassService.whetherGoodsShelves(crawGoodsiInfo,database); //判断此商品上架时间是否已经存在
						//for(int k=0;k<2;k++){
							Map.put("codng","GBK");
							Map.put("url",Fields.TMALL_GOODS_TIME_URL.replace("PAGE",String.valueOf(page)).replace("ITEMID",crawGoodsiInfo.getEgoodsId()).replace("SELLERID",crawGoodsiInfo.getPlatform_sellerid()));
							String item=httpClientService.interfaceSwitch(ip, Map,database);//请求数据
							if(item.contains("lastPage")){
								int pageItem=tmall_DataCrawlService.tmallGoodsOnTimeData(item,crawGoodsiInfo,page,accountId,database,goodsId);
								if(pageItem!=0){
									if(pageItem>page){
										Map.put("url",Fields.TMALL_GOODS_TIME_URL.replace("PAGE",String.valueOf(pageItem)).replace("ITEMID",crawGoodsiInfo.getEgoodsId()).replace("SELLERID",crawGoodsiInfo.getPlatform_sellerid()));
										item=httpClientService.interfaceSwitch(ip, Map,database);//请求数据
										pageItem=tmall_DataCrawlService.tmallGoodsOnTimeData(item,crawGoodsiInfo,pageItem,accountId,database,goodsId);
									}	
								}else{
									crawlerPublicClassService.shelfLifeDoesNotExist(database,crawGoodsiInfo, crawGoodsiInfo.getCust_account_id(), goodsId);
								}
							}
						//}
				} catch (Exception e) {
					logger.error(">>> " + setUrl + ": Analysis Tmall Data Fail,please check! >>>");
					logger.error(e.getMessage());
				}

			}else if(dataType.equalsIgnoreCase(Fields.PLATFORM_JD)){
				logger.info(">>> Invoke whetherGoodsShelves >>>");
				try {
					int page = 0; int pageItemSum=0;
					//List<Craw_goods_Fixed_Info>fiexe=crawlerPublicClassService.whetherGoodsShelves(crawGoodsiInfo,database); //判断此商品上架时间是否已经存在
						//for (int k = 0; k < 2; k++) {
							Map.put("coding", "GBK");
							Map.put("urlRef",crawGoodsiInfo.getGoods_url());// Fields.JD_URL_APP + crawGoodsiInfo.getEgoodsId() + ".html"
							Map.put("url", Fields.JD_GOODS_TIME_URL_PC.replace("ITEMID", crawGoodsiInfo.getEgoodsId()).replace("PAGE", String.valueOf(page)));
							String item = httpClientService.interfaceSwitch(ip, Map,database);//请求数据
							if (item.contains("productCommentSummary")) {
								int pageItem = jd_DataCrawlService.jdGoodsOnTime(item, crawGoodsiInfo, Integer.valueOf(page+1), database,goodsId);
								if (pageItem != 0 && pageItem!=1) { 
//									if(pageItem==100){
//										pageItemSum=1;
//									}
									Map.put("url", Fields.JD_GOODS_TIME_URL_PC.replace("ITEMID", crawGoodsiInfo.getEgoodsId()).replace("PAGE", String.valueOf(pageItemSum-1)));//pageItem - 1
									item = httpClientService.interfaceSwitch(ip, Map,database);
									pageItem = jd_DataCrawlService.jdGoodsOnTime(item, crawGoodsiInfo, pageItem, database,goodsId);
								} else {
									crawlerPublicClassService.shelfLifeDoesNotExist(database,crawGoodsiInfo, crawGoodsiInfo.getCust_account_id(), goodsId);
								}
							}else{
								crawlerPublicClassService.shelfLifeDoesNotExist(database,crawGoodsiInfo, crawGoodsiInfo.getCust_account_id(), goodsId);
							}
				} catch (Exception e) {
					logger.error(" >>> whetherGoodsShelves fail , please check!>>>");
					e.printStackTrace();
				}
				logger.info(">>> Finish invoke whetherGoodsShelves >>>");

			}
		}if(type.equalsIgnoreCase(Fields.COUNT_2)){//获取京东评论数
			try{
					//Map.put("coding", "GBK");
					//Map.put("urlRef", Fields.JD_URL_APP + crawGoodsiInfo.getEgoodsId() + ".html");
					//Map.put("url", Fields.JD_GOODS_TIME_URL_PC.replace("ITEMID", crawGoodsiInfo.getEgoodsId()).replace("PAGE", String.valueOf(page)));
					String item = httpClientService.interfaceSwitch(ip, Map,database);//请求数据 
					if(item.contains("CommentsCount")){
					jd_DataCrawlService. serchCommentsGoods(timeDate,dataType,item,database,keyworId,dataEgoodsId);//获取商品评论数 	
					}
					
					
					/*if(item.toString().contains("productCommentSummary")){
						JSONObject currPriceJson = JSONObject.fromObject(item);
						jd_DataCrawlService. CommentsGoods(timeDate,crawGoodsiInfo,goodsId,currPriceJson,database,Fields.STATUS_COUNT_1);//获取商品评论数 
					}else{
						CrawKeywordsInfo info =new CrawKeywordsInfo();
						info.setPlatform_name(crawGoodsiInfo.getPlatform_name_en());
						info.setCust_keyword_id(String.valueOf(crawGoodsiInfo.getCust_keyword_id()));
						info.setCust_keyword_name(crawGoodsiInfo.getEgoodsId());
						crawlerPublicClassService.goodsCommentInfo(timeDate,database,info,Fields.STATUS_OFF,goodsId, Fields.STATUS_COUNT);
					}*/
			}catch(Exception e){
				logger.error(" >>> whetherGoodsShelves fail , please check!>>>"+e.getMessage());  
			}

		}
		return goodsMap;
	}

	public Alibaba_DataCrawlService getAlibaba_DataCrawlService() {
		return alibaba_DataCrawlService;
	}

	public void setAlibaba_DataCrawlService(Alibaba_DataCrawlService alibaba_DataCrawlService) {
		this.alibaba_DataCrawlService = alibaba_DataCrawlService;
	}

	public CommodityPrices getCommodityPrices() {
		return commodityPrices;
	}

	public void setCommodityPrices(CommodityPrices commodityPrices) {
		this.commodityPrices = commodityPrices;
	}

	public Map<String, Object> getGoodsMap() {
		return goodsMap;
	}

	public void setGoodsMap(Map<String, Object> goodsMap) {
		this.goodsMap = goodsMap;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getSetUrl() {
		return setUrl;
	}

	public void setSetUrl(String setUrl) {
		this.setUrl = setUrl;
	}

	public String getIPPROXY() {
		return IPPROXY;
	}

	public void setIPPROXY(String iPPROXY) {
		IPPROXY = iPPROXY;
	}

	public String getSum() {
		return Sum;
	}

	public void setSum(String sum) {
		Sum = sum;
	}

	public String getTimeDate() {
		return timeDate;
	}
	public void setTimeDate(String timeDate) {
		this.timeDate = timeDate;
	}

	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getDatabase() {
		return database;
	}
	public void setDatabase(String database) {
		this.database = database;
	}
	public String getStorage() {
		return storage;
	}
	public void setStorage(String storage) {
		this.storage = storage;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public String getStatus_type() {
		return Status_type;
	}
	public void setStatus_type(String status_type) {
		Status_type = status_type;
	}
	public Craw_goods_Info getCrawGoodsiInfo() {
		return crawGoodsiInfo;
	}
	public void setCrawGoodsiInfo(Craw_goods_Info crawGoodsiInfo) {
		this.crawGoodsiInfo = crawGoodsiInfo;
	}
	public String getKeyworId() {
		return keyworId;
	}
	public void setKeyworId(String keyworId) {
		this.keyworId = keyworId;
	}
	public String getDataEgoodsId() {
		return dataEgoodsId;
	}
	public void setDataEgoodsId(String dataEgoodsId) {
		this.dataEgoodsId = dataEgoodsId;
	}


}
