package com.eddc.task;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.eddc.model.CommodityPrices;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.model.Craw_goods_InfoVO;
import com.eddc.model.Craw_goods_Price_Info;
import com.eddc.service.CrawlerPublicClassService;
import com.eddc.service.GuoMei_DataCrawlService;
import com.eddc.service.HttpClientService;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.StringHelper;
import com.eddc.util.publicClass;
import com.enterprise.support.utility.Validation;
@Component
@Scope("prototype")
public class GuoMei_DataCrawlTask  implements  Callable<Map<String, Object>> { 
	Logger logger = Logger.getLogger(GuoMei_DataCrawlTask.class);
	@Autowired
	CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	HttpClientService httpClientService;
	@Autowired
	private GuoMei_DataCrawlService guoMei_DataCrawlService;
	private CommodityPrices commodityPrices;
	private CrawKeywordsInfo crawKeywordsInfo;
	private Map<String,Object>guomeiData;
	private String accountId;
	private String dataType;
	private String setUrl;
	private String IPPROXY;
	private String Sum; 
	private String timeDate;
	private String type;
	private String ip;
	private String database;
	private String storage;
	public Map<String, Object> call() throws Exception {//call解析数据
		Map<String,String>Map=new HashMap<String, String>();
		Map=publicClass.parameterMap(setUrl,accountId,dataType,timeDate,IPPROXY);
		if(Fields.STYPE_1.equals(type)){//解析数据
			logger.info("国美平台开始解析数据"+"-------"+this.Sum+"---------------"+SimpleDate.SimpleDateFormatData().format(new Date()));
			try { 
				String goodsId=StringHelper.encryptByString(crawKeywordsInfo.getCust_keyword_name()+Fields.GUOMEI);//生成goodSId
				Map.put("egoodsId", crawKeywordsInfo.getCust_keyword_name());
				Map.put("keywordId",crawKeywordsInfo.getCust_keyword_id());
				Map.put("goodsId", goodsId);
				Thread.sleep(1000); 
				String StringMessage=httpClientService.interfaceSwitch(ip, Map,database);//请求数据
					//String status =whetherShelves.parseGoodsStatusByItemPage(StringMessage);//判断商品是否下架
				    String status=Fields.STATUS_ON;
					Map<String, Object> sum_status=crawlerPublicClassService.InsertDatStatus(status,database,crawKeywordsInfo,goodsId,timeDate,dataType,setUrl,storage);
					if(Integer.valueOf(sum_status.get("status").toString())==Integer.valueOf(Fields.STATUS_OFF) || Integer.valueOf(sum_status.get("status").toString())==Integer.valueOf(Fields.STATUS_EXCEPTION)){
						guomeiData.putAll(sum_status);
						return guomeiData; 
					}
					Map<String, String> itemFields=guoMei_DataCrawlService.GuoMeiParseItemPage(StringMessage,Map,database);//解析数据
					if(!Validation.isEmpty(itemFields.get(Fields.CURRENTPRICE))){//店铺不能为空
					Map.putAll(itemFields);
					Craw_goods_InfoVO info=crawlerPublicClassService.itemFieldsData(Map,database,storage); //商品详情插入数据库 
					Craw_goods_Price_Info price=crawlerPublicClassService.parseItemPrice(Map,crawKeywordsInfo.getCust_keyword_id(),goodsId,timeDate,dataType,Fields.STATUS_COUNT_1,database,storage);//APP端价格插入数据库
					guomeiData.put(Fields.TABLE_CRAW_GOODS_INFO, info);
					guomeiData.put(Fields.TABLE_CRAW_GOODS_PRICE_INFO, price);
					guomeiData.putAll(Map);
			   }   
			} catch (Exception e) { 
				logger.info("解析国美数据失败-"+SimpleDate.SimpleDateFormatData().format(new Date())+e.toString());
			}
		}else if(Fields.STYPE_6.equals(type)){
			try {
				Map.put("egoodsId", commodityPrices.getEgoodsId());
				Map<String,Object>priceData=guoMei_DataCrawlService.dataCrawGuoMeiPriceP_PC(Map,commodityPrices,ip,database);//PC价格
				guomeiData.putAll(priceData);
			} catch (Exception e) {
				logger.info("解析国美PC价格解析失败------------"+e.toString()+"---------"+SimpleDate.SimpleDateFormatData().format(new Date()));
			}
		}
		return guomeiData;  
	} 
	public CrawKeywordsInfo getCrawKeywordsInfo() {
		return crawKeywordsInfo;
	}
	public void setCrawKeywordsInfo(CrawKeywordsInfo crawKeywordsInfo) {
		this.crawKeywordsInfo = crawKeywordsInfo;
	}

	public String getSum() {
		return Sum;
	}
	
	public void setSum(String sum) {
		Sum = sum;
	}
	
	public String getTimeDate() {
		return timeDate;
	}
	
	public void setTimeDate(String timeDate) {
		this.timeDate = timeDate;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public CrawlerPublicClassService getCrawlerPublicClassService() {
		return crawlerPublicClassService;
	}
	
	public void setCrawlerPublicClassService(
			CrawlerPublicClassService crawlerPublicClassService) {
		this.crawlerPublicClassService = crawlerPublicClassService;
	}
	public String getAccountId() {
		return accountId;
	}
	
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	
	public String getDataType() {
		return dataType;
	}
	
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	
	public String getSetUrl() {
		return setUrl;
	}
	
	public void setSetUrl(String setUrl) {
		this.setUrl = setUrl;
	}
	
	public String getIPPROXY() {
		return IPPROXY;
	}
	
	public void setIPPROXY(String iPPROXY) {
		IPPROXY = iPPROXY;
	}
	public CommodityPrices getCommodityPrices() {
		return commodityPrices;
	}
	public void setCommodityPrices(CommodityPrices commodityPrices) {
		this.commodityPrices = commodityPrices;
	}
	/**
	 * @return the ip
	 */
	public String getIp() {
		return ip;
	}
	/**
	 * @param ip the ip to set
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}
	/**
	 * @return the guomeiData
	 */
	public Map<String, Object> getGuomeiData() {
		return guomeiData;
	}
	/**
	 * @param guomeiData the guomeiData to set
	 */
	public void setGuomeiData(Map<String, Object> guomeiData) {
		this.guomeiData = guomeiData;
	}
	/**
	 * @return the database
	 */
	public String getDatabase() {
		return database;
	}
	/**
	 * @param database the database to set
	 */
	public void setDatabase(String database) {
		this.database = database;
	}
	/**
	 * @return the storage
	 */
	public String getStorage() {
		return storage;
	}
	/**
	 * @param storage the storage to set
	 */
	public void setStorage(String storage) {
		this.storage = storage;
	}
	
}
