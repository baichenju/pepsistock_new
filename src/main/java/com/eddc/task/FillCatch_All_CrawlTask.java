///**   
// * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
// * 
// * @Package: com.eddc.task 
// * @author: jack.zhao   
// * @date: 2018年3月7日 上午10:25:36 
// */
//package com.eddc.task;
//
//import java.sql.SQLException;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.Map;
//
//import org.apache.commons.lang.StringUtils;
//import org.apache.log4j.Logger;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Scope;
//import org.springframework.stereotype.Component;
//
//import com.eddc.model.CommodityPrices;
//import com.eddc.model.CrawKeywordsInfo;
//import com.eddc.service.Amazon_DataCrawlService;
//import com.eddc.service.CrawlerPublicClassService;
//import com.eddc.service.GuoMei_DataCrawlService;
//import com.eddc.service.HttpClientService;
//import com.eddc.service.Jd_DataCrawlService;
//import com.eddc.service.Jumei_DataCrawlService;
//import com.eddc.service.Kaola_DataCrawlService;
//import com.eddc.service.Miya_DataCrawlService;
//import com.eddc.service.Suning_DataCrawlService;
//import com.eddc.service.Tmall_DataCrawlService;
//import com.eddc.service.Vip_DataCrawlService;
//import com.eddc.service.Yhd_DataCrawlService;
//import com.eddc.util.Fields;
//import com.eddc.util.SimpleDate;
//import com.eddc.util.StringHelper;
//import com.eddc.util.WhetherShelves;
//import com.eddc.util.publicClass;
//import com.enterprise.support.utility.Validation;
//
///**   
// *    
// * 项目名称：Price_monitoring_crawler   
// * 类名称：FillCatch_All_CrawlTask   
// * 类描述：   
// * 创建人：jack.zhao   
// * 创建时间：2018年3月7日 上午10:25:36   
// * 修改人：jack.zhao   
// * 修改时间：2018年3月7日 上午10:25:36   
// * 修改备注：   
// * @version    
// *    
// */
//@Component
//@Scope("prototype")
//public class FillCatch_All_CrawlTask extends Thread{
//	Logger logger = Logger.getLogger(GuoMei_DataCrawlTask.class);
//	//private SimpleDateFormat fort=new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
//	@Autowired
//	CrawlerPublicClassService crawlerPublicClassService;
//	@Autowired
//	HttpClientService httpClientService;
//	@Autowired
//	private GuoMei_DataCrawlService guoMei_DataCrawlService;
//	@Autowired
//	private WhetherShelves whetherShelves;
//	@Autowired
//	private Jd_DataCrawlService jd_DataCrawlService;
//	@Autowired
//	private Jumei_DataCrawlService jumei_DataCrawlService;
//	@Autowired
//	private Kaola_DataCrawlService kaola_DataCrawlService;
//	@Autowired
//	private Miya_DataCrawlService miya_DataCrawlService;
//	@Autowired
//	private Suning_DataCrawlService suning_DataCrawlService;
//	@Autowired
//	private Tmall_DataCrawlService tmall_DataCrawlService;
//	@Autowired
//	private Vip_DataCrawlService vip_DataCrawlService;
//	@Autowired
//	private Yhd_DataCrawlService yhd_DataCrawlService;
//	@Autowired
//	private Amazon_DataCrawlService amazon_DataCrawlService;
//	private CommodityPrices commodityPrices;
//	private CrawKeywordsInfo crawKeywordsInfo;
//	private String accountId;
//	private String dataType;
//	private String setUrl;
//	private String IPPROXY;
//	private String Sum; 
//	private String timeDate;
//	private String type;
//	private String cookie;
//	private String code;
//	private String cityCode;
//	private String database;
//	 private String storage;
//	public void run(){
//		Map<String,String>Map=new HashMap<String, String>();
//		String status=Fields.STATUS_ON;
//		String goodsId="";
//		Map=publicClass.parameterMap(setUrl,accountId,dataType,timeDate,IPPROXY);
//		if(!Validation.isEmpty(cookie)){
//			Map.put("cookie", cookie.replaceAll("\"","\'"));
//		} 
//		if(Fields.STYPE_1.equals(type)){//解析数据
//			try {
//				Map.put("egoodsId", crawKeywordsInfo.getCust_keyword_name());
//				Map.put("accountId", accountId);
//				String StringMessage=httpClientService.GoodsProductItem(Map);//抓取Itemyem
//				Thread.sleep(1000);
//				if(dataType.equals(Fields.GUOMEI)){
//					status=Fields.STATUS_ON;
//					goodsId=StringHelper.encryptByString(crawKeywordsInfo.getCust_keyword_name()+Fields.GUOMEI);//生成goodSId
//				}else if(dataType.equals(Fields.PLATFORM_KAOLA)){
//					goodsId=StringHelper.encryptByString(crawKeywordsInfo.getCust_keyword_name()+Fields.PLATFORM_KAOLA);//生成goodSId
//					status=Fields.STATUS_ON;
//				}else if(dataType.equals(Fields.PLATFORM_SUNING)){
//					goodsId=StringHelper.encryptByString(crawKeywordsInfo.getCust_keyword_name()+Fields.PLATFORM_SUNING);//生成goodSId
//					status =whetherShelves.getGoodsStatus(StringMessage);//判断商品是否下架
//				}else if(dataType.equals(Fields.PLATFORM_VIP)){
//					goodsId=StringHelper.encryptByString(crawKeywordsInfo.getCust_keyword_name()+Fields.PLATFORM_VIP);//生成goodSId
//					status =whetherShelves.parseGoodsStatusVip(StringMessage);//判断商品是否下架
//				}else if(dataType.equals(Fields.PLATFORM_YHD)){
//					goodsId=StringHelper.encryptByString(crawKeywordsInfo.getCust_keyword_name()+Fields.PLATFORM_YHD);//生成goodSId
//					status =whetherShelves.parseGoodsStatusByItemPage(StringMessage);//判断商品是否下架
//				}else if(dataType.equals(Fields.PLATFORM_TAOBAO_EN)){
//					goodsId=StringHelper.encryptByString(crawKeywordsInfo.getCust_keyword_name()+Fields.PLATFORM_TAOBAO_EN);//生成goodSId
//					status =whetherShelves.parseGoodsStatusByItemPage(StringMessage);//判断商品是否下架
//				}else if(dataType.equals(Fields.PLATFORM_TMALL_EN)){
//					goodsId=StringHelper.encryptByString(crawKeywordsInfo.getCust_keyword_name()+Fields.PLATFORM_TMALL_EN);//生成goodSId
//					status =whetherShelves.parseGoodsStatusByItemPage(StringMessage);//判断商品是否下架
//				}else if(dataType.equals(Fields.PLATFORM_MiA_EN)|| dataType.equals(Fields.PLATFORM_MiA_GLOBAL_EN)){
//					goodsId=StringHelper.encryptByString(crawKeywordsInfo.getCust_keyword_name()+Fields.PLATFORM_MiA_EN);//生成goodSId
//					status =whetherShelves.parseGoodsStatusMia(StringMessage);//判断商品是否下架
//				}else if(dataType.equals(Fields.PLATFORM_JD)){
//					goodsId=StringHelper.encryptByString(crawKeywordsInfo.getCust_keyword_name()+Fields.PLATFORM_JD);//生成goodSId
//					status =whetherShelves.parseGoodsStatusByItemPage(StringMessage);//判断商品是否下架
//				}else if(dataType.equals(Fields.PLATFORM_JUMEI) || dataType.equals(Fields.PLATFORM_JUMEI_GLOBAL_EN)){
//					goodsId=StringHelper.encryptByString(crawKeywordsInfo.getCust_keyword_name()+Fields.PLATFORM_JUMEI);//生成goodSId
//					status =whetherShelves.parseGoodsStatusJumei(StringMessage,crawKeywordsInfo.getCust_keyword_name());//判断商品是否下架
//				}else if(dataType.equals(Fields.PLATFORM_AMAZON) ){
//					goodsId=StringHelper.encryptByString(crawKeywordsInfo.getCust_keyword_name()+Fields.PLATFORM_AMAZON);//生成goodSId
//					status =whetherShelves.parseGoodsStatusByItemPage(StringMessage);//判断商品是否下架
//				}
//				java.util.Map<String, Object> sum_status=crawlerPublicClassService.InsertDatStatus(status,database,crawKeywordsInfo,goodsId,timeDate,dataType,setUrl,storage);
//				if(Integer.valueOf(sum_status.get("status").toString())==Integer.valueOf(Fields.STATUS_OFF) || Integer.valueOf(sum_status.get("status").toString())==Integer.valueOf(Fields.STATUS_EXCEPTION)){
//					return; 
//				}
//				//Map.putAll(sum_status);
//				if(dataType.equals(Fields.GUOMEI)){
//					Map<String, String> itemFields=guoMei_DataCrawlService.GuoMeiParseItemPage(StringMessage,Map);//解析数据
//					if(!Validation.isEmpty(itemFields.get(Fields.CURRENTPRICE))){//店铺不能为空
//						Map.putAll(itemFields);
//						crawlerPublicClassService.itemFieldsData(Map); //商品详情插入数据库 
//						crawlerPublicClassService.parseItemPrice(Map,crawKeywordsInfo.getCust_keyword_id(),goodsId,timeDate,dataType,Fields.STATUS_COUNT_1);//APP端价格插入数据库		
//					}
//				}else if(dataType.equals(Fields.PLATFORM_JD)){
//					Map<String, String> itemFields = null;
//					System.out.println(StringMessage);
//					if(StringUtils.isNotEmpty(cityCode)){
//						itemFields =jd_DataCrawlService.JDparseItemPageByCity(Map,cityCode,StringMessage);//解析数据
//					}else{
//						//itemFields =jd_DataCrawlService.JDparseItemPage(StringMessage,Map);//解析数据
//					}
//					Map.putAll(itemFields);
//					crawlerPublicClassService.itemFieldsData(Map); //商品详情插入数据库 
//					crawlerPublicClassService.parseItemPrice(Map,crawKeywordsInfo.getCust_keyword_id(),goodsId,timeDate,dataType,Fields.STATUS_COUNT_1);//APP端价格插入数据库		
//				}else if(dataType.equals(Fields.PLATFORM_JUMEI) || dataType.equals(Fields.PLATFORM_JUMEI_GLOBAL_EN)){
//					jumei_DataCrawlService.JumeiParseItemPage(StringMessage,crawKeywordsInfo,Map,"1");//解析数据	
//				}else if(dataType.equals(Fields.PLATFORM_KAOLA)){
//					kaola_DataCrawlService.KaolaParseItemPage(StringMessage,crawKeywordsInfo,Map);//解析数据
//				}else if(dataType.equals(Fields.PLATFORM_MiA_EN)|| dataType.equals(Fields.PLATFORM_MiA_GLOBAL_EN)){
//					miya_DataCrawlService.MiyaParseItemPage(StringMessage,crawKeywordsInfo,Map);//解析数据
//				}else if(dataType.equals(Fields.PLATFORM_SUNING)){
//					Map<String, String> itemFields =suning_DataCrawlService.SuningParseItemPage(StringMessage,Map);//解析数据
//					Map.putAll(itemFields);
//				}else if(dataType.equals(Fields.PLATFORM_TMALL_EN) || dataType.equals(Fields.PLATFORM_TAOBAO_EN)){
//					Map<String, String> itemFieldsTmall=tmall_DataCrawlService.TmallparseItemH5Message(StringMessage,goodsId,timeDate,crawKeywordsInfo.getCust_keyword_id(),code,dataType);
//					itemFieldsTmall.putAll(Map); 
//					itemFieldsTmall.put("message", null);
//					itemFieldsTmall.put("goodsUrl", Fields.TAOBAO_URL+crawKeywordsInfo.getCust_keyword_name());
//					crawlerPublicClassService.itemFieldsData(itemFieldsTmall); 
//				}else if(dataType.equals(Fields.PLATFORM_VIP)){
//					vip_DataCrawlService.vipParseItemPage(StringMessage,crawKeywordsInfo,Map);//解析数据
//				}else if(dataType.equals(Fields.PLATFORM_YHD)){
//					Map<String, String> itemFields=yhd_DataCrawlService.YhdParseItemPage(StringMessage,Map);//解析数据
//					if(!Validation.isEmpty(itemFields.get(Fields.SHOPNAME))){//店铺不能为空
//					Map.putAll(itemFields);
//					crawlerPublicClassService.itemFieldsData(Map); //商品详情插入数据库 
//					crawlerPublicClassService.parseItemPrice(Map,crawKeywordsInfo.getCust_keyword_id(),goodsId,timeDate,dataType,Fields.STATUS_COUNT_1);//APP端价格插入数据库	
//					}
//				}else if(dataType.equals(Fields.PLATFORM_AMAZON)){
//					//amazon_DataCrawlService.amazonParseItemPage(StringMessage,Map,storage);//解析数据
//				}
//			} catch (Exception e) {
//				e.printStackTrace();
//
//			}
//		}else if(Fields.STYPE_6.equals(type)){
//			Map.put("egoodsId", commodityPrices.getEgoodsId());
//           if(dataType.equals(Fields.PLATFORM_YHD)){   
//				try {
//					yhd_DataCrawlService.dataCrawYHDPriceP_PC(Map, commodityPrices);
//				} catch (SQLException e) {
//					logger.info("解析"+dataType+"PC价格解析失败------------"+e.toString()+"---------"+SimpleDate.SimpleDateFormatData().format(new Date()));
//				}
//           }else if(dataType.equals(Fields.GUOMEI)){
//        	   try {
//        		   guoMei_DataCrawlService.dataCrawGuoMeiPriceP_PC(Map, commodityPrices,Fields.COUNT_01);//PC价格
//        	   } catch (Exception e) {
//        		   logger.info("解析国美PC价格解析失败------------"+e.toString()+"---------"+SimpleDate.SimpleDateFormatData().format(new Date()));
//        	   }
//           }else if(dataType.equals(Fields.PLATFORM_JD)){
//        	   try {
//   				jd_DataCrawlService.JDparseItemPagePrice(commodityPrices,accountId,IPPROXY);//PC端价格
//   			} catch (Exception e) {
//   				logger.info("PC价格解析失败------------"+e.toString()+"---------"+SimpleDate.SimpleDateFormatData().format(new Date()));
//   			}
//           }
//           }else if(dataType.equals(Fields.PLATFORM_VIP)){
//        	   try {
//        		   Thread.sleep(1000);
//        		   Map.put("cookie","wap_consumer=A1; WAP[p_area]=%25E4%25B8%258A%25E6%25B5%25B7; m_vip_province=103101; vip_first_visitor=1; vip_ipver=31; _smt_uid=5a17acb2.2f6af19c; vipte_viewed_vip_ht_sh=317821761; vipte_viewed_vip_nh=335841733; WAP[p_wh]=VIP_SH; warehouse=VIP_SH; vipte_viewed_vip_sh=211587640%2C335411897%2C335411900%2C335411909%2C335411921; vipAc=8f752d3cd4a214b99ea4efcec21fca4e; VipDFT=0; user_class=a; VipUINFO=luc%3Aa%7Csuc%3Aa%7Cbct%3Ac_new%7Chct%3Ac_new%7Cbdts%3A0%7Cbcts%3A0%7Ckfts%3A0%7Cc10%3A0%7Crcabt%3A0%7Cp2%3A0%7Cp3%3A1%7Cp4%3A0%7Cp5%3A0; vip_province=103101; vip_wh=VIP_SH; vipte_viewed_vip_ht_hz=270863159%2C270863155; mars_pid=208; mars_cid=1511500544993_51d12e9779dbe3c1d66affeac21fa7e3; mars_sid=e908667511390b422556a01b9d389436; visit_id=288EB5B62257C6981E6160FFADF85493; _jzqco=%7C%7C%7C%7C%7C1.1321206542.1511500977804.1511511961487.1511512321081.1511511961487.1511512321081..0.0.36.36");
//        		   Map.put("egoodsId", commodityPrices.getEgoodsId());
//        		   String AppMessage;
//        		   try {
//        			   AppMessage = httpClientService.GoodsProductDetailsData(Map);
//        			   commodityPrices.setBatch_time(timeDate);//重新赋值
//        			   vip_DataCrawlService.ResolutionCellPrices(commodityPrices, AppMessage);//解析数据 
//        		   } catch (Exception e) {
//        			   logger.info("请求唯品会PC数据失败-------------"+e.getMessage()+"------------"+SimpleDate.SimpleDateFormatData().format(new Date()));
//        		   }
//        	   } catch (InterruptedException e) {
//        		   logger.info("请求唯品会PC数据失败-------------"+e.getMessage()+"------------"+SimpleDate.SimpleDateFormatData().format(new Date()));
//        	   }
//           }else if(dataType.equals(Fields.PLATFORM_SUNING)){
//        	   try {
//        		   suning_DataCrawlService.suning_DataCrawlPrice_PC(Map,commodityPrices);//PC端价格
//        	   } catch (Exception e) {
//        		   logger.info("解析苏宁PC价格解析失败------------"+e.toString()+"---------"+SimpleDate.SimpleDateFormatData().format(new Date()));
//        	   }
//           }else if(dataType.equals(Fields.PLATFORM_KAOLA)){
//        	   try {
//        		   Thread.sleep(1000); 
//        		   Map.put("cookie","davisit=9; __kaola_usertrack=20171128172240703361; _da_ntes_uid=20171128172240703361; NTES_KAOLA_ADDRESS_CONTROL=310000|310100|310101|1; _ntes_nnid=02fbc6af1afb2bd4e7ad7af8d8592474,1511860960407; JSESSIONID-WKL-8IO=QrTcn3rcDBNn47hmdQV%2Bd6n3hpoB1U%5Cjm%5Cs02yrnj1ZQpP%2F4JhCtn7Le3nAM%2BatReu3XN1mC6RSr%2Fy25yIIZOwOQgxIOiy7pjxXax16yP7w9exc6L%5CG67gZ8B7P9lY0ZtbTZZy5ci1ea7AOyZlizWQJWnN%2BziOHLKgS3iUucSAuLUm1z%3A1511947362187; _klhtxd_=31; __sonar=18195530199085568723; usertrack=O2+1I1oeIQw9EpDEAwO5Ag==; _jzqckmp=1; HTONLINE=74ba3546e508e1e96c55fe2931879c1cf087e614; NTESwebSI=CBBA1A5DB3DAD8C19A1905D78C0EEC04.hzabj-kaola-web28.server.163.org-8081; beta_trafic=0; davisit=1; __da_ntes_utmfc=utmcsr%3D(direct)%7Cutmccn%3D(direct)%7Cutmcmd%3D(none); _adwc=55934509; _adwp=55934509.2726495513.1511923976.1511923976.1511923976.1; _adwr=55934509%230; __ag_cm_=1; NTES_KAOLA_RV=1823181_1511923989256_0; _adwb=55934509; ag_fid=4rU9f4k8eTf75fZF; __xsptplus421=421.1.1511923979.1511923991.2%234%7C%7C%7C%7C%7C%23%23NZbyOM0SppCdio_I_9Hwq3y3BkXyw_82%23; _jzqa=1.2495650379652093400.1511923976.1511923976.1511923976.1; _jzqc=1; _qzja=1.1730353322.1511923975604.1511923975604.1511923975605.1511923988302.1511924874926..0.0.3.1; _qzjb=1.1511923975605.3.0.0.0; _qzjc=1; _qzjto=3.1.0; _ga=GA1.2.1426520452.1511860961; _gid=GA1.2.1754969022.1511860961; _gat=1; __da_ntes_utma=2525167.1044588744.1511923975.1511923975.1511923975.1; __da_ntes_utmb=2525167.9.10.1511923975; __da_ntes_utmz=2525167.1511923975.1.1.utmcsr%3D(direct)%7Cutmccn%3D(direct)%7Cutmcmd%3D(none); _jzqb=1.6.10.1511923976.1; current_env=online");
//        		   Map.put("egoodsId", commodityPrices.getEgoodsId());
//        		   String PC_price_Message =httpClientService.GoodsProductDetailsData(Map);//Fields.CLIENT_MOBILE
//        		   if(Validation.isEmpty(PC_price_Message)){
//        			   PC_price_Message =httpClientService.requestData(Map);//Fields.CLIENT_MOBILE
//        		   }
//        		   commodityPrices.setBatch_time(timeDate);//重新赋值
//        		   kaola_DataCrawlService.ResolutionCellPhonePrices(commodityPrices, PC_price_Message);//解析数据
//        	   } catch (InterruptedException e) {
//        		   logger.info("请求考拉PC数据失败-------------"+e.getMessage()+"------------"+SimpleDate.SimpleDateFormatData().format(new Date()));
//        	   } catch (Exception e) {
//        		   logger.info("请求考拉PC数据失败-------------"+e.getMessage()+"------------"+SimpleDate.SimpleDateFormatData().format(new Date()));
//        	   } 
//           }else if(dataType.equals(Fields.PLATFORM_MiA_EN)|| dataType.equals(Fields.PLATFORM_MiA_GLOBAL_EN)){
//        	   try {
//   				Thread.sleep(1000);
//   				String AppMessage =httpClientService.GoodsProductDetailsData(Map);//Fields.CLIENT_MOBILE
//   				commodityPrices.setBatch_time(timeDate);//重新赋值
//   				miya_DataCrawlService.ResolutionCellPhonePrices(commodityPrices, AppMessage,Map);//解析数据
//   			} catch (InterruptedException e) {
//   				  logger.info("请求蜜芽PC数据失败-------------"+e.getMessage()+"------------"+SimpleDate.SimpleDateFormatData().format(new Date()));
//   			} catch (Exception e) {
//   				  logger.info("请求蜜芽PC数据失败-------------"+e.getMessage()+"------------"+SimpleDate.SimpleDateFormatData().format(new Date()));
//   			}  
//           }	
//	}
//
//	public GuoMei_DataCrawlService getGuoMei_DataCrawlService() {
//		return guoMei_DataCrawlService;
//	}
//	public void setGuoMei_DataCrawlService(GuoMei_DataCrawlService guoMei_DataCrawlService) {
//		this.guoMei_DataCrawlService = guoMei_DataCrawlService;
//	}
//
//	public CommodityPrices getCommodityPrices() {
//		return commodityPrices;
//	}
//
//	public void setCommodityPrices(CommodityPrices commodityPrices) {
//		this.commodityPrices = commodityPrices;
//	}
//	public CrawKeywordsInfo getCrawKeywordsInfo() {
//		return crawKeywordsInfo;
//	}
//	public void setCrawKeywordsInfo(CrawKeywordsInfo crawKeywordsInfo) {
//		this.crawKeywordsInfo = crawKeywordsInfo;
//	}
//
//	public String getAccountId() {
//		return accountId;
//	}
//	public void setAccountId(String accountId) {
//		this.accountId = accountId;
//	}
//	public String getDataType() {
//		return dataType;
//	}
//	public void setDataType(String dataType) {
//		this.dataType = dataType;
//	}
//	public String getSetUrl() {
//		return setUrl;
//	}
//	public void setSetUrl(String setUrl) {
//		this.setUrl = setUrl;
//	}
//	public String getIPPROXY() {
//		return IPPROXY;
//	}
//	public void setIPPROXY(String iPPROXY) {
//		IPPROXY = iPPROXY;
//	}
//	public String getSum() {
//		return Sum;
//	}
//	public void setSum(String sum) {
//		Sum = sum;
//	}
//	public String getTimeDate() {
//		return timeDate;
//	}
//	public void setTimeDate(String timeDate) {
//		this.timeDate = timeDate;
//	}
//	public String getType() {
//		return type;
//	}
//	public void setType(String type) {
//		this.type = type;
//	}
//	public String getCookie() {
//		return cookie;
//	}
//	public void setCookie(String cookie) {
//		this.cookie = cookie;
//	}
//	public String getCode() {
//		return code;
//	}
//	public void setCode(String code) {
//		this.code = code;
//	}
//	public String getDatabase() {
//		return database;
//	}
//	public void setDatabase(String database) {
//		this.database = database;
//	}
//
//	/**
//	 * @return the storage
//	 */
//	public String getStorage() {
//		return storage;
//	}
//
//	/**
//	 * @param storage the storage to set
//	 */
//	public void setStorage(String storage) {
//		this.storage = storage;
//	}
//	
//}
