package com.eddc.task;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.alibaba.druid.util.StringUtils;
import com.eddc.model.CommodityPrices;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.model.Craw_goods_InfoVO;
import com.eddc.model.Craw_goods_Price_Info;
import com.eddc.service.CrawlerPublicClassService;
import com.eddc.service.HttpClientService;
import com.eddc.service.Suning_DataCrawlService;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.StringHelper;
import com.eddc.util.WhetherShelves;
import com.eddc.util.publicClass;
import com.github.pagehelper.util.StringUtil;
@Component
@Scope("prototype")
public class Suning_DataCrawlTask  implements Callable<Map<String, Object>> {
	Logger logger = Logger.getLogger(Suning_DataCrawlTask.class);
	@Autowired
	CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	HttpClientService httpClientService;
	@Autowired
	private Suning_DataCrawlService suning_DataCrawlService;
	@Autowired
	private WhetherShelves whetherShelves;
	private CommodityPrices commodityPrices;
	private CrawKeywordsInfo crawKeywordsInfo;
	private Map<String,Object>suningData;
	private String accountId;
	private String dataType;
	private String setUrl;
	private String IPPROXY;
	private String Sum;
	private String timeDate;
	private String codeCookice;
	private String type;
	private String ip;
	private String database;
	private String storage;
	@SuppressWarnings("static-access")
	public Map<String, Object> call() throws Exception {//call解析数据 
		Map<String,String>Map=new HashMap<String, String>();
		Map=publicClass.parameterMap(setUrl,accountId,dataType,timeDate,IPPROXY);
		Map.put("ip",ip);  
		if(Fields.STYPE_1.equals(type)){//解析数据
			logger.info("苏宁平台开始解析数据"+"-------"+this.Sum+"---------------"+SimpleDate.SimpleDateFormatData().format(new Date()));
			try { 
				String goodsId=StringHelper.encryptByString(crawKeywordsInfo.getCust_keyword_name()+Fields.PLATFORM_SUNING);//生成goodSId
				Map.put("egoodsId", crawKeywordsInfo.getCust_keyword_name());
				Map.put("keywordId",crawKeywordsInfo.getCust_keyword_id());
				Map.put("cookie",codeCookice); 
				Map.put("goodsId", goodsId);
				String StringMessage=httpClientService.interfaceSwitch(ip, Map,database);//请求数据
				String status =whetherShelves.getGoodsStatus(StringMessage);//判断商品是否下架
				
				Map<String, Object> sum_status=crawlerPublicClassService.InsertDatStatus(status,database,crawKeywordsInfo,goodsId,timeDate,dataType,setUrl,storage);
				if(Integer.valueOf(sum_status.get("status").toString())==Integer.valueOf(Fields.STATUS_OFF) || Integer.valueOf(sum_status.get("status").toString())==Integer.valueOf(Fields.STATUS_EXCEPTION)){
					suningData.putAll(sum_status);
					return suningData; 
				}
				Map<String, String> itemFields=suning_DataCrawlService.SuningParseItemPage(StringMessage,Map,database);//解析数据
				//开始插入商品详情
				Map.putAll(itemFields);
				if(StringUtil.isNotEmpty(Map.get("currentPrice"))){
					Craw_goods_InfoVO info=crawlerPublicClassService.itemFieldsData(Map,database,storage); //商品详情 
					Craw_goods_Price_Info price=crawlerPublicClassService.parseItemPrice(Map,crawKeywordsInfo.getCust_keyword_id(),goodsId,timeDate,dataType,Fields.STATUS_COUNT_1,database,storage);//手机端价格
					suningData.put(Fields.TABLE_CRAW_GOODS_INFO, info);
					suningData.put(Fields.TABLE_CRAW_GOODS_PRICE_INFO, price);
					suningData.putAll(Map);
				}
			} catch (Exception e) { 
				logger.info("解析苏宁数据失败-"+SimpleDate.SimpleDateFormatData().format(new Date())+e.toString());
			}
		}else if(Fields.STYPE_6.equals(type)){
			try {
				Map.put("egoodsId", commodityPrices.getEgoodsId());
				Map<String, Object> suningPrice=suning_DataCrawlService.suning_DataCrawlPrice_PC(Map,commodityPrices,database);//PC端价格
				if(suningPrice.toString().contains("currentPrice")){
					suningData.putAll(suningPrice);
				}
			} catch (Exception e) {
				logger.info("解析苏宁PC价格解析失败------------"+e.toString()+"---------"+SimpleDate.SimpleDateFormatData().format(new Date()));
			}
		}
		return suningData; 
	}
	public CrawKeywordsInfo getCrawKeywordsInfo() {
		return crawKeywordsInfo;
	}
	public void setCrawKeywordsInfo(CrawKeywordsInfo crawKeywordsInfo) {
		this.crawKeywordsInfo = crawKeywordsInfo;
	}

	public String getSum() {
		return Sum;
	}

	public void setSum(String sum) {
		Sum = sum;
	}

	public String getTimeDate() {
		return timeDate;
	}

	public void setTimeDate(String timeDate) {
		this.timeDate = timeDate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public CrawlerPublicClassService getCrawlerPublicClassService() {
		return crawlerPublicClassService;
	}

	public void setCrawlerPublicClassService(
			CrawlerPublicClassService crawlerPublicClassService) {
		this.crawlerPublicClassService = crawlerPublicClassService;
	}
	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getSetUrl() {
		return setUrl;
	}

	public void setSetUrl(String setUrl) {
		this.setUrl = setUrl;
	}

	public String getIPPROXY() {
		return IPPROXY;
	}

	public void setIPPROXY(String iPPROXY) {
		IPPROXY = iPPROXY;
	}
	public CommodityPrices getCommodityPrices() {
		return commodityPrices;
	}
	public void setCommodityPrices(CommodityPrices commodityPrices) {
		this.commodityPrices = commodityPrices;
	}
	public String getCodeCookice() {
		return codeCookice;
	}
	public void setCodeCookice(String codeCookice) {
		this.codeCookice = codeCookice;
	}
	/**
	 * @return the ip
	 */
	public String getIp() {
		return ip;
	}
	/**
	 * @param ip the ip to set
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}
	/**
	 * @return the suningData
	 */
	public Map<String, Object> getSuningData() {
		return suningData;
	}
	/**
	 * @param suningData the suningData to set
	 */
	public void setSuningData(Map<String, Object> suningData) {
		this.suningData = suningData;
	}
	/**
	 * @return the database
	 */
	public String getDatabase() {
		return database;
	}
	/**
	 * @param database the database to set
	 */
	public void setDatabase(String database) {
		this.database = database;
	}
	/**
	 * @return the storage
	 */
	public String getStorage() {
		return storage;
	}
	/**
	 * @param storage the storage to set
	 */
	public void setStorage(String storage) {
		this.storage = storage;
	}

}
