package com.eddc.task;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.service.CrawlerPublicClassService;
import com.eddc.service.HttpClientService;
import com.eddc.service.Search_DataCrawlService;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.publicClass;
import com.github.pagehelper.util.StringUtil;

@Component
@Scope("prototype")
public class Keywords_DataCrawlTask  extends Thread{
	Logger log = Logger.getLogger(Keywords_DataCrawlTask.class);
	@Autowired
	private CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	private HttpClientService httpClientService;
	@Autowired
	private Search_DataCrawlService pepsi_DataCrawlService;
	private CrawKeywordsInfo crawKeywordsInfo;
	private String accountId;
	private String dataType;
	private String setUrl;
	private String IPPROXY;
	private String timeDate;
	private String type;
	private String codeCookie;
	private int status;
	private String status_type;
	private String  ip;
	private String database;
	private String search_price;
	private String oriUrl="";
	private String ref="";
	public void run() {
		Map<String,String>Map=new HashMap<String, String>();
		String StringMessage="";
		Map=publicClass.parameterMap(setUrl,accountId,dataType,timeDate,IPPROXY);
		Map.put("ip",ip);
		Map.put("status", String.valueOf(status));
		Map.put("status_type",status_type);
		Map.put("database", database);
		Map.put("accountId", accountId);
		Map.put("platform", dataType);
		Map.put(Fields.IPPROXY, IPPROXY);
		Map.put("accountId", accountId);
		Map.put("egoodsId", crawKeywordsInfo.getCust_keyword_name());
		Map.put("cust_keyword_id", crawKeywordsInfo.getCust_keyword_id());
		Map.put("search_price", search_price);
			try { 
				for(int i=1;i<=crawKeywordsInfo.getSearch_page_nums();i++){//天猫
					if(crawKeywordsInfo.getPlatform_name().equals(Fields.PLATFORM_TMALL_EN)||crawKeywordsInfo.getPlatform_name().equals(Fields.PLATFORM_TMALL_GLOBAL_EN)||crawKeywordsInfo.getPlatform_name().equals(Fields.PLATFORM_TAOBAO_EN)){
						Map<String,String>tmall=tmallData(Map,crawKeywordsInfo,i);
						Map.put("status", Fields.STATUS_ON);
				 		Map.putAll(tmall);
						
					}else if(crawKeywordsInfo.getPlatform_name().equals(Fields.PLATFORM_JD)){//京东
						Map<String,String>jd=jdData(Map,crawKeywordsInfo,i);
						Map.put("status", Fields.STATUS_ON);
						Map.putAll(jd);
					} 
					Thread.sleep(2000);
					if(Map.get("status").contains(Fields.STATUS_ON)){
						Map.put("count",String.valueOf(i));
						try {
							StringMessage=httpClientService.interfaceSwitch(ip, Map,database);//请求数据
							if(StringUtil.isEmpty(StringMessage)){
								StringMessage=httpClientService.GoodsProduct(Map,database);//抓取Itemyem
								if(StringUtil.isEmpty(StringMessage)){
									StringMessage=httpClientService.getJsoupResultMessage(Map);	
								}
								if(StringUtil.isEmpty(StringMessage)){
									StringMessage=httpClientService.GoodsProduct(Map,database);	
								}
							}
						} catch (Exception e) {
							StringMessage=httpClientService.GoodsProduct(Map,database);//抓取Itemyem
						}
						String disable =pepsi_DataCrawlService.ParseItemPage(StringMessage,crawKeywordsInfo,Map);//解析数据
						if(disable.contains(Fields.DISABLE)){
							return;  
						}
					}
				}
			} catch (Exception e) {
				log.info("抓取搜索页面信息失败-------"+SimpleDate.SimpleDateFormatData().format(new Date())+e.toString());
				e.printStackTrace(); 
			}
		}
	public  Map<String,String>tmallData(Map<String,String>Map,CrawKeywordsInfo crawKeywordsInfo, int i) throws UnsupportedEncodingException{
	    String price[]=Map.get("search_price").toString().split(",");
		if(crawKeywordsInfo.getCust_keyword_url().contains("search_radio_tmall")||crawKeywordsInfo.getCust_keyword_url().contains("filter_tianmao=tmall")||crawKeywordsInfo.getCust_keyword_url().contains("search_radio_all")){
			String keyWord=URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");
			if(i==1){
				setUrl=crawKeywordsInfo.getCust_keyword_url().replace("SEARCH", keyWord).replace("PAGE",String.valueOf(i))+"&filter=reserve_price%5B"+price[0]+"%2C"+price[1]+"%5D";
			}else{
				setUrl=crawKeywordsInfo.getCust_keyword_url().replace("SEARCH", keyWord).replace("PAGE",String.valueOf(i*44))+"&filter=reserve_price%5B"+price[0]+"%2C"+price[1]+"%5D";
			}
			Map.put("url", setUrl);
		}
		return Map;
	}
	//京东
	public  Map<String,String>jdData(Map<String,String>Map,CrawKeywordsInfo crawKeywordsInfo, int i)
			throws UnsupportedEncodingException{
		int mod =i%2;
		if(crawKeywordsInfo.getCust_keyword_url().contains("search.jd.com")){//京东搜索PC
			String keyWord=URLEncoder.encode(crawKeywordsInfo.getCust_keyword_name(), "UTF-8");
			if(mod==0){
				oriUrl=Fields.JD_SEARCH_URL.replace("KEYWORD", keyWord).replace("PAGE",String.valueOf(i)).replace("SUM",String.valueOf(((i-1)*26)))+"&scrolling=y&log_id=1524643976.14897&tpl=3_M&ev=exprice_"+Map.get("search_price").toString()+"%5E";
			}else {
				oriUrl=Fields.JD_SEARCH_URL.replace("KEYWORD", keyWord).replace("PAGE",String.valueOf(i)).replace("SUM",String.valueOf(((i-1)*26)))+"&click=2&ev=exprice_"+Map.get("search_price").toString()+"%5E";
				ref=Fields.JD_SEARCH_REF_URL+keyWord+"&enc=utf-8&qrst=1&rt=1&stop=1&vt=2&stock=1&page="+i+"&s="+((i-1)*26)+"&click=2&qrst=unchange&ev=exprice_"+Map.get("search_price").toString()+"%5E";
			}  
			Map.put("url", oriUrl);
			Map.put("urlRef", ref);
		}
		return Map; 
	}
	public CrawlerPublicClassService getCrawlerPublicClassService() {
		return crawlerPublicClassService;
	}
	public void setCrawlerPublicClassService(
			CrawlerPublicClassService crawlerPublicClassService) {
		this.crawlerPublicClassService = crawlerPublicClassService;
	}
	public HttpClientService getHttpClientService() {
		return httpClientService;
	}
	public void setHttpClientService(HttpClientService httpClientService) {
		this.httpClientService = httpClientService;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getDataType() {
		return dataType;
	}
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	public String getSetUrl() {
		return setUrl;
	}
	public void setSetUrl(String setUrl) {
		this.setUrl = setUrl;
	}
	public String getIPPROXY() {
		return IPPROXY;
	}
	public void setIPPROXY(String iPPROXY) {
		IPPROXY = iPPROXY;
	}
	public CrawKeywordsInfo getCrawKeywordsInfo() {
		return crawKeywordsInfo;
	}
	public void setCrawKeywordsInfo(CrawKeywordsInfo crawKeywordsInfo) {
		this.crawKeywordsInfo = crawKeywordsInfo;
	}
	public String getTimeDate() {
		return timeDate;
	}
	public void setTimeDate(String timeDate) {
		this.timeDate = timeDate;
	}
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCodeCookie() {
		return codeCookie;
	}
	public void setCodeCookie(String codeCookie) {
		this.codeCookie = codeCookie;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getStatus_type() {
		return status_type;
	}
	public void setStatus_type(String status_type) {
		this.status_type = status_type;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getDatabase() {
		return database;
	}
	public void setDatabase(String database) {
		this.database = database;
	}
	public String getSearch_price() {
		return search_price;
	}
	public void setSearch_price(String search_price) {
		this.search_price = search_price;
	}

} 
