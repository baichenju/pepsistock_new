package com.eddc.service;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.eddc.model.Craw_goods_Vendor_Info;
import com.eddc.model.Craw_goods_Vendor_InfoVO;
import com.eddc.model.Craw_goods_Vendor_price_Info;
import com.eddc.util.BatchInsertData;
import com.eddc.util.BeanMapUtil;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.StringHelper;
import com.eddc.util.Validation;
import com.github.pagehelper.util.StringUtil;
import com.google.common.collect.Lists;
@Service
public class Alibaba_DataCrawlService {
	private static Logger logger=Logger.getLogger(Alibaba_DataCrawlService.class);
	@Autowired
	HttpClientService httpClientService;
	@Autowired
	CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	SqlSessionTemplate sqlSessionTemplate;
	@Autowired
	BatchInsertData batchInsertData;
	@SuppressWarnings("unchecked")
	public Map<String, Object> alibabaParseItemPage(String itemPage,Map<String,String>mapType,String database,String storage) throws Exception{
		Map<String,Object>messageData=new HashMap<String,Object>();
		Map<String,Object> insertItem=new HashMap<String,Object>();
		List<Map<String,Object>> insertItems = Lists.newArrayList();
		Map<String,Object> insertItemPrice=new HashMap<String,Object>();
		List<Map<String,Object>> insertprices = Lists.newArrayList();
		List<Craw_goods_Vendor_price_Info> list = new ArrayList<Craw_goods_Vendor_price_Info>();
		StringBuffer property=new  StringBuffer();
		String platform_goods_detail="";
		String platform_vendorname="";String purchase_unit="";String goods_description_rate=""; String service_attitude_rate="";
		String delivery_speed_rate="";String purchase_back_rate="";String purchase_qty="";String purchase_comment_num="";String image="";
		String goodsId=StringHelper.encryptByString( mapType.get("egoodsId")+Fields.platform_1688);//生成goodSId
		messageData.put("batch_time", mapType.get("timeDate"));
		messageData.put("channel", Fields.CLIENT_PC);
		messageData.put("goodsId", goodsId);
		messageData.put("accountId", mapType.get("accountId"));
		messageData.put("egoodsId",  mapType.get("egoodsId"));
		messageData.put("keywordId",mapType.get("keywordId"));
		Document docss =Jsoup.parse(itemPage.toString());
		if(StringUtil.isNotEmpty(docss.getElementsByClass("d-title").text())){
			String goodsName=docss.getElementsByClass("d-title").text();//商品名称
			String promotion=docss.getElementsByClass("d-content static-content detail-vip-login-content").text().replace("|","");
			String platform_vendoraddress=docss.getElementsByClass("delivery-addr").text();//物流
			if(docss.toString().contains("vertical-img")){
				image=docss.getElementsByClass("vertical-img").get(0).getElementsByTag("img").attr("src").toString();//商品图片
			}
			if(docss.toString().contains("bargain-number")){
				purchase_qty=docss.getElementsByClass("bargain-number").get(0).getElementsByTag("em").text();//成交件数	
			}
			if(docss.toString().contains("satisfaction-number")){
				purchase_comment_num=docss.getElementsByClass("satisfaction-number").get(0).getElementsByTag("em").text();//评价
				if(purchase_comment_num.contains(Fields.MYRIAD)){
					purchase_comment_num=purchase_comment_num.replace(Fields.MYRIAD,"").trim();
					purchase_comment_num=String.valueOf((Integer.valueOf(purchase_comment_num.toString())*10000));
				}
			}
			String platform_goods_satisfaction=StringHelper.nullToString(StringHelper.getResultByReg(docss.toString(), "property=\"og:product:score\" content=\"([^<>\"]+)"));//商品满意度     
			String platform_vendortype=docss.getElementsByClass("biz-type-model").text();//经营模式
			if(StringUtil.isNotEmpty(docss.getElementsByClass("description-value-higher-hm  bsr-value-higher-common ").text())){
				goods_description_rate=docss.getElementsByClass("description-value-higher-hm  bsr-value-higher-common ").text();//货描 高于或者低于均值
				if(goods_description_rate.contains(Fields.ZERO)){
					goods_description_rate=Fields.ZERO_ONE; 
				}else{
					goods_description_rate=goods_description_rate.split("%")[0]+"%";
				}
			}
			if(StringUtil.isNotEmpty(docss.getElementsByClass("description-value-higher-xy  bsr-value-higher-common ").text())){
				service_attitude_rate=docss.getElementsByClass("description-value-higher-xy  bsr-value-higher-common ").text();//服务
				if(service_attitude_rate.contains(Fields.ZERO)){
					service_attitude_rate=Fields.ZERO_ONE; 
				}else{
					service_attitude_rate=service_attitude_rate.split("%")[0]+"%";
				}
			}
			if(StringUtil.isNotEmpty(docss.getElementsByClass("description-value-higher-fh  bsr-value-higher-common ").text())){
				delivery_speed_rate=docss.getElementsByClass("description-value-higher-fh  bsr-value-higher-common ").text();//发货
				if(delivery_speed_rate.contains(Fields.ZERO)){
					delivery_speed_rate=Fields.ZERO_ONE; 
				}else{
					delivery_speed_rate=delivery_speed_rate.split("%")[0]+"%";
				}
			}
			if(StringUtil.isNotEmpty(docss.getElementsByClass("description-show-ht description-value-ht").text())){
				purchase_back_rate=docss.getElementsByClass("description-show-ht description-value-ht").text();//回头率
				if(purchase_back_rate.contains(Fields.ZERO)){
					purchase_back_rate=Fields.ZERO_ONE; 
				}else{
					purchase_back_rate=purchase_back_rate.split("%")[0]+"%";
				}
			}
			if(StringUtil.isNotEmpty(docss.getElementsByClass("nameArea").text())){
				platform_vendorname=docss.getElementsByClass("nameArea").get(0).getElementsByTag("a").text();//供应商名称	
			}else{
				platform_vendorname=docss.getElementsByClass("name has-tips ").text();//供应商名称
			}
			messageData.put("platform_vendorid",StringHelper.encryptByString(goodsName+Fields.platform_1688));//供应商Id
			Elements price=docss.getElementsByClass("price").select("td"); //价格
			for(int k=0;k<price.size();k++){
				if(!price.get(k).text().contains(Fields.PRICE)){
					if(price.get(k).toString().contains("begin")){
						JSONObject currPriceJson = JSONObject.fromObject(price.get(k).getElementsByTag("td").attr("data-range").toString());
						if(StringUtil.isNotEmpty(currPriceJson.get("end").toString())){
							messageData.put("purchase_amount",currPriceJson.get("begin")+"-"+currPriceJson.get("end"));//起批量	
						}else{
							messageData.put("purchase_amount",currPriceJson.get("begin").toString());//起批量	
						}
						messageData.put("purchase_price",currPriceJson.get("price").toString());//价格
						if(StringUtil.isNotEmpty(docss.getElementsByClass("amount").text())){
							purchase_unit=docss.getElementsByClass("amount").get(0).getElementsByClass("unit").get(0).text();
						}
						messageData.put("purchase_unit",purchase_unit );//件 ，个，台，套 
						Craw_goods_Vendor_price_Info Vendor_price=crawGoodsVendorPriceInfoData(messageData);
						list.add(Vendor_price);
						if(storage.equalsIgnoreCase(Fields.COUNT_4)){
							insertItemPrice= BeanMapUtil.convertBean2MapWithUnderscoreName(crawGoodsVendorPriceInfoData(messageData));
							insertprices.add(insertItemPrice); 			
						}
						
					}

				}
			}
			if(insertprices !=null && insertprices.size()>0){
				batchInsertData.insertIntoData(insertprices,database,Fields.TABLE_CRAW_VENDOR_PRICE_INFO);	
			}
			try {
				if(!price.toString().contains("data-range=")){
					messageData.put("purchase_price",price.get(price.size()-1).getElementsByClass("value").text());//价格
					messageData.put("purchase_unit",docss.getElementsByClass("amount").get(0).getElementsByClass("unit").text() );//件 ，个，台，套 
					messageData.put("purchase_amount",docss.getElementsByClass("amount").get(0).getElementsByClass("value").text());//起批量	
					Craw_goods_Vendor_price_Info Vendor_price=crawGoodsVendorPriceInfoData(messageData);
					list.add(Vendor_price);
					if(storage.equalsIgnoreCase(Fields.COUNT_4)){
					insertItemPrice= BeanMapUtil.convertBean2MapWithUnderscoreName(crawGoodsVendorPriceInfoData(messageData));
					insertprices.add(insertItemPrice); 
					batchInsertData.insertIntoData(insertprices,database,Fields.TABLE_CRAW_VENDOR_PRICE_INFO);	
					}
				}
			} catch (Exception e) {

			}
			//产品属性
			try{
				Elements met= docss.getElementsByClass("widget-custom offerdetail_ditto_attributes").get(0).getElementsByClass("obj-content").get(0).getElementsByTag("table").get(0).getElementsByTag("tbody").get(0).getElementsByTag("tr");
				for(Element mss:met){
					Elements tdment=mss.select("td");
					for(int i=0;i<tdment.size();i++){
						if(i%2==0){
							property.append(tdment.get(i).getElementsByClass("de-feature").text()+":");  
						}else{
							property.append(tdment.get(i).getElementsByClass("de-value").text()+"&&"); 
						}
					} 
				}
			 platform_goods_detail=property.toString().substring(0,property.toString().length()-2);
			}catch (Exception e){
				property=null;
			}
			messageData.put("platform_goods_detail",platform_goods_detail);//-商品详情
			messageData.put("platform_vendoraddress",platform_vendoraddress);//供应商地址
			messageData.put("platform_vendorname", platform_vendorname);//供应商名称
			messageData.put("platform_goodsname", goodsName);//---商品名称
			messageData.put("platform_goods_picurl", image );//---商品图片
			messageData.put("platform_goodsurl", mapType.get("url").toString());//商品页链接
			messageData.put("purchase_promotion",promotion);//采购促销
			messageData.put("platform_vendoraddress",platform_vendoraddress);//供应商地址
			messageData.put("purchase_qty",purchase_qty);//采购数量
			messageData.put("purchase_comment_num",purchase_comment_num);//评价数
			messageData.put("platform_goods_satisfaction",platform_goods_satisfaction);//商品满意度
			messageData.put("platform_vendortype",platform_vendortype);//供应商类型
			messageData.put("goods_description_rate",goods_description_rate);//货描
			messageData.put("service_attitude_rate",service_attitude_rate);//服务
			messageData.put("delivery_speed_rate", delivery_speed_rate);//发货
			messageData.put("purchase_back_rate",purchase_back_rate);//供应商类型
			messageData.put(Fields.TABLE_CRAW_VENDOR_INFO, crawGoodsVendorInfoData(messageData));
			messageData.put("Vendor_priceList", list);//价格
			if(storage.equalsIgnoreCase(Fields.COUNT_4)){
			insertItem= BeanMapUtil.convertBean2MapWithUnderscoreName(crawGoodsVendorInfoData(messageData));
			insertItems.add(insertItem); 
			batchInsertData.insertIntoData(insertItems,database,Fields.TABLE_CRAW_VENDOR_INFO);
			}
		}
		return messageData;
	}

	public Craw_goods_Vendor_InfoVO crawGoodsVendorInfoData(Map<String,Object>data){
		Craw_goods_Vendor_InfoVO info=new Craw_goods_Vendor_InfoVO();
		info.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
		info.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
		info.setChannel(data.get("channel").toString());
		info.setCust_keyword_id(Integer.valueOf(data.get("keywordId").toString()));
		info.setDelivery_speed_rate(data.get("delivery_speed_rate").toString());//发货 高于或者低于均值
		info.setEgoodsid(data.get("egoodsId").toString());
		info.setPlatform_vendorid(data.get("platform_vendorid").toString());
		info.setGoods_description_rate(data.get("goods_description_rate").toString());
		info.setGoods_status(Integer.valueOf(Fields.STATUS_ON));
		info.setGoodsid(data.get("goodsId").toString());
		info.setPlatform_goods_detail(data.get("platform_goods_detail").toString());
		info.setPlatform_goods_picurl(data.get("platform_goods_picurl").toString());
		info.setPlatform_goods_satisfaction(data.get("platform_goods_satisfaction").toString());
		info.setPlatform_goodsname(data.get("platform_goodsname").toString());
		info.setPlatform_goodsurl(data.get("platform_goodsurl").toString());
		info.setPlatform_vendoraddress(data.get("platform_vendoraddress").toString());
		info.setPlatform_vendorname(data.get("platform_vendorname").toString());
		info.setPlatform_vendortype(data.get("platform_vendortype").toString());
		info.setPurchase_back_rate(data.get("purchase_back_rate").toString());
		info.setPurchase_comment_num(data.get("purchase_comment_num").toString());
		info.setPurchase_promotion(data.get("purchase_promotion").toString());
		info.setPurchase_qty(data.get("purchase_qty").toString());
		info.setService_attitude_rate(data.get("service_attitude_rate").toString());
		info.setBatch_time(data.get("batch_time").toString());
		return info;
	}

	public Craw_goods_Vendor_price_Info  crawGoodsVendorPriceInfoData(Map<String,Object>data){
		Craw_goods_Vendor_price_Info info=new Craw_goods_Vendor_price_Info();
		//SimpleDateFormat fort=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		info.setBatch_time(data.get("batch_time").toString());
		info.setChannel(data.get("channel").toString());
		info.setCust_keyword_id(Integer.valueOf(data.get("keywordId").toString()));
		info.setEgoodsid(data.get("egoodsId").toString());
		info.setPurchase_amount(data.get("purchase_amount").toString());
		info.setPurchase_price(data.get("purchase_price").toString());
		info.setGoodsid(data.get("goodsId").toString());
		info.setPurchase_unit(data.get("purchase_unit").toString());
		info.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
		info.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
		return info;

	}
	//判断商品是否下架
	public List<Craw_goods_Vendor_Info>goodsShopMessage(String egoodsId){
		return crawlerPublicClassService.goodsShopMessage(egoodsId);

	}
}
