package com.eddc.service;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.eddc.model.CommodityPrices;
import com.eddc.model.Craw_goods_Fixed_Info;
import com.eddc.model.Craw_goods_Info;
import com.eddc.model.Craw_goods_Price_Info;
import com.eddc.model.Craw_goods_comment_Info;
import com.eddc.model.Craw_goods_pic_Info;
import com.eddc.util.BatchInsertData;
import com.eddc.util.BeanMapUtil;
import com.eddc.util.Fields;
import com.eddc.util.JSONUtil;
import com.eddc.util.SimpleDate;
import com.eddc.util.StringHelper;
import com.eddc.util.Validation;
import com.eddc.util.publicClass;
import com.github.pagehelper.util.StringUtil;
import com.google.common.collect.Lists;
@Service
public class Jd_DataCrawlService {
	private static Logger logger=Logger.getLogger(Jd_DataCrawlService.class);
	//private static SimpleDateFormat fort=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	@Autowired
	HttpClientService httpClientService;
	@Autowired
	CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	private BatchInsertData batchInsertData;
	/**
	 * 京东item页面数据解析parse
	 * param itemContent  商品详情数据
	 * param egoodsID     商品id
	 * param pictureUrl   商品图片url
	 * param goodsName    商品名称
	 * param ecatId       商品ecatId
	 * param shopId       商品shopId
	 * param shopName     商品店铺名称
	 * param platform_shoptype 商品平台店铺类型
	 * param message      商品message
	 * param itemUrl      商品详情url
	 * param goodsId      商品goodsId
	 * param verderId     商品verderId 
	 * param jdPrice      商品价格
	 * param Stock        库存
	 * param delivery_place 发货地址+
	 * param seller_location 卖家地址
	 * param  Promotion 促销
	 * 
	 * */
	@SuppressWarnings("unchecked")
	public Map<String,String>JDparseItemPage(String itemUrlContent,Map<String,String>Map){
		Map<String, String> map = new HashMap<String, String>();
		Map<String,Object> insertItem=new HashMap<String,Object>();
		List<Map<String,Object>> insertImage= Lists.newArrayList();
		Document docs =Jsoup.parse(itemUrlContent.toString());
		String shopId ="0";//店铺Id
		String message="null";
		String shopName = null;//店铺
		String goodsName="";//商品名称
		String jdPrice="";//商品价格
		String Stock=Fields.IN_STOCK;//库存
		String delivery_place="";//发货地
		String seller_location="";//卖家地址
		String delivery_info="";//运费
		String pictureUrl="";//商品图片
		String proprietary=Fields.PROPRIETARY;//是否自营
		String Promotion="";//促销 
		String subCatId="";
		String originalPrice="";
		String reserve_num="";
		String provinceName="";
		String cityName="";
		String versionId="";
		goodsName=docs.getElementsByClass("title-text").text();//商品名称
		if(StringUtil.isEmpty(goodsName)){
			goodsName=docs.getElementsByClass("fn_text_wrap").text();//商品名称
		}
		delivery_place=docs.getElementsByClass("address address01 J_ping").text();
		delivery_info=docs.getElementsByClass("fareMoney").text(); 
		if(docs.toString().contains("spec_image")){
			pictureUrl= docs.getElementById("spec_image").attr("src").toString();	
		}else if(StringUtil.isEmpty(pictureUrl)){//
			try{pictureUrl= docs.getElementById("scroll-imgs").getElementsByTag("li").get(0).attr("src").toString();	}catch (Exception e) {}	
		}
		if(StringUtil.isNotEmpty(pictureUrl.toString())){
			if(docs.toString().contains("provinceName")){
				try {seller_location=docs.getElementById("provinceName").text();} catch (Exception e) {}
			}
			if(Validation.isEmpty(delivery_info)){
				delivery_info=Fields.DONTPACK_MAIL;
			}else{
				delivery_info=Fields.PACK_MAIL;
			}
			if(docs.toString().contains("customerService")){
				String[] str = docs.toString().split("customerService");
				shopName=StringHelper.nullToString(StringHelper.getResultByReg(str[1], "name\":\"([^<>\"]+)\"")).replaceAll("'", "");
			}
			try {shopId = docs.getElementById("shopId").val();} catch (Exception e) {}
			if(StringUtil.isEmpty(shopId)){
				try {
					shopId=docs.getElementsByClass("shop-part bdr-tb").get(0).getElementsByTag("a").attr("href");
					shopId=StringHelper.nullToString(StringHelper.getResultByReg(shopId, "shopId=([^<>\"]+)&skuId")).replaceAll("'", "");
					subCatId=StringHelper.nullToString(StringHelper.getResultByReg(shopId, "&categoryId=([^<>\"]+)")).replaceAll("'", "");
				} catch (Exception e) {
				}
			}
			//促销
			if(docs.toString().contains("activityList")){
				String temp = docs.toString().substring(docs.toString().indexOf("activityList"), docs.toString().lastIndexOf("prompt")-2);
				temp="{\""+temp+"}";
				JSONObject jsonObject = JSONObject.fromObject(temp);
				logger.info(jsonObject); 
				JSONArray jsonList = jsonObject.getJSONArray("activityList");
				if(jsonList.size()>0){
					for(int i=0;i<jsonList.size();i++){
						JSONObject json = JSONObject.fromObject(jsonList.toArray()[i]);	
						if(Validation.isEmpty(Promotion)){
							Promotion=json.getString("text")+":"+json.getString("value");
						}else{
							Promotion+="&&"+json.getString("text")+":"+json.getString("value");
						}
					}
				}
			}
			try {
				jdPrice =docs.getElementById("jdPrice").val();
			} catch (Exception e) {
				if(docs.toString().contains("specJdPrice")){
					jdPrice=docs.getElementById("specJdPrice").text();
					if(Validation.isEmpty("spec_price")){
						jdPrice=docs.getElementById("spec_price").text();
					}
				}
				originalPrice=jdPrice;
			}
			if(docs.toString().contains("isExist")){
				Stock=docs.getElementsByClass("isExist").html();
				if(Stock.contains(Fields.SPOT)){ 
					Stock=Fields.IN_STOCK;
				}else{ 
					Stock=Fields.IS_NOT_STOCK;
				} 
			}else if(docs.toString().contains("stockStatus")){
				Stock =docs.getElementById("stockStatus").html();
				if(Stock.contains(Fields.IS_NOT_STOCK)){//
					Stock=Fields.IS_NOT_STOCK;
				}else{
					Stock=Fields.IN_STOCK;
				}
			}
			if(docs.toString().contains("label-text white-text")){
				proprietary=docs.getElementsByClass("label-text white-text").text().replaceAll(Fields.JD_SECONDS_KILL, "").trim();//是否自营
			}
			if(docs.toString().contains("categoryId")){
				subCatId=docs.getElementById("categoryId").val();//
			}
		}else{
			String promov="";
			try {
				try{
					if(docs.toString().contains("yuyue")){
						promov=docs.toString().substring(docs.toString().indexOf("window._itemInfo = ({")+20,docs.toString().lastIndexOf("hasAddress")-2);	
						promov=promov+"}}";
					}else{
						promov=docs.toString().substring(docs.toString().indexOf("window._itemInfo = ({")+20,docs.toString().lastIndexOf("adurl")-2);
						promov=promov+"}]}]}"; 
					}					
				}catch(Exception e){
					promov=docs.toString().substring(docs.toString().indexOf("window._itemInfo = ({")+20,docs.toString().lastIndexOf("bigouinfo")+13);
					promov=promov+"}";	
				}
				JSONObject jsons =JSONObject.fromObject(promov); 
				if(jsons.toString().contains("stock")){
					provinceName=jsons.getJSONObject("stock").getJSONObject("area").getString("provinceName");//城市
					cityName=jsons.getJSONObject("stock").getJSONObject("area").getString("cityName");//区
					Stock=jsons.getJSONObject("stock").getString("StockStateName");//库存
				}
				
				seller_location=provinceName + cityName;//卖家位置
				jdPrice=jsons.getJSONObject("price").getString("p").toString();//现价
				//originalPrice=jsons.getJSONObject("price").getString("m").toString();//原价
				originalPrice=jsons.getJSONObject("price").getString("op");
				pictureUrl=docs.getElementById("firstImg").attr("src").trim().toString();//图片
				if(jsons.toString().contains("yuyue")){
					reserve_num=jsons.getJSONObject("yuyue").getString("num").toString();//预约人数
				}
				if(jsons.getJSONObject("stock").toString().contains("self_D")){
					shopName=jsons.getJSONObject("stock").getJSONObject("self_D").getString("vender");//店铺 
					subCatId=jsons.getJSONObject("stock").getJSONObject("self_D").getString("cg");
					versionId=jsons.getJSONObject("stock").getJSONObject("self_D").getString("vid");//
					if(jsons.getJSONObject("stock").getJSONObject("self_D").toString().contains("shopId")){
						shopId=jsons.getJSONObject("stock").getJSONObject("self_D").getString("shopId");	
					}
				}else{
					if(jsons.toString().contains("stock")){
						shopName=jsons.getJSONObject("stock").getJSONObject("D").getString("vender");//店铺
						subCatId=jsons.getJSONObject("stock").getJSONObject("D").getString("cg");
						shopId=jsons.getJSONObject("stock").getJSONObject("D").getString("shopId");	
						versionId=jsons.getJSONObject("stock").getJSONObject("D").getString("vid");//
					}
				}

				if(shopName.contains(Fields.YES_PROPRIETARY)){
					proprietary=Fields.YES_PROPRIETARY;
				}
				JSONArray arry=jsons.getJSONArray("promov2");
				if(arry.size()>0){
					JSONArray pis=JSONObject.fromObject(arry.toArray()[0]).getJSONArray("pis");
					for(int i=0;i<pis.size();i++){
						JSONObject jso =JSONObject.fromObject(pis.toArray()[i]);
						Set<Entry<String, Object>> entrySet = jso.entrySet();
						for(Entry<String, Object> entry : entrySet) {
							if(entry.getValue().toString().contains(Fields.FULL)||entry.getValue().toString().contains(Fields.PROMOTION_DETAILS) || entry.getValue().toString().contains(Fields.LIMIT)){
								Promotion+=entry.getValue().toString()+"&&";
							}
						}
					}
				}
				if(jsons.toString().contains("bankpromo")){
					Promotion+="&&"+jsons.getJSONObject("bankpromo").getString("title");
				}
			} catch (Exception e) {

			}

		}
		if(StringUtil.isEmpty(Promotion)){
			if(docs.getElementsByClass("J_ping desc right_shorter").text().contains(Fields.REDUCTION_OF)){
				Promotion=Fields.ADVERTISING+":"+docs.getElementsByClass("J_ping desc right_shorter").text();	
			}
		}
		if(StringUtil.isNotEmpty(Promotion)){
			Promotion=Promotion.substring(0, Promotion.length()-2);
		}
		//优惠卷促销暂时不抓取
		try {
			if(StringUtil.isNotEmpty(subCatId)){
				String cid=subCatId.replaceAll(":", ",").replaceAll("_", ",").toString();
				cid=subCatId.substring(subCatId.lastIndexOf(",")+1);
				Map.put("url",Fields.PLATFORM_APP_ROOL.replace("CID", cid).replace("SKUID",Map.get("egoodsId").toString()).replace("POPID", versionId));
				String StringMessagePromotion = httpClientService.interfaceSwitch(Map.get("ip").toString(), Map,Map.get("database").toString());//请求数据
				String item=StringMessagePromotion.toString().substring(StringMessagePromotion.toString().indexOf("try"),StringMessagePromotion.toString().lastIndexOf("catch"));
				item=item.toString().substring(item.toString().indexOf("(")+1,item.toString().lastIndexOf(")"));  
				JSONObject jsonObject = JSONObject.fromObject(item);
				JSONArray jsonList = jsonObject.getJSONArray("coupons");
				for(int i=0;i<jsonList.size();i++){ 
					JSONObject json=JSONObject.fromObject(jsonList.toArray()[i]);
					if(StringUtil.isEmpty(Promotion)){
						Promotion=Fields.ROLL+":"+json.getString("timeDesc") +" "+Fields.FULL+json.getString("quota")+Fields.REDUCTION_OF+json.getString("discount"); 
					}else{
						Promotion+="&&"+Fields.ROLL+":"+json.getString("timeDesc") +" "+Fields.FULL+json.getString("quota")+Fields.REDUCTION_OF+json.getString("discount"); 
					}
				}
			}

		} catch (Exception e) {

		}
		if(docs.toString().contains("promomiao")){	
			if(StringUtil.isEmpty(Promotion)){
				Promotion=Fields.JD_SECONDS_KILL+":"+jdPrice;
			}else{
				Promotion+="&&"+Fields.JD_SECONDS_KILL+":"+jdPrice;
			}
		}
		if(docs.toString().contains("yuyue")){	
			if(StringUtil.isEmpty(Promotion)){
				Promotion=Fields.JD_PRESELL+":"+jdPrice;
			}else{
				Promotion+="&&"+Fields.JD_PRESELL+":"+jdPrice;
			}
		}
		if(Map.get("status_type").equalsIgnoreCase(Fields.COUNT_4)){
			try {
				if(docs.toString().contains("window._itemOnly")){
					String lengths="window._itemOnly =";
					String promov=docs.toString().trim().substring(docs.toString().indexOf("window._itemOnly =  ({")+lengths.length(),docs.toString().trim().lastIndexOf("window._isLogin"));	
					promov=promov.substring(promov.indexOf("(")+1,promov.lastIndexOf(")"));
					JSONObject currPriceJson = JSONObject.fromObject(promov);
					JSONArray jsonArray = JSONArray.fromObject(currPriceJson.getJSONObject("item").get("image").toString());
					for (int i = 0; i < jsonArray.size(); i++) { 
						String imageurl=Fields.IMAGE_URL+jsonArray.getString(i);
						Craw_goods_pic_Info info=crawlerPublicClassService.commodityImages(Map,imageurl,i);//获取小图片
						insertItem= BeanMapUtil.convertBean2MapWithUnderscoreName(info);
						insertImage.add(insertItem);
					}
				}
			} catch (Exception e) {
				logger.error("抓取商品小图片失败"+e+Map.get("egoodsId").toString());
			}	
		}
		message=shopId+","+proprietary;
		map.put("shopName", shopName);//店铺名称
		map.put("region",seller_location);//卖家位置
		map.put("shopid", shopId);//商品Id
		map.put("platform_shoptype", proprietary);//平台商店类型
		map.put("subCatId", subCatId.replaceAll(":", ",").replaceAll("_", ",").toString());
		map.put("sellerId", null);
		map.put("sellerName", shopName);//卖家店铺名称
		map.put("picturl", pictureUrl);//图片url 
		map.put("goodsName", goodsName==""?goodsName:docs.title());//商品名称
		map.put("delivery_place", delivery_place);//交易地址
		map.put("inventory", Stock);//商品库存
		map.put("transactNum", null);//月销
		map.put("postageFree", delivery_info);//是不包邮
		map.put("rateNum", null);//商品总评论数
		map.put("skuId", Map.get("egoodsId"));//商品sku
		map.put("originalPrice", originalPrice==""?jdPrice:originalPrice.toString());//原价
		map.put("currentPrice", jdPrice);//现价
		map.put("promotion", Promotion);//促销
		map.put("message", message);   
		map.put("product_location", productLocation_message(goodsName));//产地
		map.put("goodsUrl", Fields.JD_URL_PC+Map.get("egoodsId")+".html ");
		map.put("channel",Fields.CLIENT_MOBILE); 
		map.put("deposit", null);
		map.put("coupons", null);
		map.put("sale_qty", null);
		map.put("reserve_num", reserve_num);
		if(insertImage.size()>0){
			batchInsertData.insertIntoData(insertImage,Map.get("database").toString(),Fields.TABLE_CRAW_GOODS_PIC_INFO);	
		}
		return map;

	}

	@SuppressWarnings("unchecked")
	public Map<String,String>JDparseItemPageInventory(String itemUrlContent,Map<String,String>Map){
		Map<String, String> map = new HashMap<String, String>();
		String delivery_info=Fields.PACK_MAIL;String shopId="";String shopName="";String reserve_num="";	String coupons=""; String promotion="";String pictureUrl="";
		String goodsName="";String jdPrice="";String subCatId="";String provinceName="";String cityName="";
		String countyName="";String defaultAddress="";String skuId="";String Stock="";String seller_location="";String message="";
		String versionId="";String proprietary=Fields.PROPRIETARY; String originalPrice="";
		if(itemUrlContent.toString().contains("bigpath")){
			JSONObject currPriceJsons = JSONObject.fromObject(itemUrlContent);
			if(currPriceJsons.toString().contains("yuYue")){
				reserve_num=currPriceJsons.getJSONObject("yuYue").getString("yuyueNum");//预约商品
			}
			if(StringUtil.isNotEmpty(currPriceJsons.getJSONObject("stock").getString("jdPrice"))){
				goodsName=currPriceJsons.getJSONObject("ware").getString("wname"); 
				jdPrice =currPriceJsons.getJSONObject("stock").getString("jdPrice");
				subCatId=currPriceJsons.getJSONObject("ware").getString("category").replace(";", ",");
				pictureUrl=JSONObject.fromObject(currPriceJsons.getJSONObject("ware").getJSONArray("images").toArray()[0]).getString("bigpath");	
				provinceName=currPriceJsons.getJSONObject("defaultAddress").getString("provinceName");
				cityName=currPriceJsons.getJSONObject("defaultAddress").getString("cityName");
				countyName=currPriceJsons.getJSONObject("defaultAddress").getString("countyName");
				defaultAddress=provinceName+ cityName + countyName;
				skuId=currPriceJsons.getJSONObject("ware").getString("wareId");
				if(currPriceJsons.getJSONObject("ware").getJSONObject("mobileShopInfo").toString().contains("shopId")){
					shopId=currPriceJsons.getJSONObject("ware").getJSONObject("mobileShopInfo").getString("shopId"); 
				}
				if(!currPriceJsons.getJSONObject("ware").getJSONObject("mobileShopInfo").toString().equals("null")){
					shopName=currPriceJsons.getJSONObject("ware").getJSONObject("mobileShopInfo").getString("name");
				}else{
					if(currPriceJsons.getJSONObject("stock").toString().contains("iconList")){
						shopName=JSONObject.fromObject(currPriceJsons.getJSONObject("stock").getJSONArray("iconList").toArray()[0]).getString("name"); // 
						if(shopName.contains(Fields.JING_MUST_REACH)){
							shopName=Fields.JD_SUPERMARKET;
						}
					}
				}
				if(currPriceJsons.getJSONObject("ware").getJSONObject("mobileShopInfo").toString().contains("content")){
					JSONArray josn= currPriceJsons.getJSONObject("ware").getJSONObject("mobileShopInfo").getJSONArray("shopCouponsList");
					for(int i=0;i<josn.size();i++){
						JSONObject obj=JSONObject.fromObject(josn.toArray()[i]);
						if(StringUtil.isEmpty(coupons)){
							coupons=obj.getString("content").replace("￥", Fields.FULL);
						}else{
							coupons+="&&"+obj.getString("content").replace("￥", Fields.FULL);
						}
					}
				}
				if(currPriceJsons.getJSONObject("proInformation").toString().contains("unicodeValue")){
					JSONArray josn= currPriceJsons.getJSONObject("proInformation") .getJSONArray("activityList");
					for(int i=0;i<josn.size();i++){
						JSONObject obj=JSONObject.fromObject(josn.toArray()[i]); 
						if(StringUtil.isEmpty(coupons)){
							coupons=obj.getString("text")+":"+obj.getString("value");
						}else{
							coupons+="&&"+obj.getString("text")+":"+obj.getString("value");
						}
					}
				}
				promotion=coupons;
				proprietary=currPriceJsons.getJSONObject("ware").getString("shortService");
				if(proprietary.contains(Fields.PLATFORM_JD_CN)){
					proprietary=Fields.YES_PROPRIETARY;
				}else{
					proprietary=Fields.PROPRIETARY;
				}
				message=shopId+","+proprietary;
				delivery_info=currPriceJsons.getJSONObject("stock").getString("fare").toString();
				if(delivery_info.contains(Fields.FULL)){
					delivery_info=Fields.DONTPACK_MAIL;
				}else {
					delivery_info=Fields.PACK_MAIL;
				}
				Stock=currPriceJsons.getJSONObject("stock").getString("status");
				if(Stock.contains(Fields.SPOT)){
					Stock=Fields.IN_STOCK;
				}else{
					Stock=Fields.IS_NOT_STOCK;
				}
				//广告添加促销
				try {
					if(StringUtil.isEmpty(promotion)){
						promotion=Fields.ADVERTISING+":"+currPriceJsons.getJSONObject("ware").getJSONObject("ad").getString("adword");	
					}else{
						promotion+=Fields.ADVERTISING+":"+currPriceJsons.getJSONObject("ware").getJSONObject("ad").getString("adword");	
					}
				} catch (Exception e) {
				}
			}
		}else{
			try {
				String item=itemUrlContent.toString().substring(itemUrlContent.toString().indexOf("{"), itemUrlContent.toString().lastIndexOf("}")+1);
				String messages=item.toString().replace("\\x3E","").replace("\\x3Ca", "").replace("\\x5C", "").replace("\\x27","").replace("\\x2", "").replace("\\x3CFa", "").replace("\\x3C\\x2Fa\\x3E", "").trim();
				JSONObject jsonObject = JSONObject.fromObject(messages);
				goodsName=jsonObject.getString("skuName");
				pictureUrl=jsonObject.getString("image");
				//originalPrice=jsonObject.getJSONObject("price").getString("m");
				originalPrice=jsonObject.getJSONObject("price").getString("op");
				jdPrice=jsonObject.getJSONObject("price").getString("p");
				if(jsonObject.containsKey("yuyue")){
					reserve_num=jsonObject.getJSONObject("yuyue").getString("num").toString();//预约人数
				}
				
				if(jsonObject.containsKey("stock")){
					provinceName=jsonObject.getJSONObject("stock").getJSONObject("area").getString("provinceName");//城市
					cityName=jsonObject.getJSONObject("stock").getJSONObject("area").getString("cityName");//区
					Stock=jsonObject.getJSONObject("stock").getString("StockStateName");//库存
					defaultAddress=provinceName + cityName;//交易地址
				}
				if(provinceName.contains("北京")) {
					System.out.println(jsonObject.toString());
				}

				
				if(jsonObject.getJSONObject("stock").toString().contains("self_D")){
					shopName=jsonObject.getJSONObject("stock").getJSONObject("self_D").getString("vender");//店铺 
					subCatId=jsonObject.getJSONObject("stock").getJSONObject("self_D").getString("cg");
					versionId=jsonObject.getJSONObject("stock").getJSONObject("self_D").getString("vid");//
					if(jsonObject.getJSONObject("stock").getJSONObject("self_D").toString().contains("shopId")){
						shopId=jsonObject.getJSONObject("stock").getJSONObject("self_D").getString("shopId");	
					}
				}else{
					if(jsonObject.toString().contains("stock")){
						shopName=jsonObject.getJSONObject("stock").getJSONObject("D").getString("vender");//店铺
						subCatId=jsonObject.getJSONObject("stock").getJSONObject("D").getString("cg");
						if(jsonObject.getJSONObject("stock").getJSONObject("D").toString().contains("shopId")){
							shopId=jsonObject.getJSONObject("stock").getJSONObject("D").getString("shopId");		
						}
						versionId=jsonObject.getJSONObject("stock").getJSONObject("D").getString("vid");//
					}
				}
				if(jsonObject.getJSONObject("stock").toString().contains("samProductFreight")){//
                	delivery_info=jsonObject.getJSONObject("stock").getString("samProductFreight");
                	if(delivery_info.contains(Fields.FULL)){
    					delivery_info=Fields.DONTPACK_MAIL;
    				}else {
    					delivery_info=Fields.PACK_MAIL;
    				}
                }
				JSONArray arry=jsonObject.getJSONArray("promov2");
				try {
					if(arry.size()>0){
						if(arry.toString().contains("pis")){
							JSONArray pis=JSONObject.fromObject(arry.toArray()[0]).getJSONArray("pis");
							for(int i=0;i<pis.size();i++){
								JSONObject jso =JSONObject.fromObject(pis.toArray()[i]);
								Set<Entry<String, Object>> entrySet = jso.entrySet();
								for(Entry<String, Object> entry : entrySet) {
									if(entry.getValue().toString().contains(Fields.FULL)||entry.getValue().toString().contains(Fields.PROMOTION_DETAILS) || entry.getValue().toString().contains(Fields.LIMIT)){
										promotion+=entry.getValue().toString()+"&&";
									}
								}
							}	
						}
					}
				} catch (Exception e) {
					logger.error("解析促销出现问题了>>>>>>>>>>"+e);
				}
				if(jsonObject.toString().contains("bankpromo")){
					if(Validation.isEmpty(promotion)){
						promotion+=jsonObject.getJSONObject("bankpromo").getString("title");	
					}else{
						promotion+="&&"+jsonObject.getJSONObject("bankpromo").getString("title");
					}
					
				}
				if(StringUtil.isNotEmpty(promotion)){
					promotion=promotion.substring(0, promotion.length()-2);
				}
				if(jsonObject.toString().contains("promomiao")){	
					if(StringUtil.isEmpty(promotion)){
						promotion=Fields.JD_SECONDS_KILL+":"+jdPrice;
					}else{
						promotion+="&&"+Fields.JD_SECONDS_KILL+":"+jdPrice;
					}
				}
				if(jsonObject.toString().contains("yuyue")){	
					if(StringUtil.isEmpty(promotion)){
						promotion=Fields.JD_PRESELL+":"+jdPrice;
					}else{
						promotion+="&&"+Fields.JD_PRESELL+":"+jdPrice;
					}
				}
				if(shopName.contains(Fields.YES_PROPRIETARY)){
					proprietary=Fields.YES_PROPRIETARY;
				}
				message=shopId+","+proprietary;	
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("解析出错了"+e);
			}

		} 
		map.put("shopName", shopName);//店铺名称
		map.put("region",provinceName);//卖家位置
		map.put("shopid", shopId);//商品Id
		map.put("platform_shoptype", proprietary);//平台商店类型
		map.put("subCatId", subCatId);
		map.put("sellerName", shopName);//卖家店铺名称
		map.put("picturl", pictureUrl);//图片url 
		map.put("goodsName", goodsName);//商品名称
		map.put("delivery_place", defaultAddress);//交易地址
		map.put("inventory", Stock);//商品库存
		map.put("transactNum", null);//月销
		map.put("postageFree", delivery_info);//是不包邮
		map.put("rateNum", null);//商品总评论数
		map.put("skuId", skuId);//商品sku
		map.put("originalPrice", originalPrice);//原价
		map.put("currentPrice", jdPrice);//现价
		map.put("promotion", promotion);//促销
		map.put("message", message);   
		map.put("product_location", productLocation_message(goodsName));//产地
		map.put("goodsUrl", Fields.JD_URL_PC+Map.get("egoodsId")+".html ");
		map.put("channel",Fields.CLIENT_MOBILE); 
		map.put("deposit", null);
		map.put("coupons", null);
		map.put("sale_qty", null);
		map.put("reserve_num", reserve_num);
		map.put("sellerId", null);
		return map;
	}

	public Map<String,String>JDparseItemPageByCity(Map<String,String>Map,String cityCode,String itemUrlContent){
		Map<String, String> map = new HashMap<String, String>();
		Document docs =Jsoup.parse(itemUrlContent.toString());
		String shopId ="0";//店铺Id
		String message="null";
		String shopName = null;//店铺
		String goodsName="";//商品名称
		String jdPrice="";//商品价格
		String Stock=Fields.IN_STOCK;//库存
		String delivery_place="";//发货地
		String seller_location="";//卖家地址
		String delivery_info="";//运费
		String pictureUrl="";//商品图片
		String proprietary=Fields.PROPRIETARY;//是否自营
		String Promotion="";//促销
		String subCatId="";
		String skuId="";
		subCatId=docs.getElementById("categoryId").val();//
		goodsName=docs.getElementsByClass("title-text").text();//商品名称
		seller_location=docs.getElementById("provinceName").text();
		pictureUrl= docs.getElementById("spec_image").attr("src").toString();
		skuId=docs.getElementById("preWareId").val();
		delivery_info=docs.getElementsByClass("fareMoney").text();
		shopId = docs.getElementById("shopId").val();
		delivery_place = getStockDeliveryPlace(cityCode);
		if(Validation.isEmpty(delivery_info)){
			delivery_info=Fields.DONTPACK_MAIL;
		}else{
			delivery_info=Fields.PACK_MAIL;
		}
		if(docs.toString().contains("customerService")){
			String[] str = docs.toString().split("customerService");
			shopName=StringHelper.nullToString(StringHelper.getResultByReg(str[1], "name\":\"([^<>\"]+)\"")).replaceAll("'", "");
		}
		//促销
		if(docs.toString().contains("activityList")){
			String temp = docs.toString().substring(docs.toString().indexOf("activityList"), docs.toString().lastIndexOf("prompt")-2);
			temp="{\""+temp+"}";
			JSONObject jsonObject = JSONObject.fromObject(temp);
			//System.out.println(jsonObject);
			JSONArray jsonList = jsonObject.getJSONArray("activityList");
			if(jsonList.size()>0){
				for(int i=0;i<jsonList.size();i++){
					JSONObject json = JSONObject.fromObject(jsonList.toArray()[i]);
					if(Validation.isEmpty(Promotion)){
						Promotion=json.getString("text")+":"+json.getString("value");
					}else{
						Promotion+="&&"+json.getString("text")+":"+json.getString("value");
					}
				}
			}
		}

		try {
			jdPrice =docs.getElementById("jdPrice").val();
		} catch (Exception e) {
			jdPrice=docs.getElementById("specJdPrice").text();
			if(Validation.isEmpty("spec_price")){
				jdPrice=docs.getElementById("spec_price").text();
			}
		}
		String stockUrl = "http://c0.3.cn/stock?skuId="+skuId+"&area="+cityCode+"&venderId=1000003263&cat=737,738,751&extraParam={%22originid%22:%221%22}";
		Map.put("url",stockUrl);
		try {
			Map<String,String> result = httpClientService.getResultMap(Map);
			String stockInfo = result.get("result");
			int retryCount = 0;
			while(!StringUtils.isNotEmpty(stockInfo) && retryCount < 3){
				Map<String,String> resultRetry = httpClientService.getResultMap(Map);
				stockInfo = resultRetry.get("result");
				retryCount++;
			}
			if(StringUtils.isNotEmpty(stockInfo)){
				JSONObject jsonObject = JSONObject.fromObject(stockInfo);
				JSONObject stockObject = (JSONObject) jsonObject.get("stock");
				if(stockObject!=null){
					String skuState = String.valueOf(stockObject.get("skuState"));
					String stockDesc = String.valueOf(stockObject.get("StockState"));
					if(StringUtils.isNotEmpty(skuState) && StringUtils.isNotEmpty(stockDesc)){
						if(skuState.equals("1") && !stockDesc.contains("34")){
							Stock=Fields.IN_STOCK;
						}else if(!skuState.equals("1") || stockDesc.contains("34")){
							Stock=Fields.IS_NOT_STOCK;
						}
					}
					else if(!StringUtils.isNotEmpty(skuState) && StringUtils.isNotEmpty(stockDesc)){
						if(stockDesc.contains("34")){
							Stock=Fields.IS_NOT_STOCK;
						}else{
							Stock=Fields.IN_STOCK;
						}
					} 
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		if(docs.toString().contains("label-text white-text")){
			proprietary=docs.getElementsByClass("label-text white-text").text().replaceAll("京东秒杀", "").trim();//是否自营
		}
		message=shopId+","+proprietary;
		map.put("shopName", shopName);//店铺名称
		map.put("region",seller_location);//卖家位置
		map.put("shopid", shopId);//商品Id
		map.put("platform_shoptype", proprietary);//平台商店类型
		map.put("subCatId", subCatId.replaceAll("_", ",").toString());
		map.put("sellerId", null);
		map.put("sellerName", shopName);//卖家店铺名称
		map.put("picturl", pictureUrl);//图片url
		map.put("goodsName", goodsName);//商品名称
		map.put("delivery_place", delivery_place);//交易地址
		map.put("inventory", Stock);//商品库存
		map.put("transactNum", null);//月销
		map.put("postageFree", delivery_info);//是不包邮
		map.put("rateNum", null);//商品总评论数
		map.put("skuId", skuId);//商品sku
		map.put("originalPrice", jdPrice);//原价
		map.put("currentPrice", jdPrice);//现价
		map.put("promotion", Promotion);//促销
		map.put("message", message);
		map.put("product_location", productLocation_message(goodsName));//产地
		map.put("goodsUrl", Fields.JD_URL_PC+Map.get("egoodsId")+".html ");
		map.put("channel",Fields.CLIENT_MOBILE); 
		map.put("deposit", null);
		map.put("coupons", null);
		map.put("sale_qty", null);
		map.put("reserve_num", null);
		return map;

	}

	private String getStockDeliveryPlace(String cityCode) {
		//{"3_51035_39620_0", "2_2813_51976_0", "19_1601_3633_0", "22_1930_50946_0", "17_1381_3583_0"};
		String result = "";
		if("1_72_2799_0".equals(cityCode)){
			result = "北京";
		}else if("2_2813_51976_0".equals(cityCode)){
			result = "上海";
		}else if("19_1601_3633_0".equals(cityCode)){
			result = "广州";
		}else if("22_1930_50946_0".equals(cityCode)){
			result = "成都";
		}
		else if("17_1381_3583_0".equals(cityCode)){
			result = "武汉";
		}
		return result;
	}

	/**   
	 *    
	 * 项目名称：springmvc_crawler   
	 * 类名称：DataCraw_Task_JD_job_Service
	 * 类描述：抓取解析PC端价格
	 * 创建人：jack.zhao   
	 * 创建时间：2017年5月17日 上午10:11:33   
	 * 修改人：jack.zhao   
	 * 修改时间：2017年5月17日 上午10:11:33   
	 * 修改备注：   
	 * @version    
	 * @throws Exception 
	 *    
	 */
	public Craw_goods_Price_Info JDparseItemPagePrice(CommodityPrices commodityPrices,String accountId,String ipDatabase,String database) throws Exception{
		//int count=0;
		String itemUrlContent = null;
		Map<String,Object>map=new HashMap<String, Object>();
		String currentPrice="",promotion="";String originalPrice="";
		long time=System.currentTimeMillis();
		String itemurl=Fields.JD_PC_PRICE+time+"&skuIds="+URLEncoder.encode("J_"+commodityPrices.getEgoodsId().trim(), "utf-8");
		Map<String,String>mapLog=new HashMap<String, String>();
		mapLog.put("platform", Fields.PLATFORM_JD);
		mapLog.put("accountId", accountId);
		mapLog.put("status", Fields.STATUS_ON);
		mapLog.put("egoodsId",commodityPrices.getEgoodsId());
		mapLog.put("url", itemurl);
		mapLog.put("cookie",null);
		mapLog.put("coding", "GBK");
		mapLog.put("urlRef",Fields.JD_URL_PC +commodityPrices.getEgoodsId()+".html");
		mapLog.put("client",Fields.CLIENT_PC);	
		mapLog.put("messageLog",Fields.PC_PRICE);
		mapLog.put(Fields.IPPROXY, ipDatabase);
		try {

			itemUrlContent =httpClientService.GoodsProduct(mapLog,database);//请求数据
			if(Validation.isEmpty(itemUrlContent)){
				itemUrlContent =httpClientService.requestData(mapLog);//请求数据	
			}
		} catch (Exception e) {
			itemUrlContent =httpClientService.GoodsProduct(mapLog,database);//请求数据
		}
		if(!Validation.isEmpty(itemUrlContent)){
			try {
				//currentPrice =  JSONUtil.JsonToList(itemUrlContent).get(0).toString();
				String Price =  JSONUtil.JsonToList(itemUrlContent).get(0).toString();
				currentPrice=JSONUtil.JsonToMap(Price).get("p").toString();
				originalPrice=JSONUtil.JsonToMap(Price).get("op").toString(); 
				if(Validation.isEmpty(originalPrice)){
					originalPrice=currentPrice;
				}
			} catch (Exception e) {
				logger.info("价格获取失败"+e.getMessage()+"-----------"+SimpleDate.SimpleDateFormatData().format(new Date()));
			}
		}

		try {
			promotion=pcPromotion(commodityPrices,mapLog,database);//促销
			mapLog.put("messageLog",Fields.PRIOMOTION_DATA+Fields.SUCCESS);
		} catch (Exception e) {
			mapLog.put("messageLog",Fields.PRIOMOTION_DATA+Fields.FAILURE+e.toString());
			logger.info("获取促销信息失败--------------"+e.getMessage()+"----------"+SimpleDate.SimpleDateFormatData().format(new Date()));
		}
		map.put("channel",Fields.CLIENT_PC);
		map.put("currentPrice",currentPrice);
		map.put("originalPrice",originalPrice);
		map.put("promotion",promotion);
		map.put("goodsId", commodityPrices.getGoodsid());
		map.put("keywordId", commodityPrices.getCust_keyword_id());
		map.put("skuId", commodityPrices.getEgoodsId());
		Craw_goods_Price_Info price=crawlerPublicClassService.commdityPricesData(commodityPrices,map);//插入PC端价格
		return price; 

	}
	/**   
	 *    
	 * 项目名称：crawler_Price_Monitoring   
	 * 类名称：JDPcAPI   
	 * 类描述：   抓取商品的促销
	 * 创建人：jack.zhao   
	 * 创建时间：2017年3月3日 上午10:11:33   
	 * 修改人：jack.zhao   
	 * 修改时间：2017年3月3日 上午10:11:33   
	 * 修改备注：   
	 * @version    
	 * @throws Exception 
	 * String egoodsId,String shopId,String venderId,String ecatId,String accountId
	 *     commodityPrices.getEgoodsId(), commodityPrices.getPlatform_shopid(), venderId[0], commodityPrices.getPlatform_category(),
	 */
	public  String  pcPromotion(CommodityPrices commodityPrices,Map<String,String>map ,String database) throws Exception{
		String 	Promotion="",Promotionurl="";String venderId="";
		String time = ""+System.currentTimeMillis();
		StringBuffer prom = new StringBuffer();
		if(Validation.isEmpty(commodityPrices.getPlatform_shopid())){
			venderId="0";
		}else{
			venderId=commodityPrices.getPlatform_shopid();
		}
		String proUrl =Fields.JD_URL_PROMOTION+commodityPrices.getEgoodsId()+"&area=2_0_0_0&shopId="+venderId+"&venderId="+venderId+"&cat="+commodityPrices.getPlatform_category()+"&_="+time;
		map.put("url", proUrl);
		map.put("messageLog",Fields.PRIOMOTION_DATA);
		try {
			Promotionurl =httpClientService.GoodsProductDetailsData(map);//请求数据	
			if(Validation.isEmpty(Promotionurl)){
				Promotionurl =httpClientService.GoodsProduct(map,database );//请求数据
			}
		} catch (Exception e) {
			Promotionurl =httpClientService.requestData(map);//请求数据	
		}
		if(Promotionurl.contains("\"title\":\"")){
			String proInfo = StringHelper.getResultByReg(Promotionurl, "\"title\":\"([^\"]+)");
			if(proInfo.contains(Fields.COUPONS)){
				prom.append(Fields.TOP_UP_COUPONS).append(proInfo+"&&");
			}
		}
		if(!Validation.isEmpty(Promotionurl)){
			Promotion=Promotionurl.substring(Promotionurl.indexOf("{"),Promotionurl.lastIndexOf("}")+1);
			JSONObject json=JSONObject.fromObject(Promotion);
			JSONArray arry=json.getJSONObject("prom").getJSONArray("pickOneTag");
			if(arry.size()>0){
				for(int i=0;i<arry.size();i++){
					JSONObject itemJson = JSONObject.fromObject(arry.toArray()[i].toString());
					String name=itemJson.getString("name").toString();
					if(name.contains(Fields.MEMBER_SPECIALS)){//会员特价
						continue;
					}
					prom.append(name+":").append(itemJson.getString("content").toString()+"&&");	
				}
			}else{
				JSONArray promTags=json.getJSONObject("prom").getJSONArray("tags");
				if(promTags.size()>0){ 
					for(int i=0;i<promTags.size();i++){
						JSONObject itemJson = JSONObject.fromObject(promTags.toArray()[i].toString());
						if(itemJson.getString("name").contains(Fields.MEMBER_SPECIALS)){
							continue;	
						}
						prom.append(itemJson.getString("name")+":"+itemJson.getString("content"));
					}
				}else{
					Promotion=Fields.NO_SALES_PROMOTION;//"暂无促销信息";	
				}
			}
		}
		if(prom.length()>0){
			String pm = prom.toString();
			Promotion = pm.substring(0, pm.length() - 2);
		} 
		if(Promotion.contains("{")){
			Promotion="";	
		}
		return Promotion;

	}
	//产地拆分
	public String productLocation_message(String location){
		String message="";
		for(int i=0;i<publicClass.location().size();i++){
			if(location.contains(publicClass.location().get(i))){
				message=publicClass.location().get(i);
			}
			if(Validation.isEmpty(message)){
				if(location.contains(Fields.LOCATION_LIBERO)){
					message=Fields.LOCATION_RUIDIAN;
				}else if(location.contains(Fields.LOCATION_BEILAMI)){
					message=Fields.LOCATION_AODALIY;
				}else if(location.contains(Fields.LOCATION_MEIZANCHEN)){
					message=Fields.LOCATION_MEIGUO;
				}else if(location.contains(Fields.LOCATION_FRISO)){
					message=Fields.LOCATION_HELAN;
				}else if(location.contains(Fields.LOCATION_COW)){
					message=Fields.LOCATION_XINXILAN;	
				}else if(location.contains(Fields.LOCATION_AOZHOUS)){
					message=Fields.LOCATION_AOZHOU;
				}
			}
		}
		return message;
	}

	//解析商品上架时间
	@SuppressWarnings("unchecked")
	public int jdGoodsOnTime(String item,Craw_goods_Info goodsd_info,int page,String database,String goodsId) throws Exception{
		Map<String,Object> insertItemTime=new HashMap<String,Object>();
		List<Map<String,Object>> insertprices = Lists.newArrayList();
		JSONObject currPriceJson = JSONObject.fromObject(item);
		int lastPage=currPriceJson.getInt("maxPage");
		//List<Craw_goods_Fixed_Info>fiexe=crawlerPublicClassService.whetherGoodsShelves(goodsd_info,database); //判断此商品上架时间是否已经存在
		//	if(fiexe.size()==0){
		if(page==lastPage || lastPage==0){
			JSONArray json = currPriceJson.getJSONArray("comments");
			Craw_goods_Fixed_Info  info=new Craw_goods_Fixed_Info();
			info.setCust_account_id(Integer.valueOf(goodsd_info.getCust_account_id()));
			info.setCust_keyword_id(Integer.valueOf(goodsd_info.getCust_keyword_id()));
			info.setEgoodsid(goodsd_info.getEgoodsId());
			info.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
			info.setPlatform_name_en(goodsd_info.getPlatform_name_en());
			info.setFixed_status(Fields.STATUS_COUNT_1);
			info.setGoodsId(goodsId);
			if(lastPage!=0){
				try{info.setFirst_reviewtime(JSONObject.fromObject(json.toArray()[json.size()-1]).getString("referenceTime"));	}catch(Exception e){}		
			}
			if(StringUtil.isNotEmpty(info.getFirst_reviewtime())){
				insertItemTime=BeanMapUtil.convertBean2MapWithUnderscoreName(info);
				insertprices.add(insertItemTime); 
				batchInsertData.insertIntoData(insertprices,database,Fields.TABLE_CRAW_GOODS_FIXED_INFO);//插入商品上架时间	
			}  
		}else{
			return lastPage; 
		}
		if(page==1){
			CommentsGoods(goodsd_info.getBatch_time(),goodsd_info,goodsd_info.getGoodsId(),currPriceJson,database,Fields.STATUS_COUNT_1);//获取商品评论数 
		}
		return lastPage;
	} 

	//获取商品评论数
	@SuppressWarnings("unchecked")
	public int CommentsGoods(String time,Craw_goods_Info goodsd_info,String goodsId,JSONObject currPriceJson,String database,int comment_status){
		List<Map<String,Object>> listComment = Lists.newArrayList();
		Map<String,Object> insertItemTime=new HashMap<String,Object>();
		int count=1;
		try {
			goodsd_info.setBatch_time(time);
			Craw_goods_comment_Info comment=new Craw_goods_comment_Info();
			comment.setBatch_time(time);
			comment.setCust_keyword_id(Integer.valueOf(goodsd_info.getCust_keyword_id()));
			comment.setEgoodsId(goodsd_info.getEgoodsId());
			comment.setGoodsId(goodsId);
			comment.setComment_status(comment_status);
			comment.setPlatform_name_en(goodsd_info.getPlatform_name_en());
			comment.setTtl_comment_num(currPriceJson.getJSONObject("productCommentSummary").getString("commentCount"));//总评论数
			comment.setPos_comment_num(currPriceJson.getJSONObject("productCommentSummary").getString("goodCount"));////好评率
			comment.setNeu_comment_num(currPriceJson.getJSONObject("productCommentSummary").getString("generalCount"));//中评率
			comment.setNeg_comment_num(currPriceJson.getJSONObject("productCommentSummary").getString("poorCount"));//差评率
			comment.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
			comment.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
			insertItemTime=BeanMapUtil.convertBean2MapWithUnderscoreName(comment);
			listComment.add(insertItemTime); 
			batchInsertData.insertIntoData(listComment,database,Fields.CRAW_GOODS_COMMENT_INFO);//插入商品评论
		}catch (Exception e) {
			count=0;
		}
		return count; 
	}
	//搜索商品价格
	@SuppressWarnings("unchecked")
	public Map<String,Object>serchPrice(String timeDate,String dataType,String StringMessage,String database,String keyworId,String dataEgoodsId) throws Exception{
		StringMessage="{\"list\":"+StringMessage+"}";
		JSONObject jsonObject = JSONObject.fromObject(StringMessage);
		JSONArray jsonList = jsonObject.getJSONArray("list");
		// List<Craw_goods_Price_Info>dataList=new ArrayList<Craw_goods_Price_Info>();
		List<Map<String,Object>> listComment = Lists.newArrayList();
		Map<String,Object> insertItemTime=new HashMap<String,Object>();
		String keywords[]=keyworId.split(",");
		for(int i=0;i<jsonList.size();i++){
			JSONObject itemJson = JSONObject.fromObject(jsonList.toArray()[i].toString());
			String egoodsId=itemJson.getString("id").replace("J_", "").trim();
			String goodsId = StringHelper.encryptByString(egoodsId + Fields.PLATFORM_JD);//生成goodSId
			Craw_goods_Price_Info price=new Craw_goods_Price_Info();
			price.setBatch_time(timeDate);
			price.setChannel(Fields.CLIENT_MOBILE);
			price.setCurrent_price(itemJson.getString("p"));
			price.setOriginal_price(itemJson.getString("op"));
			price.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
			price.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
			price.setGoodsid(goodsId);
			price.setSKUid(egoodsId);
			try {
				price.setCust_keyword_id(keywords[i].toString());
			} catch (Exception e) {
				price.setCust_keyword_id("");
			}
			insertItemTime=BeanMapUtil.convertBean2MapWithUnderscoreName(price);
			listComment.add(insertItemTime);
		}
		batchInsertData.insertIntoData(listComment,database,Fields.TABLE_CRAW_GOODS_PRICE_INFO);//插入价格
		//crawlerPublicClassService.jdupdateStatus(dataEgoodsId.toString(),database);//更新已经抓取商品的状态
		// crawlerPublicClassService.priceSynchronousData(dataEgoodsId,database);//同步数据		
		return insertItemTime;
	}
	//搜索商品评论数
	@SuppressWarnings("unchecked")
	public Map<String,Object>serchCommentsGoods(String timeDate,String dataType,String StringMessage,String database,String keyworId,String dataEgoodsId) throws Exception{
		JSONObject jsonObject = JSONObject.fromObject(StringMessage);
		JSONArray jsonList = jsonObject.getJSONArray("CommentsCount");
		List<Map<String,Object>> listComment = Lists.newArrayList();
		Map<String,Object> insertItemTime=new HashMap<String,Object>();
		String keywords[]=keyworId.split(",");
		for(int ii=0;ii<jsonList.size();ii++){
			JSONObject itemJson = JSONObject.fromObject(jsonList.toArray()[ii].toString());
			String egoodsId=itemJson.getString("SkuId").trim();
			String goodsId = StringHelper.encryptByString(egoodsId + Fields.PLATFORM_JD);//生成goodSId
			Craw_goods_comment_Info comment=new Craw_goods_comment_Info();
			comment.setBatch_time(timeDate);
			try {
				comment.setCust_keyword_id(Integer.valueOf(keywords[ii].toString()));
			} catch (Exception e) {
				comment.setCust_keyword_id(Integer.valueOf(keyworId));
			}
			comment.setEgoodsId(egoodsId);
			comment.setGoodsId(goodsId);
			comment.setComment_status(Fields.STATUS_COUNT_1);
			comment.setPlatform_name_en(dataType);
			comment.setTtl_comment_num(itemJson.getString("CommentCount"));//总评论数
			comment.setPos_comment_num(itemJson.getString("GoodCount"));////好评率
			comment.setNeu_comment_num(itemJson.getString("GeneralCount"));//中评率
			comment.setNeg_comment_num(itemJson.getString("PoorCount"));//差评率
			comment.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
			comment.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
			insertItemTime=BeanMapUtil.convertBean2MapWithUnderscoreName(comment);
			listComment.add(insertItemTime);
		}
		batchInsertData.insertIntoData(listComment,database,Fields.CRAW_GOODS_COMMENT_INFO);//插入商品评论	
		return insertItemTime;
	}
}
