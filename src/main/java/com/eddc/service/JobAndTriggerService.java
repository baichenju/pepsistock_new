package com.eddc.service;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eddc.framework.TargetDataSource;
import com.eddc.mapper.JobAndTriggerMapper;
import com.eddc.model.Craw_keywords_temp_Info_forsearch_history;
import com.eddc.model.JobAndTrigger;
import com.eddc.model.QrtzCrawlerTable;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
@Service
public class JobAndTriggerService { 
	@Autowired
	private JobAndTriggerMapper jobAndTriggerMapper;
	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(JobAndTriggerService.class); 
	//private  SimpleDateFormat data=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public PageInfo<JobAndTrigger> getJobAndTriggerDetails(int pageNum, int pageSize) {
		PageHelper.startPage(pageNum, pageSize);
		List<JobAndTrigger> list = jobAndTriggerMapper.getJobAndTriggerDetails();
		PageInfo<JobAndTrigger> page = new PageInfo<JobAndTrigger>(list);
		return page;
	}
	public List<QrtzCrawlerTable>queryJob(String job_Name,String ip,String docker,String platform){
		List<QrtzCrawlerTable> list = jobAndTriggerMapper.queryJob(job_Name,ip,docker);
		return list;	
	}
	
	@TargetDataSource
	public List<Craw_keywords_temp_Info_forsearch_history>generationGrabbing(String pageNum,String databases){
		List<Craw_keywords_temp_Info_forsearch_history>list=new ArrayList<Craw_keywords_temp_Info_forsearch_history>();
		try {
			list=jobAndTriggerMapper.generationGrabbing();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@TargetDataSource
	public List<Craw_keywords_temp_Info_forsearch_history>grabTotalNumberGoodsData(String pageNum,String databases){
		List<Craw_keywords_temp_Info_forsearch_history>list=new ArrayList<Craw_keywords_temp_Info_forsearch_history>();
		try {
			list=jobAndTriggerMapper.grabTotalNumberGoodsData();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	@TargetDataSource
	public int crawlergoodsStatus(String databasess,String databases){
		
		return jobAndTriggerMapper.crawlergoodsStatus();
	}
	



	public void InsertQrtzCrawl(QrtzCrawlerTable qrtz){
		jobAndTriggerMapper.InsertQrtzCrawl(qrtz);	
	}
	public void UpdateQrtzCrawl(String job_name,String status){
		jobAndTriggerMapper.UpdateQrtzCrawl(job_name,status);
	}
	public void DeleteQrtzCrawl(String job_name){
		jobAndTriggerMapper.DeleteQrtzCrawl(job_name);
	}
	public int updateIP(String JOB_NAME,String IP){
		return jobAndTriggerMapper.updateIP(JOB_NAME,IP);
	}
	public int update_Datasource(String JOB_NAME,String DATASOURCE){
		return jobAndTriggerMapper.update_Datasource(JOB_NAME,DATASOURCE);
	}
	public int Update_reschedule_job_thread(String JOB_NAME,String THREAD_SUM,String PLATFORM){
		return jobAndTriggerMapper.Update_reschedule_job_thread(JOB_NAME,THREAD_SUM,PLATFORM);
	}
	public int Update_reschedule_job_docker(String JOB_NAME,String DOCKER,String PLATFORM){
		return jobAndTriggerMapper.updateIP(JOB_NAME,DOCKER);
	}
	public int reschedule_job_inventory(String JOB_NAME,String INVENTORY,String PLATFORM){
		return jobAndTriggerMapper.reschedule_job_inventory(JOB_NAME,INVENTORY,PLATFORM);
	}
	public int 	reschedule_job_client (String JOB_NAME,String CLIENT,String PLATFORM){
		return jobAndTriggerMapper.reschedule_job_client(JOB_NAME,CLIENT,PLATFORM);
	}
	public int 	reschedule_job_storage (String JOB_NAME,String storage,String PLATFORM){
		return jobAndTriggerMapper.reschedule_job_storage(JOB_NAME,storage,PLATFORM);
	}
	public int 	reschedule_job_database (String JOB_NAME,String database,String PLATFORM){
		return jobAndTriggerMapper.reschedule_job_database(JOB_NAME,database,PLATFORM);
	}
	public int 	reschedule_job_docker (String JOB_NAME,String DOCKER,String PLATFORM){
		return jobAndTriggerMapper.reschedule_job_docker(JOB_NAME,DOCKER,PLATFORM);
	}
	public int 	reschedule_job_sums (String JOB_NAME,String SUMS,String PLATFORM){
		return jobAndTriggerMapper.reschedule_job_sums(JOB_NAME,SUMS,PLATFORM);
	}
	
}
