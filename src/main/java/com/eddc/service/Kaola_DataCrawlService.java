package com.eddc.service;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.eddc.model.CommodityPrices;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.model.Craw_goods_InfoVO;
import com.eddc.model.Craw_goods_Price_Info;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.StringHelper;
import com.eddc.util.Validation;
import com.github.pagehelper.util.StringUtil;
@Service
public class Kaola_DataCrawlService {
	@Autowired
	CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	HttpClientService httpClientService;
	private static Logger logger = LoggerFactory.getLogger(Kaola_DataCrawlService.class); 
	//private static SimpleDateFormat forts=new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
	/**
	 * 项目名称：springmvc_crawler
	 * 类名称：GoodsProductDetails
	 * 类描述：解析考拉页面信息
	 * 创建人：Jack
	 * 创建时间：2017年06月13日 上午10:50:10
	 * 修改备注：
	 *
	 * @throws SQLException
	 * @version
	 */
	public Map<String, Object> KaolaParseItemPage(String item, CrawKeywordsInfo wordsInfo, Map<String, String> map,String database,String storage ) {
		Map<String, String> data = new HashMap<String, String>();
		Map<String, Object> dataitem= new HashMap<String, Object>();
		data.putAll(map);
		String shopType = Fields.PROPRIETARY;
		String sellerName =Fields.PROPRIETARY; //店铺
		String postageFree = Fields.DONTPACK_MAIL; //是否包邮
		String inventory =Fields.IN_STOCK; //有货无货
		String promotion = ""; //促销
		String image = ""; //图片路径
		String presentPrice = ""; //现价
		String originalPrice = ""; //原价
		String delivery_place = ""; //发货地址delivery_place
		String platformGoodsName="";
		String product_location="";//原产地
		String StringMessage="";//请求促销详情
		String categoryId="";//类别Id
		long time=System.currentTimeMillis();//设置毫秒时间
		Document docs = Jsoup.parse(item.toString());
		try {
			platformGoodsName=docs.title().replace("'", "`");//商品名称
			if(StringUtil.isEmpty(platformGoodsName)){
				platformGoodsName=StringHelper.getResultByReg(docs.toString(),"qzoneContent: \"([^,\"]+)\",");
			}
			image =docs.getElementsByClass("v-img").attr("src").trim();//商品图片
			if(docs.toString().contains(Fields.NETEASE_PROPRIETARY)){
				sellerName=Fields.NETEASE_PROPRIETARY;
				shopType = Fields.YES_PROPRIETARY;
			}
			map.put("urlRef", Fields.KAOLA_APP_URL+wordsInfo.getCust_keyword_name());
			map.put("url",Fields.KAOLA_APP_URL_PROMOTION_NEW.toString().replace("EGOODSID",wordsInfo.getCust_keyword_name())+"&t="+time );
			StringMessage=httpClientService.GoodsProductItem(map);//抓取促销 
			if(Validation.isEmpty(StringMessage)){
				StringMessage=httpClientService.requestData(map);//抓取促销	
			}  
			if(StringMessage.toString().contains("marketPrice")){
				JSONObject currPriceJson = JSONObject.fromObject(StringMessage);
				originalPrice=currPriceJson.getJSONObject("data").getJSONObject("skuPrice").getString("marketPrice");//原价
				presentPrice=currPriceJson.getJSONObject("data").getJSONObject("skuPrice").getString("currentPrice");//现价
				inventory=currPriceJson.getJSONObject("data").getString("goodsCurrentStore");//库存
				delivery_place=currPriceJson.getJSONObject("data").getString("warehouseCityShow");//商品卖家地址
				try {
					JSONArray json = currPriceJson.getJSONObject("data").getJSONArray("promotionList");//促销
					for(int i=0;i<json.size();i++){
						JSONObject promotionJson=JSONObject.fromObject(json.toArray()[i]);
						if(StringUtil.isEmpty(promotion)){
							promotion=promotionJson.getString("promotionContent");
						}else{
							promotion+="&&"+promotionJson.getString("promotionContent");
						}
					}	
				} catch (Exception e) {
					promotion="";
				}
				JSONArray contentList = currPriceJson.getJSONObject("data").getJSONObject("goodsDeclare").getJSONArray("contentList");//vip 优惠
				if(contentList.size()>0){
					promotion+="&&"+JSONObject.fromObject(contentList.toArray()[0]).getString("detailTitle");
				}
				postageFree=currPriceJson.getJSONObject("data").getJSONObject("goodsPostageInfo").getString("postageShowStr");//运费
				if(postageFree.contains(Fields.FULL)){
					postageFree=Fields.DONTPACK_MAIL;
				}else{
					postageFree=Fields.PACK_MAIL; 
				} 
			}
			data.put("originalPrice", originalPrice.replaceAll("\"", ""));//原价
			data.put("currentPrice", presentPrice.replaceAll("\"", ""));//现价
			data.put("shopName", sellerName);//店铺名称
			data.put("region",delivery_place);//卖家位置
			data.put("platform_shoptype", shopType.replaceAll("\"", ""));//平台商店类型
			data.put("sellerName", sellerName);//卖家店铺名称
			data.put("picturl", image.replaceAll("\"", ""));//图片url 
			data.put("goodsName", platformGoodsName.replaceAll("\"", "").replaceAll("'", "`"));//商品名称
			data.put("delivery_place",Fields.SHANGHAI);//交易地址
			data.put("inventory", inventory);//商品库存
			data.put("postageFree",postageFree.replaceAll("\"", ""));//是不包邮
			data.put("skuId", wordsInfo.getCust_keyword_name());//商品sku
			data.put("promotion", promotion);//促销
			data.put("accountId",wordsInfo.getCust_account_id());
			data.put("goodsId", data.get("goodsId"));
			data.put("egoodsId", wordsInfo.getCust_keyword_name());
			data.put("timeDate", data.get("timeDate"));
			data.put("keywordId",wordsInfo.getCust_keyword_id());
			data.put("platform_name",data.get("platform"));
			data.put("goodsUrl",Fields.KAOLA_PC_URL+wordsInfo.getCust_keyword_name()+ ".html");
			data.put("channel",Fields.CLIENT_MOBILE);
			data.put("feature1", "1");
			data.put("product_location", product_location);//产地 
			data.put("subCatId", categoryId);//商品类别Id
			data.put("rateNum", null);//商品总评论数 
			data.put("transactNum", null);//月销
			data.put("message", null);   
			data.put("deposit", null);
			data.put("coupons", null);
			data.put("sale_qty", null);
			data.put("reserve_num", null);
			data.put("shopid", null);//商品Id
			data.put("sellerId", null);
			Craw_goods_InfoVO info =crawlerPublicClassService.itemFieldsData(data,database,storage); //商品详情插入数据库 
			Craw_goods_Price_Info price=crawlerPublicClassService.parseItemPrice(data,wordsInfo.getCust_keyword_id(),data.get("goodsId"),data.get("timeDate"),data.get("platform"),Fields.STATUS_COUNT_1,database,storage);//APP端价格插入数据库
			dataitem.put(Fields.TABLE_CRAW_GOODS_PRICE_INFO, price);
			dataitem.put(Fields.TABLE_CRAW_GOODS_INFO, info);
			dataitem.putAll(data);
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("插入信息失败------" + e.getMessage() + "----------------" + SimpleDate.SimpleDateFormatData().format(new Date()));
		}
		return dataitem;
	}

	/**
	 * 项目名称：springmvc_crawler
	 * 类名称：GoodsProductDetails
	 * 类描述：解析考拉PC页面价格信息
	 * 创建人：Jack
	 * 创建时间：2017年06月13日 上午10:50:10
	 * 修改备注：
	 *
	 * @throws SQLException
	 * @version
	 */
	public Map<String, Object> ResolutionCellPhonePrices(CommodityPrices commodityPrices, String AppMessage) {
		Map<String, Object> data = new HashMap<String, Object>();
		String promotion = "";
		String presentPrice = "";
		String originalPrice = "";
		try {
			if(!Validation.isEmpty(AppMessage)){
				JSONObject jsonObject = JSONObject.fromObject(AppMessage);
				JSONArray json = jsonObject.getJSONObject("data").getJSONArray("skuDetailList");
				if(json.size()>0){
					presentPrice=JSONObject.fromObject(json.toArray()[0]).getJSONObject("skuPrice").getString("currentPrice").toString();//现价
					originalPrice=JSONObject.fromObject(json.toArray()[0]).getJSONObject("skuPrice").getString("marketPrice").toString();//原价 
					promotion=jsonObject.getJSONObject("data").getJSONObject("goodsDeclare").getString("title").replace("[","").replace("]","").replace("\"","").trim();//促销
					data.put("channel", Fields.CLIENT_PC);
					data.put("currentPrice",presentPrice);
					data.put("originalPrice", originalPrice);
					data.put("promotion", promotion);
					data.put("goodsId", commodityPrices.getGoodsid());
					data.put("keywordId", commodityPrices.getCust_keyword_id());
					data.put("SKUid",commodityPrices.getEgoodsId());
					Craw_goods_Price_Info price=crawlerPublicClassService.commdityPricesData(commodityPrices,data);//插入PC端价格
					data.put(Fields.TABLE_CRAW_GOODS_PRICE_INFO, price);
				}
				/**
				presentPrice=jsonObject.getJSONObject("data").getJSONObject("goodsPromotionResult").getString("currentPrice");//现价
				originalPrice=jsonObject.getJSONObject("data").getJSONObject("goodsPromotionResult").getString("marketPrice");//原价 
				promotion_message=jsonObject.getJSONObject("data").getJSONObject("goodsPromotionResult").getString("memberGoodsDiscount");//会员优惠价
				promotion=jsonObject.getJSONObject("data").getJSONObject("goodsPromotionResult").getString("wapZhendanPromotionTitle");//促销
				promotion=promotion_message.replaceAll("null", "").trim()+promotion.replaceAll("null", "").trim();
				 **/
			}
			
		} catch (Exception e) {
			logger.info("解析考拉PC端信息失败------" + e.toString() + "----------------" + SimpleDate.SimpleDateFormatData().format(new Date()));
		}
		return data ;

	}
}
