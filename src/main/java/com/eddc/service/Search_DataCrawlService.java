package com.eddc.service;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.eddc.framework.TargetDataSource;
import com.eddc.mapper.CrawlerPublicClassMapper;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.model.Craw_goods_InfoVO;
import com.eddc.model.Craw_goods_Price_InfoVO;
import com.eddc.model.Craw_goods_Vendor_Info;
import com.eddc.model.Craw_goods_Vendor_price_Info;
import com.eddc.model.Craw_goods_comment_Info;
import com.eddc.model.Craw_goods_crowdfunding_Info;
import com.eddc.model.Craw_keywords_temp_Info_forsearch;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.task.Amazon_DataCrawlTask;
import com.eddc.task.Jd_DataCrawlTask;
import com.eddc.util.BatchInsertData;
import com.eddc.util.BeanMapUtil;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.StringHelper;
import com.eddc.util.Validation;
import com.github.pagehelper.util.StringUtil;
import com.google.common.collect.Lists;
@Service
public class Search_DataCrawlService {
	@Autowired
	private CrawlerPublicClassMapper crawlerPublicClassMapper;
	@Autowired
	CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	HttpClientService httpClientService;
	@Autowired
	Jd_DataCrawlTask jd_DataCrawlTask;
	@Autowired
	Amazon_DataCrawlTask amazon_DataCrawlTask;
	@Autowired
	private BatchInsertData batchInsertData;
	private static Logger logger = LoggerFactory.getLogger(Search_DataCrawlService.class); 
	//同步数据
	@TargetDataSource
	public void SynchronousData(List<QrtzCrawlerTable>listjobName,String database, String className,int sum){
	try {crawlerPublicClassMapper.SearchSynchronousData(listjobName.get(0).getPlatform(),listjobName.get(0).getUser_Id(),className,sum);} catch (Exception e) {}
	}
 
	/**
	 * 项目名称：springmvc_crawler
	 * 类名称：GoodsProductDetails
	 * 类描述：解析页面信息
	 * 创建人：Jack
	 * 创建时间：2017年06月13日 上午10:50:10
	 * 修改备注：
	 *
	 * @throws SQLException
	 * @version
	 * @throws ClassNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public String  ParseItemPage(String item, CrawKeywordsInfo wordsInfo, Map<String, String> map) throws ClassNotFoundException {
		Map<String, String> data = new HashMap<String, String>();
		List<Element> itemPage=new ArrayList<Element>();
		Map<String,Object>itemData=new HashMap<String,Object>();
		JSONArray priceInfoJson=new JSONArray();
		data.putAll(map);
		String disable="";
		try{ 
			if(wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_TMALL_EN)||wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_TMALL_GLOBAL_EN) ||wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_TAOBAO_EN)){
				itemData=tmallDetailsData(item,wordsInfo,map);
				itemPage=(List<Element>)itemData.get(Fields.ITMEPAGE);
				disable=itemData.get(Fields.DISABLE).toString();
				priceInfoJson=(JSONArray) itemData.get(Fields.PRICEINFOJSON);
				if(priceInfoJson.size()>0){
					priceInfoJson=(JSONArray) itemData.get(Fields.PRICEINFOJSON);	
				}
			}else if(wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_JD)){
				itemData=jdDetailsData(item,wordsInfo,map);
				itemPage=(List<Element>)itemData.get(Fields.ITMEPAGE);
				disable=itemData.get(Fields.DISABLE).toString();
				priceInfoJson=(JSONArray) itemData.get(Fields.PRICEINFOJSON);
				if(priceInfoJson.size()>0){
					priceInfoJson=(JSONArray) itemData.get(Fields.PRICEINFOJSON);	
				}
			}else if(wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_YHD)){
				itemData=yhdDetailsData(item,wordsInfo,map);//map
				itemPage=(List<Element>)itemData.get(Fields.ITMEPAGE);
				disable=itemData.get(Fields.DISABLE).toString();
				priceInfoJson=(JSONArray) itemData.get(Fields.PRICEINFOJSON);
				if(priceInfoJson.size()>0){
					priceInfoJson=(JSONArray) itemData.get(Fields.PRICEINFOJSON);	
				}
			}else if(wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_SUNING)){
				itemData=suningDetailsData(item,wordsInfo,map.get(Fields.COUNT));
				itemPage=(List<Element>)itemData.get(Fields.ITMEPAGE);
				disable=itemData.get(Fields.DISABLE).toString();
				priceInfoJson=(JSONArray) itemData.get(Fields.PRICEINFOJSON);
				if(priceInfoJson.size()>0){
					priceInfoJson=(JSONArray) itemData.get(Fields.PRICEINFOJSON);	
				}
			}else if(wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_AMAZON)||wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_AMAZON_UN)){
				itemData=amzonDetailsData(item,wordsInfo,map.get(Fields.COUNT));
				itemPage=(List<Element>)itemData.get(Fields.ITMEPAGE);
				disable=itemData.get(Fields.DISABLE).toString();
				priceInfoJson=(JSONArray) itemData.get(Fields.PRICEINFOJSON);
				if(priceInfoJson.size()>0){
					priceInfoJson=(JSONArray) itemData.get(Fields.PRICEINFOJSON);	
				}
			}else if(wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.platform_1688)){
				itemData=Details_1688Data(item,wordsInfo,map.get(Fields.COUNT));
				itemPage=(List<Element>)itemData.get(Fields.ITMEPAGE);
				priceInfoJson=(JSONArray) itemData.get(Fields.PRICEINFOJSON);
				disable=itemData.get(Fields.DISABLE).toString();
				if(priceInfoJson.size()>0){
					priceInfoJson=(JSONArray) itemData.get(Fields.PRICEINFOJSON);	
					if(disable.equalsIgnoreCase((map.get("count").toString()))){
						disable=Fields.DISABLE;
					}
				}
			}else if(wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_TAOBAO_ZC)){
				itemData=taobaoZcDetailsData(item,wordsInfo,map.get(Fields.COUNT));
				itemPage=(List<Element>)itemData.get(Fields.ITMEPAGE);
				disable=itemData.get(Fields.DISABLE).toString();
				priceInfoJson=(JSONArray) itemData.get(Fields.PRICEINFOJSON);
				if(priceInfoJson.size()>0){
					priceInfoJson=(JSONArray) itemData.get(Fields.PRICEINFOJSON);	
				}
			}else if(wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_JD_ZC)){
				itemData=jdZcDetailsData(item,wordsInfo,map.get(Fields.COUNT));
				itemPage=(List<Element>)itemData.get(Fields.ITMEPAGE);
				disable=itemData.get(Fields.DISABLE).toString();
				priceInfoJson=(JSONArray) itemData.get(Fields.PRICEINFOJSON);
				if(priceInfoJson.size()>0){
					priceInfoJson=(JSONArray) itemData.get(Fields.PRICEINFOJSON);	
				}
			}else if(wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_KICKSTARTER)){
				itemData=kickstarterDetailsData(item,wordsInfo,map.get(Fields.COUNT));
				itemPage=(List<Element>)itemData.get(Fields.ITMEPAGE);
				disable=itemData.get(Fields.DISABLE).toString();
				priceInfoJson=(JSONArray) itemData.get(Fields.PRICEINFOJSON);
				if(priceInfoJson.size()>0){
					priceInfoJson=(JSONArray) itemData.get(Fields.PRICEINFOJSON);	
				}
			}else{
				disable=Fields.DISABLE;
				return disable;
			} 
			insertData(itemPage,map.get("database").toString(),wordsInfo,map,priceInfoJson); //插入解析数据   
		} catch (Exception e) {
			e.printStackTrace();
			disable="";
		} 
		return disable;
	}

	//天猫数据
	public  Map<String,Object> tmallDetailsData(String item, CrawKeywordsInfo wordsInfo,Map<String,String> map){
		List<Element> itemPage=new ArrayList<Element>();//map.get(Fields.COUNT)
		Map<String,Object> data=new HashMap<String,Object>();
		JSONArray priceInfoJson=new JSONArray(); List<Element>messages=new ArrayList<Element>();
		String disable="";
		if(item.contains("J_TItems")){
			item=item.toString().replaceAll("&quot;","").replace("\\", "").replace("//", ""); 
			Document docs =Jsoup.parse(item);
			itemPage =docs.getElementsByClass("detail");
			messages=docs.getElementsByClass("J_TGoldData");
			if(Validation.isNotEmpty(docs.getElementsByClass("ui-page-s-len").text())){
				String page=docs.getElementsByClass("ui-page-s-len").text().split("/")[1];
				if(map.get(Fields.COUNT).equalsIgnoreCase(page)){
					disable=Fields.DISABLE; 	
				}
			}
		}else if(item.contains("J-listContainer")){//天猫
			Document docs =Jsoup.parse(item); 
			if(docs.toString().contains("product-list")){
				String totalPage=docs.getElementById("totalPage").val();
				if(map.get(Fields.COUNT).equalsIgnoreCase(totalPage)){
					disable=Fields.DISABLE; 
				}
				itemPage=docs.getElementById("J-listContainer").getElementsByClass("product-list").get(0).getElementsByClass("product");
			} 
		}else if(item.contains("i2iTags")){//tmall taobao search
			Document docs =Jsoup.parse(item);
			String itemPageMessage=docs.toString().substring(+docs.toString().indexOf("itemlist"),docs.toString().lastIndexOf(",\"bottomsearch\":")).toString(); 
			itemPageMessage="{\""+itemPageMessage+"}";

			JSONObject currPriceJson = JSONObject.fromObject(itemPageMessage);
			priceInfoJson = currPriceJson.getJSONObject("itemlist").getJSONObject("data").getJSONArray("auctions");
			if(priceInfoJson.size()==0){
				disable=Fields.DISABLE;  
			}
		}else if(item.contains("J_ItemList")){//天猫指定类目搜索解析
			Document docs =Jsoup.parse(item);
			itemPage=docs.getElementsByClass("product  ");
			if(itemPage.size()==0){
				disable=Fields.DISABLE;   
			}
		}else if(item.contains("listItem")){//手机端搜索商品
			JSONObject jsonObject = JSONObject.fromObject(item);
			String totalPage=jsonObject.getString("totalPage");
			if(map.get(Fields.COUNT).equalsIgnoreCase(totalPage)){
				disable=Fields.DISABLE; 
			}
			priceInfoJson = jsonObject.getJSONArray("listItem");
			if(priceInfoJson.size()==0){
				disable=Fields.DISABLE;  
			}
		}else if(item.contains("total_page")){//tmall手机端根据店铺搜索商品
			String itemPageMessage=item.toString().substring(item.indexOf("{"), item.lastIndexOf("}")+1);
			JSONObject currPriceJson = JSONObject.fromObject(itemPageMessage);
			priceInfoJson = currPriceJson.getJSONArray("items");
			if(priceInfoJson.size()==0 || map.get(Fields.COUNT).equalsIgnoreCase(currPriceJson.getString("total_page"))){
				disable=Fields.DISABLE;  
			}
		}
		data.put(Fields.PRICEINFOJSON, priceInfoJson);
		data.put(Fields.DISABLE, disable);
		data.put(Fields.ITMEPAGE, itemPage);
		data.put(Fields.MESSAGEIMAGE, messages);
		return data;
	}
	//京东数据
	public  Map<String,Object> jdDetailsData(String item, CrawKeywordsInfo wordsInfo,Map<String,String>map){
		List<Element> itemPage=new ArrayList<Element>();
		Map<String,Object> data=new HashMap<String,Object>();
		JSONArray priceInfoJson=new JSONArray();
		String disable="";
		if(item.contains("J_goodsList")||item.contains("gl-item")){//京东pc 搜索 
			Document docs =Jsoup.parse(item);
			itemPage=docs.getElementsByClass("gl-item");
			if(wordsInfo.getCust_keyword_url().contains("search.jd.com")){
				if(itemPage.size()==0){
					disable=Fields.DISABLE;   
				}
			}else if(wordsInfo.getCust_keyword_url().contains("list.jd.com")){
				if(itemPage.size()==0 || StringUtil.isEmpty(docs.getElementsByClass("p-num").toString())){// 
					disable=Fields.DISABLE;  
				}   
			}
			if(StringUtil.isNotEmpty(docs.getElementsByClass("fp-text").toString())){
				if(map.get(Fields.COUNT).trim().contains(docs.getElementsByClass("fp-text").get(0).getElementsByTag("i").text())){
					disable=Fields.DISABLE;  
				}else if(Integer.valueOf(map.get(Fields.COUNT))>Integer.valueOf(docs.getElementsByClass("fp-text").get(0).getElementsByTag("i").text())){
					disable=Fields.DISABLE;
				}	
			}
		}else if(item.contains("wareCount") || item.contains("wareList")){
			String itmes=item.toString().replace("\\\"", "\"");
			String stockDesc=StringHelper.nullToString(StringHelper.getResultByReg(itmes,"\"wareCount\":([^}}]+)"));
			if(stockDesc.equalsIgnoreCase(Fields.STATUS_OFF) || !stockDesc.contains("wareId")){ 
				disable=Fields.DISABLE;   
			}else{
				String message=itmes.substring(itmes.indexOf("\"wareList\":{\""), itmes.lastIndexOf("}]}}\"}")+4);
				JSONObject currPriceJsons = JSONObject.fromObject("{"+message);
				priceInfoJson= currPriceJsons.getJSONObject("wareList").getJSONArray("wareList");   
			}
		}
		
		if(item.contains(Fields.SHOPNAME_NO)){
			disable=Fields.DISABLE;   
		}
		if(item.contains(Fields.SEARCH_PAGE)){
			String page=item.toString().substring(item.toString().indexOf(Fields.SEARCH_PAGE), item.toString().lastIndexOf("SEARCH.sort_html"));
			if(page.contains(",")){
				page=page.split(",")[1].toString().trim();
				int count=Integer.valueOf(map.get("count").toString());
				count=count-2;
				if(Integer.valueOf(page)==count){
					disable=Fields.DISABLE; 
				}
			}
				
		}
		//System.out.println(item);
		data.put(Fields.PRICEINFOJSON, priceInfoJson);
		data.put(Fields.DISABLE, disable);
		data.put(Fields.ITMEPAGE, itemPage);
		return data;	
	}
	//一号店解析
	public  Map<String,Object> yhdDetailsData(String item, CrawKeywordsInfo wordsInfo,Map<String, String> map){//
		List<Element> itemPage=new ArrayList<Element>();
		Map<String,Object> data=new HashMap<String,Object>();
		JSONArray priceInfoJson=new JSONArray();
		String disable="";
		if(item.contains("middle") && item.contains("title_box")){//yhd
			Document docs =Jsoup.parse(item);
			String touchweb=docs.getElementsByClass("touchweb_com-noresult J_ping").attr("report-eventparam").toString();
			if(touchweb.contains(Fields.NO_RESULT) || StringUtil.isEmpty(docs.select("li").text())){
				disable=Fields.DISABLE;   
			}else{ 
				itemPage=docs.select("li");
			}
		}else if(item.contains("mod_search_pro")){
			StringBuffer su=new StringBuffer();
			try {
				JSONObject currPriceJson = JSONObject.fromObject(item);
				String value=currPriceJson.get("value").toString();
				Document docs =Jsoup.parse(value);
				Elements  dom=docs.getElementsByClass("mod_search_pro");
				String dataMessage=dom.toString();
				su.append("<html><head>"+dataMessage+"</head></html>");
				Document jsoupDocument =Jsoup.parse(su.toString());
				itemPage=jsoupDocument.getElementsByClass("mod_search_pro");
			} catch (Exception e) {
				Document docs =Jsoup.parse(item);  
				Document docss =Jsoup.parse(docs.outerHtml().replace("&quot;", "")); 
				itemPage=docss.getElementsByClass("\\mod_search_pro\\");
				if(Validation.isEmpty(itemPage)){
					itemPage=docss.getElementsByClass("mod_search_pro");	
				}
				try {
					if(docs.toString().contains(Fields.NO_RESULT) ){
						disable=Fields.DISABLE; 
					}
					if(map.get("kk").equalsIgnoreCase(Fields.COUNT_0)){
						int page;
						try {
							Document pageDocm =Jsoup.parse(docs.outerHtml().replace("&quot;", "")); 
							page=Integer.valueOf(pageDocm.getElementById("\\pageCountPage\\").val().replace("\\", "").replace("/", "")+1);
						} catch (Exception e2) {
							page=Integer.valueOf(docs.getElementById("pageCountPage").val())+1;
							
						}
						
						if(map.get(Fields.COUNT).equals(String.valueOf(page))){
							disable=Fields.DISABLE;   
						}
					}
					
				 } catch (Exception e2) {
				}
			}
		}
		data.put(Fields.PRICEINFOJSON, priceInfoJson);
		data.put(Fields.DISABLE, disable);
		data.put(Fields.ITMEPAGE, itemPage);
		return data;
	}
	//苏宁
	public  Map<String,Object> suningDetailsData(String item, CrawKeywordsInfo wordsInfo,String count){
		List<Element> itemPage=new ArrayList<Element>();
		Map<String,Object> data=new HashMap<String,Object>();
		JSONArray priceInfoJson=new JSONArray();
		String disable="";
		if(item.contains("sellPoint")){
			Document docs =Jsoup.parse(item);
			itemPage=docs.getElementsByClass("border-out");
			if(itemPage.contains("border-out")){
				disable=Fields.DISABLE;      
			}
		}
		data.put(Fields.PRICEINFOJSON, priceInfoJson);
		data.put(Fields.DISABLE, disable);
		data.put(Fields.ITMEPAGE, itemPage);
		return data;
	}
	//亚马逊
	public  Map<String,Object> amzonDetailsData(String item, CrawKeywordsInfo wordsInfo,String count){
		List<Element> itemPage=new ArrayList<Element>();
		Map<String,Object> data=new HashMap<String,Object>();
		JSONArray priceInfoJson=new JSONArray();
		String disable="";
		Document docs =Jsoup.parse(item);
		if(wordsInfo.getCust_keyword_url().contains("www.amazon.com")){
			if(wordsInfo.getCust_keyword_type().contains("keyword")){
				itemPage= docs.getElementsByClass("s-result-item celwidget  ");	
			}else if(wordsInfo.getCust_keyword_type().contains("category")){
				if(docs.toString().contains(docs.getElementsByClass("a-row s-result-list-parent-container").toString())){
					if(docs.getElementsByClass("a-row s-result-list-parent-container").toString().contains("li")){
						disable=Fields.DISABLE;        
					}else if(docs.getElementsByClass("a-row s-result-list-parent-container").toString().contains("li")){
						itemPage=docs.getElementsByClass("a-row s-result-list-parent-container").get(0).select("li");
					}else if(docs.getElementsByClass("a-fixed-left-grid-col a-col-right").toString().contains("a-ordered-list a-vertical")){
						itemPage=docs.getElementsByClass("a-ordered-list a-vertical").select("li");
				    }else if(itemPage.size()==0){//zg_itemImmersion 
						itemPage=docs.getElementsByClass("zg-item-immersion").select("li");
						if(itemPage.size()==0){
							itemPage=docs.getElementsByClass("zg_itemWrapper");	
						}	
				    }
				}else if(docs.toString().contains(docs.getElementsByClass("a-ordered-list a-vertical").toString())){
					if(docs.toString().contains(docs.getElementsByClass("a-ordered-list a-vertical").toString())){
						itemPage=docs.getElementsByClass("a-ordered-list a-vertical").select("li");
						if(itemPage.size()==0){
							itemPage=docs.getElementsByClass("a-row s-result-list-parent-container").select("li");
							if(count.contains(docs.getElementsByClass("pagnDisabled").text())){
								disable=Fields.DISABLE;  	
							}
						} 
					}else{
						disable=Fields.DISABLE;  
					}
				}
			}
			if(itemPage.size()==0){
				disable=Fields.DISABLE;       
			}
		}else if(wordsInfo.getCust_keyword_url().contains("www.amazon.cn")&& item.contains("a-row s-result-list-parent-container")){
			if(docs.toString().contains(Fields.AMAZONCN_NO)){
				disable=Fields.DISABLE;       
			}else{
				itemPage=docs.getElementsByClass("a-row s-result-list-parent-container").get(1).select("li");
			}
		}else if(item.contains("zg-item-immersion") || item.contains("zg_itemWrapper")){
			if(StringUtil.isNotEmpty(docs.getElementsByClass("a-row a-spacing-top-mini").text())){
				if(docs.getElementsByClass("a-row a-spacing-top-mini").get(0).getElementsByTag("li").last().toString().contains("a-disabled a-last")){
					disable=Fields.DISABLE;
				}else{
					String page=docs.getElementsByClass("a-row a-spacing-top-mini").get(0).getElementsByTag("li").last().getElementsByTag("a").attr("href").toString().split("&pg=")[1].toString();
					if(page.contains(count)){
						disable=Fields.DISABLE; 	
					}	
				}
			} 
			itemPage=docs.getElementsByClass("zg-item-immersion");
			if(StringUtil.isEmpty(itemPage.toString())){
				itemPage=docs.getElementsByClass("zg_itemImmersion");
				if(itemPage.toString().contains(Fields.AMAZONSUA_PAGE)){
					disable=Fields.DISABLE; 	
				}
			}
		} 
		
		if(itemPage.size()==0){
			try { 
				disable=docs.getElementById("g").getElementsByTag("img").attr("alt").toString();
				if(disable.contains(Fields.AMAZONSUA_PAGE)){
					disable=Fields.DISABLE; 	  
				}
			} catch (Exception e) {
				logger.info("item 页面不存在");
			}
		}
		data.put(Fields.PRICEINFOJSON, priceInfoJson);
		data.put(Fields.DISABLE, disable);
		data.put(Fields.ITMEPAGE, itemPage);
		return data;
	}
	// 1688
	public  Map<String,Object> Details_1688Data(String item, CrawKeywordsInfo wordsInfo,String count){
		List<Element> itemPage=new ArrayList<Element>();
		Map<String,Object> data=new HashMap<String,Object>();
		JSONArray priceInfoJson=new JSONArray();
		String disable="";
		if(wordsInfo.getCust_keyword_url().contains("offer_search.htm")){
			if(item.toString().contains("s-widget-breadcrumb")){
				try {
					Document docs =Jsoup.parse(item);
					itemPage=docs.getElementsByClass("sm-offer-item sw-dpl-offer-item");
					disable=docs.getElementsByClass("sm-widget-offer").get(0).getElementsByTag("em").text();
					if(StringUtils.isEmpty(disable)){
						disable=Fields.DISABLE;  	
					}else if(Integer.valueOf(disable)<20){
						disable=Fields.DISABLE; 
					}
				} catch (Exception e) {
					logger.info("解析数据失败"+e.getMessage()); 
				}
			}else if(item.contains("i2ip4pinfo")||item.contains("offerResult")){
				item=item.substring(item.indexOf("(")+1,item.lastIndexOf(")"));
				try {
					JSONObject currPriceJson = JSONObject.fromObject(item);
					String content = currPriceJson.getJSONObject("content").getJSONObject("offerResult").getString("html");
					Document docs =Jsoup.parse(content);
					itemPage=docs.getElementsByClass("sm-offer-item sw-dpl-offer-item ");		
				} catch (Exception e) {
					String message=item.substring(item.indexOf("content"),item.lastIndexOf(")"));
					message="{\""+message;
					JSONObject currPriceJsons = JSONObject.fromObject(message);
					String contenst = currPriceJsons.getJSONObject("content").getJSONObject("offerResult").getString("html");
					Document docs =Jsoup.parse(contenst);
					itemPage=docs.getElementsByClass("sm-offer-item sw-dpl-offer-item ");		
					e.printStackTrace();
					logger.info("解析数据失败"+e.getMessage());
				}	
			}else{
				Document docs =Jsoup.parse(item);
				itemPage=docs.getElementsByClass("sm-offer ").select("li");
				if(StringUtil.isNotEmpty(docs.getElementsByClass("sw-dpl-tip-guide").text())){
					disable=Fields.DISABLE;       		
				}
			}
			if(itemPage.size()==0){
				disable=Fields.DISABLE;       	
			}
		}else if(wordsInfo.getCust_keyword_url().contains("m.1688.com")){
			  int recordCount=0; int pagen=0; int pages=0;
			 item = item.substring(item.indexOf("(")+1, item.lastIndexOf(")"));
			 JSONObject jsonObject = JSONObject.fromObject(item);
			 String page=jsonObject.getJSONObject("data").getString("found");
			 if (Integer.valueOf(page) > 0) {
				 recordCount =Integer.valueOf(page);
				 pagen = recordCount % 20;
				 pages = recordCount / 20;
				 if (pagen != 0) {
					 recordCount = pages + 1;
				 } else {
					 recordCount = pages;
				 }
			 }
			 disable=String.valueOf(recordCount);
			 priceInfoJson = jsonObject.getJSONObject("data").getJSONArray("offers");
		}else {
			Document docs =Jsoup.parse(item);
			itemPage=docs.getElementsByClass("sm-offerShopwindow3 high");
			if(itemPage.size()==0){
			 disable=Fields.DISABLE;       	
			}
		}
		data.put(Fields.PRICEINFOJSON, priceInfoJson);
		data.put(Fields.DISABLE, disable);
		data.put(Fields.ITMEPAGE, itemPage);
		return data;
	}	
	
	//封装 淘宝众筹信息
	public  Map<String,Object> taobaoZcDetailsData(String item, CrawKeywordsInfo wordsInfo,String count){
		List<Element> itemPage=new ArrayList<Element>();
		Map<String,Object> data=new HashMap<String,Object>();
		JSONArray priceInfoJson=new JSONArray();
		String disable="";
		if(item.contains("recommendPriceStr")){ 
		     JSONObject currPriceJson = JSONObject.fromObject(item);
		     priceInfoJson = currPriceJson.getJSONArray("data");
		}else{
			disable=Fields.DISABLE;
		}
		data.put(Fields.PRICEINFOJSON, priceInfoJson);
		data.put(Fields.DISABLE, disable);
		data.put(Fields.ITMEPAGE, itemPage);
		return data;
	}
	//封装 京东众筹信息
		public  Map<String,Object> jdZcDetailsData(String item, CrawKeywordsInfo wordsInfo,String count){
			Map<String,Object> data=new HashMap<String,Object>();
			List<Element> itemPage=new ArrayList<Element>();
			JSONArray priceInfoJson=new JSONArray();
			String disable="";
			if(item.contains("support")){
				Document docs =Jsoup.parse(item);
				itemPage=docs.getElementsByClass("info type_now");
				if(itemPage.size()==0){
					try {
						String page=docs.toString().substring(docs.toString().indexOf(".pagination(")+12,docs.toString().lastIndexOf("current_page"));
						page.replace(",{", "").trim();
						if(Integer.valueOf(count)>=Integer.valueOf(page)){
							disable=Fields.DISABLE;
						}
						if(page.equalsIgnoreCase(count)){
							disable=Fields.DISABLE;
						}
					 } catch (Exception e) {
				   }
					itemPage=docs.getElementsByClass("infos clearfix");
					if(docs.toString().contains("info type_succeed")){
						List<Element> itemPagedata=docs.getElementsByClass("info type_succeed");
						itemPage.addAll(itemPagedata);
					}
					if(docs.toString().contains("info type_xm")){
						List<Element> itemPagetype=docs.getElementsByClass("info type_xm");
						itemPage.addAll(itemPagetype);
					}
					if(itemPage.size()==0){
						disable=Fields.DISABLE;
					}
				} 
			}else{
				disable=Fields.DISABLE;
			}
			data.put(Fields.PRICEINFOJSON, priceInfoJson);
			data.put(Fields.DISABLE, disable);
			data.put(Fields.ITMEPAGE, itemPage);
			return data;
		}
		
		//封装 kickstarter众筹信息
		public  Map<String,Object> kickstarterDetailsData(String item, CrawKeywordsInfo wordsInfo,String count){
			Map<String,Object> data=new HashMap<String,Object>();
			List<Element> itemPage=new ArrayList<Element>();
			JSONArray priceInfoJson=new JSONArray();
			String disable="";
			if(item.contains("data-project")){
				Document docs =Jsoup.parse(item);
				itemPage=docs.getElementsByClass("js-react-proj-card col-full col-sm-12-24 col-lg-8-24");
				if(itemPage.size()==0){
					disable=Fields.DISABLE;	
				}
			}
			data.put(Fields.PRICEINFOJSON, priceInfoJson);
			data.put(Fields.DISABLE, disable);
			data.put(Fields.ITMEPAGE, itemPage);
			return data;
		}
		
	
	 //天猫解析数据
	public Map<String,Object>tmallMessage(Element element,CrawKeywordsInfo wordsInfo, JSONObject messagejson,Map<String,String>map,int sum,int number) throws Exception{
		Map<String,Object>messageData=new HashMap<String,Object>();
		String comment_count="";
		String keywordName="";  String image="";String goodsName="";String price=""; String item_loc="";String view_sales="";
		String platform_shoptype="";String goodsUrl="";String platform_shopname="";
		String user_id="";String fastPostFee="";//是否包邮
		String time=SimpleDate.SimpleDateFormatData().format(new Date());
		if(!Validation.isEmpty(element)){
			if(element.toString().contains("item-name J_TGoldData")){//天猫旗舰店商品搜索
				keywordName=element.getElementsByClass("item-name J_TGoldData").attr("atpanel").toString().replace("\\", "").toString().split(",")[1].toString();
				goodsUrl=element.getElementsByClass("item-name J_TGoldData").attr("href").toString();
				goodsName=element.getElementsByClass("item-name J_TGoldData").text();
				price=element.getElementsByClass("c-price").text();
				view_sales=element.getElementsByClass("sale-num").text();
				platform_shopname=wordsInfo.getCust_keyword_remark().replace("+PC", "").trim();
				platform_shoptype=Fields.YES_PROPRIETARY;
				if(map.toString().contains(Fields.MESSAGEIMAGE)){
					try {
						Document document =Jsoup.parse(map.get(Fields.MESSAGEIMAGE));
						String J_TGoldData=document.getElementsByClass("J_TGoldData").get(number).toString();
						String length="data-ks-lazyload=\"";
						image=J_TGoldData.substring(J_TGoldData.indexOf("data-ks-lazyload=\"")+length.length(), J_TGoldData.lastIndexOf("src")-2);
					} catch (Exception e) {
						logger.error("获取图片失败>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+e);
					}
				}
			}else if(element.toString().contains("data-itemid")){//天猫超市搜索
				keywordName=element.getElementsByClass("product").attr("data-itemid").toString();
				price=element.getElementsByClass("ui-price").get(0).getElementsByTag("strong").text();
				goodsUrl=element.getElementsByClass("product-title").get(0).getElementsByTag("a").attr("href");
				goodsName=element.getElementsByClass("product-title").get(0).getElementsByTag("a").text();
				image=element.getElementsByClass("product-img").get(0).getElementsByTag("img").attr("src").toString();
				view_sales=element.getElementsByClass("item-sum").get(0).getElementsByTag("strong").text();
			}else if(element.toString().contains("product")&& element.toString().contains("product-iWrap")){
				keywordName=element.attr("data-id").toString();//商品egoodsId
				image=element.getElementsByClass("productImg-wrap").get(0).getElementsByTag("img").attr("src").toString();
				if(StringUtil.isEmpty(image)){
					image=element.getElementsByClass("productImg-wrap").get(0).getElementsByTag("img").attr("data-ks-lazyload").toString();
				}
				goodsUrl=element.getElementsByClass("productImg-wrap").get(0).getElementsByTag("a").attr("href").toString();
				price=element.getElementsByClass("productPrice").get(0).getElementsByTag("em").attr("title");
				goodsName=element.getElementsByClass("productTitle").get(0).getElementsByTag("a").attr("title").trim();
				platform_shopname=element.getElementsByClass("productShop-name").text();
			}	
		}else{
			 if(messagejson.toString().contains("raw_title")){
				 if(wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_TMALL_EN)){
						if(!messagejson.toString().contains(Fields.PLATFORM_TMALL_INTERNATIONAL_BABY)){
							keywordName=messagejson.getString("nid");
							goodsName=messagejson.getString("raw_title");
							platform_shopname=messagejson.getString("nick");
							goodsUrl=messagejson.getString("detail_url");
							goodsUrl="https:"+goodsUrl;
							image=messagejson.getString("pic_url");
							price=messagejson.getString("view_price");
							item_loc=messagejson.getString("item_loc");//当前位置
							user_id=messagejson.getString("user_id");
							try {view_sales=messagejson.getString("view_sales").substring(0,messagejson.getString("view_sales").length()-3);} catch (Exception e) {}//月销量
							comment_count=messagejson.getString("comment_count");//评论数
							if(platform_shopname.contains(Fields.TMART)||platform_shopname.contains(Fields.FLAGSHIP_STORE)){
								platform_shoptype=Fields.YES_PROPRIETARY;
							}else{
								platform_shoptype=Fields.PROPRIETARY;
							}
						}
					}else if(wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_TAOBAO_EN)){
						if(!messagejson.toString().contains(Fields.SEARCH_TMALL)&&!messagejson.toString().contains(Fields.PLATFORM_TMALL_INTERNATIONAL_BABY)){
							keywordName=messagejson.getString("nid");
							goodsName=messagejson.getString("raw_title");
							platform_shopname=messagejson.getString("nick");
							price=messagejson.getString("view_price");
							goodsUrl=messagejson.getString("detail_url");
							goodsUrl="https:"+goodsUrl;
							image=messagejson.getString("pic_url");
							 item_loc=messagejson.getString("item_loc");//当前位置
							 user_id=messagejson.getString("user_id");
							 try {view_sales=messagejson.getString("view_sales").substring(0,messagejson.getString("view_sales").length()-3);} catch (Exception e) {}
							 comment_count=messagejson.getString("comment_count");//评论数
							if(platform_shopname.contains(Fields.TMART)||platform_shopname.contains(Fields.FLAGSHIP_STORE)){
								platform_shoptype=Fields.YES_PROPRIETARY;
							}else{
								platform_shoptype=Fields.PROPRIETARY;
							}	
						}
					}else if(wordsInfo.getPlatform_name().equalsIgnoreCase(Fields.PLATFORM_TMALL_GLOBAL_EN)){
						if(messagejson.toString().contains(Fields.PLATFORM_TMALL_INTERNATIONAL_BABY)){
							keywordName=messagejson.getString("nid");
							goodsName=messagejson.getString("raw_title");
							platform_shopname=messagejson.getString("nick");
							goodsUrl=messagejson.getString("detail_url");
							goodsUrl="https:"+goodsUrl;
							image=messagejson.getString("pic_url");
							price=messagejson.getString("view_price");
							user_id=messagejson.getString("user_id");
							 item_loc=messagejson.getString("item_loc");//当前位置
							 try {view_sales=messagejson.getString("view_sales").substring(0,messagejson.getString("view_sales").length()-3);} catch (Exception e) {}
							 comment_count=messagejson.getString("comment_count");//评论数
							if(platform_shopname.contains(Fields.TMART)||platform_shopname.contains(Fields.FLAGSHIP_STORE)){
								platform_shoptype=Fields.YES_PROPRIETARY;
							}else{
								platform_shoptype=Fields.PROPRIETARY;
							}	  
						}
				    } 
			 }else if(messagejson.toString().contains("tItemType")){//taobao手机端搜索
				 keywordName=messagejson.getString("item_id"); 
				 goodsName=messagejson.getString("name");
				 if(messagejson.toString().contains("area")){item_loc=messagejson.getString("area");}//当前位置
				 fastPostFee=messagejson.getString("fastPostFee");//是否包邮
				// String sold=messagejson.getString("sold");//付款人数
				// String originalPrice=messagejson.getString("originalPrice");//原价
				 platform_shopname=messagejson.getString("nick");//店铺
				 image=messagejson.getString("img2");//图片
				 image="https:"+image;
				 goodsUrl=Fields.TMALL_APP_URL+keywordName;
				 price=messagejson.getString("price");//现价
				 user_id=messagejson.getString("userId");//用户Id
				 if(messagejson.toString().contains("commentCount")){
					 comment_count=messagejson.getString("commentCount");//评论数
				 }
				 if(platform_shopname.contains(Fields.TMART)||platform_shopname.contains(Fields.FLAGSHIP_STORE)){
					 platform_shoptype=Fields.YES_PROPRIETARY;
				 }else{
					 platform_shoptype=Fields.PROPRIETARY;
				 }
				 if(fastPostFee.equals("0.00")){
					 fastPostFee=Fields.PACK_MAIL; 
				 }else{
					 fastPostFee=Fields.DONTPACK_MAIL;  
				 }
				 
			 }else if(messagejson.toString().contains("quantity")){//tmall手机端指定店铺开发
				 keywordName=messagejson.getString("item_id"); 
				 goodsName=messagejson.getString("title").replace("<span class=H>", "").replace("</span>", "").trim();
				 image=messagejson.getString("img");//图片
				 image="https:"+image;
				 goodsUrl=messagejson.getString("url");
				 price=messagejson.getString("price");//现价
				 view_sales=messagejson.getString("sold");//月销量
				 platform_shopname=wordsInfo.getCust_keyword_remark().replace("+Mobile", "").trim();;
				 platform_shoptype=Fields.YES_PROPRIETARY;
			 }
			
		}
		if(StringUtil.isNotEmpty(keywordName)){
			String goodsId=StringHelper.encryptByString(keywordName+wordsInfo.getPlatform_name());
			messageData.put("item_loc", item_loc);
			messageData.put("count", map.get(Fields.COUNT));
			messageData.put("number",number);
			messageData.put("sum", sum);
			messageData.put("keywordName", keywordName);
			messageData.put("goodsName", goodsName);
			messageData.put("goods_pic_url", image);
			messageData.put("batch_time", time);
			messageData.put("price", price);
			messageData.put("goodsUrl", goodsUrl);
			messageData.put("platform_shoptype", platform_shoptype);
			messageData.put("platform_shopname", platform_shopname);
			messageData.put("ttl_comment_num", comment_count);
			messageData.put("sale_qty", view_sales);
			messageData.put("ItemPrice", 0);
			messageData.put("bsr_rank", 0);
			messageData.put("channel", Fields.CLIENT_MOBILE);
			messageData.put("goodsId", goodsId);
			messageData.put("skuId", keywordName);
			messageData.put("sellerid", user_id);
		}
		return messageData;	 
	}
	//京东 
	public Map<String,Object>jdMessage(Element element,CrawKeywordsInfo wordsInfo, JSONObject messagejson,Map<String,String>map,int sum,int number){
		Map<String,Object>messageData=new HashMap<String,Object>();
		String keywordName=""; String image="";String goodsName="";String price="";String place_name="";String inventory="";
		String platform_shoptype="";String goodsUrl="";String platform_shopname="";String ttl_comment_num="";String sale_qty="";String sellerid="";
		String time=SimpleDate.SimpleDateFormatData().format(new Date());
		if(!Validation.isEmpty(element)){
			goodsUrl=element.getElementsByClass("gl-item").get(0).getElementsByClass("p-img").get(0).getElementsByTag("a").attr("href").toString();
			image=element.getElementsByClass("gl-item").get(0).getElementsByClass("p-img").get(0).getElementsByTag("img").attr("src").toString();
			if(StringUtil.isEmpty(image)){
				image=element.getElementsByClass("gl-item").get(0).getElementsByClass("p-img").get(0).getElementsByTag("img").attr("data-lazy-img").toString();//
			}if(StringUtil.isEmpty(image)){
				image=element.getElementsByClass("gl-item").get(0).getElementsByClass("p-img").get(0).getElementsByTag("img").attr("source-data-lazy-img").toString();//
			} 
			keywordName=element.getElementsByClass("gl-item").attr("data-sku").toString(); 
			if(StringUtil.isEmpty(keywordName)){
				keywordName=element.getElementsByClass("gl-item").get(0).getElementsByTag("div").attr("data-sku").toString(); 
			}
			goodsName=element.getElementsByClass("gl-item").get(0).getElementsByClass("p-name").get(0).getElementsByTag("em").text();
			if(StringUtil.isEmpty(goodsName)){
				goodsName=element.getElementsByClass("gl-item").get(0).getElementsByClass("p-img").get(0).getElementsByTag("a").attr("title").toString();
			} 
			if(StringUtil.isNotEmpty(element.getElementsByClass("J_im_icon").text())){
				platform_shopname=element.getElementsByClass("gl-item").get(0).getElementsByClass("J_im_icon").get(0).getElementsByTag("a").attr("title");
			}
			if(StringUtil.isNotEmpty(element.getElementsByClass("p-price").text())){
				price=element.getElementsByClass("p-price").get(0).getElementsByTag("i").text();
			}
			if(StringUtil.isNotEmpty(element.getElementsByClass("p-stock").text())){
				inventory=element.getElementsByClass("p-stock").text();//库存
			}else{
				inventory=Fields.IN_STOCK;
			}
			if(StringUtil.isNotEmpty(element.getElementsByClass("p-commit").text())){//评论数
				ttl_comment_num=element.getElementsByClass("p-commit").get(0).getElementsByTag("strong").get(0).getElementsByTag("a").text().substring(0, element.getElementsByClass("p-commit").get(0).getElementsByTag("strong").get(0).getElementsByTag("a").text().length()-1);
				try {
					if(ttl_comment_num.contains(Fields.MYRIAD)){
						ttl_comment_num=ttl_comment_num.substring(0,ttl_comment_num.length()-1);
						ttl_comment_num=String.valueOf((int)(Float.valueOf(ttl_comment_num)*10000));
					}
				} catch (Exception e) {
					logger.info(element.getElementsByClass("p-commit").get(0).getElementsByTag("strong").get(0).getElementsByTag("a").text().substring(0, element.getElementsByClass("p-commit").get(0).getElementsByTag("strong").get(0).getElementsByTag("a").text().length()-1));
				}
				
			}
		}else{
			keywordName=messagejson.getString("wareId");
			goodsName=messagejson.getString("wname");
			price=messagejson.getString("jdPrice");
			image=messagejson.getString("goods_pic_url");
			ttl_comment_num=messagejson.getString("totalCount");
			goodsUrl=Fields.JD_URL_APP+keywordName;
			if(messagejson.getString("self").equals("true")){
				platform_shoptype=Fields.YES_PROPRIETARY;
			}else{
				platform_shoptype=Fields.PROPRIETARY;
			}
		}
		String goodsId=StringHelper.encryptByString(keywordName+wordsInfo.getPlatform_name());
		messageData.put("count", map.get(Fields.COUNT));
		messageData.put("number",number);
		messageData.put("sum", sum);
		messageData.put("keywordName", keywordName);
		messageData.put("goodsName", goodsName);
		messageData.put("goods_pic_url", image);
		messageData.put("batch_time", time);
		messageData.put("price", price);
		messageData.put("goodsUrl", goodsUrl);
		messageData.put("platform_shoptype", platform_shoptype);
		messageData.put("platform_shopname", platform_shopname);
		messageData.put("ttl_comment_num", ttl_comment_num);
		messageData.put("sale_qty", sale_qty);
		messageData.put("ItemPrice", 0);
		messageData.put("bsr_rank", 0);
		messageData.put("channel", Fields.CLIENT_MOBILE);
		messageData.put("goodsId", goodsId);
		messageData.put("skuId", keywordName);
		messageData.put("sellerid", sellerid);
		messageData.put("inventory", inventory);
		if(map.toString().contains("place_name")){
			if(Validation.isNotEmpty(map.get("place_name"))){
				place_name=map.get("place_name");
			}
		}
		messageData.put("item_loc",place_name);
		return messageData;
	}
	//一号店
	public Map<String,Object>yhdMessage(Element element,CrawKeywordsInfo wordsInfo, JSONObject messagejson,Map<String,String>map,int sum,int number){
		Map<String,Object>messageData=new HashMap<String,Object>();
		String promotion="";
		String keywordName="";  String image="";String goodsName="";String price="";String sellerid="";String place_name="";
		String platform_shoptype="";String goodsUrl="";String platform_shopname="";String ttl_comment_num="";String sale_qty="";
		String time=SimpleDate.SimpleDateFormatData().format(new Date());
		if(!Validation.isEmpty(element)){
			if(element.toString().contains("pic_box")&& element.toString().contains("middle")){//1号店关键词搜索APP
				keywordName=element.getElementsByClass("bug_car").attr("productId").toString();
				goodsName=element.getElementsByClass("title_box").text();//商品名称
				if(StringUtil.isNotEmpty(element.getElementsByClass("new_price").text())){
					price=element.getElementsByClass("new_price").get(0).getElementsByTag("i").text();//价格
				}
				image=element.getElementsByClass("pic_box").get(0).getElementsByTag("img").attr("src").toString();//商品图片
				goodsUrl=element.getElementsByClass("item").attr("href").toString();//商品url
				if(StringUtil.isNotEmpty(element.getElementsByClass("self_sell").toString())){
					platform_shoptype=element.getElementsByClass("self_sell").text();
				}else{
					platform_shoptype=Fields.PROPRIETARY;
				} 
			}else if(element.toString().contains("itemBox") ||element.toString().contains("proPrice")){
				keywordName=element.getElementsByClass("itemBox").attr("comproid").trim();//商品egoodsId
				if(StringUtil.isEmpty(keywordName)){
					keywordName=element.getElementsByClass("\\itemBox\\").attr("comproid").toString().replace("\\", "");
				}
				goodsUrl=element.getElementsByClass("\\img\\").attr("href").replace("\\", "");
				if(StringUtil.isEmpty(goodsUrl)){
					goodsUrl=element.getElementsByClass("img").attr("href");
				}
				try {
					image=element.getElementsByClass("\\img\\").get(0).getElementsByTag("img").attr("src").replace("\\","");	
				} catch (Exception e) {
					image=element.getElementsByClass("img").get(0).getElementsByTag("img").attr("src");	
				}
				try {
					goodsName=element.getElementById("\\pdlink2_"+keywordName+"\\").text().replace("\"", "").replace("\\n\\t", "").replace("\\t", "").replace("\\n","").trim();
				    if(wordsInfo.getCust_keyword_id().equals("102")){
				    	System.out.println(element.getElementById("\\pdlink2_"+keywordName+"\\").text().replace("\"", "").replace("\\n\\t", "").replace("\\t", "").replace("\\n",""));
				    }
				 }catch (Exception e) {
					goodsName=element.getElementById("pdlink2_"+keywordName+"").attr("title").trim();	
				}
				try {
					price=element.getElementsByClass("\\proPrice\\").get(0).getElementsByTag("em").attr("yhdprice").replace("\\", "");
				} catch (Exception e) {
					price=element.getElementsByClass("proPrice").get(0).getElementsByTag("em").attr("yhdprice");	
				}
				try {
					platform_shopname=element.getElementsByClass("\\o_1\\").attr("title").toString().replace("\\","");
					if(Validation.isEmpty(platform_shopname)){
						platform_shopname=element.getElementsByClass("o_1").attr("title").toString();		
					}
				} catch (Exception e) {
					platform_shopname=element.getElementsByClass("o_1").attr("title").toString();	
				}
				promotion=element.getElementsByClass("\\item_bg_icon_q\\").text().replace("\\", "").replaceAll("n", "").replaceAll("t","").trim();
				if(Validation.isEmpty(promotion)){
					promotion=element.getElementsByClass("item_bg_icon_q").text().replaceAll("n", "").replaceAll("t","").trim();
				}
				try {
					ttl_comment_num=element.getElementsByClass("\\comment\\").get(0).getElementsByTag("a").attr("experienceCount").replace("\\", "").trim().toString();
				} catch (Exception e) {
					ttl_comment_num=element.getElementsByClass("comment").get(0).getElementsByTag("a").attr("experienceCount").trim().toString();
				}
		    }
		}
		messageData.put("count", map.get(Fields.COUNT));
		messageData.put("number",number);
		messageData.put("sum", sum);
		messageData.put("keywordName", keywordName);
		messageData.put("goodsName", goodsName);
		messageData.put("goods_pic_url", image);
		messageData.put("batch_time", time);
		messageData.put("price", price);
		messageData.put("goodsUrl", goodsUrl);
		messageData.put("platform_shoptype", platform_shoptype);
		messageData.put("platform_shopname", platform_shopname);
		messageData.put("ttl_comment_num", ttl_comment_num);
		messageData.put("sale_qty", sale_qty);
		messageData.put("ItemPrice", 0);
		messageData.put("bsr_rank", 0);
		messageData.put("channel", Fields.CLIENT_MOBILE);
		messageData.put("skuId", keywordName);
		messageData.put("sellerid", sellerid);
		messageData.put("promotion", promotion);
		messageData.put("goodsId", StringHelper.encryptByString(keywordName+wordsInfo.getPlatform_name()));
		if(map.toString().contains("place_name")){
		if(Validation.isNotEmpty(map.get("place_name"))){
			place_name=map.get("place_name");
		  }
		}
		messageData.put("item_loc",place_name);
		return messageData;
	}
	//苏宁
	public Map<String,Object>suningMessage(Element element,CrawKeywordsInfo wordsInfo, JSONObject messagejson,String count,int sum,int number){
		Map<String,Object>messageData=new HashMap<String,Object>();
		//SimpleDateFormat data=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String keywordName=""; String image="";String goodsName="";String price="";String item_loc="";String sellerid="";
		String platform_shoptype="";String goodsUrl="";String platform_shopname="";String ttl_comment_num="";String sale_qty="";
		String time=SimpleDate.SimpleDateFormatData().format(new Date());
		if(!Validation.isEmpty(element)){
			if(element.toString().contains("sellPoint") && element.toString().contains("partnumber")){//苏宁搜索
				goodsName=element.getElementsByClass("search-loading").attr("alt").toString();
				image=element.getElementsByClass("search-loading").attr("src2").toString();
				goodsUrl=element.getElementsByClass("sellPoint").attr("href").toString();
				keywordName=element.getElementsByClass("res-opt").last().getElementsByTag("a").attr("partnumber").toString().split("-")[1].toString();	
				platform_shopname=element.getElementsByClass("seller oh no-more ").attr("salesName").toString();
				if(StringUtil.isEmpty(platform_shopname)){
					platform_shopname=element.getElementsByClass("seller oh no-cut ").attr("salesName").toString();
				}
				if(StringUtil.isNotEmpty(platform_shopname)){
					if(platform_shopname.contains(Fields.YES_PROPRIETARY)){
						platform_shoptype=Fields.YES_PROPRIETARY;
					}else{
						Fields.YES_PROPRIETARY=Fields.PROPRIETARY;
					}	
				}
			}
		}
		messageData.put("count", count);
		messageData.put("number",number);
		messageData.put("sum", sum);
		messageData.put("keywordName", keywordName);
		messageData.put("goodsName", goodsName);
		messageData.put("goods_pic_url", image);
		messageData.put("batch_time", time);
		messageData.put("price", price);
		messageData.put("goodsUrl", goodsUrl);
		messageData.put("platform_shoptype", platform_shoptype);
		messageData.put("platform_shopname", platform_shopname);
		messageData.put("ttl_comment_num", ttl_comment_num);
		messageData.put("sale_qty", sale_qty);
		messageData.put("item_loc", item_loc); 
		messageData.put("ItemPrice", 0);
		messageData.put("bsr_rank", 0);
		messageData.put("sellerid", sellerid);
		messageData.put("goodsId", StringHelper.encryptByString(keywordName+wordsInfo.getPlatform_name()));
		messageData.put("channel", Fields.CLIENT_MOBILE);
		messageData.put("skuId", keywordName);
		return messageData;
	}
	//解析1688_PC
	@SuppressWarnings("unchecked")
	public Map<String,Object>Message_1688(Element element,CrawKeywordsInfo wordsInfo, JSONObject messagejson,String count,int sum,int number,String database) throws Exception{
		Map<String,Object>messageData=new HashMap<String,Object>();
		Map<String,Object> insertItemPrice=new HashMap<String,Object>();
		List<Map<String,Object>> insertprices = Lists.newArrayList();
		String tableNamePrice=Fields.TABLE_CRAW_VENDOR_PRICE_INFO;
		SimpleDateFormat data=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String keywordName=""; String image="";String goodsName=""; String companyName="";String SourceFactory=""; String purchase_back_rate="";
		String goodsUrl=""; String platform_vendoraddress="";String purchase_unit="";
	   String purchase_price="";String purchase_qty="";
		String time=data.format(new Date());
		messageData.put("batch_time", time);
		messageData.put("channel", Fields.CLIENT_PC);
		String regEx="[^0-9]";  int ItemPrice=0;
	 	Pattern p = Pattern.compile(regEx);  
		if(!Validation.isEmpty(element)){
			if(element.toString().contains("sm-previewCompany sw-mod-previewCompanyInfo")){
				keywordName=element.attr("offerId");//egoodsid
				goodsUrl=element.getElementsByClass("sw-ui-photo220-box").attr("href").toString();//goodsUrl
				image=element.getElementsByClass("sw-ui-photo220-box").get(0).getElementsByTag("img").attr("src").trim().toString();//图片url
				goodsName=element.getElementsByClass("sw-ui-photo220-box").get(0).getElementsByTag("img").attr("alt").trim().toString();//商品名称
				companyName=element.getElementsByClass("sm-previewCompany sw-mod-previewCompanyInfo").text();//公司名称
				purchase_price=element.getElementsByClass("su-price").text().replace("¥", "").trim();//价格
				platform_vendoraddress=element.getElementsByClass("su-city").text();//地址
				String bookedCount=element.getElementsByClass("sm-offerShopwindow-trade").text().trim();//成交
				Matcher mat = p.matcher(bookedCount);  
				purchase_qty=mat.replaceAll("").trim();
				if(StringUtil.isNotEmpty(element.getElementsByClass("sm-widget-repurchaseicon").text())){
				purchase_back_rate=element.getElementsByClass("sm-widget-repurchaseicon").get(0).getElementsByTag("span").text();//回头率
				}	
			}else if(element.toString().contains("sm-offer-photo sw-dpl-offer-photo")){
				goodsName=element.getElementsByClass("sm-offer-photo sw-dpl-offer-photo").get(0).getElementsByTag("a").attr("title").toString();
				goodsUrl=element.getElementsByClass("sm-offer-photo sw-dpl-offer-photo").get(0).getElementsByTag("a").attr("href").toString();
				image=element.getElementsByClass("sm-offer-photo sw-dpl-offer-photo").get(0).getElementsByTag("img").attr("src").toString();
				SourceFactory=element.getElementsByClass("brand-name").text();//源头工厂
				platform_vendoraddress=element.getElementsByClass("sm-offer-location").text();//sm-offer-location 广东深圳市 地址
				keywordName=element.attr("offerid").toString();
				if(StringUtil.isEmpty(keywordName)){
					keywordName=element.getElementsByClass("sm-offer-companyName sw-dpl-offer-companyName ").attr("offerid").trim().toString();
				}
					if(StringUtil.isEmpty(image)){
					image=element.getElementsByClass("sm-offer-photo sw-dpl-offer-photo").get(0).getElementsByTag("img").attr("data-lazy-src").toString();
				} 
				try {
					companyName=element.getElementsByClass("sm-offer-company sw-dpl-offer-company").get(0).getElementsByTag("a").attr("title").toString();//公司名称
				} catch (Exception e) {
					companyName=element.getElementsByClass("sm-offer-companyName sw-dpl-offer-companyName ").text().trim();//公司名称
				}
				try {
					if(element.getElementsByClass("sm-widget-offershopwindowshoprepurchaserate-old").get(0).getElementsByTag("span").size()>2){
						purchase_back_rate=element.getElementsByClass("sm-widget-offershopwindowshoprepurchaserate-old").get(0).getElementsByTag("span").get(2).text();//回头率	
					}
				} catch (Exception e) {
					if(StringUtil.isNotEmpty(element.getElementsByClass("sm-widget-offershopwindowshoprepurchaserate ").text())){
						try {purchase_back_rate=element.getElementsByClass("sm-widget-offershopwindowshoprepurchaserate ").get(0).getElementsByTag("span").get(2).text();} catch (Exception e2) {}//回头率
						SourceFactory=element.getElementsByClass("sm-widget-offershopwindowshoprepurchaserate ").attr("i").toString();//生产加工
					}
				}
				Elements  purchases=element.getElementsByClass("sm-offer-dealInfo sm-offer-dealInfo-3").select("span");
				if(purchases.size()==0){
					purchases=element.getElementsByClass("s-widget-offershopwindowdealinfo sm-offer-dealInfo sm-offer-dealInfo-3").select("span");
					if(purchases.size()==0){
						purchases=element.getElementsByClass("s-widget-offershopwindowdealinfo sm-offer-dealInfo sm-offer-dealInfo-1").select("span");	
					}
					if(purchases.size()==0){
						purchases=element.getElementsByClass("s-widget-offershopwindowdealinfo sm-offer-dealInfo sm-offer-dealInfo-2").select("span");	
					}
				}
				for(Element span :purchases){ 
					purchase_unit=span.getElementsByTag("em").attr("title").substring(span.getElementsByTag("em").attr("title").length()-1, span.getElementsByTag("em").attr("title").length());// 件 个 套 台
					String purchase_amount=span.getElementsByTag("em").attr("title").substring(0, span.getElementsByTag("em").attr("title").length()-1);//   起价数
					purchase_price=span.getElementsByTag("i").attr("title").replace("¥","");//价格
					messageData.put("purchase_price", purchase_price);
					messageData.put("purchase_unit", purchase_unit);
					messageData.put("purchase_amount", purchase_amount);
					messageData.put("keywordName", keywordName);
					ItemPrice=1;
					if(StringUtil.isNotEmpty(keywordName)){
						messageData.put("goodsId", StringHelper.encryptByString(keywordName+wordsInfo.getPlatform_name()));
						if(Integer.valueOf(wordsInfo.getCrawling_status()).equals(Fields.STATUS_10)){
							insertItemPrice=BeanMapUtil.convertBean2MapWithUnderscoreName(crawGoodsVendorPriceInfoData(wordsInfo,messageData));
							insertprices.add(insertItemPrice);
						}
					}
				  }
				if(insertprices.size()>0){
					batchInsertData.insertIntoData(insertprices,database,tableNamePrice);
				  }
				}
		}else{
			  SourceFactory=messagejson.get("bizType").toString();//生产加工
			  platform_vendoraddress=messagejson.get("city").toString();//深圳市
	    	  companyName=messagejson.get("companyName").toString();//公司名称
	    	  image=messagejson.get("offerPicUrl").toString();//图片url
	    	  purchase_price=messagejson.get("price").toString();//价格
	    	  purchase_back_rate=messagejson.get("shopRepurchaseRate").toString();//回头率
	    	  goodsName=messagejson.get("simpleSubject").toString();//商品名称
	    	  purchase_unit=messagejson.get("unit").toString();//台
	    	  purchase_qty=messagejson.get("bookedCount").toString();//成交笔
	    	  ItemPrice=0;
	    	  
		}
	    if(StringUtil.isNotEmpty(purchase_price)){
	    	 messageData.put("purchase_price", purchase_price);
	   	    messageData.put("purchase_amount", purchase_qty);
	   	    messageData.put("purchase_unit", purchase_unit);
	 		messageData.put("goodsId", StringHelper.encryptByString(keywordName+wordsInfo.getPlatform_name()));
	 		messageData.put("platform_vendorid",StringHelper.encryptByString(companyName+Fields.platform_1688));//供应商Id
	 		messageData.put("companyName", companyName);//供应商名称
	 		messageData.put("count", count);
	 		messageData.put("number",number);
	 		messageData.put("sum", sum);
	 		messageData.put("keywordName", keywordName);
	 		messageData.put("goodsName", goodsName);
	 		messageData.put("goods_pic_url", image);
	 		messageData.put("goodsUrl", goodsUrl);
	 		messageData.put("purchase_back_rate", purchase_back_rate);
	 		messageData.put("SourceFactory", SourceFactory);
	 		messageData.put("ItemPrice", ItemPrice);
	 		messageData.put("bsr_rank", 0);
	 		messageData.put("purchase_qty", purchase_qty);
	 		messageData.put("channel", Fields.CLIENT_MOBILE);
	 		messageData.put("platform_vendoraddress", platform_vendoraddress);
	 		messageData.put("skuId", keywordName);
	    }
		return messageData;
	}
	
	//亚马逊
	public Map<String,Object>amazonMessage(Element element,CrawKeywordsInfo wordsInfo, JSONObject messagejson,Map<String,String>map,int sum,int number) throws Exception{
		Map<String,Object>messageData=new HashMap<String,Object>();
		//SimpleDateFormat data=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String keywordName=""; String image="";String goodsName="";String price="";	String bsr_rank="";	String item_loc="";String sellerid="";
		String platform_shoptype="";String goodsUrl="";String platform_shopname="";String ttl_comment_num="";String sale_qty="";
		String time=SimpleDate.SimpleDateFormatData().format(new Date());
		if(!Validation.isEmpty(element)){ 
			if(element.toString().contains("s-results-list-atf") || element.toString().contains("data-asin")){//美国亚马逊
				keywordName=element.attr("data-asin").toString(); 
				goodsName=element.getElementsByClass("a-link-normal s-access-detail-page  s-color-twister-title-link a-text-normal").attr("title").toString();
				price=element.getElementsByClass("a-offscreen").text().replace("$","").replace("[Sponsored]", "").trim();
				if(StringUtil.isEmpty(price)){
				price=element.getElementsByClass("a-size-base a-color-base").text().replace("$","").replace("[Sponsored]", "").trim();
				}
				image=element.getElementsByClass("a-link-normal a-text-normal").get(0).getElementsByTag("img").attr("src").toString();
				goodsUrl=element.getElementsByClass("a-link-normal a-text-normal").attr("href");
				if(StringUtil.isNotEmpty(element.getElementsByClass("a-column a-span5 a-span-last").text().toString())){
					ttl_comment_num=element.getElementsByClass("a-column a-span5 a-span-last").get(0).getElementsByClass("a-size-small a-link-normal a-text-normal").text().replace(",","");//评论数
					if(ttl_comment_num.contains("Save")){
						ttl_comment_num=element.getElementsByClass("a-column a-span5 a-span-last").get(0).getElementsByClass("a-size-small a-link-normal a-text-normal").first().text().replace(",","");//评论数
					}
				}
				if(StringUtil.isEmpty(ttl_comment_num)){
					ttl_comment_num=element.getElementsByClass("a-size-small a-link-normal a-text-normal").last().text().replace(",","");//评论数
					if(ttl_comment_num.contains("Save")){
						ttl_comment_num=element.getElementsByClass("a-size-small a-link-normal a-text-normal").first().text().replace(",","");//评论数	
					}
				}
			}else if(element.toString().contains("a-row s-result-list-parent-container")){
				keywordName=element.attr("data-asin").toString();
				goodsName=element.getElementsByClass("a-link-normal a-text-normal").get(0).getElementsByTag("img").attr("alt");
				image=element.getElementsByClass("a-link-normal a-text-normal").get(0).getElementsByTag("img").attr("src");
				platform_shopname=element.getElementsByClass("a-size-small a-color-secondary").get(1).text();//品牌
				price=element.getElementsByClass("a-size-base a-color-price s-price a-text-bold").text().replace("￥","");
				goodsUrl=element.getElementsByClass("a-link-normal a-text-normal").attr("href");
			}else if(element.toString().contains("a-ordered-list a-vertical")){//
				goodsName=element.getElementsByClass("a-section a-spacing-small").get(0).getElementsByClass("a-section a-spacing-small").get(0).getElementsByTag("img").attr("alt").replace("\"", "");
				image=element.getElementsByClass("a-section a-spacing-small").get(0).getElementsByClass("a-section a-spacing-small").get(0).getElementsByTag("img").attr("src");
				ttl_comment_num=element.getElementsByClass("a-icon-row a-spacing-none").get(0).getElementsByClass("a-size-small a-link-normal").text().replace(",","");
				price=element.getElementsByClass("p13n-sc-price").text().replace("$","");
				keywordName=element.getElementsByClass("a-link-normal").attr("href").toString().split("/")[3].toString();
				goodsUrl=element.getElementsByClass("a-link-normal").attr("href").toString();
				bsr_rank=element.getElementsByClass("zg-badge-text").text().replace("#","");
			}else if(element.toString().contains("zg-item-immersion")){//类目搜索
				bsr_rank=element.getElementsByClass("zg-badge-text").text().replace("#",""); 
				goodsName=element.getElementsByClass("a-section a-spacing-small").get(0).getElementsByTag("img").attr("alt").toString().replace("\"", "`");
				image=element.getElementsByClass("a-section a-spacing-small").get(0).getElementsByTag("img").attr("src").toString();
				ttl_comment_num=element.getElementsByClass("a-size-small a-link-normal").text().replace(",","");
				price=element.getElementsByClass("p13n-sc-price").text().replace("$","");
				goodsUrl=element.getElementsByClass("a-size-small a-link-normal").attr("href").toString();
				if(StringUtil.isEmpty(goodsUrl)){
					goodsUrl=element.getElementsByClass("a-link-normal a-text-normal").attr("href").toString();
					keywordName=goodsUrl.split("/")[3].toString();
				}else{
					keywordName=goodsUrl.split("/")[2].toString();  
				}
				goodsUrl=Fields.AMAZON_URL_KEYWORD_EN_URL+goodsUrl;

			}else if(element.toString().contains("zg_itemWrapper")){
				bsr_rank=element.getElementsByClass("zg_rankNumber").text().replace(".", "");//
				image=element.getElementsByClass("zg_itemWrapper").get(0).getElementsByClass("a-link-normal").get(0).getElementsByTag("img").attr("src").toString(); 
				goodsUrl=element.getElementsByClass("zg_itemWrapper").get(0).getElementsByClass("a-link-normal").attr("href").toString();
				goodsName=element.getElementsByClass("zg_itemWrapper").get(0).getElementsByClass("a-link-normal").get(0).getElementsByTag("img").attr("alt").toString();
				ttl_comment_num=element.getElementsByClass("a-size-small a-link-normal").text().replace(",","");
				price=element.getElementsByClass("p13n-sc-price").text().replace("$","").toString();
				if(StringUtil.isNotEmpty(goodsUrl)){
					keywordName=goodsUrl.split("/")[3].toString();
				}
				goodsUrl=Fields.AMAZON_URL_KEYWORD_EN_URL+goodsUrl; 
			}
		}
		if(StringUtil.isNotEmpty(keywordName.toString())){
         if(price.contains("-")){ 
        	 price=price.split("-")[1].trim(); 
         }
         if(StringUtil.isEmpty(price)){
        	 String priceWhole=element.getElementsByClass("sx-price-whole").text();
        	 String priceFractional=element.getElementsByClass("sx-price-fractional").text();
        	 if(StringUtil.isNotEmpty(priceWhole)){
        		 price=priceWhole+"."+priceFractional.replace("[Sponsored]", "").trim(); 
        	 }else{
        		 price=element.getElementsByClass("a-offscreen").text().replace("$", "").replace("[Sponsored]","").trim();
        	 } 
        	 if(StringUtil.isEmpty(price)){
        		 price=element.getElementsByClass("a-size-base a-color-base").text().replace("$", "").replace("[Sponsored]","").trim(); 
        	 }
         }else if(StringUtil.isEmpty(price)){
        	 price=element.getElementsByClass("a-offscreen").text().replace("$", "").replace("[Sponsored]", "").trim();
         }
         if(price.contains(".")){
        	 try {
        		 price=price.split(" ")[0].trim(); 
			} catch (Exception e) {
			} 
         }
        if(StringUtil.isNotEmpty(price)){price=price.replace(",", "");	}
        String goodsId=StringHelper.encryptByString(keywordName+wordsInfo.getPlatform_name());
        if(ttl_comment_num.contains("Save")){
        	ttl_comment_num="";	
        }
		messageData.put("count", map.get(Fields.COUNT));
		messageData.put("number",number);
		messageData.put("sum", sum);
		messageData.put("keywordName", keywordName);
		messageData.put("goodsName", goodsName);
		messageData.put("goods_pic_url", image);
		messageData.put("batch_time", time);
		messageData.put("goodsUrl", goodsUrl);
		messageData.put("platform_shoptype", platform_shoptype);
		messageData.put("platform_shopname", platform_shopname);
		messageData.put("ttl_comment_num", ttl_comment_num);
		messageData.put("sale_qty", sale_qty);
		messageData.put("item_loc", item_loc);
		messageData.put("goodsId", goodsId);
		messageData.put("ItemPrice", 0);
		messageData.put("bsr_rank", bsr_rank);
		messageData.put("channel", Fields.CLIENT_MOBILE);//database
		messageData.put("sellerid", sellerid);
		messageData.put("skuId", keywordName);
		messageData.put("price", price);
		}
		return messageData;
	}   
	//淘宝众筹页面解析
	public Map<String,Object>taobaoZcMessage(Element element,CrawKeywordsInfo wordsInfo, JSONObject messagejson,String count,int sum,int number){
		Map<String,Object>messageData=new HashMap<String,Object>();
		messageData.put("cust_keyword_id", wordsInfo.getCust_keyword_id());
		messageData.put("goodsId", StringHelper.encryptByString(messagejson.get("id")+wordsInfo.getPlatform_name()));
		messageData.put("egoodsId",messagejson.get("id"));
		messageData.put("platform_goods_name",messagejson.get("name"));//商品名称
		messageData.put("platform_name_en",wordsInfo.getPlatform_name());//平台
		messageData.put("platform_category",wordsInfo.getCust_keyword_name());//所在类
		messageData.put("curr_amount",messagejson.get("curr_money"));//已筹金额 目前累计资金
		messageData.put("target_amount", messagejson.get("target_money"));//-目标金额 
		messageData.put("finish_per",messagejson.get("finish_per"));//达成率
		messageData.put("support_count",messagejson.get("buy_amount"));//支持人数
		messageData.put("focus_count", messagejson.get("focus_count"));//-喜欢人数
		messageData.put("remain_day",messagejson.get("remain_day"));//剩余天数
		messageData.put("begin_date",messagejson.get("begin_date"));//开始日期
		messageData.put("end_date",messagejson.get("end_date"));//结束日期
		messageData.put("goods_status",messagejson.get("status"));//筹款状态
		messageData.put("goods_url","https:"+messagejson.get("link"));//商品详情页面
		messageData.put("goods_pic_url", messagejson.get("image"));//商品图片
		messageData.put("update_time",SimpleDate.SimpleDateFormatData().format(new Date()));
		messageData.put("update_date",SimpleDate.SimpleDateFormatData().format(new Date()));
		messageData.put("batch_time",SimpleDate.SimpleDateFormatData().format(new Date()));
		return messageData;
	}
	//京东众筹页面解析
	@SuppressWarnings("static-access")
	public Map<String,Object>jdZcMessage(Element element,CrawKeywordsInfo wordsInfo, JSONObject messagejson,String count,int sum,int number){
		Map<String,Object>messageData=new HashMap<String,Object>();
		Calendar  calendar=new GregorianCalendar();
		calendar.setTime(new Date());String goods_status="";
		int focus_count=0;
		if(StringUtil.isNotEmpty(element.getElementsByClass("support").text())){
			if(element.getElementsByClass("support").get(0).getElementsByTag("span").text().contains(Fields.THOUSAND)){
				focus_count=Integer.valueOf(Pattern.compile("[^0-9]").matcher(element.getElementsByClass("support").get(0).getElementsByTag("span").text()).replaceAll(""))*1000;
			}else if(element.getElementsByClass("support").get(0).getElementsByTag("span").text().contains(Fields.BEST)){
				focus_count=Integer.valueOf(Pattern.compile("[^0-9]").matcher(element.getElementsByClass("support").get(0).getElementsByTag("span").text()).replaceAll(""))*100;
			}else if(element.getElementsByClass("support").get(0).getElementsByTag("span").text().contains(Fields.MYRIAD)){
				focus_count=Integer.valueOf(Pattern.compile("[^0-9]").matcher(element.getElementsByClass("support").get(0).getElementsByTag("span").text()).replaceAll(""))*10000;
			}else{
				focus_count=Integer.valueOf(element.getElementsByClass("support").get(0).getElementsByTag("span").text());
			}
			try {
				goods_status=element.getElementsByClass("support").attr("onclick").replace("(", "").replace(")","").trim();
				goods_status=goods_status.split(",")[1].toString();
				if(Integer.valueOf(goods_status)==2){
					goods_status=Fields.RAISE_OF;
				}else if(Integer.valueOf(goods_status)==1){
					goods_status=Fields.PREHEATING;
				}else if(Integer.valueOf(goods_status)==7){
					goods_status=Fields.RAISE_SUCCESS;
				}else if(Integer.valueOf(goods_status)==6){
					goods_status=Fields.PROJECT_SUCCESS;
				}
			} catch (Exception e) {
			}
			 
			int day=Integer.valueOf(Pattern.compile("[^0-9]").matcher(element.getElementsByClass("fore3").get(0).getElementsByClass("p-percent").text()).replaceAll(""));
			calendar.add(calendar.DATE,day);//把日期往后增加一天.整数往后推,负数往前移动 
			messageData.put("cust_keyword_id", wordsInfo.getCust_keyword_id());
			String egoodsId=Pattern.compile("[^0-9]").matcher(element.getElementsByClass("support").attr("id")).replaceAll("");
			messageData.put("goodsId", StringHelper.encryptByString(egoodsId+wordsInfo.getPlatform_name()));
			messageData.put("egoodsId", egoodsId);
			messageData.put("platform_goods_name",element.getElementsByClass("link-tit").attr("title").trim());//商品名称
			messageData.put("platform_name_en",wordsInfo.getPlatform_name());//平台
			messageData.put("platform_category",wordsInfo.getCust_keyword_name());//所在类
			messageData.put("curr_amount",element.getElementsByClass("fore2").get(0).getElementsByClass("p-percent").text().replace("￥","").trim());//已筹金额 目前累计资金
			messageData.put("target_amount", "");//-目标金额
			messageData.put("finish_per",element.getElementsByClass("fore1").get(0).getElementsByClass("p-percent").text().replace("%", ""));//达成率
			messageData.put("support_count","");//支持人数
			messageData.put("focus_count",focus_count);//-喜欢人数
			messageData.put("remain_day",String.valueOf(day));//剩余天数
			messageData.put("begin_date",SimpleDate.SimpleDateData().format(new Date()));//开始日期
			messageData.put("end_date",SimpleDate.SimpleDateData().format(calendar.getTime()));//结束日期
			messageData.put("goods_status",goods_status);//筹款状态
			messageData.put("goods_url","https://z.jd.com"+element.getElementsByClass("link-pic").attr("href").trim());//商品详情页面
			messageData.put("goods_pic_url",element.getElementsByClass("lazyout").attr("src").trim());//商品图片
			messageData.put("update_time",SimpleDate.SimpleDateFormatData().format(new Date()));
			messageData.put("update_date",SimpleDate.SimpleDateFormatData().format(new Date()));
			messageData.put("batch_time",SimpleDate.SimpleDateFormatData().format(new Date())); 	
		}
		return messageData;
	}
	
	//kickstarter众筹页面解析
	public Map<String,Object>kickstarterMessage(Element element,CrawKeywordsInfo wordsInfo, JSONObject messagejson,String count,int sum,int number) throws ParseException{
		Map<String,Object>messageData=new HashMap<String,Object>();
		try {
			if(element.toString().contains("converted_pledged_amount")){
			String data=element.attr("data-project").trim();
			JSONObject jsonObject = JSONObject.fromObject(data);
			String start=SimpleDate.formatDuring(jsonObject.getLong("launched_at"));//开始时间
			String end=SimpleDate.formatDuring(jsonObject.getLong("deadline"));//结束时间
	        long totalTime=SimpleDate.dateSubtract(start,end)-1;//获取项目时间 结束日期当天不计算
			//项目剩余时间 当前时间减开始时间 然后 项目总时间减去已用掉时间等于剩余时间
			long ElapsedTime=SimpleDate.dateSubtract(start,SimpleDate.SimpleDateData().format(new Date()));
			long Days=totalTime - ElapsedTime; //获取剩余天数

			messageData.put("cust_keyword_id", wordsInfo.getCust_keyword_id());
			messageData.put("goodsId", StringHelper.encryptByString(jsonObject.getString("id")+wordsInfo.getPlatform_name()));
			messageData.put("egoodsId",jsonObject.getString("id"));
			messageData.put("platform_goods_name",jsonObject.getString("name"));//商品名称
			messageData.put("platform_name_en",wordsInfo.getPlatform_name());//平台
			messageData.put("platform_category",wordsInfo.getCust_keyword_name());//所在类
			messageData.put("curr_amount",jsonObject.getString("converted_pledged_amount"));//已筹金额 目前累计资金
			messageData.put("target_amount",jsonObject.getString("goal"));//-目标金额 
			messageData.put("finish_per",jsonObject.getString("percent_funded"));//达成率
			messageData.put("support_count",jsonObject.getString("backers_count"));//支持人数 
			messageData.put("focus_count", "0");//-喜欢人数
			messageData.put("remain_day",Days);//剩余天数
			messageData.put("begin_date",SimpleDate.formatDuring(jsonObject.getLong("launched_at")));//开始日期
			messageData.put("end_date",end);//结束日期
			messageData.put("goods_status","");//筹款状态
			messageData.put("goods_url",jsonObject.getJSONObject("urls").getJSONObject("web").getString("project"));//商品详情页面
			messageData.put("goods_pic_url",jsonObject.getJSONObject("photo").getString("full"));//商品图片
			messageData.put("update_time",SimpleDate.SimpleDateFormatData().format(new Date()));
			messageData.put("update_date",SimpleDate.SimpleDateFormatData().format(new Date()));
			messageData.put("batch_time",SimpleDate.SimpleDateFormatData().format(new Date()));
			}
		} catch (Exception e) {
			logger.error("封装 kickstarter 对象失败>>>>>>>>>>>>>>"+e.getMessage());
		}
		
		return messageData;
	}

		/**
		 * 项目名称：springmvc_crawler
		 * 类名称：GoodsProductDetails
		 * 类描述：解析封装数据插入
		 * 创建人：Jack
		 * 创建时间：2018年03月16日 上午10:50:10
		 * 修改备注：
		 *
		 * @throws SQLException
		 * @version
		 * @throws Exception 
		 */
		@SuppressWarnings("unchecked")
		@TargetDataSource
		public void insertData(List<Element> items,String database,CrawKeywordsInfo wordsInfo,Map<String, String> map,JSONArray priceInfoJson) throws Exception{
			List<Map<String,Object>> insertItems = Lists.newArrayList();
			List<Map<String,Object>> insertItems_history = Lists.newArrayList();
			List<Map<String,Object>> insertprices = Lists.newArrayList();
			List<Map<String,Object>> insertComment = Lists.newArrayList();
	
            String tableName="";String tableNamePrice=""; String tableName_history=""; String comment="";
			if(items.size()>0){
				for(int i=0;i<items.size();i++){
					Map<String,Object>dataMessage=new HashMap<String,Object>();
					Map<String,Object> insertItemPrice=new HashMap<String,Object>();
					Map<String,Object> insertItem_history=new HashMap<String,Object>();
					Map<String,Object> insertItem=new HashMap<String,Object>();
					Map<String,Object>messageData=new HashMap<String,Object>();
					Map<String,Object>dataComment=new HashMap<String,Object>();
					if(wordsInfo.getPlatform_name().equals(Fields.PLATFORM_TMALL_EN)||wordsInfo.getPlatform_name().equals(Fields.PLATFORM_TMALL_GLOBAL_EN) ||wordsInfo.getPlatform_name().equals(Fields.PLATFORM_TAOBAO_EN)){
						dataMessage=tmallMessage(items.get(i),wordsInfo,null,map,items.size(),i);
					}else if(wordsInfo.getPlatform_name().equals(Fields.PLATFORM_JD)){
						dataMessage=jdMessage(items.get(i),wordsInfo,null,map,items.size(),i);
					}else if(wordsInfo.getPlatform_name().equals(Fields.PLATFORM_YHD)){
						dataMessage=yhdMessage(items.get(i),wordsInfo,null,map,items.size(),i);
					}else if(wordsInfo.getPlatform_name().equals(Fields.PLATFORM_SUNING)){
						dataMessage=suningMessage(items.get(i),wordsInfo,null,map.get(Fields.COUNT),items.size(),i);
					}else if(wordsInfo.getPlatform_name().equals(Fields.PLATFORM_AMAZON)||wordsInfo.getPlatform_name().contains(Fields.PLATFORM_AMAZON_UN) ){
						dataMessage=amazonMessage(items.get(i),wordsInfo,null,map,items.size(),i);
					}else if(wordsInfo.getPlatform_name().equals(Fields.PLATFORM_JD_ZC)){
						dataMessage=jdZcMessage(items.get(i),wordsInfo,null,map.get(Fields.COUNT),items.size(),i);
					}else if(wordsInfo.getPlatform_name().equals(Fields.PLATFORM_KICKSTARTER)){
						dataMessage=kickstarterMessage(items.get(i),wordsInfo,null,map.get(Fields.COUNT),items.size(),i);
					}else if(wordsInfo.getPlatform_name().equals(Fields.platform_1688)){
						dataMessage=Message_1688(items.get(i),wordsInfo,null,map.get(Fields.COUNT),items.size(),i,map.get("database"));	
					}
					if(dataMessage.toString().contains(Fields.KEYWORDNAME) ||dataMessage.toString().contains(Fields.EGOODSID) ){
						if(Integer.valueOf(wordsInfo.getCrawling_status()).equals(Fields.STATUS_11) ||Integer.valueOf(wordsInfo.getCrawling_status()).equals(Fields.STATUS_12)){
							tableName= Fields.TABLE_CRAW_CRAW_KEYWORD_TEMP_INFO_FORESARCH;
							insertItem = BeanMapUtil.convertBean2MapWithUnderscoreName(keywordsInfo(wordsInfo,map.get(Fields.COUNT),i,items.size(),dataMessage.get(Fields.KEYWORDNAME).toString()));
							insertItems.add(insertItem); 	
						}else{
							if(Integer.valueOf(wordsInfo.getCrawling_status()).equals(Fields.STATUS_10) ||Integer.valueOf(wordsInfo.getCrawling_status()).equals(Fields.STATUS_13)){
								if(!wordsInfo.getPlatform_name().equals(Fields.platform_1688)&& !wordsInfo.getPlatform_name().equals(Fields.PLATFORM_JD_ZC) && !wordsInfo.getPlatform_name().equals(Fields.PLATFORM_KICKSTARTER)){
									tableName_history= Fields.TABLE_CRAW_CRAW_KEYWORD_TEMP_INFO_FORESARCH;
									insertItem_history = BeanMapUtil.convertBean2MapWithUnderscoreName(keywordsInfo(wordsInfo,map.get(Fields.COUNT),i,items.size(),dataMessage.get(Fields.KEYWORDNAME).toString()));
									insertItems_history.add(insertItem_history); 			
								}
							}
							if(wordsInfo.getPlatform_name().contains(Fields.platform_1688)){
								tableName= Fields.TABLE_CRAW_VENDOR_INFO;
								tableNamePrice=Fields.TABLE_CRAW_VENDOR_PRICE_INFO;
							}else if(wordsInfo.getPlatform_name().contains(Fields.PLATFORM_JD_ZC) || wordsInfo.getPlatform_name().contains(Fields.PLATFORM_TAOBAO_ZC) ||wordsInfo.getPlatform_name().equals(Fields.PLATFORM_KICKSTARTER)){
								tableName= Fields.CRAW_GOODS_TRANSLATE_INFO;
							}else{
								tableName= Fields.TABLE_CRAW_GOODS_INFO;
								tableNamePrice=Fields.TABLE_CRAW_GOODS_PRICE_INFO;
							}
							messageData.putAll(dataMessage);
							if(wordsInfo.getPlatform_name().contains(Fields.platform_1688)){
								insertItem = BeanMapUtil.convertBean2MapWithUnderscoreName(crawGoodsVendorInfoData(wordsInfo,messageData));
								insertItemPrice=BeanMapUtil.convertBean2MapWithUnderscoreName(crawGoodsVendorPriceInfoData(wordsInfo,messageData));	
	
							}else if(wordsInfo.getPlatform_name().equals(Fields.PLATFORM_JD_ZC)  || wordsInfo.getPlatform_name().contains(Fields.PLATFORM_TAOBAO_ZC) || wordsInfo.getPlatform_name().contains(Fields.PLATFORM_KICKSTARTER)){
								insertItem = BeanMapUtil.convertBean2MapWithUnderscoreName(crawGoodsCrowdfundingInfo(wordsInfo,messageData));
							}else{
								insertItem = BeanMapUtil.convertBean2MapWithUnderscoreName(crawGoodsInfoData(wordsInfo,messageData));
								if(Integer.valueOf(messageData.get("ItemPrice").toString())==0){
									insertItemPrice=BeanMapUtil.convertBean2MapWithUnderscoreName(crawGoodsPriceInfoData(wordsInfo,messageData));

								}	
							}
							if(messageData.toString().contains("ttl_comment_num")){
								comment=Fields.CRAW_GOODS_COMMENT_INFO;
								if(StringUtil.isNotEmpty(messageData.get("ttl_comment_num").toString().trim())){
									messageData.put("comment_status", Fields.STATUS_COUNT_1);
									dataComment=BeanMapUtil.convertBean2MapWithUnderscoreName(goodsCommentInfo(wordsInfo,messageData));
									insertComment.add(dataComment);	
									
								}/*else{
									messageData.put("comment_status", Fields.STATUS_COUNT);
									dataComment=BeanMapUtil.convertBean2MapWithUnderscoreName(goodsCommentInfo(wordsInfo,messageData));	
									insertComment.add(dataComment);	
								}	*/
							}
							if(insertItemPrice.size()>0){
								insertprices.add(insertItemPrice); 		
							}
							insertItems.add(insertItem);
						}
					} 
				}
			}else{
				for(int i=0;i<priceInfoJson.size();i++){
					Map<String,Object>dataMessage=new HashMap<String,Object>();
					Map<String,Object> insertItemPrice=new HashMap<String,Object>();
					Map<String,Object> insertItem_history=new HashMap<String,Object>();
					Map<String,Object> insertItem=new HashMap<String,Object>();
					Map<String,Object>messageData=new HashMap<String,Object>();
					Map<String,Object>dataComment=new HashMap<String,Object>();
					JSONObject messagejson=JSONObject.fromObject(priceInfoJson.toArray()[i]);
					if(wordsInfo.getPlatform_name().equals(Fields.PLATFORM_TMALL_EN)||wordsInfo.getPlatform_name().equals(Fields.PLATFORM_TMALL_GLOBAL_EN) ||wordsInfo.getPlatform_name().equals(Fields.PLATFORM_TAOBAO_EN)){
						dataMessage=tmallMessage(null,wordsInfo,messagejson,map,priceInfoJson.size(),i);
					}else if(wordsInfo.getPlatform_name().equals(Fields.PLATFORM_JD)){
						dataMessage=jdMessage(null,wordsInfo,messagejson,map,priceInfoJson.size(),i);
					}else if(wordsInfo.getPlatform_name().equals(Fields.platform_1688)){
						dataMessage=Message_1688(null,wordsInfo,messagejson,map.get(Fields.COUNT),items.size(),i,map.get("database"));	
					}else if(wordsInfo.getPlatform_name().equals(Fields.PLATFORM_TAOBAO_ZC)){
						dataMessage=taobaoZcMessage(null,wordsInfo,messagejson,map.get(Fields.COUNT),items.size(),i);	
					}else if(wordsInfo.getPlatform_name().equals(Fields.PLATFORM_JD_ZC)){
						dataMessage=jdZcMessage(null,wordsInfo,messagejson,map.get(Fields.COUNT),items.size(),i);	
					}
					if(dataMessage.size()>0){ 
						messageData.putAll(dataMessage);
						if(Integer.valueOf(wordsInfo.getCrawling_status()).equals(Fields.STATUS_11) ||Integer.valueOf(wordsInfo.getCrawling_status()).equals(Fields.STATUS_12)){
							tableName= Fields.TABLE_CRAW_CRAW_KEYWORD_TEMP_INFO_FORESARCH;
							insertItem = BeanMapUtil.convertBean2MapWithUnderscoreName(keywordsInfo(wordsInfo,map.get("count"),i,priceInfoJson.size(),dataMessage.get("keywordName").toString())); 
							insertItems.add(insertItem); 		
						}else{
						if(Integer.valueOf(wordsInfo.getCrawling_status()).equals(Fields.STATUS_10) ||Integer.valueOf(wordsInfo.getCrawling_status()).equals(Fields.STATUS_13)){
							if(!wordsInfo.getPlatform_name().equals(Fields.platform_1688)&& !wordsInfo.getPlatform_name().equals(Fields.PLATFORM_TAOBAO_ZC) && !wordsInfo.getPlatform_name().equals(Fields.PLATFORM_JD_ZC) && !wordsInfo.getPlatform_name().equals(Fields.PLATFORM_KICKSTARTER)){
								tableName_history= Fields.TABLE_CRAW_CRAW_KEYWORD_TEMP_INFO_FORESARCH;
								insertItem_history = BeanMapUtil.convertBean2MapWithUnderscoreName(keywordsInfo(wordsInfo,map.get(Fields.COUNT),i,items.size(),dataMessage.get(Fields.KEYWORDNAME).toString()));
								insertItems_history.add(insertItem_history); 		
							}	
						}
						if(wordsInfo.getPlatform_name().equals(Fields.platform_1688)){
							tableName= Fields.TABLE_CRAW_VENDOR_INFO;
							tableNamePrice=Fields.TABLE_CRAW_VENDOR_PRICE_INFO;
							insertItem = BeanMapUtil.convertBean2MapWithUnderscoreName(crawGoodsVendorInfoData(wordsInfo,messageData));
							if(Integer.valueOf(wordsInfo.getCrawling_status()).equals(Fields.STATUS_10) ||Integer.valueOf(wordsInfo.getCrawling_status()).equals(Fields.STATUS_13)){
								insertItemPrice=BeanMapUtil.convertBean2MapWithUnderscoreName(crawGoodsVendorPriceInfoData(wordsInfo,messageData));
							}
						}else if(wordsInfo.getPlatform_name().equals(Fields.PLATFORM_TAOBAO_ZC) ||wordsInfo.getPlatform_name().equals(Fields.PLATFORM_JD_ZC) || wordsInfo.getPlatform_name().equals(Fields.PLATFORM_KICKSTARTER)){
							tableName= Fields.CRAW_GOODS_TRANSLATE_INFO;
							insertItem = BeanMapUtil.convertBean2MapWithUnderscoreName(crawGoodsCrowdfundingInfo(wordsInfo,messageData));
						}else{
							if(dataMessage.size()>0){ 
							tableName= Fields.TABLE_CRAW_GOODS_INFO;
							tableNamePrice=Fields.TABLE_CRAW_GOODS_PRICE_INFO;
							insertItem = BeanMapUtil.convertBean2MapWithUnderscoreName(crawGoodsInfoData(wordsInfo,messageData));
							insertItemPrice=BeanMapUtil.convertBean2MapWithUnderscoreName(crawGoodsPriceInfoData(wordsInfo,messageData));	
						   }
						}
						
						if(messageData.toString().contains("ttl_comment_num")){
							comment=Fields.CRAW_GOODS_COMMENT_INFO;
							if(StringUtil.isNotEmpty(messageData.get("ttl_comment_num").toString())){
								messageData.put("comment_status", Fields.STATUS_COUNT_1);
								dataComment=BeanMapUtil.convertBean2MapWithUnderscoreName(goodsCommentInfo(wordsInfo,messageData));
								insertComment.add(dataComment);	
								
							}/*else{
								messageData.put("comment_status", Fields.STATUS_COUNT);
								dataComment=BeanMapUtil.convertBean2MapWithUnderscoreName(goodsCommentInfo(wordsInfo,messageData));	
								insertComment.add(dataComment);	
							}	*/
						}
						if(insertItemPrice.size()>0){
							insertprices.add(insertItemPrice);	
						}
						if(insertItem.size()>0){
							insertItems.add(insertItem); 		
						}
					  }
					}
				}
			}
 
			if(insertItems.size()>0){
				batchInsertData.insertIntoData(insertItems,map.get("database").toString(),tableName);
			}
			if(insertprices.size()>0){
				batchInsertData.insertIntoData(insertprices,map.get("database").toString(),tableNamePrice);	
			}	
			if(insertItems_history.size()>0){
				batchInsertData.insertIntoData(insertItems_history,map.get("database").toString(),tableName_history);
			}
			if(insertComment.size()>0){
				batchInsertData.insertIntoData(insertComment,map.get("database").toString(),comment);	
			}
		}
		// 封装对象
		public Craw_goods_InfoVO  crawGoodsInfoData(CrawKeywordsInfo wordsInfo,Map<String,Object>data){
			Craw_goods_InfoVO info=new Craw_goods_InfoVO();
			//SimpleDateFormat data_time=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			info.setPosition((Integer.valueOf(data.get("number").toString())+1)+"/"+Integer.valueOf(data.get("count").toString())+"*"+Integer.valueOf(data.get("sum").toString()));
			info.setCust_keyword_id(Integer.valueOf(wordsInfo.getCust_keyword_id()));
			info.setBatch_time(data.get("batch_time").toString());
			info.setPlatform_name_en(wordsInfo.getPlatform_name());
			info.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
			info.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
			info.setGoodsId(data.get("goodsId").toString());
			info.setPlatform_goods_name(data.get("goodsName").toString());
			info.setEgoodsId(data.get("keywordName").toString());
			info.setGoods_pic_url(data.get("goods_pic_url").toString());
			info.setGoods_status(Integer.valueOf(Fields.STATUS_ON));
			info.setGoods_url(data.get("goodsUrl").toString());
			info.setPlatform_shoptype(data.get("platform_shoptype").toString());
			info.setPlatform_sellername(data.get("platform_shopname").toString());
			info.setPlatform_shopname(data.get("platform_shopname").toString());
			info.setSale_qty(data.get("sale_qty").toString());
			info.setTtl_comment_num(data.get("ttl_comment_num").toString());
			info.setBsr_rank(data.get("bsr_rank").toString());//排名
			info.setPlatform_sellerid(data.get("sellerid").toString());// 评论Id
			info.setDelivery_place(data.get("item_loc").toString());//位置
			if(Validation.isNotEmpty(data.get("inventory"))){
				info.setInventory(data.get("inventory").toString());//是否有货
			}
			return info;
		}
		public Craw_goods_Vendor_Info  crawGoodsVendorInfoData(CrawKeywordsInfo wordsInfo,Map<String,Object>data){
			Craw_goods_Vendor_Info info=new Craw_goods_Vendor_Info();
			//SimpleDateFormat data_time=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			info.setCust_keyword_id(Integer.valueOf(wordsInfo.getCust_keyword_id()));
			info.setBatch_time(data.get("batch_time").toString());
			info.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
			info.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
			info.setGoodsid(data.get("goodsId").toString());
			info.setPlatform_goodsname(data.get("goodsName").toString());
			info.setEgoodsid(data.get("keywordName").toString());
			info.setPlatform_goods_picurl(data.get("goods_pic_url").toString());
			info.setPlatform_goodsurl(data.get("goodsUrl").toString());
			info.setPlatform_vendorid(data.get("platform_vendorid").toString());//供应商Id
			info.setPlatform_vendorname(data.get("companyName").toString());//供应商名称
			info.setPlatform_vendortype(data.get("SourceFactory").toString());//源头工厂
			info.setPurchase_back_rate(data.get("purchase_back_rate").toString());//回头率	
			info.setGoods_status(Integer.valueOf(Fields.STATUS_ON));//商品上架 
			info.setChannel(Fields.CLIENT_MOBILE);
			if(data.toString().contains("platform_vendoraddress")){
			info.setPlatform_vendoraddress(data.get("platform_vendoraddress").toString());//商品地址	
			}
			return info;
		}
			//众筹表
		public Craw_goods_crowdfunding_Info  crawGoodsCrowdfundingInfo(CrawKeywordsInfo wordsInfo,Map<String,Object>data){
			Craw_goods_crowdfunding_Info info=new Craw_goods_crowdfunding_Info();
			try {
				info.setCust_keyword_id(Integer.valueOf(wordsInfo.getCust_keyword_id()));
				info.setGoodsId(data.get("goodsId").toString());
				info.setEgoodsId(data.get("egoodsId").toString());
				info.setPlatform_goods_name(data.get("platform_goods_name").toString());
				info.setPlatform_name_en(data.get("platform_name_en").toString());//所在类
				info.setPlatform_category(data.get("platform_category").toString());//所在类
				info.setCurr_amount(Float.valueOf(data.get("curr_amount").toString()));//已筹金额 目前累计资金
				info.setFinish_per(Float.valueOf(data.get("finish_per").toString()));//达成率
				info.setFocus_count(Integer.valueOf(data.get("focus_count").toString()));//-喜欢人数
				info.setRemain_day(Integer.valueOf(data.get("remain_day").toString()));//剩余天数
				info.setBegin_date(data.get("begin_date").toString());//开始日期
				info.setEnd_date(data.get("end_date").toString());//结束日期
				info.setGoods_url(data.get("goods_url").toString());//商品详情页面
				info.setGoods_pic_url(data.get("goods_pic_url").toString());//商品图片
				info.setUpdate_time(data.get("update_time").toString());
				info.setUpdate_date(data.get("update_date").toString());
				info.setBatch_time(data.get("batch_time").toString());
				info.setGoods_status(data.get("goods_status").toString());//筹款状态
				info.setPrice_status(Fields.STATUS_COUNT_1);
				if(StringUtil.isNotEmpty(data.get("support_count").toString())){
					info.setSupport_count(Integer.valueOf(data.get("support_count").toString()));//支持人数); 	
				}else{
					info.setSupport_count(0);	
				}
				if(StringUtil.isNotEmpty(data.get("target_amount").toString())){
					info.setTarget_amount(Float.valueOf(data.get("target_amount").toString()));//-目标金额
				}else{
					info.setTarget_amount(0);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return info;
		}
		
		public Craw_goods_Vendor_price_Info  crawGoodsVendorPriceInfoData(CrawKeywordsInfo wordsInfo,Map<String,Object>data){
			Craw_goods_Vendor_price_Info info=new Craw_goods_Vendor_price_Info();
			info.setBatch_time(data.get("batch_time").toString());
			info.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
			info.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
			info.setGoodsid(data.get("goodsId").toString());
			info.setPurchase_price(data.get("purchase_price").toString());//价格
			info.setPurchase_amount(data.get("purchase_amount").toString());//件数
			info.setCust_keyword_id(Integer.valueOf(wordsInfo.getCust_keyword_id()));//商品keywordId
			info.setEgoodsid(data.get("keywordName").toString());//商品egoodsId
			info.setChannel(data.get("channel").toString());//渠道
			info.setPurchase_unit(data.get("purchase_unit").toString());//件 个 台
			info.setChannel(data.get("channel").toString());
			return info;  
		}
		
		public Craw_goods_Price_InfoVO  crawGoodsPriceInfoData(CrawKeywordsInfo wordsInfo,Map<String,Object>data){
			Craw_goods_Price_InfoVO info=new Craw_goods_Price_InfoVO();
			info.setBatch_time(data.get("batch_time").toString());
			info.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
			info.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
			info.setGoodsid(data.get("goodsId").toString());
			info.setCurrent_price(data.get("price").toString());
			info.setOriginal_price(data.get("price").toString());
			info.setCust_keyword_id(wordsInfo.getCust_keyword_id());
			info.setChannel(data.get("channel").toString());
			if(data.toString().contains("skuId")){
			 info.setSkuId(data.get("skuId").toString());	
			}
			if(data.toString().contains("promotion")){
			 info.setPromotion(data.get("promotion").toString());
			}
			return info;  
		}
         
		// 获取商品的评论数
		public Craw_goods_comment_Info goodsCommentInfo(CrawKeywordsInfo info,Map<String,Object>messageData) throws Exception{
			Craw_goods_comment_Info comment=new Craw_goods_comment_Info();
			try { 
				comment.setBatch_time(messageData.get("batch_time").toString()); 
				comment.setCust_keyword_id(Integer.valueOf(info.getCust_keyword_id()));
				comment.setEgoodsId(messageData.get("keywordName").toString());
				comment.setGoodsId(messageData.get("goodsId").toString());
				comment.setPlatform_name_en(info.getPlatform_name());
				comment.setComment_status(Integer.valueOf(messageData.get("comment_status").toString()));
				comment.setTtl_comment_num(messageData.get("ttl_comment_num").toString());//总评论数
				comment.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
				comment.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));	
			} catch (Exception e) {
				logger.error("插入评论失败"+e.getMessage());
			} 
			return comment;
		}

		public Craw_keywords_temp_Info_forsearch  keywordsInfo(CrawKeywordsInfo wordsInfo,String count,int number ,int sum,String keywordName){
			Craw_keywords_temp_Info_forsearch info=new Craw_keywords_temp_Info_forsearch();
			info.setPosition((number+1)+"/"+count+"*"+sum);
			info.setCust_keyword_name(keywordName);
			info.setCrawling_status(Fields.STATUS_ON);
			info.setFeature_1(Fields.STATUS_ON);
			info.setCust_account_id(wordsInfo.getCust_account_id());
			info.setCust_keyword_id(wordsInfo.getCust_keyword_id());
			info.setPlatform_name(wordsInfo.getPlatform_name());
			info.setPlatform_shopname(wordsInfo.getCust_keyword_remark());
			info.setCust_account_name(wordsInfo.getCust_account_name()+":"+wordsInfo.getCust_keyword_name()+"/"+wordsInfo.getCust_keyword_remark());
			info.setCust_keyword_type(wordsInfo.getCust_keyword_type()); 
			info.setCrawling_fre(wordsInfo.getCrawling_fre());
			info.setAlways_abnormal(wordsInfo.getAlways_abnormal());
			info.setIs_presell(wordsInfo.getIs_presell());
			info.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
			info.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
	        if(Fields.STATUS_10==Integer.valueOf(wordsInfo.getCrawling_status())|| Fields.STATUS_13==Integer.valueOf(wordsInfo.getCrawling_status())){
				info.setCrawed_status(Integer.valueOf(Fields.COUNT_01.toString()));	
			}else{
				info.setCrawed_status(Integer.valueOf(Fields.COUNT_0.toString()));	
			}
			return info;
		}
}
