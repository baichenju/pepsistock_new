package com.eddc.service;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.eddc.model.CommodityPrices;
import com.eddc.model.Craw_goods_Price_Info;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.StringHelper;
import com.eddc.util.Validation;
import com.github.pagehelper.util.StringUtil;
@Service
public class Suning_DataCrawlService {
	private static Logger logger=Logger.getLogger(Suning_DataCrawlService.class);
	//private static SimpleDateFormat fort=new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
	@Autowired
	HttpClientService httpClientService;
	@Autowired
	CrawlerPublicClassService crawlerPublicClassService;
	public Map<String, String> SuningParseItemPage(String itemPage,Map<String,String>mapType,String database){
		Map<String, String> map=new HashMap<String, String>();
		String shopName="";String goodsName=""; String proprietary=Fields.PROPRIETARY; String catalogId="";String partNumber="";
		String Stock="";String shopId=""; String jdPrice="";String originalPrice="";String vendor="";String promotion="";String priceType="";
		String vendorType="";String pgPrice="";
		if(!Validation.isEmpty(itemPage)){
			String strs=itemPage.substring(itemPage.indexOf("{"), itemPage.lastIndexOf("}")+1);
			JSONObject currPriceJson = JSONObject.fromObject(strs);
			logger.info(currPriceJson);
			goodsName=currPriceJson.getJSONObject("data").getJSONObject("data1").getJSONObject("data").getJSONObject("itemInfoVo").get("itemName").toString();
			if(currPriceJson.getJSONObject("data").getJSONObject("data1").getJSONObject("data").toString().contains("brandName")){
				try { 
					shopName=currPriceJson.getJSONObject("data").getJSONObject("data1").getJSONObject("data").get("brandName").toString();	
				} catch (Exception e) {
					shopName=currPriceJson.getJSONObject("data").getString("snsltDesc");
				}
				if(shopName.contains(Fields.YES_PROPRIETARY)){ 
					proprietary=Fields.YES_PROPRIETARY;
				}else{
					proprietary=Fields.PROPRIETARY;
				}	
			}else if(currPriceJson.getJSONObject("data").get("snsltDesc").toString().contains(Fields.YES_PROPRIETARY)){
				proprietary=Fields.YES_PROPRIETARY;
			}else{
				proprietary=Fields.PROPRIETARY;
			}
			if(currPriceJson.getJSONObject("data").getJSONObject("data1").getJSONObject("data").toString().contains("categoryId")){
				catalogId=currPriceJson.getJSONObject("data").getJSONObject("data1").getJSONObject("data").getJSONObject("groupInfoVO").get("categoryId").toString();	
			}
		    partNumber=currPriceJson.getJSONObject("data").getJSONObject("data1").getJSONObject("data").get("partNumber").toString();
		    Stock=currPriceJson.getJSONObject("data").get("invStatus").toString();
		     if(Stock.equals(Fields.STATUS_ON_2)){
		    	Stock=Fields.IS_NOT_STOCK;
		    	shopId = "4"; 
		    }else if(Stock.equals(Fields.STATUS_OFF_3)){
		    	Stock=Fields.IS_NOT_STOCK;
		    }else{
		    	Stock=Fields.IN_STOCK;	
		    } 
		    JSONArray priceInfoJson = currPriceJson.getJSONObject("data").getJSONObject("price").getJSONArray("saleInfo");
		    if(priceInfoJson.size()>0){
		    	 jdPrice=JSONObject.fromObject(priceInfoJson.toArray()[0]).get("promotionPrice").toString();//现价
				    if(priceInfoJson.contains("netPrice")){//原价
				    	originalPrice=JSONObject.fromObject(priceInfoJson.toArray()[0]).get("netPrice").toString();//原价
				    }else{
				    	originalPrice=jdPrice;
				    }
				    pgPrice=JSONObject.fromObject(priceInfoJson.toArray()[0]).get("pgPrice").toString();  //品团价格 SPELL_GROUP_PRICE
				
		    }
		    vendor=JSONObject.fromObject(priceInfoJson.toArray()[0]).get("vendor").toString();
		    priceType=JSONObject.fromObject(priceInfoJson.toArray()[0]).get("priceType").toString();
		    vendorType=JSONObject.fromObject(priceInfoJson.toArray()[0]).get("vendorType").toString();
		    map.put("vendor", vendor);
		    map.put("priceType", priceType);
		    map.put("vendorType", vendorType);
		    map.put("shopName", shopName);//店铺名称
			map.put("region",null);//卖家位置
			map.put("shopid", shopId);//商品Id
			map.put("platform_shoptype", proprietary);//平台商店类型
			map.put("subCatId", catalogId);
			map.put("sellerId", null);
			map.put("sellerName", shopName);//卖家店铺名称
			map.put("picturl",  Fields.SUNING_PC_IMAGE+partNumber+"_2_400x400.jpg?ver=2003");//图片url 
			map.put("goodsName", goodsName);//商品名称
			map.put("delivery_place", null);//交易地址
			map.put("inventory", Stock);//商品库存
			map.put("transactNum", null);//月销
			map.put("postageFree", Fields.DONTPACK_MAIL);//是不包邮
			map.put("rateNum", null);//商品总评论数
			map.put("skuId", mapType.get("egoodsId").toString());//商品sku
			map.put("originalPrice", originalPrice);//原价
			map.put("currentPrice", jdPrice);//现价
			map.put("goodsUrl",Fields.SUNING_PC_URL_1+mapType.get("egoodsId")+".html");
			map.put("channel",Fields.CLIENT_MOBILE); 
			map.put("message", null);   
			map.put("deposit", null);
			map.put("coupons", null);
			map.put("sale_qty", null);
			map.put("reserve_num", null);
			mapType.putAll(map);
			try {
				promotion=getPromotionFromMobile(partNumber,mapType,database);//获取促销
			} catch (Exception e) {
				promotion="";
			}
		}
		if(StringUtil.isNotEmpty(pgPrice)){
			promotion+="&&"+Fields.SPELL_GROUP_PRICE+pgPrice;
		}
		map.put("promotion", promotion);//促销
		return map;

	}
	//APP促销
	public  String getPromotionFromMobile(String egoodsId,Map<String,String> priceMap,String database) throws Exception {
		String currentPrice_mobile = priceMap.get("currentPrice");
		String oriPrice_mobile = priceMap.get("originalPrice");
		String priceType=priceMap.get("priceType");
		if (Validation.isEmpty(oriPrice_mobile)) {
			oriPrice_mobile = currentPrice_mobile;
		}
		String vendorType = priceMap.get("vendorType");
		String vendor = priceMap.get("vendor");
		if (Validation.isEmpty(vendorType)) {
			vendor = "0000000000";
		}
		priceMap.put("ip", "-");
		priceMap.put("messageLog",Fields.MOBILE_PRICE);
		priceMap.put("url",Fields.SUNING_APP_URL_PRICE+egoodsId+"_"+vendor+"_021_0210101_5_R9000372_10051_"+currentPrice_mobile+"_0_20___"+vendor+"_"+oriPrice_mobile+"_0_000000000"+egoodsId+"_999_"+priceType+"_8183__000198183_313007.html?callback=detailCommonLogic");
		priceMap.put("urlRef", Fields.SUNING_APP_URL_1+egoodsId+".html");
		String  urlContent=""; int count=0;
		while (Validation.isEmpty(urlContent)) { 
			count++;
			Thread.sleep(1000); 
			if (count > 5) {
				break;
			}
			try {
				urlContent=httpClientService.interfaceSwitch(priceMap.get("ip"), priceMap,database);//请求数据
				if(Validation.isEmpty(urlContent)){
					urlContent=httpClientService.requestData(priceMap);//抓取App促销
				}
			} catch (Exception e) {
				logger.info("请求苏宁促销App失败"+e.getMessage());
			}
		} 
		String activity = StringHelper.getResultByReg(urlContent, "activityList\":\\[(.+?)\\]");
		if (Validation.isNull(activity)) {
			return "";
		}
		List<String> activitys = StringHelper.getResultListByReg(activity, "\\{(.+?)\\}");
		String result = "";
		for (String sub : activitys) { 
			String temp = StringHelper.getResultByReg(sub, "\"activityDescription\":\"(.*?)\"");
			if (Validation.isEmpty(temp)) {
				continue;
			}
			if (temp.contains(Fields.FULL)&&temp.contains(Fields.YUAN)) {
				temp = StringHelper.getResultByReg(temp, Fields.EXPRESSION);
				if (Validation.isEmpty(temp)||Fields.NULL.equals(temp)) {
					continue;
				}
				temp = Fields.COUPONREDMPTION+temp+Fields.MONEY;
			}
			if (temp.contains(Fields.HOUSEHOLDS_IS_FULL)&&temp.contains(Fields.REDUCTION_OF)) {
				continue;
			}
			result +=temp+"&&";
		}
		if (result.endsWith("&&")) {
			result = result.substring(0, result.lastIndexOf("&&"));
		}
		if (Validation.isEmpty(result)) {
			result = "";
		}
		return result;
	}
	//pc端价格 
	public Map<String, Object> suning_DataCrawlPrice_PC(Map<String,String>map ,CommodityPrices commodityPrices,String database) throws Exception{
		String shopIdContent = "";String refPrice="";String currentPrice="";
		String invStatus="";String shopId="";String originalPrice="";	String promotionContent="";
		Map<String, Object> result = new HashMap<String, Object>();
		map.put("coding", "GBK");
		map.put("messageLog",Fields.PC_PRICE);
		map.put("urlRef", Fields.SUNING_PC_URL_1+commodityPrices.getEgoodsId()+".html");
	    int count=0;
		while (Validation.isEmpty(shopIdContent)) { 
			count++;
			Thread.sleep(1000); 
			if (count > 6) {
				break;
			}
			try {
				shopIdContent=httpClientService.interfaceSwitch(map.get("ip"), map,database);//请求数据
			} catch (Exception e) {
				logger.info("请求苏宁PC端价格失败"+e.toString()+SimpleDate.SimpleDateFormatData().format(new Date()));
			}
		}
		try { //timeDate
			String temp = shopIdContent.substring(shopIdContent.indexOf("{"), shopIdContent.lastIndexOf("}") + 1);
			JSONObject currPriceJson = JSONObject.fromObject(temp);
			JSONArray arry=currPriceJson.getJSONObject("data").getJSONObject("price").getJSONArray("saleInfo"); 
			for(int i=0;i<arry.size();i++){
				JSONObject json=JSONObject.fromObject(arry.toArray()[i].toString());
				invStatus=json.get("invStatus").toString();
				shopId=json.get("vendorCode").toString();
				refPrice=json.get("refPrice").toString();
				originalPrice=json.get("netPrice").toString();
				currentPrice=json.get("promotionPrice").toString();
				if ("3".equals(invStatus)&&Validation.isEmpty(refPrice)&&Validation.isEmpty(currentPrice)) {
					originalPrice="暂无报价";
					currentPrice="暂无报价";
				}
			}
			try {
				//获取促销
			 promotionContent = getPromotionContent(commodityPrices.getEgoodsId(),map,database);
			} catch (Exception e) {
				promotionContent="";
			}

			//当前地区不销售
			result.put("shopId", shopId);
			result.put("refPrice", refPrice);
			result.put("originalPrice", originalPrice);
			result.put("currentPrice", currentPrice);
			result.put("promotion", promotionContent);
			result.put("channel",Fields.CLIENT_PC);
			result.put("goodsId", commodityPrices.getGoodsid());
			result.put("keywordId", commodityPrices.getCust_keyword_id());
			result.put("SKUid", commodityPrices.getEgoodsId());
			if(StringUtil.isNotEmpty(currentPrice)){
				Craw_goods_Price_Info price=crawlerPublicClassService.commdityPricesData(commodityPrices,result);//插入PC端价格
				result.put(Fields.TABLE_CRAW_GOODS_PRICE_INFO, price);
				result.putAll(map);	
			}
		} catch (Exception e) {
			logger.info("解析Pc 端价格失败"+e.toString()+SimpleDate.SimpleDateFormatData().format(new Date()));
		}
		return result;
		
	}
	//获取pc端促销
	@SuppressWarnings("static-access")
	public String getPromotionContent(String egoodsId, Map<String,String>map,String database) throws Exception{
		String Promotion="";String item="";
		map.put("ip", "-");
		map.put("messageLog",Fields.PRIOMOTION_DATA);
		map.put("url", Fields.SUNING_PC_PROMOTION+egoodsId+"_0000000000_021_0210501_89.00_0_11_1_3,30_pds__0__FourPage.promInfoCallback.vhtm?callback=FourPage.promInfoCallback");
		map.put("urlRef", Fields.SUNING_PC_URL_1+egoodsId+".html");
		map.put("coding", "UTF-8"); 
		int count=0;
		while (Validation.isEmpty(item)) { 
			count++;
			Thread.sleep(1000); 
			if (count > 6) {
				break;
			}
			try {
				item=httpClientService.interfaceSwitch(map.get("ip"), map,database);//请求数据
			} catch (Exception e) {
				logger.info("请求苏宁pc端促销失败"+e.getMessage());
			}
		}
		if(!Validation.isEmpty(item)){
			String temp = item.substring(item.indexOf("{"), item.lastIndexOf("}") + 1);
			JSONObject currPriceJson = JSONObject.fromObject(temp);
			JSONArray jsonList = currPriceJson.getJSONArray("promotions");
			if(jsonList.size()>0){
				for(int i=0;i<jsonList.size();i++){
					JSONObject json = JSONObject.fromObject(jsonList.toArray()[i]);
					if(Validation.isEmpty(Promotion)){
						Promotion=json.getString("activityDesc");
					}else{
						Promotion+="&&"+json.getString("activityDesc");
					}
				} 
				//Promotion=currPriceJson.fromObject(currPriceJson.getJSONArray("promotions").toArray()[0]).get("activityDesc").toString(); 	
			}else{ 
				Promotion="";	 
			}
		}
		return Promotion;

	} 

}
