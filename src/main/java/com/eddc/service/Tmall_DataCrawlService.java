package com.eddc.service;
import java.util.*;
import java.util.Map.Entry;

import com.eddc.mapper.CrawlerPublicClassMapper;
import com.eddc.model.*;
import com.eddc.util.*;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.util.StringUtil;
import com.google.common.collect.Lists;
import com.eddc.util.JsoupUtil;
@Service
public class Tmall_DataCrawlService {
	private static Logger logger=Logger.getLogger(Tmall_DataCrawlService.class);
	//private static SimpleDateFormat forts=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	@Autowired
	HttpClientService httpClientService;
	@Autowired
	CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	private BatchInsertData batchInsertData;
	@Autowired
	CrawlerPublicClassMapper mapper;
	

	
	@SuppressWarnings("unchecked")
	public  Map<String, Object>productItemWrawler(String StringMessage,CrawKeywordsInfo kweywordInfo,Map<String,String>mapItem) throws Exception{

		Map<String,Object > map=new HashMap<String,Object >();

		Map<String,Object> insertItem=new HashMap<String,Object>();

		//价格
		Map<String, Object> insertPriceMap = new HashMap<String, Object>();
		List<Map<String, Object>> insertPriceList = Lists.newArrayList();

		//详情
		Map<String, Object> insertItemMap = new HashMap<String, Object>();
		List<Map<String, Object>> insertItemList = Lists.newArrayList();
		//图片
		List<Map<String,Object>> insertImage= Lists.newArrayList();
		List<Map<String,Object>> commentList= Lists.newArrayList();
		//商品规格属性
		Map<String,Object> commodityProperty=new HashMap<String,Object>();
		List<Map<String,Object>> attributeList=Lists.newArrayList();

		StringBuffer promotion=new StringBuffer();

		List<Craw_goods_Price_Info>priceList=new ArrayList<Craw_goods_Price_Info>();
		Craw_goods_InfoVO infovo=new Craw_goods_InfoVO();

		Document doc=Jsoup.parseBodyFragment(StringMessage);
		String productName=doc.getElementsByClass("main cell").html();
		String shop_name=doc.getElementsByClass("shop-name").html();
		String goodsId=mapItem.get("goodsId").toString();
		String batch_time=mapItem.get("timeDate").toString();
		String platform=mapItem.get("dataType").toString();
		String client=mapItem.get("client").toString();
		int inventoryCount=0;
		//判断商品是否存在
		if(StringUtils.isNotBlank(productName)) {
			ArrayList<Element> element =doc.select("script");
			for(int i=0;i<element.size();i++) {

				if(element.get(i).data().toString().contains("var _DATA_Detail = ")) {
					String[] elScriptList = element.get(i).data().toString().split("var _DATA_Detail = ");
					String itemMessage=elScriptList[1].replace(";","").trim();

					//获取商品库存
					String[] dataDetail = element.get(i+1).data().toString().split("var _DATA_Mdskip = ");
					String inventory=dataDetail[1].trim();
					JSONObject inventoryMessage = JSONObject.fromObject(inventory);	
					JSONObject json = JSONObject.fromObject(itemMessage);
					//商品地址
					if(inventoryMessage.getJSONObject("delivery").containsKey("completedTo")) {
						String completedTo=inventoryMessage.getJSONObject("delivery").getString("completedTo");
						infovo.setDelivery_place(completedTo);
					}else if(inventoryMessage.getJSONObject("delivery").containsKey("from")) {
						String from=inventoryMessage.getJSONObject("delivery").getString("from");
						infovo.setSeller_location(from);
					}else if(inventoryMessage.getJSONObject("delivery").containsKey("to")){
						if(StringUtils.isNotBlank(infovo.getSeller_location())) {
							infovo.setSeller_location(infovo.getSeller_location()+""+inventoryMessage.getJSONObject("delivery").getString("to"));
						}else {
							infovo.setSeller_location(inventoryMessage.getJSONObject("delivery").getString("to"));	
						}
					}
					
				
					if(StringUtils.isEmpty(infovo.getDelivery_place())) {
						infovo.setDelivery_place(mapItem.get("delivery"));
					}
					if(infovo.getDelivery_place().contains("区")) {
						infovo.setSeller_location(mapItem.get("delivery")+" "+infovo.getSeller_location());
						infovo.setDelivery_place(mapItem.get("delivery")+" "+infovo.getDelivery_place());
					   }
					
					if(inventoryMessage.getJSONObject("delivery").containsKey("postage")) {
						//运费
						String postage=inventoryMessage.getJSONObject("delivery").getString("postage");
						infovo.setDelivery_info(postage);	
					}
					if(StringUtils.isEmpty(productName)) {
						//商品名称
						productName=json.getJSONObject("item").getString("title");
					}
					if(StringUtils.isEmpty(shop_name)) {
						//店铺名称
						shop_name=json.getJSONObject("seller").getString("shopName");
					}
					//店铺Id
					if(json.getJSONObject("seller").containsKey("shopId")) {
						infovo.setPlatform_shopid(json.getJSONObject("seller").getString("shopId"));
					}

					//用户Id
					String userId=json.getJSONObject("seller").getString("userId");
					//行业Id
					String categoryId=json.getJSONObject("item").getString("categoryId");
					//评论数
					if(json.getJSONObject("rate").containsKey("totalCount")) {
						String commentCount=json.getJSONObject("rate").getString("totalCount");
						infovo.setTtl_comment_num(commentCount);
					}

					infovo.setPlatform_shopname(shop_name);//卖家店铺名称
					infovo.setPlatform_sellername(shop_name);
					infovo.setPlatform_goods_name(productName);


					infovo.setPlatform_sellerid(userId);
					infovo.setPlatform_category(categoryId);
					infovo.setGoods_url(Fields.TMALL_URL+mapItem.get("egoodsId"));
					if(shop_name.contains("超市")) {
						infovo.setPlatform_shoptype("超市");
					}else if(shop_name.contains("旗舰")) {
						infovo.setPlatform_shoptype("天猫旗舰店");
					}else if(shop_name.contains("天猫会员店")) {
						infovo.setPlatform_shoptype("天猫会员店");
					}else {
						infovo.setPlatform_shoptype("非自营");
					}
					//判断商品小图是否存在
					if(StringUtil.isNotEmpty(json.getJSONObject("item").get("images").toString())){
						JSONArray jsonArray = JSONArray.fromObject(json.getJSONObject("item").getString("images"));
						for (int ii = 0; ii < jsonArray.size(); ii++) {
							try {
								if(ii==0){
									String productImage="http:"+jsonArray.getString(ii);	
									infovo.setGoods_pic_url(productImage);
								}
							} catch (Exception e) {
								logger.error("==>>解析商品小图片失败 ，error:{}<<==",e);
							}

						}
					}
					//获取商品价格
					if(json.getJSONObject("skuBase").containsKey("props")) {
						//促销

						if(inventoryMessage.getJSONObject("price").containsKey("shopProm")) {
							if(!inventoryMessage.getJSONObject("price").toString().contains("\"shopProm\":null")) {
								JSONArray shopPromList = JSONArray.fromObject(inventoryMessage.getJSONObject("price").getJSONArray("shopProm"));
								for(int w=0;w<shopPromList.size();w++) {
									JSONObject PromMessage = JSONObject.fromObject(shopPromList.toArray()[w].toString());
									String prom=PromMessage.getString("content").replace("[", "").replace("]", "").replace("\"", "");
									if(shopPromList.size()==w+1) {
										promotion.append(prom); 
									}else {
										promotion.append(prom+"&&");
									}
								}
							}
						}
					}
					//解析价格
					if(!json.getJSONObject("skuBase").toString().contains("\"props\":null")) {
						JSONArray skuBaseList = JSONArray.fromObject(json.getJSONObject("skuBase").getJSONArray("props"));
						if(skuBaseList.size()==1) {
							JSONObject valuesprops = JSONObject.fromObject(skuBaseList.toArray()[0].toString());
							JSONArray valuespropsList = JSONArray.fromObject(valuesprops.getJSONArray("values"));
							for(int kk=0;kk<valuespropsList.size();kk++) {
								JSONObject valuespropsNameList = JSONObject.fromObject(valuespropsList.toArray()[kk].toString());
								String productSkuName=valuespropsNameList.getString("name");

								//商品价格
								JSONArray skuIdList = JSONArray.fromObject(json.getJSONObject("skuBase").getJSONArray("skus"));
								JSONObject productSkuId = JSONObject.fromObject(skuIdList.toArray()[kk].toString());
								String productSkuMessage=productSkuId.getString("skuId");

								String originalPrice=json.getJSONObject("mock").getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject(productSkuMessage).getJSONObject("price").getString("priceText");							
								String productPrice=inventoryMessage.getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject(productSkuMessage).getJSONObject("price").getString("priceText");
								String inventorys =inventoryMessage.getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject(productSkuMessage).getString("quantity");
								if(StringUtils.isNotBlank(inventorys)) {
                                	inventoryCount=+Integer.valueOf(inventorys);
                                }
								Craw_goods_Price_Info priceInfo = new Craw_goods_Price_Info();
								priceInfo.setInventory(inventorys);//库存
								priceInfo.setSKUid(productSkuMessage);
								priceInfo.setSku_name(productSkuName);
								priceInfo.setBatch_time(batch_time);
								priceInfo.setGoodsid(goodsId);
								priceInfo.setCust_keyword_id(kweywordInfo.getCust_keyword_id());
								priceInfo.setPromotion(promotion.toString());
								priceInfo.setCurrent_price(productPrice);
								priceInfo.setOriginal_price(originalPrice);
								priceInfo.setTo_use_amount(infovo.getTtl_comment_num());
								priceInfo.setDelivery_place(infovo.getDelivery_place());
								//priceInfo.setSale_qty(totalSoldQuantity);//销量
								priceInfo.setChannel(client);//渠道
								priceInfo.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
								priceInfo.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
								priceList.add(priceInfo);
								if(client.equalsIgnoreCase("all")){
									for(int k=0;k<2;k++){
										if(k==0){
											priceInfo.setChannel(Fields.CLIENT_MOBILE);	
										}else{
											priceInfo.setChannel(Fields.CLIENT_PC);	
										}
										insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(priceInfo);
										insertPriceList.add(insertPriceMap);
									}
								}else{
									priceInfo.setChannel(Fields.CLIENT_MOBILE);
									insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(priceInfo);
									insertPriceList.add(insertPriceMap);
								}
							}
						}else {	 
							JSONObject skuBase = JSONObject.fromObject(skuBaseList.toArray()[1].toString());
							JSONArray valuesList = JSONArray.fromObject(skuBase.getJSONArray("values"));
							for(int s=0;s<valuesList.size();s++) {
								//颜色分类
								JSONObject skuName = JSONObject.fromObject(valuesList.toArray()[s].toString());
								String name=skuName.getString("name");
								//尺码
								JSONObject skuType = JSONObject.fromObject(skuBaseList.toArray()[0].toString());
								String skuTypeName=skuType.getString("name");

								JSONArray valuespropsList = JSONArray.fromObject(skuType.getJSONArray("values"));
								for(int kk=0;kk<valuespropsList.size();kk++) {
									JSONObject valuespropsNameList = JSONObject.fromObject(valuespropsList.toArray()[kk].toString());
									String productSkuName=name+","+skuTypeName+","+valuespropsNameList.getString("name");	
									//商品价格
									JSONArray skuIdList = JSONArray.fromObject(json.getJSONObject("skuBase").getJSONArray("skus"));
									JSONObject productSkuId = JSONObject.fromObject(skuIdList.toArray()[kk].toString());
									String productSkuMessage=productSkuId.getString("skuId");


									String originalPrice=json.getJSONObject("mock").getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject(productSkuMessage).getJSONObject("price").getString("priceText");							
									String productPrice=inventoryMessage.getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject(productSkuMessage).getJSONObject("price").getString("priceText");

									String inventorys =inventoryMessage.getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject(productSkuMessage).getString("quantity");
									if(StringUtils.isNotBlank(inventorys)) {
	                                	inventoryCount=+Integer.valueOf(inventorys);
	                                }
									Craw_goods_Price_Info priceInfo = new Craw_goods_Price_Info();
									priceInfo.setInventory(inventorys);//库存
									priceInfo.setSKUid(productSkuMessage);
									priceInfo.setSku_name(productSkuName);
									priceInfo.setBatch_time(batch_time);
									priceInfo.setGoodsid(goodsId);
									priceInfo.setDelivery_place(infovo.getDelivery_place());
									priceInfo.setCust_keyword_id(kweywordInfo.getCust_keyword_id());
									priceInfo.setPromotion(promotion.toString());
									priceInfo.setCurrent_price(productPrice);
									priceInfo.setOriginal_price(originalPrice);
									priceInfo.setPromotion(promotion.toString());
									priceInfo.setTo_use_amount(infovo.getTtl_comment_num());
									priceInfo.setChannel(client);//渠道
									priceInfo.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
									priceInfo.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
									priceList.add(priceInfo);
									if(client.equalsIgnoreCase("all")){
										for(int k=0;k<2;k++){
											if(k==0){
												priceInfo.setChannel(Fields.CLIENT_MOBILE);	
											}else{
												priceInfo.setChannel(Fields.CLIENT_PC);	
											}
											insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(priceInfo);
											insertPriceList.add(insertPriceMap);
										}
									}else{
										priceInfo.setChannel(Fields.CLIENT_MOBILE);
										insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(priceInfo);
										insertPriceList.add(insertPriceMap);
									}
								}

							}

						}

					}else {
						//获取商品价格
						Craw_goods_Price_Info priceInfo = new Craw_goods_Price_Info();
						if(inventoryMessage.getJSONObject("price").containsKey("extraPrices")) {
							if(!inventoryMessage.getJSONObject("price").toString().contains("\"extraPrices\":null")) {
								//原价
								JSONArray priceListData = JSONArray.fromObject(inventoryMessage.getJSONObject("price").getJSONArray("extraPrices"));
								for(int y=0;y<priceListData.size();y++) {
									JSONObject priceMessage = JSONObject.fromObject(priceListData.toArray()[y].toString());
									String originalPrice=priceMessage.getString("priceText");
									priceInfo.setOriginal_price(originalPrice);
								}
								//现价
								String currentPrice=inventoryMessage.getJSONObject("price").getJSONObject("price").getString("priceText");
								priceInfo.setCurrent_price(currentPrice);

							}
						}else {
							String originalPrice=inventoryMessage.getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject("0").getJSONObject("price").getString("priceText");
							String currentPrice =inventoryMessage.getJSONObject("price").getJSONObject("price").getString("priceText");
							priceInfo.setCurrent_price(currentPrice);
							priceInfo.setCurrent_price(originalPrice);
						}
						//库存
						String quantity=inventoryMessage.getJSONObject("skuCore").getJSONObject("sku2info").getJSONObject("0").getString("quantity");
						
						if(StringUtils.isNotBlank(quantity)) {
                        	inventoryCount=+Integer.valueOf(quantity);
                        }
						priceInfo.setInventory(quantity);//库存
						priceInfo.setBatch_time(batch_time);
						priceInfo.setGoodsid(goodsId);
						priceInfo.setCust_keyword_id(kweywordInfo.getCust_keyword_id());
						priceInfo.setPromotion(promotion.toString());
						priceInfo.setDelivery_place(infovo.getDelivery_place());
						priceInfo.setPromotion(promotion.toString());
						priceInfo.setTo_use_amount(infovo.getTtl_comment_num());
						priceInfo.setChannel(client);//渠道
						priceInfo.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
						priceInfo.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
						priceList.add(priceInfo);
						if(client.equalsIgnoreCase("all")){
							for(int k=0;k<2;k++){
								if(k==0){
									priceInfo.setChannel(Fields.CLIENT_MOBILE);	
								}else{
									priceInfo.setChannel(Fields.CLIENT_PC);	
								}
								insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(priceInfo);
								insertPriceList.add(insertPriceMap);
							}
						}else{
							priceInfo.setChannel(Fields.CLIENT_MOBILE);
							insertPriceMap = BeanMapUtil.convertBean2MapWithUnderscoreName(priceInfo);
							insertPriceList.add(insertPriceMap);
						}

					}	
				}

			}
			//商品详情
			infovo.setEgoodsId(mapItem.get("egoodsId"));
			infovo.setGoodsId(goodsId);
			infovo.setGoods_status(1);//状态
			infovo.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
			infovo.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
			infovo.setBatch_time(batch_time);
			infovo.setGoods_url(Fields.TMALL_URL+mapItem.get("egoodsId"));
			infovo.setPlatform_name_en(platform);
			infovo.setCust_keyword_id(Integer.valueOf(kweywordInfo.getCust_keyword_id()));
			infovo.setInventory(String.valueOf(inventoryCount));
			//数据详情插入
			insertItemMap = BeanMapUtil.convertBean2MapWithUnderscoreName(infovo);
			insertItemList.add(insertItemMap);


				//插入商品详情 价格
				if(StringUtil.isNotEmpty(infovo.getPlatform_goods_name())){
					if(insertPriceList!=null && insertPriceList.size()>0){

						try {
							batchInsertData.insertIntoData(insertPriceList, mapItem.get("database").toString(), Fields.TABLE_CRAW_GOODS_PRICE_INFO);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					if(insertItemList!=null && insertItemList.size()>0){
						try {
							batchInsertData.insertIntoData(insertItemList,mapItem.get("database").toString(), Fields.TABLE_CRAW_GOODS_INFO);
						} catch (Exception e) {
							e.printStackTrace();
						}	
					}
				}
		}

		return map;
	}
	
	/**   
	 *    
	 * 项目名称：crawler_Price_Monitoring   
	 * 类名称：TmallPcAPI   
	 * 类描述：   解析天猫数据H5页面
	 * 创建人：jack.zhao   
	 * 创建时间：2017年2月20日 下午5:30:24   
	 * 修改备注：   
	 * @version    
	 * @throws Exception 
	 *    
	 */
	@SuppressWarnings("unchecked")
	public  Map<String, Object> TmallparseItemH5Message_alibaba(String itemUrlContent,CrawKeywordsInfo kweywordInfo,Map<String,String>mapItem) throws Exception{
		Map<String,Object > map=new HashMap<String,Object >();

		Map<String,String > message=new HashMap<String,String >();
		Map<String,String > mapMobile=new HashMap<String,String >();
		Map<String,Object> insertItem=new HashMap<String,Object>();
		List<Map<String,Object>> insertImage= Lists.newArrayList();
		List<Map<String,Object>> priceInfo= Lists.newArrayList();
		List<Craw_goods_Price_Info>priceList=new ArrayList<Craw_goods_Price_Info>();
		mapMobile.put("channel",Fields.CLIENT_MOBILE);
		message.putAll(mapItem); int comment_status=0;
		String shopName="",platform_shoptype=null,subCatId=null,sellerId=null,picturl=null,goodsName=null,delivery_place=null;
		String postageFree=Fields.DONTPACK_MAIL;
		String originalPrice=null;
		String currentPrice=null;
		String skuId=null;
		String skuName=null;
		StringBuffer promotion=new StringBuffer();
		String shopId=null; 
		String location="";
		String totalSoldQuantity="";
		String commentCount="";
		String quantity="";
		String priceDesc="";
		String	deposit="";//定金
		String  coupons="";//卷
		String reserve_num="";//预定件数
		String present="";
		String goodsId=mapItem.get("goodsId").toString();
		String time=mapItem.get("timeDate").toString();
		String platform=mapItem.get("dataType").toString();
		String client=mapItem.get("client").toString();
		String shop_name = "";
		int total_quantity = 0;
		int pageTop=Integer.valueOf(mapItem.get("pageTop").toString());
		String database=mapItem.get("database").toString();
		String storage=mapItem.get("storage").toString();
		Document doc = Jsoup.parseBodyFragment(itemUrlContent);
		try {
			goodsName = doc.getElementsByClass("main cell").html();
			shop_name = doc.getElementsByClass("shop-name").html();
		}catch (Exception e ){
			logger.error("解析商品店铺信息失败");
		}
		try {
			//判断商品是否存在
			if (StringUtils.isNotBlank(goodsName)) {
				//FileUtils.writeStringToFile(new File("D://tmall//"+parameter.getEgoodsId()+".html"),StringMessage,"utf-8");
				ArrayList<Element> element = doc.select("script");
				for (int i = 0; i < element.size(); i++) {

					if (element.get(i).data().toString().contains("var _DATA_Detail = ")) {
						String[] elScriptList = element.get(i).data().toString().split("var _DATA_Detail = ");
						String itemMessage = elScriptList[1].replace(";", "").trim();
						Calendar c = Calendar.getInstance();
						long timeInMillis = c.getTimeInMillis();
						int code = Integer.parseInt(mapItem.get("code")) / 100;
						int code2 = code + 1;
						String sellerTag = "";
						String sellerTag2 = "";
						String sellerTag3 = "";
						String sellerTag4 = "";
						String isUseInventoryCenter = "true";

						String url = "";
						if ("81".equals(mapItem.get("accountId")) || "85".equals(mapItem.get("accountId"))) {
							sellerTag = "39391266";
							sellerTag2 = "1170937140066910216";
							sellerTag3 = "216806109434904736";
							sellerTag4 = "35596691079619";
							url = "https://mdskip.taobao.com/core/changeLocation.htm?queryDelivery=true&queryProm=true&tmallBuySupport=true&ref=&" +
									"areaId=" + mapItem.get("code") +
									"&_ksTS=" + timeInMillis + "_" + code +
									"&callback=jsonp" + code2 + "&isUseInventoryCenter=" +
									isUseInventoryCenter + "&cartEnable=true&" +
									"sellerUserTag3=" + sellerTag3 + "&service3C=false&" +
									"sellerUserTag2=" + sellerTag2 + "&isSecKill=false&isAreaSell=true&" +
									"sellerUserTag4=" + sellerTag4 + "&offlineShop=false&" +
									"itemId=" + mapItem.get("egoodsId") +
									"&sellerUserTag=" + sellerTag + "&showShopProm=false&tgTag=false&isPurchaseMallPage=false&isRegionLevel=true&household=false&notAllowOriginPrice=false&addressLevel=3";


						} else if ("86".equals(mapItem.get("accountId"))) {
							sellerTag = "34672672";
							sellerTag2 = "18014536485306368";
							sellerTag3 = "144185556820066432";
							sellerTag4 = "4294967683";
							isUseInventoryCenter = "false";
							url = "https://mdskip.taobao.com/core/changeLocation.htm?queryDelivery=true&queryProm=true&tmallBuySupport=true&ref=&areaId=" + mapItem.get("code")
									+ "&_ksTS=" + timeInMillis + "_" + code + "&callback=jsonp" + code2 + "&isUseInventoryCenter=" + isUseInventoryCenter + "&cartEnable=true&sellerUserTag3=" + sellerTag3 + "&service3C=false&sellerUserTag2=" + sellerTag2 + "&isSecKill=false&isAreaSell=true&sellerUserTag4=" + sellerTag4 + "&offlineShop=false&itemId=" + mapItem.get("egoodsId") +
									"&sellerUserTag=" + sellerTag + "&showShopProm=false&tgTag=false&isPurchaseMallPage=false&isRegionLevel=true&household=false&notAllowOriginPrice=false&addressLevel=3";
						}


						String cookie = mapItem.get("cookie");
						String ivPage = JsoupUtil.getHtmlByJsoup(url, cookie, "https://world.tmall.com/");

						String msg = ivPage.replaceAll(" ", "").replaceFirst("[(]", "%!@#").split("%!@#")[1];
						String inventory = msg.substring(0, msg.length() - 1);
						com.alibaba.fastjson.JSONObject js = null;
						//com.alibaba.fastjson.JSONObject inventoryMessage = com.alibaba.fastjson.JSONObject.parseObject(inventory).getJSONObject("defaultModel");
						com.alibaba.fastjson.JSONObject json = com.alibaba.fastjson.JSONObject.parseObject(itemMessage);
						//对异常json字符串进行try catch 并格式化异常json字符串
						try {
							js = com.alibaba.fastjson.JSONObject.parseObject(inventory).getJSONObject("defaultModel");
						} catch (Exception e) {
							logger.warn("==========================>json解析异常，开始格式化异常字符串并解析：<==========================");
							js = com.alibaba.fastjson.JSONObject.parseObject(JSONUtil.formatJsonString(inventory)).getJSONObject("defaultModel");
						}

						if (js.getJSONObject("deliveryDO").containsKey("deliveryAddress")) {
							location = js.getJSONObject("deliveryDO").getString("deliveryAddress");
						}
						if (StringUtils.isEmpty(location)) {
							try {
								location = ((com.alibaba.fastjson.JSONObject) js.getJSONObject("deliveryDO").getJSONObject("deliverySkuMap").getJSONArray("default").get(0)).getString("skuDeliveryAddress");
							} catch (Exception e) {
								logger.warn(e.getStackTrace());
							}
						}
						//String destination = js.getJSONObject("deliveryDO").getString("destination");

						quantity = js.getJSONObject("inventoryDO").getString("icTotalQuantity");

						//totalSoldQuantity =js.getJSONObject("inventoryDO").getString("totalQuantity");

						//解析价格
						try {
							if (js.getJSONObject("itemPriceResultDO").getJSONObject("priceInfo").containsKey("def")) {
								if (js.getJSONObject("itemPriceResultDO").getJSONObject("priceInfo").getJSONObject("def").containsKey("promotionList")) {
									com.alibaba.fastjson.JSONObject promotionList = (com.alibaba.fastjson.JSONObject) js.getJSONObject("itemPriceResultDO").getJSONObject("priceInfo").getJSONObject("def").getJSONArray("promotionList").get(0);
									currentPrice = promotionList.getString("price");
								} else {
									currentPrice = js.getJSONObject("itemPriceResultDO").getJSONObject("priceInfo").getJSONObject("def").getString("price");
								}


								//现价
								if (js.getJSONObject("itemPriceResultDO").getJSONObject("priceInfo").containsKey("def")) {
									originalPrice = js.getJSONObject("itemPriceResultDO").getJSONObject("priceInfo").getJSONObject("def").getString("price");
								}
								//获取原价

								if (StringUtils.isEmpty(originalPrice)) {
									if (json.containsKey("mock")) {
										originalPrice = json.getJSONObject("mock").getJSONObject("price").getJSONObject("price").getString("priceText");
									} else {
										originalPrice = currentPrice;
									}
								}
							}
						} catch (Exception e) {
							logger.warn("======================>价格信息解析失败<======================" + e.getMessage());
						}
						//循环获取促销
						try {
							for (Object o : js.getJSONObject("itemPriceResultDO").getJSONArray("tmallShopProm")) {
								com.alibaba.fastjson.JSONObject j = (com.alibaba.fastjson.JSONObject) o;
								for (int i1 = 0; i1 < j.getJSONArray("promPlanMsg").size(); i1++) {
									promotion.append(j.getJSONArray("promPlanMsg").get(i1)).append("&&");
								}

							}
						} catch (Exception e) {
							logger.warn("======================>促销信息解析失败<======================" + e.getMessage());
						}
						//获取邮费信息
						try {
							com.alibaba.fastjson.JSONObject postageJs = (com.alibaba.fastjson.JSONObject) js.getJSONObject("deliveryDO").getJSONObject("deliverySkuMap").getJSONArray("default").get(0);
							postageFree = postageJs.getString("postage");
							if (postageFree == null) {
								postageFree = postageJs.getString("money");
							}
						} catch (Exception e) {
							logger.warn("======================>邮费信息解析失败<======================" + e.getMessage());
						}


						if (StringUtils.isEmpty(goodsName)) {
							//商品名称
							goodsName = json.getJSONObject("item").getString("title");
						}
						if (StringUtils.isEmpty(shop_name)) {
							//店铺名称
							shop_name = json.getJSONObject("seller").getString("shopName");
						}
						//店铺Id
						if (json.getJSONObject("seller").containsKey("shopId")) {
							shopId = json.getJSONObject("seller").getString("shopId");
						}

						//用户Id
						sellerId = json.getJSONObject("seller").getString("userId");
						//行业Id
						subCatId = json.getJSONObject("item").getString("categoryId");
						//评论数
						com.alibaba.fastjson.JSONObject rate = com.alibaba.fastjson.JSONObject.parseObject(json.getString("json"));
						if (rate != null) {
							if (rate.containsKey("totalCount")) {
								commentCount = rate.getString("totalCount");
							}
						}
						if (shop_name.contains("超市")) {
							platform_shoptype = "超市";
						} else if (shop_name.contains("旗舰")) {
							platform_shoptype = "天猫旗舰店";
						} else if (shop_name.contains("天猫会员店")) {
							platform_shoptype = "天猫会员店";
						} else {
							platform_shoptype = "非自营";
						}
						//判断商品小图是否存在
						if (StringUtil.isNotEmpty(json.getJSONObject("item").get("images").toString())) {
							com.alibaba.fastjson.JSONArray jsonArray = com.alibaba.fastjson.JSONArray.parseArray(json.getJSONObject("item").getString("images"));
							for (int ii = 0; ii < jsonArray.size(); ii++) {
								try {
									if (ii == 0) {
										picturl = "http:" + jsonArray.getString(ii);
									}
								} catch (Exception e) {
									logger.error("==>>解析商品小图片失败 ，error:{}<<==", e);
								}

							}
						}
						if (StringUtils.isEmpty(location)) {
							location = mapItem.get("delivery");
						}
						if (StringUtils.isEmpty(delivery_place)) {
							delivery_place = mapItem.get("delivery");
						}

						message.put("shopName", shop_name);//卖家店铺名称
						message.put("region", location);//卖家位置
						message.put("shopid", shopId);//商品Id
						message.put("platform_shoptype", platform_shoptype);//平台商店类型
						message.put("subCatId", subCatId);
						message.put("sellerId", sellerId);
						message.put("sellerName", shopName);//卖家店铺名称
						message.put("picturl", picturl);//图片url
						message.put("goodsName", goodsName);//商品名称
						message.put("delivery_place", delivery_place);//交易地址
						message.put("inventory", quantity);//商品库存
						//message.put("transactNum", totalSoldQuantity);//月销
						message.put("postageFree", postageFree);//是不包邮
						message.put("rateNum", commentCount);//商品总评论数
						message.put("skuId", skuId);//商品sku
						message.put("originalPrice", originalPrice);//原价
						message.put("currentPrice", currentPrice);//现价
						message.put("promotion", promotion.toString());//促销
						message.put("keywordId", kweywordInfo.getCust_keyword_id());
						message.put("timeDate", time);
						message.put("platform_name", mapItem.get("platform").toString());
						message.put("goodsUrl", mapItem.get("url"));
						message.put("present", present);//获取促销赠品详情;
						message.put("channel", client);
						//存在skuid的情况
						if (json.containsKey("skuBase") && json.getJSONObject("skuBase").containsKey("skus")
								&& json.getJSONObject("skuBase").getJSONArray("skus") != null) {
							com.alibaba.fastjson.JSONArray skus = json.getJSONObject("skuBase").getJSONArray("skus");
							for (int jj = 0; jj < skus.size(); jj++) {
								skuId = ((com.alibaba.fastjson.JSONObject) skus.get(jj)).getString("skuId");
								skuName = ((com.alibaba.fastjson.JSONObject) ((com.alibaba.fastjson.JSONObject) json.getJSONObject("skuBase").getJSONArray("props").get(0)).getJSONArray("values").get(jj)).getString("name");

								if (js.getJSONObject("itemPriceResultDO").getJSONObject("priceInfo").containsKey(skuId)) {
									com.alibaba.fastjson.JSONObject priceJs = js.getJSONObject("itemPriceResultDO").getJSONObject("priceInfo").getJSONObject(skuId);
									if (priceJs.containsKey("promotionList")) {
										currentPrice = ((com.alibaba.fastjson.JSONObject) priceJs.getJSONArray("promotionList").get(0)).getString("price");
									} else {
										currentPrice = priceJs.getString("price");
									}

									originalPrice = priceJs.getString("price");

								}
								String skuQnt = js.getJSONObject("inventoryDO").getJSONObject("skuQuantity").getJSONObject(skuId).getString("quantity");
								message.put("skuId", skuId);
								message.put("inventory", skuQnt);
								message.put("skuName", skuName);
								message.put("currentPrice", currentPrice);
								crawlerPublicClassService.parseItemPrice(message, kweywordInfo.getCust_keyword_id(), goodsId, time, platform, pageTop, database, storage);//插入数据


							}
							message.put("inventory",quantity);//价格行的库存设置为总库存

						} else {

							crawlerPublicClassService.parseItemPrice(message, kweywordInfo.getCust_keyword_id(), goodsId, time, platform, pageTop, database, storage);//插入数据

						}

					}

				}
			}
		}catch (Exception e){
			logger.error("=====================>天猫价格解析失败<=====================");
		}




		if (StringUtils.isNotBlank(goodsName)){
			message.put("inventory",String.valueOf(total_quantity));
			Craw_goods_InfoVO infoVo=crawlerPublicClassService.itemFieldsData(message,database,storage);
			map.put(Fields.TABLE_CRAW_GOODS_INFO, infoVo);
			map.put("goods_info_price", priceList);
			map.putAll(message);
		}

//		if(StringUtil.isNotEmpty(commentCount)){
//			comment_status=Fields.STATUS_COUNT_1;
//		}else{
//			comment_status=Fields.STATUS_COUNT;
//		}
		//crawlerPublicClassService.goodsCommentInfo(time,database.toString(),kweywordInfo,commentCount,goodsId,comment_status);//插入评论
//		if(insertImage.size()>0){
//			batchInsertData.insertIntoData(insertImage,database,Fields.CRAW_GOODS_COMMENT_INFO);
//		}
		return map;
		
	}
	//获取商品属性值
	@SuppressWarnings("unchecked")
	public void commodityProperty(JSONObject json,CrawKeywordsInfo kweywordInfo,Map<String,String>mapItem,String database){
		try {
			Map<String,Object> insertItem=new HashMap<String,Object>();
			List<Map<String,Object>> insertList= Lists.newArrayList();
			JSONArray groupPropsList=json.getJSONObject("data").getJSONObject("props").getJSONArray("groupProps");
			for(int i=0;i<groupPropsList.size();i++){
				JSONObject jsonObject = JSONObject.fromObject(groupPropsList.toArray()[i]);
				if(jsonObject.toString().contains(Fields.INFORMATION)){
					JSONArray	jsondata=jsonObject.getJSONArray(Fields.INFORMATION);
					for(int k=0;k<jsondata.size();k++){
						JSONObject object = JSONObject.fromObject(jsondata.toArray()[k]);
						Set<Entry<String, Object>> setdata = object.entrySet();
						for(Entry<String, Object> entry : setdata) {//商品属性
							Craw_goods_attribute_Info info =new Craw_goods_attribute_Info();
							info.setAttri_value(entry.getValue().toString());//值
							info.setCraw_attri_cn(entry.getKey().toString());//key
							info.setCust_keyword_id(Integer.valueOf(kweywordInfo.getCust_keyword_id()));
							info.setEgoodsid(mapItem.get("egoodsId"));
							info.setGoodsid(mapItem.get("goodsId"));//insertItem
							info.setPlatform_name_en(mapItem.get("platform").toString());
							info.setUpdate_date(SimpleDate.SimpleDateFormatData().format(new Date()));
							info.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
							info.setBatch_time(mapItem.get("timeDate"));
							insertItem= BeanMapUtil.convertBean2MapWithUnderscoreName(info);
							insertList.add(insertItem); 	
						} 
					}

				}
			}
			if(insertList!=null && insertList.size()>0){
				batchInsertData.insertIntoData(insertList,database,Fields.CRAW_GOODS_ATTRIBUTE_INFO);	
			}
		} catch (Exception e) {
			logger.error("获取商品属性失败"+e.getMessage());
		}
	} 
		//解析商品上架时间
	@SuppressWarnings("unchecked")
	public int tmallGoodsOnTime(String time,Craw_goods_Info craw_goods_Info,int page,String accountId,String database) throws Exception{
		Map<String,Object> insertItemTime=new HashMap<String,Object>();
		List<Map<String,Object>> insertprices = Lists.newArrayList();
		String  itemMessage=time.substring(time.indexOf("(")+1, time.lastIndexOf(")"));
		logger.info(itemMessage);
		JSONObject jsonObject = JSONObject.fromObject(itemMessage);
		int lastPage=jsonObject.getJSONObject("rateDetail").getJSONObject("paginator").getInt("lastPage");
		if(page==lastPage || lastPage==0){  
			JSONArray priceInfoJson = jsonObject.getJSONObject("rateDetail").getJSONArray("rateList");//数据插入数据库
			Craw_goods_Fixed_Info  info=new Craw_goods_Fixed_Info();
			info.setCust_account_id(Integer.valueOf(accountId));
			info.setCust_keyword_id(craw_goods_Info.getCust_keyword_id());
			info.setEgoodsid(craw_goods_Info.getEgoodsId());
			info.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
			info.setPlatform_name_en(craw_goods_Info.getPlatform_name_en());
			if(lastPage!=0){
				info.setFirst_reviewtime(JSONObject.fromObject(priceInfoJson.toArray()[priceInfoJson.size()-1]).getString("rateDate"));			
			}
			if(StringUtil.isNotEmpty(info.getFirst_reviewtime())){
				insertItemTime=BeanMapUtil.convertBean2MapWithUnderscoreName(info);
				insertprices.add(insertItemTime); 
				batchInsertData.insertIntoData(insertprices,database,Fields.TABLE_CRAW_GOODS_FIXED_INFO);		
			}
		}else{
			return lastPage; 
		}
		return lastPage;
	}

	
	//解析商品上架时间
		@SuppressWarnings("unchecked")
		public int tmallGoodsOnTimeData(String time,Craw_goods_Info accountinfo,int page,String accountId,String database,String goodsId) throws Exception{
			Map<String,Object> insertItemTime=new HashMap<String,Object>();
			List<Map<String,Object>> insertprices = Lists.newArrayList();
			//time=new String(time.getBytes("ISO-8859-1"),"UTF-8"); 
			String  itemMessage=time.substring(time.indexOf("(")+1, time.lastIndexOf(")"));
			JSONObject jsonObject = JSONObject.fromObject(itemMessage);
			int lastPage=jsonObject.getJSONObject("rateDetail").getJSONObject("paginator").getInt("lastPage");
			if(page==lastPage || lastPage==0){  
				JSONArray priceInfoJson = jsonObject.getJSONObject("rateDetail").getJSONArray("rateList");//数据插入数据库
				Craw_goods_Fixed_Info  info=new Craw_goods_Fixed_Info();
				info.setCust_account_id(Integer.valueOf(accountId));
				info.setCust_keyword_id(Integer.valueOf(accountinfo.getCust_keyword_id()));
				info.setEgoodsid(accountinfo.getEgoodsId());
				info.setUpdate_time(SimpleDate.SimpleDateFormatData().format(new Date()));
				info.setPlatform_name_en(accountinfo.getPlatform_name_en());
				info.setFixed_status(Fields.STATUS_COUNT_1);
				info.setGoodsId(goodsId);
				if(lastPage!=0){
					info.setFirst_reviewtime(JSONObject.fromObject(priceInfoJson.toArray()[priceInfoJson.size()-1]).getString("rateDate"));			
				}
				if(StringUtil.isNotEmpty(info.getFirst_reviewtime())){
					insertItemTime=BeanMapUtil.convertBean2MapWithUnderscoreName(info);
					insertprices.add(insertItemTime); 
					batchInsertData.insertIntoData(insertprices,database,Fields.TABLE_CRAW_GOODS_FIXED_INFO);		
				}
			}else{
				return lastPage; 
			}
			return lastPage;
		}
}
