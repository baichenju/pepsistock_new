package com.eddc;
import org.apache.log4j.Logger;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import com.eddc.framework.DynamicDataSourceRegister;
@SpringBootApplication
@Import(DynamicDataSourceRegister.class) 
@MapperScan("com.eddc.mapper")// mapper 接口类扫描包配置 
public class Application {   
	private static Logger logger = Logger.getLogger(Application.class);
	public static void main(String[] args){ 
	SpringApplication.run(Application.class, args);  
	logger.info("============= SpringBoot Start Success =============");
	 }                                             
}                                                                                                         