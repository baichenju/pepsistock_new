package com.eddc.job;
import java.io.Serializable;
import java.util.List;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import com.eddc.method.JdData;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.service.CrawlerPublicClassService;
import com.eddc.service.JobAndTriggerService;
import com.eddc.util.Fields;
import com.eddc.util.publicClass;

@SuppressWarnings({ "serial" })
@PersistJobDataAfterExecution
@DisallowConcurrentExecution// 不允许并发执行
public class Jd_Job extends Yhd_Job implements Job, Serializable {
    private static Logger _log = Logger.getLogger(Jd_Job.class);
    @Autowired
    private JobAndTriggerService jobAndTriggerService;
    @Autowired
    private CrawlerPublicClassService crawlerPublicClassService;
    @Autowired
    private JdData jdData;
    int count = 0;
    private String tableName = Fields.TABLE_CRAW_KEYWORDS_INF;//表名称
    private String counts = Fields.STATUS_ON;
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        String jobName = context.getJobDetail().getKey().toString();
        jobName = jobName.substring(jobName.indexOf(".") + 1).toString();
        int pageTop = 0;
        _log.info(">>> [" + jobName + "] job start >>>>");
        _log.info(">>> Invoke Jd_Job execute method >>>");
        try {
            String Ip = publicClass.getLocalIP();
            List<QrtzCrawlerTable> listjobName = jobAndTriggerService.queryJob(jobName, Ip, Fields.DOCKER, Fields.PLATFORM_JD_CN);
            if (jobName.contains(Fields.SEARCH)) {
                tableName = Fields.TABLE_CRAW_CRAW_KEYWORD_TEMP_INFO_FORESARCH;
                counts = Fields.STATUS_OFF;
                pageTop = 1;
            }
           // crawlerPublicClassService.updateCrawKeywordHistory(listjobName, listjobName.get(0).getDatabases());//更新已经抓取商品的状态
            jdData.startCrawlerJd(listjobName, tableName, counts, pageTop);//开始抓取数据
            _log.info(">>> [" + jobName + "] finish >>>");
        } catch (Exception e) {
            _log.error(">>> Jd_Job execute fail,please check! >>>");
            _log.error(e.getMessage());
        }
    }

}
