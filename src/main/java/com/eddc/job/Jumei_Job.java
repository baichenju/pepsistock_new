/**   
 * Copyright © 2017 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.job 
 * @author: jack.zhao   
 * @date: 2017年11月23日 下午3:53:36 
 */
package com.eddc.job;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import com.eddc.model.CommodityPrices;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.service.CrawlerPublicClassService;
import com.eddc.service.JobAndTriggerService;
import com.eddc.task.Jumei_DataCrawlTask;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.SpringContextUtil;
import com.eddc.util.publicClass;

/**   
 *    
 * 项目名称：Price_monitoring_crawler   
 * 类名称：Jumei_Job
 * 类描述：   
 * 创建人：jack.zhao   
 * 创建时间：2017年11月23日 下午3:53:36   
 * 修改人：jack.zhao   
 * 修改时间：2017年11月23日 下午3:53:36   
 * 修改备注：   
 * @version    
 *    
 */
@SuppressWarnings("serial")
@PersistJobDataAfterExecution
@DisallowConcurrentExecution// 不允许并发执行
public class Jumei_Job implements Job, Serializable {
	private static Logger _log = Logger.getLogger(Jumei_Job.class);
	private ApplicationContext contexts= SpringContextUtil.getApplicationContext();
	@Autowired
	private JobAndTriggerService jobAndTriggerService ;
	@Autowired
	private CrawlerPublicClassService crawlerPublicClassService;
	private String regex=".*[a-zA-Z]+.*";  
	public Jumei_Job(){}
	@Override
	public void execute(JobExecutionContext context)throws JobExecutionException {
		String jobName=context.getJobDetail().getKey().toString();
		jobName=jobName.substring(jobName.indexOf(".")+1).toString();
		_log.info(jobName+"__聚美 Job执行时间: " + new Date());
		try {
			//jobAndTriggerService = (JobAndTriggerService) contexts.getBean(JobAndTriggerService.class);
			//crawlerPublicClassService = (CrawlerPublicClassService) contexts.getBean(CrawlerPublicClassService.class);
			String Ip=publicClass.getLocalIP();
			List<QrtzCrawlerTable>listjobName=jobAndTriggerService.queryJob(jobName,Ip,Fields.DOCKER,Fields.PLATFORM_JUMEI_CN);
			String time=SimpleDate.SimpleDateFormatData().format(new Date());  
			if(listjobName.size()>0){
				_log.info("开始同步聚美数据当前用户是"+listjobName.get(0).getUser_Id()+"--------"+SimpleDate.SimpleDateFormatData().format(new Date()));
				crawlerPublicClassService.SynchronousData(listjobName.get(0).getPlatform(),listjobName.get(0).getDatabases(),listjobName.get(0).getUser_Id(),Fields.TABLE_CRAW_KEYWORDS_INF);//同步数据
				_log.info("开始爬聚美数据当前用户是"+listjobName.get(0).getUser_Id()+"--------"+SimpleDate.SimpleDateFormatData().format(new Date()));
				crawlerJumeiPriceMonitoring(time,listjobName);
				if(listjobName.get(0).getClient().equals("ALL")|| listjobName.get(0).getClient().equals("all")){
					_log.info("开始爬聚美PC端价格当前用户是"+listjobName.get(0).getUser_Id()+"--------"+SimpleDate.SimpleDateFormatData().format(new Date()));
					TerminalCommodityPrices(time,listjobName);
				}
			}else{
				_log.info("当前聚美用户不存在请检查爬虫状态是否关闭！爬虫结束------"+new Date());	
			}	
		} catch (Exception e) {
			_log.info("获取JobAndTriggerService对象为空------"+new Date()+e.toString());
		}

	}
	/**  
	 * @Title: crawlerJumeiPriceMonitoring  
	 * @Description: TODO 开始解析聚美数据
	 * @param @param listjobName
	 * @param @param message    设定文件  
	 * @return void    返回类型  
	 * @throws  
	 */  
	@SuppressWarnings("rawtypes")
	public void crawlerJumeiPriceMonitoring(String time,List<QrtzCrawlerTable>listjobName){
		int jj = 0;
		ArrayList<Future> futureList = new ArrayList<Future>();
		ExecutorService taskExecutor = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());  //初始化线程池
		List<CrawKeywordsInfo>list=crawlerPublicClassService.crawAndParseInfo(listjobName,listjobName.get(0).getDatabases(),Fields.TABLE_CRAW_KEYWORDS_INF,Fields.STATUS_ON,Fields.STATUS_COUNT);//查询爬虫url条数  
		ApplicationContext context = SpringContextUtil.getApplicationContext();
		for (CrawKeywordsInfo accountInfo : list) {  jj++;
		Jumei_DataCrawlTask dataCrawlTask = (Jumei_DataCrawlTask) context.getBean(Jumei_DataCrawlTask.class);
		Map<String,Object>jumei=new HashMap<String,Object>();
		dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
		dataCrawlTask.setDataType(listjobName.get(0).getPlatform());
		dataCrawlTask.setStorage(listjobName.get(0).getStorage());
		dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource())); 
		dataCrawlTask.setTimeDate(time);
		dataCrawlTask.setSum("" + jj + "/" + list.size() + "");
		dataCrawlTask.setCrawKeywordsInfo(accountInfo);
		dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id());
		dataCrawlTask.setType(Fields.STYPE_1); 
		dataCrawlTask.setIp(listjobName.get(0).getIP());
		dataCrawlTask.setJumeiData(jumei);
		if(listjobName.get(0).getPlatform().equals(Fields.PLATFORM_JUMEI)){
			Matcher m=Pattern.compile(regex).matcher(accountInfo.getCust_keyword_name());  
			if(m.matches()){
				dataCrawlTask.setSetUrl(Fields.JUMEI_APP_URL_2 + accountInfo.getCust_keyword_name()+ "&type=jumei_deal");
			}else{
				dataCrawlTask.setSetUrl(Fields.JUMEI_APP_URL + accountInfo.getCust_keyword_name()+"&type=jumei_mall");
			}
		}else if(listjobName.get(0).getPlatform().equals(Fields.PLATFORM_JUMEI_GLOBAL_EN)){
			Matcher m=Pattern.compile(regex).matcher(accountInfo.getCust_keyword_name());  
			if(m.matches()){
				dataCrawlTask.setSetUrl(Fields.JUMEI_APP_GLOBAL_URL_2 + accountInfo.getCust_keyword_name()+ "&type=global_deal"); 
			}else{
				dataCrawlTask.setSetUrl(Fields.JUMEI_APP_GLOBAL_URL + accountInfo.getCust_keyword_name()+ "&type=global_mall"); 
			}

		}
		Future<Map<String, Object>> future=taskExecutor.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值  
		futureList.add(future);
		_log.info(Thread.currentThread().getName()+Runtime.getRuntime().availableProcessors());
		}
		//taskExecutor.shutdown();
		while (true) {
			if (taskExecutor.isTerminated()) {
				taskExecutor.shutdownNow();  
				crawlerPublicClassService.bulkInsertData(futureList,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据
				break;
			}
		}
		try {
			crawAndParseInfoAndPricefailure(time,listjobName);//检查是否有失败的商品
		} catch (Exception e) {
			_log.info("解析失败信息失败"+e.toString());
		}
	}

	@SuppressWarnings("rawtypes")
	public void crawAndParseInfoAndPricefailure(String time,List<QrtzCrawlerTable>listjobName) throws InterruptedException {
		int jj = 0; int count = 0;
		ArrayList<Future> futureList = new ArrayList<Future>();
		ExecutorService taskExecutor = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());  //初始化线程池
		do {
			List<CrawKeywordsInfo> list = crawlerPublicClassService.crawAndGrabFailureGoodsData(listjobName,listjobName.get(0).getDatabases(),Fields.TABLE_CRAW_KEYWORDS_INF,Fields.STATUS_COUNT);//开始解析数据
			ApplicationContext context = SpringContextUtil.getApplicationContext();
			for (CrawKeywordsInfo accountInfo : list) {
				jj++;
				Jumei_DataCrawlTask dataCrawlTask = (Jumei_DataCrawlTask) context.getBean(Jumei_DataCrawlTask.class);
				Map<String,Object>jumei=new HashMap<String,Object>();
				Matcher m=Pattern.compile(regex).matcher(accountInfo.getCust_keyword_name());  
				dataCrawlTask.setDataType(listjobName.get(0).getPlatform());
				dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
				dataCrawlTask.setStorage(listjobName.get(0).getStorage());
				dataCrawlTask.setTimeDate(time);
				dataCrawlTask.setSum("" + jj + "/" + list.size() + "");
				dataCrawlTask.setCrawKeywordsInfo(accountInfo);
				dataCrawlTask.setType(Fields.STYPE_1);
				dataCrawlTask.setJumeiData(jumei);
				dataCrawlTask.setIp(listjobName.get(0).getIP());
				dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id());
				dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource())); 
				if(listjobName.get(0).getPlatform().equals(Fields.PLATFORM_JUMEI)){
					if(m.matches()){
						dataCrawlTask.setSetUrl(Fields.JUMEI_APP_URL_2 + accountInfo.getCust_keyword_name()+ "&type=jumei_deal");
					}else{
						dataCrawlTask.setSetUrl(Fields.JUMEI_APP_URL + accountInfo.getCust_keyword_name()+"&type=jumei_mall");
					}
				}else if(listjobName.get(0).getPlatform().equals(Fields.PLATFORM_JUMEI_GLOBAL_EN)){
					if(m.matches()){
						dataCrawlTask.setSetUrl(Fields.JUMEI_APP_GLOBAL_URL_2 + accountInfo.getCust_keyword_name()+ "&type=global_deal"); 
					}else{
						dataCrawlTask.setSetUrl(Fields.JUMEI_APP_GLOBAL_URL + accountInfo.getCust_keyword_name()+ "&type=global_mall"); 
					}

				} 
				Future<Map<String, Object>> future=taskExecutor.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值  
				futureList.add(future);
				_log.info(Thread.currentThread().getName()+Runtime.getRuntime().availableProcessors());
			}
			jj = 0;
			//taskExecutor.shutdown();
			while (true) {
				if(((ExecutorService) taskExecutor).isTerminated()){
					taskExecutor.shutdownNow(); 
					crawlerPublicClassService.bulkInsertData(futureList,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据
					break;
				}
			}
			count++;
			if (count >6) {
				Thread.sleep(1000);
				return;
			}
		} while (true);
	}
	/**
	 * 获取PC商品价格
	 *
	 * @throws InterruptedException
	 */
	@SuppressWarnings("rawtypes")
	public void TerminalCommodityPrices(String time,List<QrtzCrawlerTable>listjobName) {
		int ii = 0;
		long times=System.currentTimeMillis();
		ArrayList<Future> futureList = new ArrayList<Future>();
		ExecutorService taskExecutor = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());  //初始化线程池
		List<CommodityPrices> listPrice = crawlerPublicClassService.CommodityPricesData(listjobName,listjobName.get(0).getDatabases(),Fields.TABLE_CRAW_KEYWORDS_INF,Fields.STATUS_COUNT);
		for (CommodityPrices accountInfo : listPrice) { ii++;
		Jumei_DataCrawlTask dataCrawlTask = (Jumei_DataCrawlTask) contexts.getBean(Jumei_DataCrawlTask.class);
		Map<String,Object>jumei=new HashMap<String,Object>();
		Matcher m=Pattern.compile(regex).matcher(accountInfo.getEgoodsId());  
		accountInfo.setPlatform_name_en(listjobName.get(0).getPlatform());
		dataCrawlTask.setStorage(listjobName.get(0).getStorage());
		dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
		accountInfo.setBatch_time(time);
		dataCrawlTask.setDataType(listjobName.get(0).getPlatform());
		dataCrawlTask.setSum("" + ii + "/" + listPrice.size() + "");
		dataCrawlTask.setCommodityPrices(accountInfo);
		dataCrawlTask.setType(Fields.STYPE_6); 
		dataCrawlTask.setJumeiData(jumei);
		dataCrawlTask.setIp(listjobName.get(0).getIP());
		dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id());
		dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource())); 
		if(listjobName.get(0).getPlatform().equals(Fields.PLATFORM_JUMEI)){
			if(m.matches()){
				dataCrawlTask.setSetUrl(Fields.JUMEI_PC_URL_Promotion_2 + accountInfo.getEgoodsId()+ "&brand_id="+accountInfo.getPlatform_category()+"&site=sh&callback=jQuery111207574128594781935_1512112116418&_="+times+"");	//国内聚美特卖商品
			}else{
				dataCrawlTask.setSetUrl(Fields.JUMEI_PC_URL_Promotion_1 + accountInfo.getEgoodsId()+"&brand_id="+accountInfo.getPlatform_category()+"&site=sh&callback=jQuery1112017022424268169556_1512110686059&_="+times+"");//国内普通商品
			}
		}else if(listjobName.get(0).getPlatform().equals(Fields.PLATFORM_JUMEI_GLOBAL_EN)){
			if(m.matches()){
				dataCrawlTask.setSetUrl(Fields.JUMEI_PC_URL_GLOBAL_Promotion_4 + accountInfo.getEgoodsId()+ ".html");//海外购极速免税店可以获取价格促销
			}else {
				dataCrawlTask.setSetUrl(Fields.JUMEI_PC_URL_GLOBAL_Promotion_3 + accountInfo.getEgoodsId()+ ".html");//海外购
			}
		} 
		Future<Map<String, Object>> future=taskExecutor.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值  
		futureList.add(future);
		_log.info(Thread.currentThread().getName()+Runtime.getRuntime().availableProcessors());
		} 
		//taskExecutor.shutdown();
		while (true) { 
			if(((ExecutorService) taskExecutor).isTerminated()){
				taskExecutor.shutdownNow(); 
				crawlerPublicClassService.bulkInsertData(futureList,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据
				break;
			}
		}
		ii = 0;
		try {// 递归补漏
			Thread.sleep(20000);
			RecursionFailureGoods(time,listjobName);
		} catch (Exception e) {
			_log.info("递归补漏失败+===========" + e.getMessage() + "---------------" + SimpleDate.SimpleDateFormatData().format(new Date()));
		}
	}

	/**
	 * 递归检查获取没有成功抓取商品的价格
	 *
	 * @throws InterruptedException
	 */
	@SuppressWarnings("rawtypes")
	public void RecursionFailureGoods(String time,List<QrtzCrawlerTable>listjobName) throws InterruptedException {
		int ii = 0; int count=0;
		do {
			ArrayList<Future> futureList = new ArrayList<Future>();
			ExecutorService taskExecutor = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());  //初始化线程池
			List<CommodityPrices> listPriceJd = crawlerPublicClassService.RecursionFailureGoodsMessage(listjobName,listjobName.get(0).getDatabases(),Fields.CLIENT_PC,Fields.TABLE_CRAW_KEYWORDS_INF);
			if (listPriceJd.size() > 0) {
				for (CommodityPrices accountInfo : listPriceJd) {
					ii++;
					Jumei_DataCrawlTask dataCrawlTask = (Jumei_DataCrawlTask) contexts.getBean(Jumei_DataCrawlTask.class);
					Matcher m=Pattern.compile(regex).matcher(accountInfo.getEgoodsId());  
					Map<String,Object>jumei=new HashMap<String,Object>();
					dataCrawlTask.setStorage(listjobName.get(0).getStorage());
					dataCrawlTask.setDataType(listjobName.get(0).getPlatform());
					dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
					dataCrawlTask.setTimeDate(time);
					dataCrawlTask.setSum("" + ii + "/" + listPriceJd.size() + "");
					accountInfo.setBatch_time(time);
					dataCrawlTask.setJumeiData(jumei);
					accountInfo.setPlatform_name_en(listjobName.get(0).getPlatform());
					dataCrawlTask.setCommodityPrices(accountInfo);
					dataCrawlTask.setType(Fields.STYPE_6);
					dataCrawlTask.setIp(listjobName.get(0).getIP());
					dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id());
					dataCrawlTask.setSetUrl(Fields.MIYA_PC_URL + accountInfo.getEgoodsId() + ".html");
					dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource()));
					if(listjobName.get(0).getPlatform().equals(Fields.PLATFORM_JUMEI)){
						if(m.matches()){
							dataCrawlTask.setSetUrl(Fields.JUMEI_PC_URL_Promotion_2 + accountInfo.getEgoodsId()+ "&brand_id="+accountInfo.getPlatform_category()+"&callback=jQuery111207574128594781935_1512112116418&_=1512112116419");	//国内聚美特卖商品
						}else{
							dataCrawlTask.setSetUrl(Fields.JUMEI_PC_URL_Promotion_1 + accountInfo.getEgoodsId()+ "&brand_id="+accountInfo.getPlatform_category()+"&site=sh&callback=jQuery1112017022424268169556_1512110686059&_=1512110686060");//国内普通商品
						}
					}else if(listjobName.get(0).getPlatform().equals(Fields.PLATFORM_JUMEI_GLOBAL_EN)){
						if(m.matches()){
							dataCrawlTask.setSetUrl(Fields.JUMEI_PC_URL_GLOBAL_Promotion_4 + accountInfo.getEgoodsId()+ ".html");//海外购极速免税店可以获取价格促销
						}else {
							dataCrawlTask.setSetUrl(Fields.JUMEI_PC_URL_GLOBAL_Promotion_3 + accountInfo.getEgoodsId()+ ".html");//海外购
						}
					}
					Future<Map<String, Object>> future=taskExecutor.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值  
					futureList.add(future);
					_log.info(Thread.currentThread().getName()+Runtime.getRuntime().availableProcessors());
				}
			}
			//taskExecutor.shutdown();
			while (true) {
				if(((ExecutorService) taskExecutor).isTerminated()){
					taskExecutor.shutdownNow(); 
					crawlerPublicClassService.bulkInsertData(futureList,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据
					break;
				}
			}
			ii = 0;
			count++;
			if (count > 6) {
				_log.info("递归补漏 第"+count+"次" + SimpleDate.SimpleDateFormatData().format(new Date()));
				Thread.sleep(5000); 
				return;
			}
		} while (true);	
	}
}
