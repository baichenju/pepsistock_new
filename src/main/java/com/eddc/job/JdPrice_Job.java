package com.eddc.job;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.model.Price_PackageVO;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.redis.JedisService;
import com.eddc.service.CrawlerPublicClassService;
import com.eddc.service.JobAndTriggerService;
import com.eddc.task.JdPrice_DataCrawlTask;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.SpringContextUtil;
import com.eddc.util.publicClass;
import com.github.pagehelper.util.StringUtil;
@PersistJobDataAfterExecution
@DisallowConcurrentExecution// 不允许并发执行
public class JdPrice_Job  implements Job,Serializable {
	private static final long serialVersionUID = 1L;
	private static Logger _log = Logger.getLogger(JdPrice_Job.class);
	@SuppressWarnings("unused")
	private ApplicationContext contexts = SpringContextUtil.getApplicationContext();
	@Autowired
	private JobAndTriggerService jobAndTriggerService;
	@Autowired
	private CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	JedisService jedisService;
	int count = 0;
	private String tableName = Fields.TABLE_CRAW_KEYWORDS_INF;//表名称
	private String counts = Fields.STATUS_ON;
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		String jobName = context.getJobDetail().getKey().toString();
		jobName = jobName.substring(jobName.indexOf(".") + 1).toString();
		int pageTop = 0;
		_log.info(">>> [" + jobName + "] job start >>>");
		_log.info(">>> Invoke Jd_Job execute method >>>");
		try {
			//jobAndTriggerService = (JobAndTriggerService) contexts.getBean(JobAndTriggerService.class);
			//crawlerPublicClassService = (CrawlerPublicClassService) contexts.getBean(CrawlerPublicClassService.class);
			// jedisService = (JedisService) contexts.getBean(JedisService.class);
			String Ip = publicClass.getLocalIP();
			List<QrtzCrawlerTable> listjobName = jobAndTriggerService.queryJob(jobName, Ip, Fields.DOCKER, Fields.PLATFORM_JD_CN);
			if (jobName.contains(Fields.SEARCH)) {
				tableName = Fields.TABLE_CRAW_CRAW_KEYWORD_TEMP_INFO_FORESARCH_HISTORY;
				counts = Fields.STATUS_OFF;
				pageTop = 1;
			}
			crawlerPublicClassService.updateCrawKeywordHistory(listjobName, listjobName.get(0).getDatabases());//更新已经抓取商品的状态
			startCrawlerJd(listjobName, tableName, counts, pageTop);
			_log.info(">>> [" + jobName + "] finish >>>");
		} catch (Exception e) {
			_log.error(">>> Jd_Job execute fail,please check! >>>");
			_log.error(e.getMessage());
		}
	}

	//启动爬虫
	public void startCrawlerJd(List<QrtzCrawlerTable> listjobName, String tableName, String count, int pageTop) throws InterruptedException, ExecutionException {
		_log.info(">>> Invoke Jd_Job startCrawlerJd method >>>");
		String time = SimpleDate.SimpleDateFormatData().format(new Date());
		//jobAndTriggerService = (JobAndTriggerService) contexts.getBean(JobAndTriggerService.class);
		//crawlerPublicClassService = (CrawlerPublicClassService) contexts.getBean(CrawlerPublicClassService.class);
		crawlerPublicClassService.synchronousCommodityPriceData(listjobName,listjobName.get(0).getDatabases());//同步商品价格数据
		crawAndParseInfoAndPriceJD(listjobName, time, null, tableName, count, pageTop);//启动爬虫
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void crawAndParseInfoAndPriceJD(List<QrtzCrawlerTable>listjobName,String time,String place_code,String tableName,String count,int pageTop) throws InterruptedException, ExecutionException{
		int p=0;
		//ApplicationContext contexts= SpringContextUtil.getApplicationContext();
		ExecutorService taskExecutor = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());  //初始化线程池 	
		CompletionService completion=new ExecutorCompletionService(taskExecutor);
		List<Price_PackageVO>priceVo=new ArrayList<Price_PackageVO>();
		String dataCookie =crawlerPublicClassService.dataCookie(listjobName.get(0).getUser_Id(),listjobName.get(0).getDatabases(),listjobName.get(0).getPlatform());//获取cookie
		List<CrawKeywordsInfo>list=crawlerPublicClassService.fillCatchPrice(listjobName,listjobName.get(0).getDatabases(),tableName,count,pageTop);//开始解析数据 
		long awaitTime=Integer.valueOf(listjobName.get(0).getDocker())*1000*60; 
		int totalSize = list.size(); //总记录数  
		int pageSize = 100; //每页N条  
		int totalPage = totalSize/pageSize; //共N页  
		int totalPagesurplus = totalSize % pageSize; //共N页  
		for(int i=0;i<totalPage;i++){
			Price_PackageVO vo=new Price_PackageVO();
			StringBuilder  egoodId =new StringBuilder ();
			StringBuilder dataEgoodsId =new StringBuilder();
			StringBuilder keyword =new StringBuilder();
			for(int j=0;j<pageSize;j++){
				if(StringUtil.isEmpty(egoodId.toString())){
					egoodId.append("J_"+list.get(i*pageSize+j).getCust_keyword_name());
					keyword.append(list.get(i*pageSize+j).getCust_keyword_id());
					dataEgoodsId.append("'"+list.get(i*pageSize+j).getCust_keyword_name()+"'");
				}else{
					egoodId.append("%2CJ_"+list.get(i*pageSize+j).getCust_keyword_name());
					keyword.append(","+list.get(i*pageSize+j).getCust_keyword_id());
					dataEgoodsId.append(","+"'"+list.get(i*pageSize+j).getCust_keyword_name()+"'");
				}
			}
			vo.setDataEgoodsId(dataEgoodsId.toString());
			vo.setEgoodsId(egoodId.toString());
			vo.setKeyword(keyword.toString());
			priceVo.add(vo); 
		}
		Price_PackageVO vos=new Price_PackageVO();
		StringBuilder  egoodIds =new StringBuilder ();
		StringBuilder dataEgoodsIds =new StringBuilder();
		StringBuilder keywords =new StringBuilder();
		for(int k=0;k<totalPagesurplus;k++){
			if(StringUtil.isEmpty(egoodIds.toString())){
				egoodIds.append("J_"+list.get(totalPage*pageSize+k).getCust_keyword_name());
				keywords.append(list.get(totalPage*pageSize+k).getCust_keyword_id());
				dataEgoodsIds.append("'"+list.get(totalPage*pageSize+k).getCust_keyword_name()+"'");
			}else{
				egoodIds.append("%2CJ_"+list.get(totalPage*pageSize+k).getCust_keyword_name());
				keywords.append(","+list.get(totalPage*pageSize+k).getCust_keyword_id());
				dataEgoodsIds.append(","+"'"+list.get(totalPage*pageSize+k).getCust_keyword_name()+"'");
			}
		}
		if(totalPagesurplus>0){
			vos.setDataEgoodsId(dataEgoodsIds.toString());
			vos.setEgoodsId(egoodIds.toString());
			vos.setKeyword(keywords.toString());
			priceVo.add(vos);	
		}
		for (Price_PackageVO  price : priceVo) { p++;
		Map<String,Object>jdMap=new HashMap<String,Object>();
		JdPrice_DataCrawlTask dataCrawlTask = (JdPrice_DataCrawlTask) SpringContextUtil.getApplicationContext().getBean(JdPrice_DataCrawlTask.class);
		dataCrawlTask.setDataType(listjobName.get(0).getPlatform());
		dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
		dataCrawlTask.setStorage(listjobName.get(0).getStorage());
		dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id());
		dataCrawlTask.setDataEgoodsId(price.getDataEgoodsId());
		dataCrawlTask.setCrawKeywordsInfo(list.get(0));
		dataCrawlTask.setKeyworId(price.getKeyword());
		dataCrawlTask.setIp(listjobName.get(0).getIP());
		dataCrawlTask.setPlace_code(place_code);
		dataCrawlTask.setCookie(dataCookie); 
		dataCrawlTask.setJdMap(jdMap);
		dataCrawlTask.setTimeDate(time);
		dataCrawlTask.setType(Fields.STYPE_9); 
		dataCrawlTask.setPage(Fields.STATUS_OFF);
		dataCrawlTask.setSum("" +p+1 + "/" + priceVo.size() + "");
		dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource())); 
		dataCrawlTask.setStatus_type(String.valueOf(listjobName.get(0).getPromotion_status()));
		dataCrawlTask.setSetUrl(Fields.JD_URL_SERCH+price.getEgoodsId());
		completion.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值  
		}
		if (pageTop == Fields.STATUS_COUNT) {
			while (true) {
				if (((ExecutorService) taskExecutor).isTerminated()) {
					taskExecutor.shutdownNow();
				}
			}
		}else{
			try {
				if(!taskExecutor.awaitTermination(awaitTime, TimeUnit.MILLISECONDS)){ 
					taskExecutor.shutdownNow();    //超时的时候向线程池中所有的线程发出中断(interrupted)。
				}else{
					taskExecutor.shutdownNow();
				} 
			} catch (Exception e) {
				taskExecutor.shutdownNow();// 超时的时候向线程池中所有的线程发出中断(interrupted)。
			}
			crawlerPublicClassService.synchronousCommodityPriceData(listjobName,listjobName.get(0).getDatabases());//同步商品价格数据
		}
	}
}
