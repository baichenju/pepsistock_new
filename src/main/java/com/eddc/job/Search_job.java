/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.job 
 * @author: jack.zhao   
 * @date: 2018年3月14日 下午12:20:51 
 */
package com.eddc.job;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import org.apache.log4j.Logger;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.springframework.beans.factory.annotation.Autowired;
import com.eddc.method.JdData;
import com.eddc.method.SearchData;
import com.eddc.method.TaobaoData;
import com.eddc.method.TmallData;
import com.eddc.method.YhdData;
import com.eddc.model.Craw_keywords_delivery_place;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.service.CrawlerPublicClassService;
import com.eddc.service.JobAndTriggerService;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.publicClass;
/**   
 *    
 * 项目名称：Price_monitoring_crawler   
 * 类名称：Pepsi_job   
 * 类描述：   
 * 创建人：jack.zhao   
 * 创建时间：2018年3月14日 下午12:20:51   
 * 修改人：jack.zhao   
 * 修改时间：2018年3月14日 下午12:20:51   
 * 修改备注：   
 * @version    
 *    
 */
@SuppressWarnings("serial")
@PersistJobDataAfterExecution
@DisallowConcurrentExecution// 不允许并发执行
public class Search_job implements Job,Serializable  {
	private static Logger _log = Logger.getLogger(Search_job.class);
	@Autowired
	public JobAndTriggerService jobAndTriggerService;
	@Autowired
	private CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	private SearchData searchData;
	@Autowired
	public JdData jdData;
	@Autowired
	private TmallData tmallData;
	@Autowired
	private TaobaoData taobaoData;
	@Autowired
	private YhdData yhdData;
	@Override 
	public void execute(JobExecutionContext context) throws JobExecutionException {
		String jobName=context.getJobDetail().getKey().toString();
		jobName=jobName.substring(jobName.indexOf(".")+1).toString();
		getProductSearch(jobName);
	} 

	public void getProductSearch(String jobNanme) {
		String Ip=publicClass.getLocalIP();
		List<QrtzCrawlerTable>listjobName=jobAndTriggerService.queryJob(jobNanme,Ip,Fields.DOCKER,jobNanme.split("_")[1].toString());
		String time=SimpleDate.SimpleDateFormatData().format(new Date());
		if(listjobName.size()>0){ 
			//同步 搜索抓取商品的egoodsId
			//获取爬虫状态
			int sum=crawlerPublicClassService.crawlerStatus(listjobName.get(0).getPlatform(),listjobName.get(0).getDatabases(),listjobName.get(0).getUser_Id());

			if(listjobName.size()>0){ 
				List<Craw_keywords_delivery_place>List_Code=crawlerPublicClassService.Keywords_Delivery_Place(listjobName,listjobName.get(0).getDatabases());
				if(List_Code.size()>0){
					for(Craw_keywords_delivery_place code:List_Code){
						try {
							if(String.valueOf(code.getSearch_status()).equals(Fields.STATUS_ON)){
								_log.info("开始执行job搜索商品信息"); 
								sum=searchData.crawlerPriceMonitoring(time,listjobName,code.getDelivery_place_code(),code.getDelivery_place_name());	
							}	
						} catch (Exception e) {
							_log.error("获取多个地区出错了"+e);
						}
					}  
				}else{ 
					sum=searchData.crawlerPriceMonitoring(time,listjobName,null,null);
				}  
				if(listjobName.get(0).getUser_Id().equals(Fields.COUNT_80)){
					if(sum==Fields.STATUS_11){
						if(listjobName.get(0).getPlatform().equals(Fields.PLATFORM_TAOBAO_EN)){
							taobaoData.crawlerTaobaoPriceMonitoring(listjobName,Fields.TABLE_CRAW_CRAW_KEYWORD_TEMP_INFO_FORESARCH, Fields.STATUS_OFF,Fields.STATUS_COUNT_1);
						}else if(listjobName.get(0).getPlatform().equals(Fields.PLATFORM_TMALL_EN)){
							tmallData.crawlerTmallPriceMonitoring(listjobName, Fields.TABLE_CRAW_CRAW_KEYWORD_TEMP_INFO_FORESARCH, Fields.STATUS_OFF,Fields.STATUS_COUNT_1);
						}else if(listjobName.get(0).getPlatform().equals(Fields.PLATFORM_JD)){
							try {jdData.startCrawlerJd(listjobName, Fields.TABLE_CRAW_CRAW_KEYWORD_TEMP_INFO_FORESARCH, Fields.STATUS_OFF,Fields.STATUS_COUNT_1);} catch (InterruptedException e) {} catch (ExecutionException e) {e.printStackTrace();}
						}else if(listjobName.get(0).getPlatform().equals(Fields.PLATFORM_YHD)){
							try {yhdData.atartCrawlerYhd(listjobName, Fields.TABLE_CRAW_CRAW_KEYWORD_TEMP_INFO_FORESARCH, Fields.STATUS_OFF,Fields.STATUS_COUNT_1);} catch (InterruptedException e) {}
						}
					}
				}
			}
		}	
	}
}
