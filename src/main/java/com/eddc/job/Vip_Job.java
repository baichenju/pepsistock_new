/**   
 * Copyright © 2017 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.job 
 * @author: jack.zhao   
 * @date: 2017年11月23日 下午3:53:36 
 */
package com.eddc.job;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.apache.log4j.Logger;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.eddc.method.VipData;
import com.eddc.model.CommodityPrices;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.service.CrawlerPublicClassService;
import com.eddc.service.JobAndTriggerService;
import com.eddc.task.Vip_DataCrawlTask;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.SpringContextUtil;
import com.eddc.util.publicClass;
/**   
 *    
 * 项目名称：Price_monitoring_crawler   
 * 类名称：Vip_Job   
 * 类描述：   
 * 创建人：jack.zhao   
 * 创建时间：2017年11月23日 下午3:53:36   
 * 修改人：jack.zhao   
 * 修改时间：2017年11月23日 下午3:53:36   
 * 修改备注：   
 * @version    
 *    
 */
@PersistJobDataAfterExecution
@DisallowConcurrentExecution// 不允许并发执行
public class Vip_Job extends Alibaba_Job implements Job {
	private static Logger _log = Logger.getLogger(Vip_Job.class);  
	@Autowired
	private JobAndTriggerService jobAndTriggerService ;
	@Autowired
	private CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	private VipData vipData;
	 private String tableName = Fields.TABLE_CRAW_KEYWORDS_INF;//表名称
	 private String counts = Fields.STATUS_ON;
	public Vip_Job(){};
	@Override
	public void execute(JobExecutionContext context)throws JobExecutionException {
		String jobName=context.getJobDetail().getKey().toString();
		jobName=jobName.substring(jobName.indexOf(".")+1).toString();
		int pageTop = 0;
		_log.info(jobName+"__唯品会 Job执行时间: " + new Date());
		try {
			String Ip=publicClass.getLocalIP();
			List<QrtzCrawlerTable>listjobName=jobAndTriggerService.queryJob(jobName,Ip,Fields.DOCKER,Fields.PLATFORM_VIP_CN);
			String time=SimpleDate.SimpleDateFormatData().format(new Date()); 
			if(listjobName.size()>0){
				_log.info("开始同步唯品会数据当前用户是"+listjobName.get(0).getUser_Id()+"--------"+SimpleDate.SimpleDateFormatData().format(new Date()));
				crawlerPublicClassService.SynchronousData(listjobName.get(0).getPlatform(),listjobName.get(0).getDatabases(),listjobName.get(0).getUser_Id(),Fields.TABLE_CRAW_KEYWORDS_INF);//同步数据
				_log.info("开始爬唯品会数据当前用户是"+listjobName.get(0).getUser_Id()+"--------"+SimpleDate.SimpleDateFormatData().format(new Date()));
				vipData.crawlerVipPriceMonitoring(time,listjobName,tableName,counts,pageTop);	
				if(listjobName.get(0).getClient().equals("ALL")|| listjobName.get(0).getClient().equals("all")){
					_log.info("开始爬唯品会PC端价格当前用户是"+listjobName.get(0).getUser_Id()+"--------"+SimpleDate.SimpleDateFormatData().format(new Date()));
				  vipData.TerminalCommodityPrices(time,listjobName,tableName,counts,pageTop);
				}
			}else{
				_log.info("当前唯品会用户不存在请检查爬虫状态是否关闭！爬虫结束------"+new Date());	
			}	
		} catch (Exception e) {
			_log.info("获取JobAndTriggerService对象为空------"+new Date()+e.toString());
		}

	}

	
}
