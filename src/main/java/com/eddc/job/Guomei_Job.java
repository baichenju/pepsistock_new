package com.eddc.job;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.apache.log4j.Logger;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import com.eddc.model.CommodityPrices;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.service.CrawlerPublicClassService;
import com.eddc.service.JobAndTriggerService;
import com.eddc.task.GuoMei_DataCrawlTask;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.SpringContextUtil;
import com.eddc.util.publicClass;
@PersistJobDataAfterExecution
@DisallowConcurrentExecution// 不允许并发执行
public class Guomei_Job implements Job{
	private static Logger _log = Logger.getLogger(Guomei_Job.class);
	private String message="";
	long date_time=System.currentTimeMillis();
	private ApplicationContext contexts= SpringContextUtil.getApplicationContext();
	@Autowired
	private JobAndTriggerService jobAndTriggerService ;
	@Autowired
	private CrawlerPublicClassService crawlerPublicClassService;
	public Guomei_Job(){}
	public void execute(JobExecutionContext context)throws JobExecutionException {
		String jobName=context.getJobDetail().getKey().toString();
		jobName=jobName.substring(jobName.indexOf(".")+1).toString();
		_log.info(jobName+"__国美 Job执行时间: " + new Date());
		try {
			//jobAndTriggerService = (JobAndTriggerService) contexts.getBean(JobAndTriggerService.class);
			//crawlerPublicClassService = (CrawlerPublicClassService) contexts.getBean(CrawlerPublicClassService.class);
			String Ip=publicClass.getLocalIP();
			List<QrtzCrawlerTable>listjobName=jobAndTriggerService.queryJob(jobName,Ip,Fields.DOCKER,Fields.GUOMEI);
			String time=SimpleDate.SimpleDateFormatData().format(new Date()); 
			if(listjobName.size()>0){
				_log.info("开始同步国美数据 当前用户是"+listjobName.get(0).getUser_Id()+"--------"+SimpleDate.SimpleDateFormatData().format(new Date()));
				crawlerPublicClassService.SynchronousData(listjobName.get(0).getPlatform(),listjobName.get(0).getDatabases(),listjobName.get(0).getUser_Id(),Fields.TABLE_CRAW_KEYWORDS_INF);//同步数据
				_log.info("开始抓取国美数据当前用户是--------"+listjobName.get(0).getUser_Id()+"抓取时间------"+SimpleDate.SimpleDateFormatData().format(new Date()));
				crawAndParseInfoAndPriceGuoMei(listjobName,time,message); //开始抓取数据
				_log.info("开始抓取国美价格数据 当前用户是--------"+listjobName.get(0).getUser_Id()+"抓取时间------"+SimpleDate.SimpleDateFormatData().format(new Date()));
				if(listjobName.get(0).getClient().equals("ALL")|| listjobName.get(0).getClient().equals("all")){
					CommodityPricesGuoMei(listjobName,time,message);//获取商品价格
				}

			}else{
				_log.info("当前国美用户不存在请检查爬虫状态是否关闭！爬虫结束------"+new Date());	
			}
		} catch (Exception e) {
			_log.info("获取JobAndTriggerService对象为空------"+new Date()+e.toString());
		}

	}

	//商品详情
	@SuppressWarnings("rawtypes")
	public void crawAndParseInfoAndPriceGuoMei (List<QrtzCrawlerTable>listjobName,String time,String messageId) throws InterruptedException{
		int jj=0;
		ExecutorService taskExecutor = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());  //初始化线程池 
		List<CrawKeywordsInfo>list=crawlerPublicClassService.crawAndParseInfo(listjobName,listjobName.get(0).getDatabases(),listjobName.get(0).getPlatform(),Fields.TABLE_CRAW_KEYWORDS_INF,Fields.STATUS_COUNT);//开始解析数据 
		ArrayList<Future> futureList = new ArrayList<Future>();
		for(CrawKeywordsInfo accountInfo:list){ jj++;
		Map<String,Object>guomei=new HashMap<String,Object>();
		GuoMei_DataCrawlTask dataCrawlTask = (GuoMei_DataCrawlTask) contexts.getBean(GuoMei_DataCrawlTask.class);
		dataCrawlTask.setDataType(listjobName.get(0).getPlatform());
		dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
		dataCrawlTask.setTimeDate(time);
		dataCrawlTask.setSum(""+jj+"/"+list.size()+"");
		dataCrawlTask.setType(Fields.STYPE_1); 
		dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id()); 
		dataCrawlTask.setCrawKeywordsInfo(accountInfo);
		dataCrawlTask.setGuomeiData(guomei);
		dataCrawlTask.setStorage(listjobName.get(0).getStorage());
		dataCrawlTask.setIp(listjobName.get(0).getIP());
		dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource())); 
		dataCrawlTask.setSetUrl(Fields.GUOMEI_APP_URL+accountInfo.getCust_keyword_name()+".html"); 
		Future<Map<String, Object>> future=taskExecutor.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值  
		futureList.add(future);
		_log.info(Thread.currentThread().getName()+Runtime.getRuntime().availableProcessors());
		}
		//taskExecutor.shutdown();
		while(true){
			if(((ExecutorService) taskExecutor).isTerminated()){
				taskExecutor.shutdownNow(); 
				_log.info("信息爬虫结束开始检查是否有漏抓数据-----"+SimpleDate.SimpleDateFormatData().format(new Date()));
				crawlerPublicClassService.bulkInsertData(futureList,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据
				crawAndParseInfoAndPricefailure(time,messageId,listjobName);//检查是否有失败的商品
				break;
			}	

		} 

	} 
	///检查失败的item数据
	@SuppressWarnings("rawtypes")
	public void crawAndParseInfoAndPricefailure(String time,String messageId,List<QrtzCrawlerTable>listjobName) throws InterruptedException{
		int jj=0; int count=0;
		ExecutorService taskExecutor = Executors.newFixedThreadPool(10);  //初始化线程池 
		ArrayList<Future> futureList = new ArrayList<Future>();
		do{  
			List<CrawKeywordsInfo>list=crawlerPublicClassService.crawAndGrabFailureGoodsData(listjobName,listjobName.get(0).getDatabases(),Fields.TABLE_CRAW_KEYWORDS_INF,Fields.STATUS_COUNT);//开始解析数据 
			for(CrawKeywordsInfo accountInfo:list){ jj++;	
			GuoMei_DataCrawlTask dataCrawlTask = (GuoMei_DataCrawlTask) contexts.getBean(GuoMei_DataCrawlTask.class);
			dataCrawlTask.setDataType(listjobName.get(0).getPlatform());
			dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
			Map<String,Object>guomei=new HashMap<String,Object>();
			dataCrawlTask.setTimeDate(time);
			dataCrawlTask.setSum(""+jj+"/"+list.size()+"");
			dataCrawlTask.setType(Fields.STYPE_1); 
			dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id()); 
			dataCrawlTask.setCrawKeywordsInfo(accountInfo);
			dataCrawlTask.setStorage(listjobName.get(0).getStorage());
			dataCrawlTask.setIp(listjobName.get(0).getIP());
			dataCrawlTask.setGuomeiData(guomei);
			dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource())); 
			dataCrawlTask.setSetUrl(Fields.GUOMEI_APP_URL+accountInfo.getCust_keyword_name()+".html"); 
			Future<Map<String, Object>> future=taskExecutor.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值  
			futureList.add(future);
			_log.info(Thread.currentThread().getName()+Runtime.getRuntime().availableProcessors());
			}
			//taskExecutor.shutdown();
			while(true){
				if(((ExecutorService) taskExecutor).isTerminated()){
					taskExecutor.shutdownNow(); 
					crawlerPublicClassService.bulkInsertData(futureList,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据
					break;
				}
			}
			jj=0;
			count++;
			Thread.sleep(5000); 
			if (count > 5) {
				Thread.sleep(5000); 
				return;
			}
		}while (true);
	}
	//商品价格
	@SuppressWarnings("rawtypes")
	public void CommodityPricesGuoMei(List<QrtzCrawlerTable>listjobName,String time,String message){
		_log.info("-----------------开始抓取商品街价格"+SimpleDate.SimpleDateFormatData().format(new Date()) +"-----------------------------------------------");
		int ii=0;
		ArrayList<Future> futureList = new ArrayList<Future>();
		ExecutorService taskExecutor = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());  //初始化线程池 
		List<CommodityPrices>listPrice=crawlerPublicClassService.CommodityPricesData(listjobName,listjobName.get(0).getDatabases(),Fields.TABLE_CRAW_KEYWORDS_INF,Fields.STATUS_COUNT);
		for(CommodityPrices accountInfo:listPrice){ ii++;	
		Map<String,Object>guomei=new HashMap<String,Object>();
		String egoodsId[]=accountInfo.getEgoodsId().toString().split("-");
		GuoMei_DataCrawlTask dataCrawlTask = (GuoMei_DataCrawlTask) contexts.getBean(GuoMei_DataCrawlTask.class);
		dataCrawlTask.setDataType(listjobName.get(0).getPlatform());
		dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
		dataCrawlTask.setTimeDate(time);
		dataCrawlTask.setSum(""+ii+"/"+listPrice.size()+"");
		accountInfo.setBatch_time(time);
		dataCrawlTask.setIp(listjobName.get(0).getIP());
		dataCrawlTask.setStorage(listjobName.get(0).getStorage());
		accountInfo.setPlatform_name_en(listjobName.get(0).getPlatform());
		dataCrawlTask.setCommodityPrices(accountInfo);
		dataCrawlTask.setGuomeiData(guomei);
		dataCrawlTask.setType(Fields.STYPE_6); 
		dataCrawlTask.setSetUrl(Fields.GUOMEI_PC_PRICE+egoodsId[0]+"/"+egoodsId[1]+"/N/21010100/210101001/1/null/flag/item/allStores?callback=allStores&_=1517206275418");
		dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource())); 
		dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id());
		Future<Map<String, Object>> future=taskExecutor.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值  
		futureList.add(future);
		_log.info(Thread.currentThread().getName()+Runtime.getRuntime().availableProcessors());
		}
		//taskExecutor.shutdown();
		while(true){
			if(((ExecutorService) taskExecutor).isTerminated()){
				taskExecutor.shutdownNow(); 
				crawlerPublicClassService.bulkInsertData(futureList,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据
				break;
			}
		}
		ii=0;
		try {// 递归补漏
			RecursionFailureGoods(time,listjobName);
		} catch (Exception e) {
			_log.info("递归补漏失败+==========="+e.getMessage()+"---------------"+SimpleDate.SimpleDateFormatData().format(new Date()));
		}
	}
	/** 
	 * 递归检查获取没有成功抓取商品的价格
	 * @throws InterruptedException 
	 */
	@SuppressWarnings("rawtypes")
	public void RecursionFailureGoods(String time,List<QrtzCrawlerTable>listjobName) throws InterruptedException{
		_log.info("-----------------开始抓取商品街价格"+SimpleDate.SimpleDateFormatData().format(new Date()) +"-----------------------------------------------");
		int ii=0;int count=0;
		ExecutorService taskExecutor = Executors.newFixedThreadPool(10);  //初始化线程池 
		ArrayList<Future> futureList = new ArrayList<Future>();
		do{
			List<CommodityPrices>listPriceSuning=crawlerPublicClassService.RecursionFailureGoods_Price(listjobName,listjobName.get(0).getDatabases(),Fields.TABLE_CRAW_KEYWORDS_INF);
			if(listPriceSuning.size()>0){
				for(CommodityPrices accountInfo:listPriceSuning){ ii++;	
				Map<String,Object>guomei=new HashMap<String,Object>();
				String egoodsId[]=accountInfo.getEgoodsId().toString().split("-");
				GuoMei_DataCrawlTask dataCrawlTask = (GuoMei_DataCrawlTask) contexts.getBean(GuoMei_DataCrawlTask.class);
				dataCrawlTask.setDataType(listjobName.get(0).getPlatform());
				dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
				dataCrawlTask.setTimeDate(time);
				dataCrawlTask.setSum(""+ii+"/"+listPriceSuning.size()+"");
				accountInfo.setBatch_time(time);
				dataCrawlTask.setIp(listjobName.get(0).getIP());
				accountInfo.setPlatform_name_en(listjobName.get(0).getPlatform());
				dataCrawlTask.setCommodityPrices(accountInfo);
				dataCrawlTask.setType(Fields.STYPE_6);
				dataCrawlTask.setStorage(listjobName.get(0).getStorage());
				dataCrawlTask.setGuomeiData(guomei);
				dataCrawlTask.setSetUrl(Fields.GUOMEI_PC_PRICE+egoodsId[0]+"/"+egoodsId[1]+"/N/21010100/210101001/1/null/flag/item/allStores?callback=allStores&_=1517206275418");
				dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource())); 
				dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id());
				Future<Map<String, Object>> future=taskExecutor.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值  
				futureList.add(future);
				_log.info(Thread.currentThread().getName()+Runtime.getRuntime().availableProcessors());
				}	
			}
			//taskExecutor.shutdown();
			while(true){
				if(((ExecutorService) taskExecutor).isTerminated()){
					taskExecutor.shutdownNow(); 
					crawlerPublicClassService.bulkInsertData(futureList,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据
					break;
				}
			}
			ii=0;
			count++;
			if (count > 8) { 
				Thread.sleep(5000); 
				_log.info("递归检查获取没有成功抓取商品的价格----第："+count+"次循环------"+SimpleDate.SimpleDateFormatData().format(new Date()));
				return;
			} 
		}while (true);
	} 
}
