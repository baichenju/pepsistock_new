/**   
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 * @Package: com.eddc.job 
 * @author: jack.zhao   
 * @date: 2018年9月17日 下午4:09:33 
 */
package com.eddc.job;

import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.eddc.method.SearchData;
import com.eddc.service.CrawlerPublicClassService;

/**   
*    
* 项目名称：selection_pepsi_crawler   
* 类名称：SynchronousData   
* 类描述：   
* 创建人：jack.zhao   
* 创建时间：2018年9月17日 下午4:09:33   
* 修改人：jack.zhao   
* 修改时间：2018年9月17日 下午4:09:33   
* 修改备注：   
* @version    
*    
*/
@Component
@Configuration
@EnableScheduling
public class SynchronousData {
@Autowired	
private CrawlerPublicClassService crawlerPublicClassService;
private static Logger _log = Logger.getLogger(SynchronousData.class);
@Scheduled(cron="40 1 0 * * ?")
public void synchronousDataMessage(){
	String CrawlElve=ResourceBundle.getBundle("docker").getString("CrawlElve");
	try {
		crawlerPublicClassService.historicalData(CrawlElve,CrawlElve);
	} catch (Exception e) {
		_log.error("同步数据失败>>>>>>>>>>>>>>>>>>>>>>>>>>>>."+e);
	}
	
}

}
