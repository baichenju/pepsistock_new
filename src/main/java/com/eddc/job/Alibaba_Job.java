/**
 * Copyright © 2018 eSunny Info. Tech Ltd. All rights reserved.
 *
 * @Package: com.eddc.job
 * @author: jack.zhao
 * @date: 2018年3月14日 下午12:20:51
 */
package com.eddc.job;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import com.eddc.model.CrawKeywordsInfo;
import com.eddc.model.Craw_keywords_delivery_place;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.service.CrawlerPublicClassService;
import com.eddc.service.JobAndTriggerService;
import com.eddc.task.Alibaba_DataCrawlTask;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.SpringContextUtil;
import com.eddc.util.publicClass;

/**
 *
 * 项目名称：Price_monitoring_crawler   
 * 类名称：Alibaba_Job   
 * 类描述：   
 * 创建人：jack.zhao   
 * 创建时间：2018年3月19日 下午12:20:51   
 * 修改人：jack.zhao   
 * 修改时间：2018年3月19日 下午12:20:51   
 * 修改备注：   
 * @version
 *
 */
@PersistJobDataAfterExecution
@DisallowConcurrentExecution
public class Alibaba_Job implements Job {
    private static Logger _log = Logger.getLogger(Alibaba_Job.class);
    private ApplicationContext context = SpringContextUtil.getApplicationContext();
    private String tableName = Fields.TABLE_CRAW_KEYWORDS_INF;
    private String counts = Fields.STATUS_ON;
    @Autowired
    private JobAndTriggerService jobAndTriggerService;
    @Autowired
    private CrawlerPublicClassService crawlerPublicClassService;
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        String jobName = context.getJobDetail().getKey().toString();
        jobName = jobName.substring(jobName.indexOf(".") + 1).toString();
        int pageTop = 0;
        _log.info(">>> [" + jobName + "] job start >>>");
        _log.info(">>> Invoke Alibaba_Job execute method >>>");
        try {
             String Ip = publicClass.getLocalIP();
            List<QrtzCrawlerTable> listjobName = jobAndTriggerService.queryJob(jobName, Ip, Fields.DOCKER, Fields.GUOMEI);
            if (listjobName.size() > 0) {
                if (jobName.contains(Fields.SEARCH)) {
                    tableName = Fields.TABLE_CRAW_CRAW_KEYWORD_TEMP_INFO_FORESARCH_HISTORY;
                    counts = Fields.STATUS_OFF;
                    pageTop = 1;
                }
                crawlerPublicClassService.updateCrawKeywordHistory(listjobName, listjobName.get(0).getDatabases());//更新已经抓取商品的状态
                alibabaCrawler(listjobName, tableName, counts, pageTop);
            } else {
                _log.info(">>> Alibaba_Job : listjobName is null,please check!");
            }
            _log.info(">>> [" + jobName + "] finish >>>");
        } catch (Exception e) {
            _log.error(">>> Alibaba_Job execute fail,please check! >>>");
            _log.error(e.getMessage());
        }

    }

    //启动爬虫
    public void alibabaCrawler(List<QrtzCrawlerTable> listjobName, String tableName, String count, int pageTop) throws InterruptedException {
        _log.info(">>> Invoke Alibaba_Job alibabaCrawler method >>>");
        String time = SimpleDate.SimpleDateFormatData().format(new Date());
        //从数据库获取城市code抓取库存
        List<Craw_keywords_delivery_place> List_Code = crawlerPublicClassService.Keywords_Delivery_Place(listjobName, listjobName.get(0).getDatabases());
        if (listjobName.size() > 0) {
            if (List_Code.size() > 0) {
                for (Craw_keywords_delivery_place place : List_Code) {
                    crawlerPublicClassService.SearchSynchronousDataAlibaba(Fields.TABLE_CRAW_VENDOR_INFO, listjobName.get(0).getDatabases(), Fields.TABLE_CRAW_VENDOR_PRICE_INFO, null);
                    crawAndParseInfoAndPriceAlibaba(listjobName, time, place.getDelivery_place_code(), tableName, count, pageTop);//启动爬虫
                }
            } else {
                crawlerPublicClassService.SearchSynchronousDataAlibaba(Fields.TABLE_CRAW_VENDOR_INFO, listjobName.get(0).getDatabases(), Fields.TABLE_CRAW_VENDOR_PRICE_INFO, null);
                crawAndParseInfoAndPriceAlibaba(listjobName, time, null, tableName, count, pageTop);//启动爬虫

            }
        } else {
            _log.info(">>> List<QrtzCrawlerTable> is empty,please Check! >>>");
        }
        _log.info(">>> Finish Alibaba_Job alibabaCrawler method >>>");
    }

    //商品详情
    @SuppressWarnings({ "rawtypes", "unchecked"})
	public void crawAndParseInfoAndPriceAlibaba(List<QrtzCrawlerTable> listjobName, String time, String code, String tableName, String count, int pageTop) throws InterruptedException {
        _log.info(">>> Invoke Alibaba_Job crawAndParseInfoAndPriceAlibaba method >>>");
        int jj = 0;
        long awaitTime = Integer.valueOf(listjobName.get(0).getDocker()) * 1000 * 60;
        ArrayList<Future> futureList = new ArrayList<Future>();
        ExecutorService taskExecutor = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());  //初始化线程池
        CompletionService completion=new ExecutorCompletionService(taskExecutor);
        List<CrawKeywordsInfo> list = crawlerPublicClassService.crawAndParseInfo(listjobName, listjobName.get(0).getDatabases(), tableName, count, pageTop);//开始解析数据
        for (CrawKeywordsInfo accountInfo : list) {
            jj++;
            Alibaba_DataCrawlTask dataCrawlTask = (Alibaba_DataCrawlTask) SpringContextUtil.getApplicationContext().getBean(Alibaba_DataCrawlTask.class);
            Map<String, Object> alibabaMap = new HashMap<String, Object>();
            dataCrawlTask.setDataType(listjobName.get(0).getPlatform());
            dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
            dataCrawlTask.setTimeDate(time);
            dataCrawlTask.setSum("" + jj + "/" + list.size() + "");
            dataCrawlTask.setType(Fields.STYPE_1);
            dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id());
            dataCrawlTask.setCrawKeywordsInfo(accountInfo);
            dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource()));
            dataCrawlTask.setCodeCookice(code);
            dataCrawlTask.setIp(listjobName.get(0).getIP());
            dataCrawlTask.setAlibabaMap(alibabaMap);
            dataCrawlTask.setStorage(listjobName.get(0).getStorage());
            dataCrawlTask.setSetUrl(Fields.PLATFORM_1688_PC_URL.replace("EGOODSID", accountInfo.getCust_keyword_name()));
            Future<Map<String, Object>> future = completion.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值
            futureList.add(future);
            _log.info(">>> " + dataCrawlTask.getSetUrl() + " finish run >>>");
        }
        _log.info(">>> Alibaba_Job Future List prepare finished >>>");
        try {
            if (pageTop == Fields.STATUS_COUNT) {
                _log.info(">>> START COUNT = 0 >>>");
                while (true) {
                    if (((ExecutorService) taskExecutor).isTerminated()) {
                        _log.info(">>> TaskExecutor has finished >>>");
                        taskExecutor.shutdownNow();
                        _log.info(">>> TaskExecutor has shutdown >>>");

                        _log.info(">>> Start batch insert crawler data >>>");
                        if(!listjobName.get(0).getStorage().contains(Fields.COUNT_4)){
        					crawlerPublicClassService.bulkInsertData(futureList,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据
        				}
                        _log.info(">>> Finish batch insert crawler data >>>");

                        Thread.sleep(10000);
                        _log.info(">>> Start check failure crawler info >>>");
                        crawAndParseInfoAndPricefailureAlibaba(time, listjobName, tableName, code, pageTop);//检查是否有失败的商品
                        _log.info(">>> Finish check failure crawler info >>>");
                        break;
                    }
                }
            } else {
                if (!taskExecutor.awaitTermination(awaitTime, TimeUnit.MILLISECONDS)) {
                    _log.info(">>> TaskExecutor timeout,shut down now ! >>>");
                    taskExecutor.shutdownNow();  // 超时的时候向线程池中所有的线程发出中断(interrupted)。
                }else{
                	taskExecutor.shutdownNow();  
                }
            }
        } catch (Exception e) {
            taskExecutor.shutdownNow();
            _log.error("crawAndParseInfoAndPriceAlibaba fail,please check!");
            _log.error(e.getMessage());
        } finally {
            if (pageTop == Fields.STATUS_COUNT_1) {
                _log.info(">>> START COUNT = 1 >>>");

                _log.info(">>> Start batch insert crawler data >>>");
                if(!listjobName.get(0).getStorage().contains(Fields.COUNT_4)){
                crawlerPublicClassService.bulkInsertData(futureList, listjobName.get(0).getDatabases(), listjobName.get(0).getStorage());//批量插入数据
                _log.info(">>> Finish batch insert crawler data >>>");
                }
                _log.info(">>> Invoke updateCrawKeywordHistory >>>");
                crawlerPublicClassService.updateCrawKeywordHistory(listjobName, listjobName.get(0).getDatabases());//更新已经抓取商品的状态
                _log.info(">>> Finish updateCrawKeywordHistory >>>");

                Thread.sleep(5000);
                _log.info(">>> Invoke SynchronousData >>>");
                crawlerPublicClassService.SynchronousData(listjobName.get(0).getPlatform(),listjobName.get(0).getDatabases(),listjobName.get(0).getUser_Id(),tableName);//同步数据
                _log.info(">>> Finish SynchronousData >>>");
                //String kye=listjobName.get(0).getPlatform()+"_"+listjobName.get(0).getUser_Id()+"_"+SimpleDate.SimpleDateFormatDataMinute().format(new Date());
                try {
                	//jedisService.setDataList(kye,futureList);
                	//jedisService.close();
                } catch (Exception e2) {
                	e2.printStackTrace();
                }
            }
        }
        _log.info(">>> Finish Alibaba_Job crawAndParseInfoAndPriceAlibaba method >>>");
    }

    ///检查失败的item数据
    @SuppressWarnings("rawtypes")
    public void crawAndParseInfoAndPricefailureAlibaba(String time, List<QrtzCrawlerTable> listjobName, String tableName, String code, int pageTop) throws InterruptedException {
        int jj = 0;
        int count = 0;
        ExecutorService taskExecutor = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());  //初始化线程池
        ArrayList<Future> futureList = new ArrayList<Future>();
        do {
            List<CrawKeywordsInfo> list = crawlerPublicClassService.crawAndGrabFailureGoodsData(listjobName, listjobName.get(0).getDatabases(), tableName, pageTop);//开始解析数据
            for (CrawKeywordsInfo accountInfo : list) {
                jj++;
                Alibaba_DataCrawlTask dataCrawlTask = (Alibaba_DataCrawlTask) context.getBean(Alibaba_DataCrawlTask.class);
                Map<String, Object> alibabaMap = new HashMap<String, Object>();
                dataCrawlTask.setDataType(listjobName.get(0).getPlatform());
                dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
                dataCrawlTask.setTimeDate(time);
                dataCrawlTask.setSum("" + jj + "/" + list.size() + "");
                dataCrawlTask.setType(Fields.STYPE_1);
                dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id());
                dataCrawlTask.setCrawKeywordsInfo(accountInfo);
                dataCrawlTask.setCodeCookice(code);
                dataCrawlTask.setStorage(listjobName.get(0).getStorage());
                dataCrawlTask.setIp(listjobName.get(0).getIP());
                dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource()));
                dataCrawlTask.setSetUrl(Fields.PLATFORM_1688_PC_URL.replace("EGOODSID", accountInfo.getCust_keyword_name()));
                dataCrawlTask.setAlibabaMap(alibabaMap);
                Future<Map<String, Object>> future = taskExecutor.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值
                futureList.add(future);
                _log.info(">>> " + dataCrawlTask.getSetUrl() + " finish run >>>");
            }
            //taskExecutor.shutdown();
            _log.info(">>> Failure future List prepare finished >>>");
            while (true) {
                if (((ExecutorService) taskExecutor).isTerminated()) {
                    taskExecutor.shutdownNow();
                    _log.info(">>> Start batch insert failure crawler data >>>");
                    crawlerPublicClassService.bulkInsertData(futureList, listjobName.get(0).getDatabases(), listjobName.get(0).getStorage());//批量插入数据
                    _log.info(">>> Finish batch insert failure crawler data >>>");
                    break;
                }

            }
            jj = 0;
            count++;
            Thread.sleep(5000);
            if (count > 5) {
                _log.info(">>> retry invoke Alibaba_Job crawAndParseInfoAndPricefailureAlibaba method : " + count + " >>>");
                Thread.sleep(5000);
                return;
            }
        } while (true);
    }
}
