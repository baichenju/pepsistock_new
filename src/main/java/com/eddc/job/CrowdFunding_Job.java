package com.eddc.job;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.apache.log4j.Logger;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;  
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import com.eddc.model.Craw_goods_crowdfunding_Info;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.redis.JedisService;
import com.eddc.service.CrawlerPublicClassService;
import com.eddc.service.JobAndTriggerService;
import com.eddc.task.CrowdFunding_DataCrawlTask;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.SpringContextUtil;
import com.eddc.util.publicClass;
@PersistJobDataAfterExecution
@DisallowConcurrentExecution// 不允许并发执行
@SuppressWarnings("serial")
public class CrowdFunding_Job extends GoodsLiftOffTime_Jbo implements Job,Serializable  {   
	private static Logger _log = Logger.getLogger(CrowdFunding_Job.class); 
	@Autowired
	private ApplicationContext contexts= SpringContextUtil.getApplicationContext();  
	@Autowired
	private CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	private JobAndTriggerService jobAndTriggerService; 
	@Autowired
	JedisService jedisService;
	private String tableName=Fields.TABLE_CRAW_GOODS_CROWDFUNDING_PRICE_INFO;//表名称
	public CrowdFunding_Job(){};
	//开始调用定时任务
	public void execute(JobExecutionContext  context)  throws JobExecutionException { 
		String jobName=context.getJobDetail().getKey().toString();
		jobName=jobName.substring(jobName.indexOf(".")+1).toString();
		int pageTop=0;
		_log.info(jobName+"__众筹 Job执行时间: " + new Date()+"----------"+context.getJobDetail().getKey());
		try {
			String Ip=publicClass.getLocalIP();
			List<QrtzCrawlerTable>listjobName=jobAndTriggerService.queryJob(jobName,Ip,Fields.DOCKER,Fields.PLATFORM_TAOBAO_ZC);
			if(listjobName.size()>0){
				_log.info(jobName+"__众筹启动爬虫程序当前用户是"+listjobName.get(0).getUser_Id()+"---------- " + new Date()+"-----------------");
				if(jobName.contains(Fields.SEARCH)){
					pageTop=1;
					crawlerCrowdFundingPriceMonitoring(listjobName,tableName,pageTop);//启动爬虫	
				}else{
					crawlerCrowdFundingFillCatch(listjobName);//启动补抓		
				}
			}else{
				_log.info("当前众筹用户不存在！爬虫结束------"+new Date());	
			}	
		} catch (Exception e) {
			_log.info("获取JobAndTriggerService对象为空------"+new Date()+e.toString());	
		}
	} 

	//开始爬虫
	public void crawlerCrowdFundingPriceMonitoring(List<QrtzCrawlerTable>listjobName,String tableName,int pageTop){ 
		String time=SimpleDate.SimpleDateFormatData().format(new Date()); 
		try {
			crawlerPublicClassService.updateCrawKeywordHistory(listjobName,listjobName.get(0).getDatabases());//更新已经抓取商品的状态
			crawlerPublicClassService.SearchSynchronousDataAlibaba(Fields.CRAW_GOODS_TRANSLATE_INFO,listjobName.get(0).getDatabases(),null,listjobName.get(0).getPlatform());//同步数据 
			crawlerPublicClassService.SearchSynchronousDataAlibaba(tableName,listjobName.get(0).getDatabases(),null,null);//同步数据  
			if(listjobName.get(0).getJob_name().contains(Fields.SEARCH)){
				crowdFunding_craw_item(listjobName,time,tableName,pageTop); //开始抓取数据
			}else{
				crawlerCrowdFundingFillCatch(listjobName);//启动补抓		
			}
			
		} catch (Exception e) {  
			_log.info("众筹爬虫失败请检查数据----------==============="+SimpleDate.SimpleDateFormatData().format(new Date()));
		}    
	}
	//开始爬数据
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void crowdFunding_craw_item(List<QrtzCrawlerTable>listjobName,String time,String tableName,int pageTop) throws InterruptedException{
		final long awaitTime=Integer.valueOf(listjobName.get(0).getDocker())*1000*60; int jj=0; 
		ArrayList<Future> futureList = new ArrayList<Future>();
		ExecutorService taskExecutor = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());  //初始化线程池 
		CompletionService completion=new ExecutorCompletionService(taskExecutor);
		List<Craw_goods_crowdfunding_Info>list=crawlerPublicClassService.crowdFundingData(listjobName.get(0).getPlatform(),listjobName.get(0).getDatabases(),pageTop,listjobName.get(0).getSums());//开始解析数据 
		for(Craw_goods_crowdfunding_Info accountInfo:list){jj++;
			CrowdFunding_DataCrawlTask dataCrawlTask = (CrowdFunding_DataCrawlTask)contexts.getBean(CrowdFunding_DataCrawlTask.class);
			Map<String,Object>TaobaozcMap=new HashMap<String,Object>();
			dataCrawlTask.setCrowdfunding(accountInfo);
			dataCrawlTask.setTaobaozcMap(TaobaozcMap);
			dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
			dataCrawlTask.setDataType(listjobName.get(0).getPlatform()); 
			dataCrawlTask.setStorage(listjobName.get(0).getStorage());
			dataCrawlTask.setTimeDate(time);
			dataCrawlTask.setSum("" + jj + "/" + list.size() + "");
			dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id());
			dataCrawlTask.setType(Fields.STYPE_1);
			dataCrawlTask.setIp(listjobName.get(0).getIP());
			dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource()));
			if(accountInfo.getPlatform_name_en().equals(Fields.PLATFORM_TAOBAO_ZC)){
				dataCrawlTask.setSetUrl(Fields.TAOBAOZC_URL+accountInfo.getEgoodsId());	
			}else{
				dataCrawlTask.setSetUrl(accountInfo.getGoods_url());		
			}
			Future<Map<String, Object>> future=completion.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值  
			futureList.add(future);
			_log.info(Thread.currentThread().getName()+Runtime.getRuntime().availableProcessors());
		}
		try {
			//taskExecutor.shutdown();
			if(pageTop==Fields.STATUS_COUNT){
				while(true){
					if(((ExecutorService) taskExecutor).isTerminated()){
						taskExecutor.shutdownNow(); 
						if(!listjobName.get(0).getStorage().equals(Fields.COUNT_4)){
							crawlerPublicClassService.bulkInsertData(futureList,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据		
						}
						break;	
					}
				}	
			}else{
				if(!taskExecutor.awaitTermination(awaitTime, TimeUnit.MILLISECONDS)){ 
					taskExecutor.shutdownNow();  // 超时的时候向线程池中所有的线程发出中断(interrupted)。  
				}else{
					taskExecutor.shutdownNow();  
				}
			}
		} catch (Exception e) {
			// 超时的时候向线程池中所有的线程发出中断(interrupted)。  
			taskExecutor.shutdownNow();  
		}finally{
			if(pageTop==Fields.STATUS_COUNT_1){
				if(!listjobName.get(0).getStorage().equals(Fields.COUNT_4)){
					crawlerPublicClassService.bulkInsertData(futureList,listjobName.get(0).getDatabases(),listjobName.get(0).getStorage());//批量插入数据
				}
				crawlerPublicClassService.updateCrawKeywordHistory(listjobName,listjobName.get(0).getDatabases());//更新已经抓取商品的状态
				//String kye=listjobName.get(0).getPlatform()+"_"+listjobName.get(0).getUser_Id()+"_"+SimpleDate.SimpleDateFormatDataMinute().format(new Date());
				try {
					//jedisService.setDataList(kye,futureList);
					//jedisService.close();
				} catch (Exception e2) {
					e2.printStackTrace();
				}
			}
		}
		
	}
	//开始补抓数据
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void crawlerCrowdFundingFillCatch(List<QrtzCrawlerTable>listjobName) throws InterruptedException{
		 int jj=0; 
		String time=SimpleDate.SimpleDateFormatData().format(new Date()); 
		ArrayList<Future> futureList = new ArrayList<Future>();
		ExecutorService taskExecutor = Executors.newFixedThreadPool(listjobName.get(0).getThread_sum());  //初始化线程池 
		CompletionService completion=new ExecutorCompletionService(taskExecutor);
		List<Craw_goods_crowdfunding_Info>list=crawlerPublicClassService.crawlerCrowdFundingFillCatchData(listjobName,listjobName.get(0).getDatabases());//开始解析数据 
		for(Craw_goods_crowdfunding_Info accountInfo:list){jj++;
			CrowdFunding_DataCrawlTask dataCrawlTask = (CrowdFunding_DataCrawlTask) contexts.getBean(CrowdFunding_DataCrawlTask.class);
			Map<String,Object>TaobaozcMap=new HashMap<String,Object>();
			dataCrawlTask.setCrowdfunding(accountInfo);
			dataCrawlTask.setTaobaozcMap(TaobaozcMap);
			dataCrawlTask.setDatabase(listjobName.get(0).getDatabases());
			dataCrawlTask.setDataType(accountInfo.getPlatform_name_en()); 
			dataCrawlTask.setStorage(listjobName.get(0).getStorage());
			dataCrawlTask.setTimeDate(time);
			dataCrawlTask.setSum("" + jj + "/" + list.size() + "");
			dataCrawlTask.setAccountId(listjobName.get(0).getUser_Id());
			dataCrawlTask.setType(Fields.STYPE_10);
			dataCrawlTask.setIp(listjobName.get(0).getIP());
			dataCrawlTask.setIPPROXY(publicClass.IpDataSource(listjobName.get(0).getDataSource()));
			if(accountInfo.getPlatform_name_en().equals(Fields.PLATFORM_TAOBAO_ZC)){
				dataCrawlTask.setSetUrl(Fields.TAOBAOZC_URL+accountInfo.getEgoodsId());	
			}else{
				dataCrawlTask.setSetUrl(accountInfo.getGoods_url());		
			}
			Future<Map<String, Object>> future=completion.submit(dataCrawlTask);//线程执行完成以后可以通过引用获取返回值  
			futureList.add(future);
			_log.info(Thread.currentThread().getName()+Runtime.getRuntime().availableProcessors());
		}
		taskExecutor.shutdownNow();
	}
	
}
