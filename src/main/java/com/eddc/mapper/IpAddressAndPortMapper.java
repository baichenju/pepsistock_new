package com.eddc.mapper;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.eddc.model.Cookies;
import com.eddc.model.Craw_IP_Monitoring;
import com.eddc.model.IpAddresPort;
@Mapper
public interface IpAddressAndPortMapper {
	public List<IpAddresPort> queryForList(@Param("platform")String platform);
	public void IpUpdate(@Param("id")int id,@Param("platform")String platform); 
	public void IpUpdate_status(@Param("platform")String platform); 
	public void Inset_Craw_IP_monitoring(Craw_IP_Monitoring  monitoring);
	public List<Cookies> queryForListLaoA();
	
}
