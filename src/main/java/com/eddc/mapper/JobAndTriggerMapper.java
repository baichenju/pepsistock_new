package com.eddc.mapper;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.eddc.model.Craw_keywords_temp_Info_forsearch_history;
import com.eddc.model.JobAndTrigger;
import com.eddc.model.QrtzCrawlerTable;
@Mapper
public interface JobAndTriggerMapper {
	public List<JobAndTrigger> getJobAndTriggerDetails();
	public List<QrtzCrawlerTable>queryJob(@Param("job_Name")String job_Name,@Param("IP")String IP ,@Param("docker")String docker);
	public void InsertQrtzCrawl(QrtzCrawlerTable qrtz);
	public void UpdateQrtzCrawl(@Param("job_name")String job_name,@Param("status")String status);
	public void DeleteQrtzCrawl(@Param("job_name")String job_name);
	public int updateIP(@Param("JOB_NAME")String JOB_NAME,@Param("IP")String IP);
	public int update_Datasource(@Param("JOB_NAME")String JOB_NAME,@Param("DATASOURCE")String DATASOURCE);
	public int Update_reschedule_job_thread(@Param("JOB_NAME")String JOB_NAME,@Param("THREAD_SUM")String THREAD_SUM,@Param("PLATFORM")String PLATFORM);
	public int Update_reschedule_job_docker(@Param("JOB_NAME")String JOB_NAME,@Param("DOCKER")String DOCKER,@Param("PLATFORM")String PLATFORM);
	public int reschedule_job_inventory(@Param("JOB_NAME")String JOB_NAME,@Param("INVENTORY")String INVENTORY,@Param("PLATFORM")String PLATFORM);
	public int 	reschedule_job_client(@Param("JOB_NAME")String JOB_NAME,@Param("CLIENT")String CLIENT,@Param("PLATFORM")String PLATFORM);
	public int 	reschedule_job_storage(@Param("JOB_NAME")String JOB_NAME,@Param("storage")String storage,@Param("PLATFORM")String PLATFORM);
	public int 	reschedule_job_database(@Param("JOB_NAME")String JOB_NAME,@Param("database")String database,@Param("PLATFORM")String PLATFORM);
	public int 	reschedule_job_docker(@Param("JOB_NAME")String JOB_NAME,@Param("DOCKER")String DOCKER,@Param("PLATFORM")String PLATFORM);
	public int 	reschedule_job_sums (@Param("JOB_NAME")String JOB_NAME,@Param("SUMS")String SUMSS,@Param("PLATFORM")String PLATFORM);
	public List<QrtzCrawlerTable>query_rawler_fill_catch();
	public List<Craw_keywords_temp_Info_forsearch_history>generationGrabbing();//监控爬虫个每个商品个数
	public List<Craw_keywords_temp_Info_forsearch_history>grabTotalNumberGoodsData();//监控爬虫总商品个数
	public int crawlergoodsStatus();//更新抓取商品状态
	
}
