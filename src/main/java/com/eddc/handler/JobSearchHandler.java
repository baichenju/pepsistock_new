package com.eddc.handler;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.eddc.method.JdData;
import com.eddc.method.SearchData;
import com.eddc.method.TaobaoData;
import com.eddc.method.TmallData;
import com.eddc.model.Craw_keywords_delivery_place;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.service.CrawlerPublicClassService;
import com.eddc.service.JobAndTriggerService;
import com.eddc.util.Fields;
import com.eddc.util.SimpleDate;
import com.eddc.util.publicClass;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import com.xxl.job.core.log.XxlJobLogger;

@JobHandler(value = "JobSearchHandler")
@Component
public class JobSearchHandler extends IJobHandler{
	@Autowired
	private SearchData searchData;
	@Autowired
	public JdData jdData;
	@Autowired
	private TmallData tmallData;
	@Autowired
	private TaobaoData taobaoData;
	@Autowired
	public JobAndTriggerService jobAndTriggerService;
	@Autowired
	private CrawlerPublicClassService crawlerPublicClassService;
	@Override
	public ReturnT<String> execute(String param) throws Exception {
		Map map=getParams(param);
		String jobName=map.get("jobName").toString();
		String platfrom=map.get("platfrom").toString(); 
		XxlJobLogger.log("【启动程序开始监控  "+jobName+" 数据】 当前服务器Ip地址是："+publicClass.Ipaddres());
		String Ip=publicClass.getLocalIP();
		 
		List<QrtzCrawlerTable>listjobName=jobAndTriggerService.queryJob(jobName,Ip,Fields.DOCKER,jobName.split("_")[1].toString());
		String time=SimpleDate.SimpleDateFormatData().format(new Date());
		if(listjobName.size()>0){ 
			//同步 搜索抓取商品的egoodsId
			//获取爬虫状态
			int sum=crawlerPublicClassService.crawlerStatus(listjobName.get(0).getPlatform(),listjobName.get(0).getDatabases(),listjobName.get(0).getUser_Id());

			if(listjobName.size()>0){ 
				List<Craw_keywords_delivery_place>List_Code=crawlerPublicClassService.Keywords_Delivery_Place(listjobName,listjobName.get(0).getDatabases());
				if(List_Code.size()>0){
					for(Craw_keywords_delivery_place code:List_Code){
						try {
							if(String.valueOf(code.getSearch_status()).equals(Fields.STATUS_ON)){
								XxlJobLogger.log("开始执行job搜索商品信息"); 
								sum=searchData.crawlerPriceMonitoring(time,listjobName,code.getDelivery_place_code(),code.getDelivery_place_name());	
							}	
						} catch (Exception e) {
							XxlJobLogger.log("获取多个地区出错了"+e);
						}
					}  
				}else{ 
					sum=searchData.crawlerPriceMonitoring(time,listjobName,null,null);
				}  
				if(listjobName.get(0).getUser_Id().equals(Fields.COUNT_80)){
					if(sum==Fields.STATUS_11){
						if(listjobName.get(0).getPlatform().equals(Fields.PLATFORM_TAOBAO_EN)){
							taobaoData.crawlerTaobaoPriceMonitoring(listjobName,Fields.TABLE_CRAW_CRAW_KEYWORD_TEMP_INFO_FORESARCH, Fields.STATUS_OFF,Fields.STATUS_COUNT_1);
						}else if(listjobName.get(0).getPlatform().equals(Fields.PLATFORM_TMALL_EN)){
							tmallData.crawlerTmallPriceMonitoring(listjobName, Fields.TABLE_CRAW_CRAW_KEYWORD_TEMP_INFO_FORESARCH, Fields.STATUS_OFF,Fields.STATUS_COUNT_1);
						}else if(listjobName.get(0).getPlatform().equals(Fields.PLATFORM_JD)){
							try {jdData.startCrawlerJd(listjobName, Fields.TABLE_CRAW_CRAW_KEYWORD_TEMP_INFO_FORESARCH, Fields.STATUS_OFF,Fields.STATUS_COUNT_1);} catch (InterruptedException e) {} catch (ExecutionException e) {e.printStackTrace();}
						}
					}
				}
			}
		}	
		return SUCCESS;
	}
	@SuppressWarnings({"rawtypes", "unchecked"})
	public Map getParams(String param) {
		Map params = new HashMap();
		if (StringUtils.isNotEmpty(param)) {
			String[] ps = StringUtils.split(param, ",");
			for (String p : ps) {
				String[] s = StringUtils.split(p, "=");
				params.put(StringUtils.trim(s[0]), StringUtils.trim(s[1]));
			}
		}
		return params;
	}
}
