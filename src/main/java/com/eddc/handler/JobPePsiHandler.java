package com.eddc.handler;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.eddc.job.Search_job;
import com.eddc.method.JdData;
import com.eddc.method.TmallData;
import com.eddc.method.YhdData;
import com.eddc.model.QrtzCrawlerTable;
import com.eddc.service.CrawlerPublicClassService;
import com.eddc.service.JobAndTriggerService;
import com.eddc.util.Fields;
import com.eddc.util.publicClass;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import com.xxl.job.core.log.XxlJobLogger;
@JobHandler(value = "jobPePsiCrawlerHandler")
@Component
public class JobPePsiHandler extends IJobHandler {
	@Autowired
	private JobAndTriggerService jobAndTriggerService;
	@Autowired
	private CrawlerPublicClassService crawlerPublicClassService;
	@Autowired
	private TmallData tmallData;
	@Autowired
	private JdData jdData;
	@Autowired
	private YhdData yhdData;
	private String tableName = Fields.TABLE_CRAW_KEYWORDS_INF;//表名称
	private String counts = Fields.STATUS_ON;
	@Override
	public ReturnT<String> execute(String param) throws Exception {
		
		Map map=getParams(param);
		String jobName=map.get("jobName").toString();
		String platfrom=map.get("platfrom").toString(); 
		XxlJobLogger.log("【启动程序开始监控  "+jobName+" 数据】 当前服务器Ip地址是："+publicClass.Ipaddres());
		int pageTop = 0;
		try {
			String Ip = publicClass.getLocalIP();
			List<QrtzCrawlerTable> listjobName = jobAndTriggerService.queryJob(jobName,null,null,platfrom);
			if (listjobName.size() > 0) {
				if (jobName.contains(Fields.SEARCH)) {
					tableName = Fields.TABLE_CRAW_CRAW_KEYWORD_TEMP_INFO_FORESARCH;
					counts = Fields.STATUS_OFF;
					pageTop = 1;
				}
				int sums=crawlerPublicClassService.getSelectSums(platfrom,listjobName.get(0).getDatabases(), listjobName.get(0).getUser_Id());
				if(sums==0) {
					if(platfrom.equalsIgnoreCase("tmall")) {
						tmallData.crawlerTmallPriceMonitoring(listjobName, tableName, counts, pageTop);//启动爬虫	
					}else if(platfrom.equalsIgnoreCase("jd")) {
						jdData.startCrawlerJd(listjobName, tableName, counts, pageTop);
					}else if(platfrom.equalsIgnoreCase("yhd")) {
						yhdData.atartCrawlerYhd(listjobName, tableName, counts, pageTop);
					}	
					XxlJobLogger.log("==>>数据抓取完成<<==");
				}else {
					XxlJobLogger.log("==>>数据已经存在不需要再次抓取<<==");
				}


			} else {
				XxlJobLogger.log("==>>此用户信息不存在<<==");
			}
		} catch (Exception e) {
			XxlJobLogger.log("==>>百事请求数据发生异常："+e+"<<==");
		}



		return SUCCESS;
	}

	@SuppressWarnings({"rawtypes", "unchecked"})
	public Map getParams(String param) {
		Map params = new HashMap();
		if (StringUtils.isNotEmpty(param)) {
			String[] ps = StringUtils.split(param, ",");
			for (String p : ps) {
				String[] s = StringUtils.split(p, "=");
				params.put(StringUtils.trim(s[0]), StringUtils.trim(s[1]));
			}
		}
		return params;
	}
}
